﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;


namespace RMMS_MVC.MVCMODELS
{
    public interface IDB
    {
        int Add();
        int Modify();
        int Delete();
        void fillup();
    }

}