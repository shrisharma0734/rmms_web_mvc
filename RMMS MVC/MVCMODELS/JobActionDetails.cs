﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace RMMS_MVC.MVCMODELS
{
    public class JobActionDetails : DBObj
    {
        #region Class Variables

        protected Decimal _JobDetailsID;
        protected Decimal _ActivityDetailID;//
        protected Decimal _JobTypeID;
        protected Decimal _ScheduleID;
        protected DateTime _ScheduleDatetime;
        protected Decimal _SSTActivityListID;
        protected Decimal _ActivityListDetailID;
        protected Decimal _SSTID;
        protected DateTime _ActualStartDatetime;
        protected Decimal _JobResponseID;
        protected DateTime _JobResponseDatetime;
        protected Decimal _RetryCount;
        protected string _Remarks;
        protected Decimal _CreatedBy;
        protected DateTime _CreatedDatetime;
        protected Decimal _ModifiedBy;
        protected DateTime _ModifiedDatetime;

        #endregion

        /// <summary>Class Properties</summary>
        #region Properties - Get and Set Accessor

        public Decimal JobDetailsID
        {
            get { return _JobDetailsID; }
            set { _JobDetailsID = value; }

        }
        public Decimal ActivityDetailID
        {
            get { return _ActivityDetailID; }
            set { _ActivityDetailID = value; }
        }
        public Decimal JobTypeID
        {
            get { return _JobTypeID; }
            set { _JobTypeID = value; }
        }
        public Decimal ScheduleID
        {
            get { return _ScheduleID; }
            set { _ScheduleID = value; }
        }
        public DateTime ScheduleDatetime
        {
            get { return _ScheduleDatetime; }
            set { _ScheduleDatetime = value; }
        }
        public Decimal SSTActivityListID
        {
            get { return _SSTActivityListID; }
            set { _SSTActivityListID = value; }
        }
        public Decimal ActivityListDetailID
        {
            get { return _ActivityListDetailID; }
            set { _ActivityListDetailID = value; }
        }
        public Decimal SSTID
        {
            get { return _SSTID; }
            set { _SSTID = value; }
        }
        public DateTime ActualStartDateTime
        {
            get { return _ActualStartDatetime; }
            set { _ActualStartDatetime = value; }
        }
        public Decimal JobResponseID
        {
            get { return _JobResponseID; }
            set { _JobResponseID = value; }
        }
        public DateTime JobResponseDateTime
        {
            get { return _JobResponseDatetime; }
            set { _JobResponseDatetime = value; }
        }
        public Decimal RetryCount
        {
            get { return _RetryCount; }
            set { _RetryCount = value; }
        }
        public String Remarks
        {
            get { return _Remarks; }
            set { _Remarks = value; }
        }
        public Decimal CreatedBy
        {
            get { return _CreatedBy; }
            set { _CreatedBy = value; }
        }
        public DateTime CreatedDatetime
        {
            get { return _CreatedDatetime; }
            set { _CreatedDatetime = value; }
        }
        public Decimal ModifiedBy
        {
            get { return _ModifiedBy; }
            set { _ModifiedBy = value; }
        }
        public DateTime ModifiedDatetime
        {
            get { return _ModifiedDatetime; }
            set { _ModifiedDatetime = value; }
        }

        #endregion

        #region  Methods

        public override void fillup()
        {


        }

        #endregion
    }

}