﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace RMMS_MVC.MVCMODELS
{
    public class DBObj : IDB
    {
        //Default constractor
        public DBObj()
        {
        }

        #region Interface Member Implementation
        public virtual int Add() { return 0; } //Insert to database
        public virtual int Modify() { return 0; } //Update to database
        public virtual int Delete() { return 0; } //Delete from database
        public virtual void fillup() { } //Fill up database fields
        #endregion
    }
}