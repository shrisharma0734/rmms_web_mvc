﻿using System;
using System.Data;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using DAL;
using RMMS_MVC.MVCActionStoreProcedure;

namespace RMMS_MVC.MVCMODELS
{
    public class JobScheduleDetails_CDMS : DBObj
    {
        public JobScheduleDetails_CDMS()
        {
            //
            // TODO: Add constructor logic here
            //
        }

        /// <summary>Class Variables</summary>
        #region Class Variables
        /// <summary>ScheduleDetailsID</summary>
        protected decimal _ScheduleDetailsID;
        /// <summary>ScheduleID</summary>
        protected decimal _ScheduleID;
        /// <Summary>ActivityListDetailID</Summary>
        protected decimal _ActivityListDetailID;
        /// <Summary>SSTID</Summary>
        protected decimal _SSTID;
        /// <Summary>ScheduleDatetime</Summary>
        protected DateTime _ScheduleDatetime;
        /// <Summary>JobTypeID</Summary>
        protected decimal _JobTypeID;
        /// <Summary>JobTypeServerPath</Summary>
        protected string _JobTypeServerPath;
        /// <Summary>JobTypeAgentPath</Summary>
        protected string _JobTypeAgentPath;
        /// <Summary>Created By</Summary>
        protected decimal _CreatedBy;
        /// <Summary>CreatedDatetime</Summary>
        protected DateTime _CreatedDatetime;
        /// <Summary>ModifiedBy</Summary>
        protected decimal _ModifiedBy;
        /// <Summary>ModifiedDatetime</Summary>
        protected DateTime _ModifiedDatetime;

        protected decimal _JobActionDetailID;
        protected decimal _ActivityDetailID;
        protected decimal _SSTActivityListID;

        protected string _SST_NAT_IP;
        protected decimal _SST_AgentPort;

        #endregion
        /// <summary>Class Properties</summary>
        #region Properties - Get and Set Accessor
        /// <summary>ScheduleDetailsID</summary>
        public decimal ScheduleDetailsID
        {
            get { return _ScheduleDetailsID; }
            set { _ScheduleDetailsID = value; }
        }
        /// <summary>ScheduleID</summary>
        public decimal ScheduleID
        {
            get { return _ScheduleID; }
            set { _ScheduleID = value; }
        }
        /// <summary>ActivityListDetailID</summary>
        public decimal ActivityListDetailID
        {
            get { return _ActivityListDetailID; }
            set { _ActivityListDetailID = value; }
        }
        /// <summary>SSTID</summary>
        public decimal SSTID
        {
            get { return _SSTID; }
            set { _SSTID = value; }
        }
        /// <summary>ScheduleDatetime</summary>
        public DateTime ScheduleDatetime
        {
            get { return _ScheduleDatetime; }
            set { _ScheduleDatetime = value; }
        }
        /// <summary>JobTypeID</summary>
        public decimal JobTypeID
        {
            get { return _JobTypeID; }
            set { _JobTypeID = value; }
        }
        /// <summary>JobTypeServerPath</summary>
        public string JobTypeServerPath
        {
            get { return _JobTypeServerPath; }
            set { _JobTypeServerPath = value; }
        }
        /// <summary>JobTypeAgentPath</summary>
        public string JobTypeAgentPath
        {
            get { return _JobTypeAgentPath; }
            set { _JobTypeAgentPath = value; }
        }

        /// <summary>Created By</summary>
        public decimal CreatedBy
        {
            get { return _CreatedBy; }
            set { _CreatedBy = value; }
        }
        /// <summary>Created Date time</summary>
        public DateTime CreatedDatetime
        {
            get { return _CreatedDatetime; }
            set { CreatedDatetime = value; }
        }
        /// <summary>Modify By</summary>
        public decimal ModifiedBy
        {
            get { return _ModifiedBy; }
            set { _ModifiedBy = value; }
        }
        /// <summary>Modify Date time</summary>
        public DateTime ModifiedDatetime
        {
            get { return _ModifiedDatetime; }
            set { _ModifiedDatetime = value; }
        }
        /// <summary> /// ActivityDetailID  /// </summary>
        public decimal ActivityDetailID
        {
            get { return _ActivityDetailID; }
            set { _ActivityDetailID = value; }
        }

        /// <summary> /// SSTActivityListID   /// </summary>
        public decimal SSTActivityListID
        {
            get { return _SSTActivityListID; }
            set { _SSTActivityListID = value; }
        }

        public string SST_NAT_IP
        {
            get { return _SST_NAT_IP; }
            set { _SST_NAT_IP = value; }
        }
        public decimal SST_AgentPort
        {
            get { return _SST_AgentPort; }
            set { _SST_AgentPort = value; }
        }

        public decimal JobActionDetailID
        {
            get { return _JobActionDetailID; }
            set { _JobActionDetailID = value; }
        }

        public DataTable dtScheduleInfo;

        #endregion


        #region Method

        public override void fillup()
        {


            BLogicS sa = new BLogicS();
            IDataReader dr = sa.GetScheduleDetailInfo(ScheduleID);
            dr.Read();

            if (dr["ActivityDetailID"] != DBNull.Value)
            {
                this._ActivityDetailID = (decimal)dr["ActivityDetailID"];
            }
            if (dr["ScheduleDatetime"] != DBNull.Value)
            {
                this._ScheduleDatetime = (DateTime)dr["ScheduleDatetime"];
            }
            if (dr["SSTActivityListID"] != DBNull.Value)
            {
                this._SSTActivityListID = (decimal)dr["SSTActivityListID"];
            }
            if (dr["ActivityListDetailID"] != DBNull.Value)
            {
                this._ActivityListDetailID = (decimal)dr["ActivityListDetailID"];
            }
            if (dr["SSTID"] != DBNull.Value)
            {
                this._SSTID = (decimal)dr["SSTID"];
            }
            if (dr["CreatedBy"] != DBNull.Value)
            {
                this._CreatedBy = (decimal)dr["CreatedBy"];
            }
            if (dr["CreatedDatetime"] != DBNull.Value)
            {
                this._CreatedDatetime = (DateTime)dr["CreatedDatetime"];
            }
            if (dr["ModifiedBy"] != DBNull.Value)
            {
                this._ModifiedBy = (decimal)dr["ModifiedBy"];
            }
            if (dr["JobTypeServerPath"] != DBNull.Value)
            {
                this._JobTypeServerPath = (string)dr["JobTypeServerPath"];
            }
            if (dr["SST_NAT_IP"] != DBNull.Value)
            {
                this._SST_NAT_IP = (string)dr["SST_NAT_IP"];
            }
            if (dr["SST_AgentPort"] != DBNull.Value)
            {
                this._SST_AgentPort = (decimal)dr["SST_AgentPort"];
            }

        }

        public void GetJobActionDetailID()
        {
            BLogicS sa = new BLogicS();
            IDataReader dr = sa.GetJobActionDetailID(SSTID, ScheduleID);
            dr.Read();

            if (dr["JobActionDetailID"] != DBNull.Value)
            {
                this._JobActionDetailID = (decimal)dr["JobActionDetailID"];
            }
        }

        public void GetMultiScheduleInfo()
        {
            BLogicS sa = new BLogicS();
            dtScheduleInfo = new DataTable();
            dtScheduleInfo = sa.SP_GetMultiCheduleInfo(ScheduleID);
        }

        #endregion



    }
}