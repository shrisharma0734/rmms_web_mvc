﻿using System;
using System.Data;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using RMMS_MVC.MVCActionStoreProcedure;

namespace RMMS_MVC.MVCMODELS
{
    public class JobScheduleMaster_CDMS : DBObj
    {
        public JobScheduleMaster_CDMS()
        {
            //
            // TODO: Add constructor logic here
            //
        }

        /// <summary>Class Variables</summary>
        #region Class Variables
        /// <summary>ScheduleID</summary>
        protected decimal _ScheduleID;
        /// <summary>ScheduleName</summary>
        protected string _ScheduleName;
        /// <Summary>SSTActivityListID</Summary>
        protected decimal _SSTActivityListID;
        /// <Summary>ScheduleDatetime</Summary>
        protected DateTime _ScheduleDatetime;
        /// <Summary>JobTypeID</Summary>
        protected decimal _JobTypeID;
        /// <Summary>ScheduleType</Summary>
        protected string _ScheduleType;
        /// <Summary>RepeatType</Summary>
        protected string _RepeatType;
        /// <Summary>ScheduleRemarks</Summary>
        protected string _ScheduleRemarks;
        /// <Summary>Created By</Summary>
        protected decimal _CreatedBy;
        /// <Summary>CreatedDatetime</Summary>
        protected DateTime _CreatedDatetime;
        /// <Summary>ModifiedBy</Summary>
        protected decimal _ModifiedBy;
        /// <Summary>ModifiedDatetime</Summary>
        protected DateTime _ModifiedDatetime;



        #endregion

        /// <summary>Class Properties</summary>
        #region Properties - Get and Set Accessor
        /// <summary>ScheduleID</summary>
        public decimal ScheduleID
        {
            get { return _ScheduleID; }
            set { _ScheduleID = value; }
        }
        /// <summary>ScheduleName</summary>
        public string ScheduleName
        {
            get { return _ScheduleName; }
            set { _ScheduleName = value; }
        }
        /// <summary>SSTActivityListID</summary>
        public decimal SSTActivityListID
        {
            get { return _SSTActivityListID; }
            set { _SSTActivityListID = value; }
        }
        /// <summary>ScheduleDatetime</summary>
        public DateTime ScheduleDatetime
        {
            get { return _ScheduleDatetime; }
            set { _ScheduleDatetime = value; }
        }
        /// <summary>JobTypeID</summary>
        public decimal JobTypeID
        {
            get { return _JobTypeID; }
            set { _JobTypeID = value; }
        }
        /// <summary>ScheduleType</summary>
        public string ScheduleType
        {
            get { return _ScheduleType; }
            set { _ScheduleType = value; }
        }

        /// <summary>RepeatType</summary>
        public string RepeatType
        {
            get { return _RepeatType; }
            set { _RepeatType = value; }
        }

        /// <summary>ScheduleRemarks</summary>
        public string ScheduleRemarks
        {
            get { return _ScheduleRemarks; }
            set { _ScheduleRemarks = value; }
        }
        /// <summary>Created By</summary>
        public decimal CreatedBy
        {
            get { return _CreatedBy; }
            set { _CreatedBy = value; }
        }
        /// <summary>Created Date time</summary>
        public DateTime CreatedDatetime
        {
            get { return _CreatedDatetime; }
            set { CreatedDatetime = value; }
        }
        /// <summary>Modify By</summary>
        public decimal ModifiedBy
        {
            get { return _ModifiedBy; }
            set { _ModifiedBy = value; }
        }
        /// <summary>Modify Date time</summary>
        public DateTime ModifiedDatetime
        {
            get { return _ModifiedDatetime; }
            set { _ModifiedDatetime = value; }
        }

        public DataTable dtScheduleDateInfo;
        #endregion


        #region  Methods

        public override void fillup()
        {
            BLogicS sa = new BLogicS();
            dtScheduleDateInfo = new DataTable();
            dtScheduleDateInfo = sa.SP_GetMultiCheduleInfo(ScheduleID);
        }


        #endregion

    }
}