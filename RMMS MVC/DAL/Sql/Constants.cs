﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;
using RMMS_MVC.Models;

namespace RMMS_MVC.DAL.Sql
{
    public class StoredProcedure
    {
        //Role
        internal const string SelectRoleMaster = "SelectRoleMaster";
        internal const string Role_CRUD = "Role_CRUD";
        internal const string DeleteRoleMaster = "DeleteRoleMaster";
        //City
        internal const string SelectCityMaster = "SelectCityMaster";
        internal const string InsertUpdateCityMaster = "InsertUpdateCityMaster";
        internal const string DeleteCityMaster = "DeleteCityMaster";
        //Bank
        internal const string SelectBankMaster = "SelectBankMaster";
        internal const string InsertUpdateBankMaster = "InsertUpdateBankMaster";
        internal const string DeleteBankMaster = "DeleteBankMaster";
        //Region
        internal const string SelectRegionMaster = "SelectRegionMaster";
        internal const string InsertUpdateRegionMaster = "InsertUpdateRegionMaster";
        internal const string DeleteBankRegionMaster = "DeleteBankRegionMaster";
        //SstGroup
        internal const string InsertUpdateGroupMaster = "InsertUpdateGroupMaster";
        internal const string SelectGroupMaster = "SelectGroupMaster";
        internal const string DeleteGroupMaster = "DeleteGroupMaster";

        internal const String SelectUserMaster = "SelectUserMaster";
        internal const String InsertUpdateDeleteUserMaster = "InsertUpdateDeleteUserMaster";
        internal const String DeleteUserMaster = "DeleteUserMaster";

        //OpenTroubleTicketDetail
        internal const string SP_OpenTroubleTicketDetail_RepeatCount = "SP_OpenTroubleTicketDetail_RepeatCount";
        internal const string SP_GetTerminalInfo = "SP_GetTerminalInfo";
        internal const string SP_ShowTermiailProblem = "SP_ShowTermiailProblem";
        internal const string SP_ShowRepeatcountDetails = "SP_ShowRepeatcountDetails";

        //Problemcategory
        internal const String InsertUpdateProblemCategory = "InsertUpdateProblemCategory";
        internal const String SelectProblemCategoryProfile = "SelectProblemCategoryProfile";
        internal const String DeleteProblemCategoryMaster = "DeleteProblemCategoryMaster";

        //Branch
        internal const String SelectBankBranchMaster = "SelectBankBranchMaster";
        internal const String InsertUpdateBranchMaster = "InsertUpdateBranchMaster";
        internal const String SP_Branch = "SP_Branch";
        internal const String DeleteBankBranchMaster = "DeleteBankBranchMaster";

        //User
        internal const string SP_Region = "SP_Region";

        //OverAll SST SSK Staus
        internal const string Get_SSTMachineDetails = "Get_SSTMachineDetails";
        internal const string SP_DoScreenShot = "SP_DoScreenShot";


        //DeviceStatusType
        internal const string InsertUpdateDeviceStatusTypeMaster = "InsertUpdateDeviceStatusTypeMaster";
        internal const string SelectDeviceStatusTypeMaster = "SelectDeviceStatusTypeMaster";
        internal const string DeleteDeviceStatusTypeMaster = "DeleteDeviceStatusTypeMaster";

        //ScreenShot
        internal const string SP_RemotlyRestart_ShowTerminalDetails="SP_RemotlyRestart_ShowTerminalDetails";
        internal const string SP_ScreenShot_Restult = "SP_ScreenShot_Restult";

        //Restart
        internal const string SP_DoRemotelyRestart = "SP_DoRemotelyRestart";//Generate ScheduledID
        internal const string SP_RemotelyAurorestart_Restult = "SP_RemotelyAurorestart_Restult";
        //SP_GetTestPullReqDetails
    }
}