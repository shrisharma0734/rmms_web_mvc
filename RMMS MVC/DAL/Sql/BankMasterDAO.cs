﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;
using RMMS_MVC.Models;


namespace RMMS_MVC.DAL.Sql
{
    public class BankMasterDAO : BaseDAO
    {
        //Return List of All Banks
        public List<BankMaster> ListAll()
        {
            List<BankMaster> lst = new List<BankMaster>();
            using (SqlConnection con = new SqlConnection(ConnectTo))
            {
                con.Open();
                SqlCommand com = new SqlCommand(StoredProcedure.SelectBankMaster, con);
                com.CommandType = CommandType.StoredProcedure;
                SqlDataReader rdr = com.ExecuteReader();
                while (rdr.Read())
                {
                    lst.Add(new BankMaster
                    {
                        BankID = Convert.ToInt32(rdr["BankID"]),
                        BankName = rdr["BankName"].ToString(),
                        BankShotName = rdr["BankShotName"].ToString(),
                        Bank_HO_Address1 = rdr["Bank_HO_Address1"].ToString(),
                        Bank_HO_Address2 = rdr["Bank_HO_Address2"].ToString(),
                        CityID = Convert.ToInt32(rdr["CityID"]),
                        CityName = rdr["CityName"].ToString(),
                        Bank_ContactName = rdr["Bank_ContactName"].ToString(),
                        Bank_ContactNo1 = rdr["Bank_ContactNo1"].ToString(),
                        Bank_ContactNo2 = rdr["Bank_ContactNo2"].ToString(),
                        Bank_EmailID1 = rdr["Bank_EmailID1"].ToString(),
                        Bank_EmailID2 = rdr["Bank_EmailID2"].ToString(),
                        Bank_FTPAddress1 = rdr["Bank_FTPAddress1"].ToString(),
                        Bank_FTPAddress2 = rdr["Bank_FTPAddress2"].ToString(),
                        Bank_FTPPort1 = Convert.ToInt32(rdr["Bank_FTPPort1"]),
                        Bank_FTPPort2 = rdr["Bank_FTPPort2"]!=DBNull.Value ? Convert.ToInt32(rdr["Bank_FTPPort2"]):default(int?),
                        Bank_FTP1UserID = rdr["Bank_FTP1UserID"].ToString(),
                        Bank_FTP2UserID = rdr["Bank_FTP2UserID"].ToString(),
                        Bank_FTP1Password = rdr["Bank_FTP1Password"].ToString(),
                        Bank_FTP2Password = rdr["Bank_FTP2Password"].ToString(),
                        Bank_ExtrctdFileLoc = rdr["Bank_ExtrctdFileLoc"].ToString()
                    });
                }
                return lst;
            }
        }
        //Method for Adding an Bank 
        public int Add(BankMaster Bank)
        {
            int i;
            using (SqlConnection con = new SqlConnection(ConnectTo))
            {
                con.Open();
                SqlCommand com = new SqlCommand(StoredProcedure.InsertUpdateBankMaster, con);
                com.CommandType = CommandType.StoredProcedure;
                com.Parameters.AddWithValue("@BankID", Bank.BankID);
                com.Parameters.AddWithValue("@BankName", Bank.BankName);
                com.Parameters.AddWithValue("@BankShotName", Bank.BankShotName);
                com.Parameters.AddWithValue("@Bank_HO_Address1", Bank.Bank_HO_Address1);
                com.Parameters.AddWithValue("@Bank_HO_Address2", Bank.Bank_HO_Address2);
                com.Parameters.AddWithValue("@CityID", Bank.CityID);
                com.Parameters.AddWithValue("@Bank_ContactName", Bank.Bank_ContactName);
                com.Parameters.AddWithValue("@Bank_ContactNo1", Bank.Bank_ContactNo1);
                com.Parameters.AddWithValue("@Bank_ContactNo2", Bank.Bank_ContactNo2);
                com.Parameters.AddWithValue("@Bank_EmailID1", Bank.Bank_EmailID1);
                com.Parameters.AddWithValue("@Bank_EmailID2", Bank.Bank_EmailID2);
                com.Parameters.AddWithValue("@Bank_FTPAddress1", Bank.Bank_FTPAddress1);
                com.Parameters.AddWithValue("@Bank_FTPAddress2", Bank.Bank_FTPAddress2);
                com.Parameters.AddWithValue("@Bank_FTPPort1", Bank.Bank_FTPPort1);
                com.Parameters.AddWithValue("@Bank_FTPPort2", Bank.Bank_FTPPort2);
                com.Parameters.AddWithValue("@Bank_FTP1UserID", Bank.Bank_FTP1UserID);
                com.Parameters.AddWithValue("@Bank_FTP2UserID", Bank.Bank_FTP2UserID);
                com.Parameters.AddWithValue("@Bank_FTP1Password", Bank.Bank_FTP1Password);
                com.Parameters.AddWithValue("@Bank_FTP2Password", Bank.Bank_FTP2Password);
                com.Parameters.AddWithValue("@Bank_ExtrctdFileLoc", Bank.Bank_ExtrctdFileLoc);
                com.Parameters.AddWithValue("@Action", "Insert");
                i = com.ExecuteNonQuery();
            }
            return i;
        }

        //Method for Updating Bank record  
        public int Update(BankMaster Bank)
        {
            int i;
            using (SqlConnection con = new SqlConnection(ConnectTo))
            {
                con.Open();
                SqlCommand com = new SqlCommand(StoredProcedure.InsertUpdateBankMaster, con);
                com.CommandType = CommandType.StoredProcedure;
                com.Parameters.AddWithValue("@BankID", Bank.BankID);
                com.Parameters.AddWithValue("@BankName", Bank.BankName);
                com.Parameters.AddWithValue("@BankShotName", Bank.BankShotName);
                com.Parameters.AddWithValue("@Bank_HO_Address1", Bank.Bank_HO_Address1);
                com.Parameters.AddWithValue("@Bank_HO_Address2", Bank.Bank_HO_Address2);
                com.Parameters.AddWithValue("@CityID", Bank.CityID);
                com.Parameters.AddWithValue("@Bank_ContactName", Bank.Bank_ContactName);
                com.Parameters.AddWithValue("@Bank_ContactNo1", Bank.Bank_ContactNo1);
                com.Parameters.AddWithValue("@Bank_ContactNo2", Bank.Bank_ContactNo2);
                com.Parameters.AddWithValue("@Bank_EmailID1", Bank.Bank_EmailID1);
                com.Parameters.AddWithValue("@Bank_EmailID2", Bank.Bank_EmailID2);
                com.Parameters.AddWithValue("@Bank_FTPAddress1", Bank.Bank_FTPAddress1);
                com.Parameters.AddWithValue("@Bank_FTPAddress2", Bank.Bank_FTPAddress2);
                com.Parameters.AddWithValue("@Bank_FTPPort1", Bank.Bank_FTPPort1);
                com.Parameters.AddWithValue("@Bank_FTPPort2", Bank.Bank_FTPPort2);
                com.Parameters.AddWithValue("@Bank_FTP1UserID", Bank.Bank_FTP1UserID);
                com.Parameters.AddWithValue("@Bank_FTP2UserID", Bank.Bank_FTP2UserID);
                com.Parameters.AddWithValue("@Bank_FTP1Password", Bank.Bank_FTP1Password);
                com.Parameters.AddWithValue("@Bank_FTP2Password", Bank.Bank_FTP2Password);
                com.Parameters.AddWithValue("@Bank_ExtrctdFileLoc", Bank.Bank_ExtrctdFileLoc);
                com.Parameters.AddWithValue("@Action", "Update");
                i = com.ExecuteNonQuery();
            }
            return i;
        }

        //Method for Deleting an Bank
        public int Delete(int ID)
        {
            int i;
            using (SqlConnection con = new SqlConnection(ConnectTo))
            {
                con.Open();
                SqlCommand com = new SqlCommand(StoredProcedure.DeleteBankMaster, con);
                com.CommandType = CommandType.StoredProcedure;
                com.Parameters.AddWithValue("@Id", ID);
                i = com.ExecuteNonQuery();
            }
            return i;
        }
    }
}