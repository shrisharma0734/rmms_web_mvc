﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using RMMS_MVC.Models;
using System.Data.SqlClient;
using System.Data;

namespace RMMS_MVC.DAL.Sql
{
    public class BankBranchMasterDAO : BaseDAO
    {
        //Return List of All Banks
        public List<BankBranchMaster> ListAll()
        {
            List<BankBranchMaster> lst = new List<BankBranchMaster>();
            using (SqlConnection con = new SqlConnection(ConnectTo))
            {
                con.Open();
                SqlCommand com = new SqlCommand(StoredProcedure.SelectBankBranchMaster , con);
                com.CommandType = CommandType.StoredProcedure;
                SqlDataReader rdr = com.ExecuteReader();
                
                while (rdr.Read())
                {
                    lst.Add(new BankBranchMaster
                    {
                        BranchID = Convert.ToInt32(rdr["BranchID"]),
                        BankID = Convert.ToInt32(rdr["BankID"]),
                        BankName = rdr["BankName"].ToString(),
                        BranchName = rdr["BranchName"].ToString(),
                        BranchCode = rdr["BranchCode"].ToString(),
                        Locality = rdr["Locality"].ToString(),
                        BranchAddress1 = rdr["BranchAddress1"].ToString(),
                        BranchAddress2 = rdr["BranchAddress2"].ToString(),
                        CityID = Convert.ToInt32(rdr["CityID"]),
                        CityName = rdr["CityName"].ToString(),
                        RegionName = rdr["RegionName"].ToString(),
                        RegionID = Convert.ToInt32(rdr["RegionID"])

                    });
                }
                return lst;
            }
        }
        //Method for Adding an Bank 
        public int Add(BankBranchMaster BankBranch)
        {
            int i;
            using (SqlConnection con = new SqlConnection(ConnectTo))
            {
                con.Open();
                SqlCommand com = new SqlCommand(StoredProcedure.InsertUpdateBranchMaster, con);
                com.CommandType = CommandType.StoredProcedure;
                com.Parameters.AddWithValue("@BranchID", BankBranch.BranchID);
                com.Parameters.AddWithValue("@BankID", BankBranch.BankID);
                com.Parameters.AddWithValue("@BranchName", BankBranch.BranchName);
                com.Parameters.AddWithValue("@BranchCode", BankBranch.BranchCode);
                com.Parameters.AddWithValue("@Locality", BankBranch.Locality);
                com.Parameters.AddWithValue("@BranchAddress1", BankBranch.BranchAddress1);
                com.Parameters.AddWithValue("@BranchAddress2", BankBranch.BranchAddress2);
                com.Parameters.AddWithValue("@CityID", BankBranch.CityID);
                com.Parameters.AddWithValue("@RegionID", BankBranch.RegionID);
                com.Parameters.AddWithValue("@CreatedBy", '1');
                com.Parameters.AddWithValue("@CreatedDatetime", SqlDbType.DateTime);
                com.Parameters.AddWithValue("@ModifiedBy", 1);
                com.Parameters.AddWithValue("@ModifiedDatetime",  SqlDbType.DateTime);
                com.Parameters.AddWithValue("@Action", "Insert");
                i = com.ExecuteNonQuery();
            }
            return i;
        }

        //Method for Updating Bank record  
        public int Update(BankBranchMaster BankBranch)
        {
            int i;
            using (SqlConnection con = new SqlConnection(ConnectTo))
            {
                con.Open();
                SqlCommand com = new SqlCommand(StoredProcedure.InsertUpdateBranchMaster, con);
                com.CommandType = CommandType.StoredProcedure;
                com.Parameters.AddWithValue("@BranchID", BankBranch.BranchID);
                com.Parameters.AddWithValue("@BankID", BankBranch.BankID);
                com.Parameters.AddWithValue("@BranchName", BankBranch.BranchName);
                com.Parameters.AddWithValue("@BranchCode", BankBranch.BranchCode);
                com.Parameters.AddWithValue("@Locality", BankBranch.Locality);
                com.Parameters.AddWithValue("@BranchAddress1", BankBranch.BranchAddress1);
                com.Parameters.AddWithValue("@BranchAddress2", BankBranch.BranchAddress2);
                com.Parameters.AddWithValue("@CityID", BankBranch.CityID);
                com.Parameters.AddWithValue("@RegionID", BankBranch.RegionID);
                com.Parameters.AddWithValue("@CreatedBy", '1');
                com.Parameters.AddWithValue("@CreatedDatetime", SqlDbType.DateTime);
                com.Parameters.AddWithValue("@ModifiedBy", 0);
                com.Parameters.AddWithValue("@ModifiedDatetime", SqlDbType.DateTime);
                com.Parameters.AddWithValue("@Action", "Update");
                i = com.ExecuteNonQuery();
            }
            return i;
        }

        //Method for Deleting an Bank
        public int Delete(int ID)
        {
            int i;
            using (SqlConnection con = new SqlConnection(ConnectTo))
            {
                con.Open();
                SqlCommand com = new SqlCommand(StoredProcedure.DeleteBankBranchMaster, con);
                com.CommandType = CommandType.StoredProcedure;
                com.Parameters.AddWithValue("@BranchID", ID);
                i = com.ExecuteNonQuery();
            }
            return i;
        }
    }
}