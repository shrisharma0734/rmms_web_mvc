﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using RMMS_MVC.Models;
using System.Data.SqlClient;
using System.Data;
using RMMS_MVC.Controllers;
using System.ComponentModel.DataAnnotations;

namespace RMMS_MVC.DAL.Sql
{
    public class UserMasterDAO : BaseDAO
    {

        //UserMasterController us = new UserMasterController();

        //Return List of All Regions for User 
        public List<UserMaster> ListRegion(string Bank_id)
        {
            List<UserMaster> lst = new List<UserMaster>();
            using (SqlConnection con = new SqlConnection(ConnectTo))
            {
                con.Open();
                UserMaster m = new UserMaster();
                SqlCommand com = new SqlCommand(StoredProcedure.SP_Region, con);
                com.Parameters.AddWithValue("@BankId", Bank_id);
                com.CommandType = CommandType.StoredProcedure;
                SqlDataReader rdr = com.ExecuteReader();
                while (rdr.Read())
                {
                    lst.Add(new UserMaster
                    {
                        RegionID = Convert.ToInt32(rdr["RegionID"]),
                        RegionName = rdr["RegionName"].ToString(),
                    });
                }
                return lst;
            }


        }

        //Return List of All Branches
        public List<UserMaster> ListBranch(string Region_id)
        {
            List<UserMaster> lst = new List<UserMaster>();
            using (SqlConnection con = new SqlConnection(ConnectTo))
            {
                con.Open();
                UserMaster m = new UserMaster();
                SqlCommand com = new SqlCommand(StoredProcedure.SP_Branch, con);
                com.Parameters.AddWithValue("@RegionId", Region_id);
                com.CommandType = CommandType.StoredProcedure;
                SqlDataReader rdr = com.ExecuteReader();
                while (rdr.Read())
                {
                    lst.Add(new UserMaster
                    {
                        BranchID = Convert.ToInt32(rdr["BranchID"]),
                        BranchName = rdr["BranchName"].ToString(),
                    });
                }
                return lst;
            }


        }


        //Return List of All User
        public List<UserMaster> ListAll()
        {
            List<UserMaster> lst = new List<UserMaster>();
            using (SqlConnection con = new SqlConnection(ConnectTo))
            {
                con.Open();
                SqlCommand com = new SqlCommand(StoredProcedure.SelectUserMaster, con);
                com.CommandType = CommandType.StoredProcedure;
                SqlDataReader rdr = com.ExecuteReader();
                while (rdr.Read())
                {
                    lst.Add(new UserMaster
                    {
                        UserID = Convert.ToInt32(rdr["UserID"]),
                        RoleID = Convert.ToInt32(rdr["RoleID"]),
                        RoleName = rdr["RoleName"].ToString(),
                        UserLoginName = rdr["UserLoginName"].ToString(),
                        UserFullName = rdr["UserFullName"].ToString(),
                        UserLoginPassword = rdr["UserLoginPassword"].ToString(),
                        UserConfirmPassword = rdr["UserLoginPassword"].ToString(),
                        DefaultPage = rdr["DefaultPage"].ToString(),
                        UserEmailID = rdr["UserEmailID"].ToString(),
                        UserContactNo = rdr["UserContactNo"].ToString(),
                        BankID = Convert.ToInt32(rdr["BankID"]),
                        BankName = rdr["BankName"].ToString(),
                        RegionID = Convert.ToInt32(rdr["Region_ID"]),
                        RegionName = rdr["RegionName"].ToString(),
                        BranchID = Convert.ToInt32(rdr["Branch_ID"]),
                        BranchName = rdr["BranchName"].ToString()
                      
                    });
                    
                }
                return lst;
            }
        }
        //Method for Adding an User 
        public int Add(UserMaster User)
        {
            int i;
            UserMasterController us = new UserMasterController();
            using (SqlConnection con = new SqlConnection(ConnectTo))
            {
                con.Open();
                SqlCommand com = new SqlCommand(StoredProcedure.InsertUpdateDeleteUserMaster, con);
                com.CommandType = CommandType.StoredProcedure;
                com.Parameters.AddWithValue("@UserID", 0);
                com.Parameters.AddWithValue("@RoleID", User.RoleID );
                com.Parameters.AddWithValue("@Region_ID", User.RegionID);
                com.Parameters.AddWithValue("@Branch_ID", User.BranchID );
                com.Parameters.AddWithValue("@Bank_ID", User.BankID ); 
                com.Parameters.AddWithValue("@UserLoginName", User.UserLoginName); 
                com.Parameters.AddWithValue("@UserFullName", User.UserFullName); 
                com.Parameters.AddWithValue("@UserLoginPassword", User.UserLoginPassword);
                com.Parameters.AddWithValue("@UserEmailID", User.UserEmailID);
                com.Parameters.AddWithValue("@UserContactNo", User.UserContactNo); 
                com.Parameters.AddWithValue("@DefaultPage", User.DefaultPage);
                if (User.UserStatus == "Active")
                {
                    com.Parameters.AddWithValue("@UserStatus", '1');
                }
                else
                {
                    com.Parameters.AddWithValue("@UserStatus", '2');
                }
                com.Parameters.AddWithValue("@CreatedBy", '1');
                com.Parameters.AddWithValue("@CreatedDatetime", SqlDbType.DateTime); 
                com.Parameters.AddWithValue("@ModifiedBy", '1'); 
                com.Parameters.AddWithValue("@ModifiedDatetime", SqlDbType.DateTime);

              
                com.Parameters.AddWithValue("@Action", "Insert");
                i = com.ExecuteNonQuery();
            }
            return i;
        }

        //Method for Updating Bank record  
        public int Update(UserMaster User)
        {
            int i;
            using (SqlConnection con = new SqlConnection(ConnectTo))
            {
                con.Open();
                SqlCommand com = new SqlCommand(StoredProcedure.InsertUpdateDeleteUserMaster, con);
                com.CommandType = CommandType.StoredProcedure;
                com.Parameters.AddWithValue("@UserID", User.UserID);
                com.Parameters.AddWithValue("@RoleID", User.RoleID);
                com.Parameters.AddWithValue("@Region_ID", User.RegionID);
                com.Parameters.AddWithValue("@Branch_ID", User.BranchID);
                com.Parameters.AddWithValue("@Bank_ID", User.BankID); 
                com.Parameters.AddWithValue("@UserLoginName", User.UserLoginName);
                com.Parameters.AddWithValue("@UserFullName", User.UserFullName);
                com.Parameters.AddWithValue("@UserLoginPassword", User.UserLoginPassword);
                com.Parameters.AddWithValue("@UserEmailID", User.UserEmailID);
                com.Parameters.AddWithValue("@UserContactNo", User.UserContactNo);
                com.Parameters.AddWithValue("@DefaultPage", User.DefaultPage);
                if (User.UserStatus == "Active")
                {
                    com.Parameters.AddWithValue("@UserStatus", 1);
                }
                else
                {
                    com.Parameters.AddWithValue("@UserStatus", 0);
                }
                com.Parameters.AddWithValue("@CreatedBy", '1');
                com.Parameters.AddWithValue("@CreatedDatetime", SqlDbType.DateTime);
                com.Parameters.AddWithValue("@ModifiedBy", '1');
                com.Parameters.AddWithValue("@ModifiedDatetime", SqlDbType.DateTime);

                com.Parameters.AddWithValue("@Action", "Update");
                i = com.ExecuteNonQuery();
              
            }
            return i;
        }

        //Method for Deleting an Bank
        public int Delete(int ID)
        {
            int i;
            using (SqlConnection con = new SqlConnection(ConnectTo))
            {
                con.Open();
                SqlCommand com = new SqlCommand(StoredProcedure.DeleteUserMaster, con);
                com.CommandType = CommandType.StoredProcedure;
                com.Parameters.AddWithValue("@UserID", ID);
                
               
                i = com.ExecuteNonQuery();
            }
            return i;
        }
    }
}