﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;
using RMMS_MVC.Models;
using RMMS_MVC.DAL.Sql;

namespace RMMS_MVC.DAL
{
    public class SstGroupMasterDAO : BaseDAO
    {


        public List<SstGroupMaster> ListAll()
        {
            List<SstGroupMaster> lst = new List<SstGroupMaster>();
            using (SqlConnection con = new SqlConnection(ConnectTo))
            {
                con.Open();
                SqlCommand com = new SqlCommand(StoredProcedure.SelectGroupMaster, con);
                com.CommandType = CommandType.StoredProcedure;
              
                SqlDataReader rdr = com.ExecuteReader();
                while (rdr.Read())
                {
                    lst.Add(new SstGroupMaster
                    {
                       SSTGroupID = Convert.ToInt32(rdr["SSTGroupID"]),
                       SSTGroupName = rdr["SSTGroupName"].ToString(),
                       SSTGroupDescription = rdr["SSTGroupDescription"].ToString(),
                        
                    });
                }
                return lst;
            }
        }
        //Method To Add Group
        public int Add(SstGroupMaster Sst)
        {
            int i;
            using (SqlConnection con = new SqlConnection(ConnectTo))
            {
                con.Open();
                SqlCommand com = new SqlCommand(StoredProcedure.InsertUpdateGroupMaster, con);
                com.CommandType = CommandType.StoredProcedure;
                com.Parameters.AddWithValue("@SSTGroupID", Sst.SSTGroupID);
                com.Parameters.AddWithValue("@SSTGroupName", Sst.SSTGroupName);
                com.Parameters.AddWithValue("@SSTGroupDescription", Sst.SSTGroupDescription);
                com.Parameters.AddWithValue("@CreatedBy", '1');
                com.Parameters.AddWithValue("@CreatedDatetime", SqlDbType.DateTime);
                com.Parameters.AddWithValue("@ModifiedBy",'1');
                com.Parameters.AddWithValue("@ModifiedDatetime", SqlDbType.DateTime);

               
                com.Parameters.AddWithValue("@Action", "Insert");
                i = com.ExecuteNonQuery();
            }
            return i;
        }
        //Methods To Update Group
        public int Update(SstGroupMaster Sst)
        {
            int i;
            using (SqlConnection con = new SqlConnection(ConnectTo))
            {
                con.Open();
                SqlCommand com = new SqlCommand(StoredProcedure.InsertUpdateGroupMaster, con);
                com.CommandType = CommandType.StoredProcedure;
                com.Parameters.AddWithValue("@SSTGroupID", Sst.SSTGroupID);
                com.Parameters.AddWithValue("@SSTGroupName", Sst.SSTGroupName);
                com.Parameters.AddWithValue("@SSTGroupDescription", Sst.SSTGroupDescription);
                com.Parameters.AddWithValue("@CreatedBy", '1');
                com.Parameters.AddWithValue("@CreatedDatetime", SqlDbType.DateTime);
                com.Parameters.AddWithValue("@ModifiedBy", '1');
                com.Parameters.AddWithValue("@ModifiedDatetime", SqlDbType.DateTime);

                com.Parameters.AddWithValue("@Action", "Update");
                i = com.ExecuteNonQuery();
            }
            return i;
        }



        //Method for Deleting an Bank
        public int Delete(int GroupID)
        {
            int i;
            using (SqlConnection con = new SqlConnection(ConnectTo))
            {
                con.Open();
                SqlCommand com = new SqlCommand(StoredProcedure.DeleteGroupMaster, con);
                com.CommandType = CommandType.StoredProcedure;
                com.Parameters.AddWithValue("@SSTGroupID", GroupID);
                i = com.ExecuteNonQuery();
            }
            return i;
        }
    }
}