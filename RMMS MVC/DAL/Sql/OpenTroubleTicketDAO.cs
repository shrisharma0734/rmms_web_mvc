﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;
using RMMS_MVC.Models;

namespace RMMS_MVC.DAL.Sql
{
    public class OpenTroubleTicketDAO:BaseDAO 
    {
        public DataSet mydata()
        {
            using (SqlConnection con = new SqlConnection(ConnectTo))
            {
                con.Open();
                SqlCommand cmd = new SqlCommand(StoredProcedure.SP_OpenTroubleTicketDetail_RepeatCount, con);
                cmd.CommandType = CommandType.StoredProcedure;
                SqlDataAdapter da = new SqlDataAdapter();
                da.SelectCommand = cmd;
                DataSet myrec = new DataSet();
                da.Fill(myrec);
                return myrec;
            }
        }
        public DataSet myterminaldata(string TerminalID)
        {
            using (SqlConnection con = new SqlConnection(ConnectTo))
            {
                con.Open();
                SqlCommand cmd = new SqlCommand(StoredProcedure.SP_GetTerminalInfo, con);
                cmd.Parameters.Add(new SqlParameter("@TerminalID",TerminalID));
                cmd.CommandType = CommandType.StoredProcedure;
                SqlDataAdapter da = new SqlDataAdapter();
                da.SelectCommand = cmd;
                DataSet myrec = new DataSet();
                da.Fill(myrec);
                return myrec;
            }
        }
        public DataSet myproblemdata(string TerminalID)
        {
            using (SqlConnection con = new SqlConnection(ConnectTo))
            {
                con.Open();
                SqlCommand cmd = new SqlCommand(StoredProcedure.SP_ShowTermiailProblem, con);
                //cmd.Parameters.Add(new SqlParameter("@Problem", Problem));
                cmd.Parameters.Add(new SqlParameter("@TerminalID", TerminalID));
                cmd.CommandType = CommandType.StoredProcedure;
                SqlDataAdapter da = new SqlDataAdapter();
                da.SelectCommand = cmd;
                DataSet myrec = new DataSet();
                da.Fill(myrec);
                return myrec;
            }
        }
        public DataSet repeatcountdata(string TerminalID, string problm, string Device)
        {
            using (SqlConnection con = new SqlConnection(ConnectTo))
            {
                con.Open();
                SqlCommand cmd = new SqlCommand(StoredProcedure.SP_ShowRepeatcountDetails, con);
                //cmd.Parameters.Add(new SqlParameter("@Problem", Problem));
                cmd.Parameters.Add(new SqlParameter("@TermialID", TerminalID));
                cmd.Parameters.Add(new SqlParameter("@ProblemName", problm));
                cmd.Parameters.Add(new SqlParameter("@DeviceStatusType", Device));
                cmd.CommandType = CommandType.StoredProcedure;
                SqlDataAdapter da = new SqlDataAdapter();
                da.SelectCommand = cmd;
                DataSet myrec = new DataSet();
                da.Fill(myrec);
                return myrec;
            }
        } 

 
    }
}