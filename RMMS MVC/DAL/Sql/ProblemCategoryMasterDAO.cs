﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using RMMS_MVC.Models;
using System.Data.SqlClient;
using System.Data;
using RMMS_MVC.Models;

namespace RMMS_MVC.DAL.Sql
{
    public class ProblemCategoryMasterDAO : BaseDAO
    {
         //Return List of All Roles
        public List<ProblemCategoryMaster> ListAll()
        {
            List<ProblemCategoryMaster> lst = new List<ProblemCategoryMaster>();
            using (SqlConnection con = new SqlConnection(ConnectTo))
            {
                con.Open();
                SqlCommand com = new SqlCommand(StoredProcedure.SelectProblemCategoryProfile, con);
                com.CommandType = CommandType.StoredProcedure;
                SqlDataReader rdr = com.ExecuteReader();

                while (rdr.Read())
                {
                    lst.Add(new ProblemCategoryMaster
                    {
                        ProblemCategoryId = Convert.ToInt32(rdr["ProblemCategoryId"]),
                        ProblemCategory = rdr["ProblemCategory"].ToString(),
                        ProblemCategoryDescription = rdr["ProblemCategoryDescription"].ToString(),
                        ProblemColorCode = rdr["ProblemColorCode"].ToString()
                    });
                }
        
                return lst;
            }
        }

        //Method for Adding an Role

        public int Add(ProblemCategoryMaster Problem)
        {
            int i;
            using (SqlConnection con = new SqlConnection(ConnectTo))
            {
                con.Open();
                SqlCommand com = new SqlCommand(StoredProcedure.InsertUpdateProblemCategory, con);
                com.CommandType = CommandType.StoredProcedure;
                com.Parameters.AddWithValue("@ProblemCategoryId", Problem.ProblemCategoryId);
                com.Parameters.AddWithValue("@ProblemCategory", Problem.ProblemCategory);
                com.Parameters.AddWithValue("@ProblemCategoryDescription", Problem.ProblemCategoryDescription);
                com.Parameters.AddWithValue("@ProblemColorCode", Problem.ProblemColorCode);
                com.Parameters.AddWithValue("@CreatedBy", '1');
                com.Parameters.AddWithValue("@CreatedDatetime", SqlDbType.DateTime);
                com.Parameters.AddWithValue("@ModifiedBy", '1');
                com.Parameters.AddWithValue("@ModifiedDatetime", SqlDbType.DateTime);
                com.Parameters.AddWithValue("@Action", "Insert");
                i = com.ExecuteNonQuery();
            }
            return i;
        }

        ////Method for Updating Employee record  
        public int Update(ProblemCategoryMaster Problem)
        {
            int i;
            using (SqlConnection con = new SqlConnection(ConnectTo))
            {
                con.Open();
                SqlCommand com = new SqlCommand(StoredProcedure.InsertUpdateProblemCategory, con);
                com.CommandType = CommandType.StoredProcedure;
                com.Parameters.AddWithValue("@ProblemCategoryId",Problem.ProblemCategoryId);
                com.Parameters.AddWithValue("@ProblemCategory", Problem.ProblemCategory);
                com.Parameters.AddWithValue("@ProblemCategoryDescription", Problem.ProblemCategoryDescription);
                com.Parameters.AddWithValue("@ProblemColorCode", Problem.ProblemColorCode);
                //com.Parameters.AddWithValue("@CreatedBy", '1');
                //com.Parameters.AddWithValue("@CreatedDatetime", SqlDbType.DateTime);
                //com.Parameters.AddWithValue("@CreatedBy", Role.CreatedBy);
                //com.Parameters.AddWithValue("@CreatedDatetime", Role.CreatedDatetime);
                //com.Parameters.AddWithValue("@CreatedBy", Role.CreatedBy);
                //com.Parameters.AddWithValue("@CreatedDatetime", Role.CreatedDatetime);
                com.Parameters.AddWithValue("@ModifiedBy",'1');
                com.Parameters.AddWithValue("@ModifiedDatetime", SqlDbType.DateTime);
                com.Parameters.AddWithValue("@Action", "Update");
                i = com.ExecuteNonQuery();
            }
            return i;
        }

        ////Method for Deleting an Employee  
        public int Delete(int id)
        {
            int i;
            using (SqlConnection con = new SqlConnection(ConnectTo))
            {
                con.Open();
                SqlCommand com = new SqlCommand(StoredProcedure.DeleteProblemCategoryMaster, con);
                com.CommandType = CommandType.StoredProcedure;
                com.Parameters.AddWithValue("@ProblemCategoryId", id);
                i = com.ExecuteNonQuery();
            }
            return i;
        }  
    }
}