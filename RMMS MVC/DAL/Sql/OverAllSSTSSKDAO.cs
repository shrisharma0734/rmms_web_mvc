﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;
using RMMS_MVC.Models;

namespace RMMS_MVC.DAL.Sql
{
    public class OverAllSSTSSKDAO : BaseDAO 
    {
        public DataSet OverAllMAchineDetails()
        {
            using (SqlConnection con = new SqlConnection(ConnectTo))
            {
                con.Open();
                SqlCommand cmd = new SqlCommand(StoredProcedure.Get_SSTMachineDetails, con);
                cmd.CommandType = CommandType.StoredProcedure;
                SqlDataAdapter da = new SqlDataAdapter();
                da.SelectCommand = cmd;
                DataSet myrec = new DataSet();
                da.Fill(myrec);
                return myrec;
            }
        }
        public DataSet myterminaldata(string TerminalID)
        {
            using (SqlConnection con = new SqlConnection(ConnectTo))
            {
                con.Open();
                SqlCommand cmd = new SqlCommand(StoredProcedure.SP_GetTerminalInfo, con);
                cmd.Parameters.Add(new SqlParameter("@TerminalID", TerminalID));
                cmd.CommandType = CommandType.StoredProcedure;
                SqlDataAdapter da = new SqlDataAdapter();
                da.SelectCommand = cmd;
                DataSet myrec = new DataSet();
                da.Fill(myrec);
                return myrec;
            }
        }
        public DataSet myproblemdata(string TerminalID)
        {
            using (SqlConnection con = new SqlConnection(ConnectTo))
            {
                con.Open();
                SqlCommand cmd = new SqlCommand(StoredProcedure.SP_ShowTermiailProblem, con);
                //cmd.Parameters.Add(new SqlParameter("@Problem", Problem));
                cmd.Parameters.Add(new SqlParameter("@TerminalID", TerminalID));
                cmd.CommandType = CommandType.StoredProcedure;
                SqlDataAdapter da = new SqlDataAdapter();
                da.SelectCommand = cmd;
                DataSet myrec = new DataSet();
                da.Fill(myrec);
                return myrec;
            }
        }
        public DataSet DoScreenShot(string SSTID, string GroupName, int UserID)
        {
            using (SqlConnection con = new SqlConnection(ConnectTo))
            {
                con.Open();
                SqlCommand cmd = new SqlCommand(StoredProcedure.SP_DoScreenShot, con);
                cmd.Parameters.Add(new SqlParameter("@SSTID", SSTID));
                cmd.Parameters.Add(new SqlParameter("@Name", GroupName));
                cmd.Parameters.Add(new SqlParameter("@UserID", UserID));
                cmd.CommandType = CommandType.StoredProcedure;
                SqlDataAdapter da = new SqlDataAdapter();
                da.SelectCommand = cmd;
                DataSet myrec = new DataSet();
                da.Fill(myrec);
                return myrec;
            }
        }
        public DataSet DoRestart(string SSTID, string GroupName, int UserID)
        {
            using (SqlConnection con = new SqlConnection(ConnectTo))
            {
                con.Open();
                SqlCommand cmd = new SqlCommand(StoredProcedure.SP_DoRemotelyRestart, con);
                cmd.Parameters.Add(new SqlParameter("@SSTID", SSTID));
                cmd.Parameters.Add(new SqlParameter("@Name", GroupName));
                cmd.Parameters.Add(new SqlParameter("@UserID", UserID));
                cmd.CommandType = CommandType.StoredProcedure;
                SqlDataAdapter da = new SqlDataAdapter();
                da.SelectCommand = cmd;
                DataSet myrec = new DataSet();
                da.Fill(myrec);
                return myrec;
            }

        }
        public DataSet ScreenShotResult(string ScheduleIDList)
        {
            using (SqlConnection con = new SqlConnection(ConnectTo))
            {
                con.Open();
                SqlCommand cmd = new SqlCommand(StoredProcedure.SP_ScreenShot_Restult, con);
                cmd.Parameters.Add(new SqlParameter("@ScheduleIDList", ScheduleIDList));
                cmd.CommandType = CommandType.StoredProcedure;
                SqlDataAdapter da = new SqlDataAdapter();
                da.SelectCommand = cmd;
                DataSet myrec = new DataSet();
                da.Fill(myrec);
                return myrec;
            }
        }
        public DataSet RestartResult(string ScheduleIDList1)
        {
            using (SqlConnection con = new SqlConnection(ConnectTo))
            {
                con.Open();
                SqlCommand cmd = new SqlCommand(StoredProcedure.SP_RemotelyAurorestart_Restult, con);
                cmd.Parameters.Add(new SqlParameter("@ScheduleIDList", ScheduleIDList1));
                cmd.CommandType = CommandType.StoredProcedure;
                SqlDataAdapter da = new SqlDataAdapter();
                da.SelectCommand = cmd;
                DataSet myrec1 = new DataSet();
                da.Fill(myrec1);
                return myrec1;
            }
        }


    }
}