﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;
using RMMS_MVC.Models;

namespace RMMS_MVC.DAL.Sql
{
    public class ScreenShotDAO:BaseDAO
    {
        public DataSet ScreenShot()
        {
            using (SqlConnection con = new SqlConnection(ConnectTo))
            {
                con.Open();
                SqlCommand cmd = new SqlCommand(StoredProcedure.SP_RemotlyRestart_ShowTerminalDetails, con);
                cmd.CommandType = CommandType.StoredProcedure;
                SqlDataAdapter da = new SqlDataAdapter();
                da.SelectCommand = cmd;
                DataSet myrec = new DataSet();
                da.Fill(myrec);
                return myrec;
            }
        }
    }
}