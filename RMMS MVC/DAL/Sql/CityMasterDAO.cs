﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;
using RMMS_MVC.Models;

namespace RMMS_MVC.DAL.Sql
{
    public class CityMasterDAO : BaseDAO
    {
        //Return list of all Cities  
        public List<CityMaster> ListAll()
        {
            List<CityMaster> lst = new List<CityMaster>();
            using (SqlConnection con = new SqlConnection(ConnectTo))
            {
                con.Open();
                SqlCommand com = new SqlCommand(StoredProcedure.SelectCityMaster, con);
                com.CommandType = CommandType.StoredProcedure;
                SqlDataReader rdr = com.ExecuteReader();
                while (rdr.Read())
                {
                    lst.Add(new CityMaster
                    {
                        CityID = Convert.ToInt32(rdr["CityID"]),
                        SSTGroupID = Convert.ToInt32(rdr["SSTGroupID"]),
                        SSTGroupName = rdr["SSTGroupName"].ToString(),
                        //ZoneName = rdr["ZoneName"].ToString(),
                        CityName = rdr["CityName"].ToString(),
                        State = rdr["state"].ToString(),
                        PinCode = Convert.ToString(rdr["pincode"])
                    });
                }
                return lst;
            }
        }

        //Method for Adding an City 
        public int Add(CityMaster city)
        {
            int i;
            using (SqlConnection con = new SqlConnection(ConnectTo))
            {
                con.Open();
                SqlCommand com = new SqlCommand(StoredProcedure.InsertUpdateCityMaster, con);
                com.CommandType = CommandType.StoredProcedure;
                com.Parameters.AddWithValue("@Id", city.CityID);
                com.Parameters.AddWithValue("@SSTGroupID", city.SSTGroupID);
                //com.Parameters.AddWithValue("@ZoneName", city.ZoneName);
                com.Parameters.AddWithValue("@CityName", city.CityName);
                com.Parameters.AddWithValue("@State", city.State);
                com.Parameters.AddWithValue("@pincode", Convert.ToInt32(city.PinCode));
                com.Parameters.AddWithValue("@Action", "Insert");
                i = com.ExecuteNonQuery();
            }
            return i;
        }

        //Method for Updating City record  
        public int Update(CityMaster city)
        {
            int i;
            using (SqlConnection con = new SqlConnection(ConnectTo))
            {
                con.Open();
                SqlCommand com = new SqlCommand(StoredProcedure.InsertUpdateCityMaster, con);
                com.CommandType = CommandType.StoredProcedure;
                com.Parameters.AddWithValue("@Id", city.CityID);
                com.Parameters.AddWithValue("@SSTGroupID", city.SSTGroupID);
                //com.Parameters.AddWithValue("@ZoneName", city.ZoneName);
                com.Parameters.AddWithValue("@CityName", city.CityName);
                com.Parameters.AddWithValue("@State", city.State);
                com.Parameters.AddWithValue("@pincode", Convert.ToInt32(city.PinCode));
                com.Parameters.AddWithValue("@Action", "Update");
                i = com.ExecuteNonQuery();
            }
            return i;
        }

        //Method for Deleting an City
        public int Delete(int ID)
        {
            int i;
            using (SqlConnection con = new SqlConnection(ConnectTo))
            {
                con.Open();
                SqlCommand com = new SqlCommand(StoredProcedure.DeleteCityMaster, con);
                com.CommandType = CommandType.StoredProcedure;
                com.Parameters.AddWithValue("@Id", ID);
                i = com.ExecuteNonQuery();
            }
            return i;
        }
    }
}