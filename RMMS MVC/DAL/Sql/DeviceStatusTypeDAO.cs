﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;
using RMMS_MVC.Models;


namespace RMMS_MVC.DAL.Sql
{
    public class DeviceStatusTypeDAO:BaseDAO
    {

        public List<DeviceStatusTypeProfile> ListAll()
        {
            List<DeviceStatusTypeProfile> lst = new List<DeviceStatusTypeProfile>();
            using (SqlConnection con = new SqlConnection(ConnectTo))
            {
                con.Open();
                SqlCommand com = new SqlCommand(StoredProcedure.SelectDeviceStatusTypeMaster, con);
                com.CommandType = CommandType.StoredProcedure;

                SqlDataReader rdr = com.ExecuteReader();
                while (rdr.Read())
                {
                    lst.Add(new DeviceStatusTypeProfile
                    {
                        StatusTypeID = Convert.ToInt32(rdr["StatusTypeId"]),
                        StatusType = rdr["StatusType"].ToString(),
                         StatusTypeDescription = rdr["StatusTypeDescription"].ToString(),

                    });
                }
                return lst;
            }
        }
        //Method To Add Group
        public int Add(DeviceStatusTypeProfile Sst)
        {
            int i;
            using (SqlConnection con = new SqlConnection(ConnectTo))
            {
                con.Open();
                SqlCommand com = new SqlCommand(StoredProcedure.InsertUpdateDeviceStatusTypeMaster, con);
                com.CommandType = CommandType.StoredProcedure;
                com.Parameters.AddWithValue("@StatusTypeID", Sst.StatusTypeID);
                com.Parameters.AddWithValue("@StatusType", Sst.StatusType);
                com.Parameters.AddWithValue("@StatusTypeDescription", Sst.StatusTypeDescription);
                com.Parameters.AddWithValue("@CreatedBy", '1');
                com.Parameters.AddWithValue("@CreatedDatetime", SqlDbType.DateTime);
                com.Parameters.AddWithValue("@ModifiedBy", '1');
                com.Parameters.AddWithValue("@ModifiedDatetime", SqlDbType.DateTime);


                com.Parameters.AddWithValue("@Action", "Insert");
                i = com.ExecuteNonQuery();
            }
            return i;
        }
        //Methods To Update Group
        public int Update(DeviceStatusTypeProfile Sst)
        {
            int i;
            using (SqlConnection con = new SqlConnection(ConnectTo))
            {
                con.Open();
                SqlCommand com = new SqlCommand(StoredProcedure.InsertUpdateDeviceStatusTypeMaster, con);
                com.CommandType = CommandType.StoredProcedure;
                com.Parameters.AddWithValue("@StatusTypeID", Sst.StatusTypeID);
                com.Parameters.AddWithValue("@StatusType", Sst.StatusType);
                com.Parameters.AddWithValue("@StatusTypeDescription", Sst.StatusTypeDescription);
                com.Parameters.AddWithValue("@CreatedBy", '1');
                com.Parameters.AddWithValue("@CreatedDatetime", SqlDbType.DateTime);
                com.Parameters.AddWithValue("@ModifiedBy", '1');
                com.Parameters.AddWithValue("@ModifiedDatetime", SqlDbType.DateTime);

                com.Parameters.AddWithValue("@Action", "Update");
                i = com.ExecuteNonQuery();
            }
            return i;
        }



        //Method for Deleting an Bank
        public int Delete(int StatusTypeId)
        {
            int i;
            using (SqlConnection con = new SqlConnection(ConnectTo))
            {
                con.Open();
                SqlCommand com = new SqlCommand(StoredProcedure.DeleteDeviceStatusTypeMaster, con);
                com.CommandType = CommandType.StoredProcedure;
                com.Parameters.AddWithValue("@StatusTypeID", StatusTypeId);
                i = com.ExecuteNonQuery();
            }
            return i;
        }


    }
}