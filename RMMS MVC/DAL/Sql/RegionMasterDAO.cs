﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using RMMS_MVC.Models;
using System.Data.SqlClient;
using System.Data;

namespace RMMS_MVC.DAL.Sql
{
    public class RegionMasterDAO : BaseDAO
    {
        //Return List of All Region
        public List<RegionMaster> ListAll()
        {
            List<RegionMaster> lst = new List<RegionMaster>();
            using (SqlConnection con = new SqlConnection(ConnectTo))
            {
                con.Open();
                SqlCommand com = new SqlCommand(StoredProcedure.SelectRegionMaster, con);
                com.CommandType = CommandType.StoredProcedure;
                SqlDataReader rdr = com.ExecuteReader();
                while (rdr.Read())
                {
                    lst.Add(new RegionMaster
                    {
                        RegionID = Convert.ToInt32(rdr["RegionID"]),
                        RegionName = rdr["RegionName"].ToString(),
                        //RegionID = Convert.ToInt32(rdr["RegionID"]),
                        BankID = Convert.ToInt32(rdr["BankID"]),
                        BankName = rdr["BankName"].ToString(),
                        RegionDescription = rdr["RegionDescription"].ToString(),
                       
                    });
                }
                return lst;
            }
        }
        //Method for Adding an Region 
        public int Add(RegionMaster Region)
        {
            int i;
            using (SqlConnection con = new SqlConnection(ConnectTo))
            {
                con.Open();
                SqlCommand com = new SqlCommand(StoredProcedure.InsertUpdateRegionMaster, con);
                com.CommandType = CommandType.StoredProcedure;
                com.Parameters.AddWithValue("@BankID", Region.BankID);
                //com.Parameters.AddWithValue("@BankName", Region.BankName);
                com.Parameters.AddWithValue("@RegionID", Region.RegionID);
                com.Parameters.AddWithValue("@RegionName", Region.RegionName);
                com.Parameters.AddWithValue("@Description", Region.RegionDescription);
                com.Parameters.AddWithValue("@Action", "Insert");
                i = com.ExecuteNonQuery();
            }
            return i;
        }

        //Method for Updating Region record  
        public int Update(RegionMaster Region)
        {
            int i;
            using (SqlConnection con = new SqlConnection(ConnectTo))
            {
                con.Open();
                SqlCommand com = new SqlCommand(StoredProcedure.InsertUpdateRegionMaster, con);
                com.CommandType = CommandType.StoredProcedure;
                com.Parameters.AddWithValue("@BankID", Region.BankID);
                //com.Parameters.AddWithValue("@BankName", Region.BankName);
                com.Parameters.AddWithValue("@RegionID", Region.RegionID);
                com.Parameters.AddWithValue("@RegionName", Region.RegionName);
                com.Parameters.AddWithValue("@Description", Region.RegionDescription);
                com.Parameters.AddWithValue("@Action", "Update");
                i = com.ExecuteNonQuery();
            }
            return i;
        }

        //Method for Deleting an Region
        public int Delete(int ID)
        {
            int i;
            using (SqlConnection con = new SqlConnection(ConnectTo))
            {
                con.Open();
                SqlCommand com = new SqlCommand(StoredProcedure.DeleteBankRegionMaster, con);
                com.CommandType = CommandType.StoredProcedure;
                com.Parameters.AddWithValue("@RegionID", ID);
              
                i = com.ExecuteNonQuery();
            }
            return i;
        }

    }
}