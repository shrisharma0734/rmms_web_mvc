﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;
using RMMS_MVC.Models;


namespace RMMS_MVC.DAL.Sql
{
    public class RoleMasterDAO : BaseDAO
    {
        //Return List of All Roles
        public List<RoleMaster> ListAll()
        {
            List<RoleMaster> lst = new List<RoleMaster>();
            using (SqlConnection con = new SqlConnection(ConnectTo))
            {
                con.Open();
                SqlCommand com = new SqlCommand(StoredProcedure.SelectRoleMaster, con);
                com.CommandType = CommandType.StoredProcedure;
                SqlDataReader rdr = com.ExecuteReader();

                while (rdr.Read())
                {
                    lst.Add(new RoleMaster
                    {
                        RoleID = Convert.ToInt32(rdr["RoleID"]),
                        RoleName = rdr["RoleName"].ToString(),
                        RoleDescription = rdr["RoleDescription"].ToString(),
                    });
                }
        
                return lst;
            }
        }

        //Method for Adding an Role

        public int Add(RoleMaster Role)
        {
            int i;
            using (SqlConnection con = new SqlConnection(ConnectTo))
            {
                con.Open();
                SqlCommand com = new SqlCommand(StoredProcedure.Role_CRUD, con);
                com.CommandType = CommandType.StoredProcedure;
                com.Parameters.AddWithValue("@RoleID", Role.RoleID);
                com.Parameters.AddWithValue("@RoleName", Role.RoleName);
                com.Parameters.AddWithValue("@RoleDescription", Role.RoleDescription);
                com.Parameters.AddWithValue("@Action", "Insert");
                i = com.ExecuteNonQuery();
            }
            return i;
        }

        //Method for Updating Employee record  
        public int Update(RoleMaster Role)
        {
            int i;
            using (SqlConnection con = new SqlConnection(ConnectTo))
            {
                con.Open();
                SqlCommand com = new SqlCommand(StoredProcedure.Role_CRUD, con);
                com.CommandType = CommandType.StoredProcedure;
                com.Parameters.AddWithValue("@RoleID", Role.RoleID);
                com.Parameters.AddWithValue("@RoleName", Role.RoleName);
                com.Parameters.AddWithValue("@RoleDescription", Role.RoleDescription);
                //com.Parameters.AddWithValue("@CreatedBy", Role.CreatedBy);
                //com.Parameters.AddWithValue("@CreatedDatetime", Role.CreatedDatetime);
                //com.Parameters.AddWithValue("@CreatedBy", Role.CreatedBy);
                //com.Parameters.AddWithValue("@CreatedDatetime", Role.CreatedDatetime);
                //com.Parameters.AddWithValue("@ModifiedBy", Role.ModifiedBy);
                //com.Parameters.AddWithValue("@ModifiedDatetime", Role.ModifiedDatetime);
                com.Parameters.AddWithValue("@Action", "Update");
                i = com.ExecuteNonQuery();
            }
            return i;
        }

        //Method for Deleting an Employee  
        public int Delete(int ID)
        {
            int i;
            using (SqlConnection con = new SqlConnection(ConnectTo))
            {
                con.Open();
                SqlCommand com = new SqlCommand(StoredProcedure.DeleteRoleMaster, con);
                com.CommandType = CommandType.StoredProcedure;
                com.Parameters.AddWithValue("@ID", ID);
                i = com.ExecuteNonQuery();
            }
            return i;
        }  

    }
}
