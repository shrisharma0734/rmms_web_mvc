﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;

namespace RMMS_MVC.DAL
{
    public class BaseDAO
    {
        //declare connection string  

        private string cs = ConfigurationManager.ConnectionStrings["strConn"].ConnectionString;

        public string ConnectTo 
        {
            get {
                return cs;
            }
        }
    }
}