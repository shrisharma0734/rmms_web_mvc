﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using DAL;
//using RMMS_MVC.MVCActionStoreProcedure;
using RMMS_MVC.MVCMODELS;

namespace RMMS_MVC.MVCActionStoreProcedure
{
    public class clsPullRequest : DBObj
    {
        #region Class Variables

        protected decimal _ScheduleID;
        protected string _Activity;
        protected string _JobType;
        protected decimal _SSTID;
        protected string _SST_ORGIP;
        protected string _SST_NAT_IP;
        protected decimal _SST_AgentPort;

        /// <summary>/// _EDCFPath_OnSST /// </summary>
        protected string _JobTypeAgentPath;

        /// <summary>/// _EDCFileName_OnSST/// </summary>
        protected string _EDCFileName_OnSST;

        #endregion


        /// <summary>Class Properties</summary>
        #region Properties - Get and Set Accessor

        public decimal ScheduleID
        {
            get { return _ScheduleID; }
            set { _ScheduleID = value; }
        }

        public string Activity
        {
            get { return _Activity; }
            set { _Activity = value; }
        }

        public string JobType
        {
            get { return _JobType; }
            set { _JobType = value; }
        }

        public decimal SSTID
        {
            get { return _SSTID; }
            set { _SSTID = value; }
        }

        public string SST_ORGIP
        {
            get { return _SST_ORGIP; }
            set { _SST_ORGIP = value; }
        }

        public string SST_NAT_IP
        {
            get { return _SST_NAT_IP; }
            set { _SST_NAT_IP = value; }
        }

        public decimal SST_AgentPort
        {
            get { return _SST_AgentPort; }
            set { _SST_AgentPort = value; }
        }



        public string EDCFileName_OnSST
        {
            get { return _EDCFileName_OnSST; }
            set { _EDCFileName_OnSST = value; }
        }

        public string JobTypeAgentPath
        {
            get { return _JobTypeAgentPath; }
            set { _JobTypeAgentPath = value; }
        }

        #endregion


        #region  Methods

        public override void fillup()
        {

            BLogicS sa = new BLogicS();
            IDataReader dr = sa.GetTestPullReqDetails(ScheduleID);
            dr.Read();

            if (dr["ScheduleID"] != DBNull.Value)
            {
                this._ScheduleID = (decimal)dr["ScheduleID"];
            }
            if (dr["Activity"] != DBNull.Value)
            {
                this._Activity = (string)dr["Activity"];
            }
            if (dr["JobType"] != DBNull.Value)
            {
                this._JobType = (string)dr["JobType"];
            }
            if (dr["SSTID"] != DBNull.Value)
            {
                this._SSTID = (decimal)dr["SSTID"];
            }
            if (dr["SST_ORGIP"] != DBNull.Value)
            {
                this._SST_ORGIP = (string)dr["SST_ORGIP"];
            }
            if (dr["SST_NAT_IP"] != DBNull.Value)
            {
                this._SST_NAT_IP = (string)dr["SST_NAT_IP"];
            }
            if (dr["SST_AgentPort"] != DBNull.Value)
            {
                this._SST_AgentPort = (decimal)dr["SST_AgentPort"];
            }
            if (dr["JobTypeAgentPath"] != DBNull.Value)
            {
                this._JobTypeAgentPath = (string)dr["JobTypeAgentPath"];
            }
            if (dr["EDCFileName_OnSST"] != DBNull.Value)
            {
                this._EDCFileName_OnSST = (string)dr["EDCFileName_OnSST"];
            }

        }


        #endregion

    }
}