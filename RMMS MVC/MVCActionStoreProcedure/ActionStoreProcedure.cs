﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Configuration;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Data.SqlClient;
//using FORBES.DAE;
using RMMS_MVC;


namespace RMMS_MVC.MVCActionStoreProcedure
{
    /// <summary>
    /// Summary description for ActionStoreProcedure
    /// </summary>
    public class BLogicS
    {
        public BLogicS()
        {
        }

        #region Return counter - int32 Scalar value

        /// <summary>
        /// Returns total count
        /// </summary>
        /// <returns></returns>
        public int AdminManagerGetTotalCounts
        {
            get { return DataAccess.GetIntScalarVal("RoleCountAll"); }
        }

        #endregion

        #region SSTMaster

        public IDataReader GetSSTFilePath(decimal SStTypeID)
        {
            SqlParameter prmID = new SqlParameter("@SStTypeID", SqlDbType.Decimal, 4);
            prmID.Value = SStTypeID;
            return DataAccess.GetFromReader("SP_getFileName", prmID);
        }

        #endregion

        #region PullRequest

        public IDataReader GetTestPullReqDetails(decimal ScheduleID)
        {
            SqlParameter prmID = new SqlParameter("@ScheduleID", SqlDbType.Decimal, 4);
            prmID.Value = ScheduleID;
            return DataAccess.GetFromReader("SP_GetTestPullReqDetails", prmID);
        }

        #endregion


        #region ScheduleDetailInfo

        public IDataReader GetScheduleDetailInfo(decimal ScheduleID)
        {
            SqlParameter prmScheduleID = new SqlParameter("@ScheduleID", SqlDbType.Decimal, 4);
            prmScheduleID.Value = ScheduleID;
            return DataAccess.GetFromReader("sp_GetScheduleDetailInfo", prmScheduleID);
        }
        public IDataReader GetJobActionDetailID(decimal SSTID, decimal ScheduleID)
        {
            SqlParameter prmSSTID = new SqlParameter("@SSTID", SqlDbType.Decimal, 4);
            prmSSTID.Value = SSTID;

            SqlParameter prmScheduleID = new SqlParameter("@ScheduleID", SqlDbType.VarChar, 4);
            prmScheduleID.Value = ScheduleID;

            return DataAccess.GetFromReader("sp_GetSSTIdFromIP", prmSSTID, prmScheduleID);
        }

        public DataTable SP_GetMultiCheduleInfo(decimal ScheduleID) // 
        {
            SqlParameter prmScheduleID = new SqlParameter("@ScheduleID", SqlDbType.Decimal, 4);
            prmScheduleID.Value = ScheduleID;
            return DataAccess.GetFromDataTable("sp_GetScheduleDetailInfo", prmScheduleID);
        }

        #endregion

        #region Transaction Pages

        #region JobResponseMaster

        public int sp_Insert_JobResponseMaster_CDMS(decimal ActivityCodeID, string JobResponseCode, string JobResponse, string ResponseDescription, decimal CreatedBy, DateTime CreatedDatetime, decimal ModifiedBy, DateTime ModifiedDatetime)
        {
            SqlParameter prmActivityCodeID = new SqlParameter("@ActivityCodeID", SqlDbType.Decimal);
            prmActivityCodeID.Value = ActivityCodeID;
            SqlParameter prmJobResponseCode = new SqlParameter("@JobResponseCode", SqlDbType.VarChar, 50);
            prmJobResponseCode.Value = JobResponseCode;
            SqlParameter prmJobResponse = new SqlParameter("@JobResponse", SqlDbType.VarChar, 50);
            prmJobResponse.Value = JobResponse;
            SqlParameter prmResponseDescription = new SqlParameter("@ResponseDescription", SqlDbType.VarChar, 50);
            prmResponseDescription.Value = ResponseDescription;
            SqlParameter prmCreatedBy = new SqlParameter("@CreatedBy", SqlDbType.Decimal);
            prmCreatedBy.Value = CreatedBy;
            SqlParameter prmCreatedDatetime = new SqlParameter("@CreatedDatetime", SqlDbType.SmallDateTime);
            prmCreatedDatetime.Value = CreatedDatetime;
            SqlParameter prmModifiedBy = new SqlParameter("@ModifiedBy", SqlDbType.Decimal);
            prmModifiedBy.Value = ModifiedBy;
            SqlParameter prmModifiedDatetime = new SqlParameter("@ModifiedDatetime", SqlDbType.SmallDateTime);
            prmModifiedDatetime.Value = ModifiedDatetime;
            int x = DataAccess.Execute("sp_Insert_JobResponseMaster_CDMS", prmActivityCodeID, prmJobResponseCode, prmJobResponse, prmResponseDescription, prmCreatedBy, prmCreatedDatetime, prmModifiedBy, prmModifiedDatetime);
            return x;


        }

        public int sp_Modify_JobResponseMaster_CDMS(decimal ActivityCodeID, string JobResponseCode, string JobResponse, string ResponseDescription, decimal ModifiedBy, DateTime ModifiedDatetime, decimal JobResponseID)
        {
            SqlParameter prmActivityCodeID = new SqlParameter("@ActivityCodeID", SqlDbType.Decimal);
            prmActivityCodeID.Value = ActivityCodeID;
            SqlParameter prmJobResponseCode = new SqlParameter("@JobResponseCode", SqlDbType.VarChar, 50);
            prmJobResponseCode.Value = JobResponseCode;
            SqlParameter prmJobResponse = new SqlParameter("@JobResponse", SqlDbType.VarChar, 50);
            prmJobResponse.Value = JobResponse;
            SqlParameter prmResponseDescription = new SqlParameter("@ResponseDescription", SqlDbType.VarChar, 50);
            prmResponseDescription.Value = ResponseDescription;
            SqlParameter prmModifiedBy = new SqlParameter("@ModifiedBy", SqlDbType.Decimal);
            prmModifiedBy.Value = ModifiedBy;
            SqlParameter prmModifiedDatetime = new SqlParameter("@ModifiedDatetime", SqlDbType.SmallDateTime);
            prmModifiedDatetime.Value = ModifiedDatetime;
            SqlParameter prmJobResponseID = new SqlParameter("@JobResponseID", SqlDbType.Decimal);
            prmJobResponseID.Value = (JobResponseID);
            int x = DataAccess.Execute("sp_Modify_JobResponseMaster_CDMS", prmActivityCodeID, prmJobResponseCode, prmJobResponse, prmResponseDescription, prmModifiedBy, prmModifiedDatetime, prmJobResponseID);
            return x;
        }

        public int sp_Delete_JobResponseMaster_CDMS(decimal JobResponseID)
        {
            SqlParameter prmJobResponseID = new SqlParameter("@prmJobResponseID", SqlDbType.Decimal);
            prmJobResponseID.Value = JobResponseID;
            int x = DataAccess.Execute("sp_Delete_JobResponseMaster_CDMS", prmJobResponseID);
            return x;
        }

        #endregion


        #region JobActionDetails

        public int sp_Insert_JobActionDetails_CDMS(decimal ActivityDetailID, decimal ScheduleID, DateTime ScheduleDatetime, decimal SSTActivityListID, decimal ActivityListDetailID, decimal SSTID, DateTime ActualStartDateTime,
                 decimal JobResponseID, DateTime JobResponseDateTime, decimal RetryCount, string Remarks, decimal CreatedBy, DateTime CreatedDatetime, decimal ModifiedBy, DateTime ModifiedDatetime)
        {
            //SqlParameter prmJobDetailsID = new SqlParameter("@JobDetailsID", SqlDbType.Decimal);
            //prmJobDetailsID.Value = (JobDetailsID);
            ClsDetails.writeStringTotransactionLog("Inside jobactiondetails");
            SqlParameter prmActivityDetailID = new SqlParameter("@ActivityDetailID", SqlDbType.Decimal);
            prmActivityDetailID.Value = (ActivityDetailID);
            SqlParameter prmScheduleID = new SqlParameter("@ScheduleID", SqlDbType.Decimal);
            prmScheduleID.Value = (ScheduleID);
            SqlParameter prmScheduleDatetime = new SqlParameter("@ScheduleDatetime", SqlDbType.SmallDateTime);
            prmScheduleDatetime.Value = ScheduleDatetime;
            SqlParameter prmSSTActivityListID = new SqlParameter("@SSTActivityListID", SqlDbType.Decimal);
            prmSSTActivityListID.Value = (SSTActivityListID);
            SqlParameter prmActivityListDetailID = new SqlParameter("@ActivityListDetailID", SqlDbType.Decimal);
            prmActivityListDetailID.Value = (ActivityListDetailID);
            SqlParameter prmSSTID = new SqlParameter("@SSTID", SqlDbType.Decimal);
            prmSSTID.Value = (SSTID);
            SqlParameter prmActualStartDateTime = new SqlParameter("@ActualStartDateTime", SqlDbType.SmallDateTime);
            prmActualStartDateTime.Value = ActualStartDateTime;
            SqlParameter prmJobResponseID = new SqlParameter("@JobResponseID", SqlDbType.Decimal);
            prmJobResponseID.Value = (JobResponseID);
            SqlParameter prmJobResponseDateTime = new SqlParameter("@JobResponseDateTime", SqlDbType.SmallDateTime);
            prmJobResponseDateTime.Value = JobResponseDateTime;
            SqlParameter prmRetryCount = new SqlParameter("@RetryCount", SqlDbType.Decimal);
            prmRetryCount.Value = (RetryCount);
            SqlParameter prmRemarks = new SqlParameter("@Remarks", SqlDbType.VarChar, 100);
            prmRemarks.Value = Remarks;
            SqlParameter prmCreatedBy = new SqlParameter("@CreatedBy", SqlDbType.Decimal);
            prmCreatedBy.Value = CreatedBy;
            SqlParameter prmCreatedDatetime = new SqlParameter("@CreatedDatetime", SqlDbType.SmallDateTime);
            prmCreatedDatetime.Value = CreatedDatetime;
            SqlParameter prmModifiedBy = new SqlParameter("@ModifiedBy", SqlDbType.Decimal);
            prmModifiedBy.Value = ModifiedBy;
            SqlParameter prmModifiedDatetime = new SqlParameter("@ModifiedDatetime", SqlDbType.SmallDateTime);
            prmModifiedDatetime.Value = ModifiedDatetime;
            int x = DataAccess.Execute("sp_Insert_jobActionDetails_CDMS", prmActivityDetailID, prmScheduleID, prmScheduleDatetime, prmSSTActivityListID, prmActivityListDetailID, prmSSTID, prmActualStartDateTime, prmJobResponseID, prmJobResponseDateTime, prmRetryCount, prmRemarks, prmCreatedBy, prmCreatedDatetime, prmModifiedBy, prmModifiedDatetime);
            return x;
        }

        public int sp_Modify_JobActionDetails_CDMS(decimal ActivityDetailID, decimal ScheduleID, DateTime ScheduleDatetime, decimal SSTActivityListID, decimal ActivityListDetailID, decimal SSTID, DateTime ActualStartDateTime,
                 decimal JobResponseID, DateTime JobResponseDateTime, decimal RetryCount, string Remarks, decimal ModifiedBy, DateTime ModifiedDatetime, decimal JobDetailsID)
        {
            SqlParameter prmActivityDetailID = new SqlParameter("@ActivityDetailID", SqlDbType.Decimal);
            prmActivityDetailID.Value = (ActivityDetailID);
            SqlParameter prmScheduleID = new SqlParameter("@ScheduleID", SqlDbType.Decimal);
            prmScheduleID.Value = (ScheduleID);
            SqlParameter prmScheduleDatetime = new SqlParameter("@ScheduleDatetime", SqlDbType.SmallDateTime);
            prmScheduleDatetime.Value = ScheduleDatetime;
            SqlParameter prmSSTActivityListID = new SqlParameter("@SSTActivityListID", SqlDbType.Decimal);
            prmSSTActivityListID.Value = (SSTActivityListID);
            SqlParameter prmActivityListDetailID = new SqlParameter("@ActivityListDetailID", SqlDbType.Decimal);
            prmActivityListDetailID.Value = (ActivityListDetailID);
            SqlParameter prmSSTID = new SqlParameter("@SSTID", SqlDbType.Decimal);
            prmSSTID.Value = (SSTID);
            SqlParameter prmActualStartDateTime = new SqlParameter("@ActualStartDateTime", SqlDbType.SmallDateTime);
            prmActualStartDateTime.Value = ActualStartDateTime;
            SqlParameter prmJobResponseID = new SqlParameter("@JobResponseID", SqlDbType.Decimal);
            prmJobResponseID.Value = (JobResponseID);
            SqlParameter prmJobResponseDateTime = new SqlParameter("@JobResponseDateTime", SqlDbType.SmallDateTime);
            prmJobResponseDateTime.Value = JobResponseDateTime;
            SqlParameter prmRetryCount = new SqlParameter("@RetryCount", SqlDbType.Decimal);
            prmRetryCount.Value = (RetryCount);
            SqlParameter prmRemarks = new SqlParameter("@Remarks", SqlDbType.VarChar, 100);
            prmRemarks.Value = Remarks;

            SqlParameter prmModifiedBy = new SqlParameter("@ModifiedBy", SqlDbType.Decimal);
            prmModifiedBy.Value = ModifiedBy;
            SqlParameter prmModifiedDatetime = new SqlParameter("@ModifiedDatetime", SqlDbType.SmallDateTime);
            prmModifiedDatetime.Value = ModifiedDatetime;
            SqlParameter prmJobDetailsID = new SqlParameter("@JobDetailsID", SqlDbType.Decimal);
            prmJobDetailsID.Value = (JobDetailsID);

            int x = DataAccess.Execute("sp_Modify_JobActionDetails_CDMS", prmActivityDetailID, prmScheduleID, prmScheduleDatetime, prmSSTActivityListID, prmActivityListDetailID, prmSSTID, prmActualStartDateTime, prmJobResponseID, prmJobResponseDateTime, prmRetryCount, prmRemarks, prmModifiedBy, prmModifiedDatetime, prmJobDetailsID);
            return x;
        }

        public int sp_Delete_JobActionDetails_CDMS(decimal JobDetailsID)
        {
            SqlParameter prmJobDetailsID = new SqlParameter("@JobDetailsID", SqlDbType.Decimal);
            prmJobDetailsID.Value = JobDetailsID;
            int x = DataAccess.Execute("sp_Delete_JobActionDetails_CDMS", prmJobDetailsID);
            return x;
        }

        #endregion


        #region ActivityMaster

        public int sp_Insert_ActivityMaster_CDMS(string ActivityCode, string Activity, string ActivityDescription, decimal CreatedBy, DateTime CreatedDatetime, decimal ModifiedBy, DateTime ModifiedDatetime)
        {
            SqlParameter prmActivityCode = new SqlParameter("@ActivityCode", SqlDbType.VarChar, 10);
            prmActivityCode.Value = ActivityCode;
            SqlParameter prmActivity = new SqlParameter("@Activity", SqlDbType.VarChar, 25);
            prmActivity.Value = Activity;
            SqlParameter prmActivityDescription = new SqlParameter("@ActivityDescription", SqlDbType.VarChar, 50);
            prmActivityDescription.Value = ActivityDescription;

            SqlParameter prmCreatedBy = new SqlParameter("@CreatedBy", SqlDbType.Decimal);
            prmCreatedBy.Value = CreatedBy;
            SqlParameter prmCreatedDatetime = new SqlParameter("@CreatedDatetime", SqlDbType.SmallDateTime);
            prmCreatedDatetime.Value = CreatedDatetime;
            SqlParameter prmModifiedBy = new SqlParameter("@ModifiedBy", SqlDbType.Decimal);
            prmModifiedBy.Value = ModifiedBy;
            SqlParameter prmModifiedDatetime = new SqlParameter("@ModifiedDatetime", SqlDbType.SmallDateTime);
            prmModifiedDatetime.Value = ModifiedDatetime;
            int x = DataAccess.Execute("sp_Insert_ActivityMaster_CDMS", prmActivityCode, prmActivity, prmActivityDescription, prmCreatedBy, prmCreatedDatetime, prmModifiedBy, prmModifiedDatetime);
            return x;

        }

        public int sp_Modify_ActivityMaster_CDMS(string ActivityCode, string Activity, string ActivityDescription, decimal ModifiedBy, DateTime ModifiedDatetime, decimal ActivityCodeID)
        {
            SqlParameter prmActivityCode = new SqlParameter("@ActivityCode", SqlDbType.VarChar, 10);
            prmActivityCode.Value = ActivityCode;
            SqlParameter prmActivity = new SqlParameter("@Activity", SqlDbType.VarChar, 25);
            prmActivity.Value = Activity;
            SqlParameter prmActivityDescriptions = new SqlParameter("@ActivityDescription", SqlDbType.VarChar, 50);
            prmActivityDescriptions.Value = ActivityDescription;

            SqlParameter prmModifiedBy = new SqlParameter("@ModifiedBy", SqlDbType.Decimal);
            prmModifiedBy.Value = ModifiedBy;
            SqlParameter prmModifiedDatetime = new SqlParameter("@ModifiedDatetime", SqlDbType.SmallDateTime);
            prmModifiedDatetime.Value = ModifiedDatetime;
            SqlParameter prmActivityCodeID = new SqlParameter("@ActivityCodeID", SqlDbType.Decimal);
            prmActivityCodeID.Value = (ActivityCodeID);

            int x = DataAccess.Execute("sp_Modify_ActivityMaster_CDMS", prmActivityCode, prmActivity, prmActivityDescriptions, prmModifiedBy, prmModifiedDatetime, prmActivityCodeID);
            return x;


        }

        public int sp_Delete_ActivityMaster_CDMS(decimal ActivityCodeID)
        {
            SqlParameter prmActivityCodeID = new SqlParameter("@JobDetailsID", SqlDbType.Decimal);
            prmActivityCodeID.Value = ActivityCodeID;
            int x = DataAccess.Execute("sp_Delete_ActivityMaster_CDMS", prmActivityCodeID);
            return x;
        }

        #endregion


        #region JobScheduleDetails

        public int sp_Insert_JobScheduleDetails_CDMS(decimal ScheduleID, decimal ActivityListDetailID, decimal SSTID, DateTime ScheduleDatetime, decimal JobTypeID, string JobTypeServerPath, string JobTypeAgentPath,
               decimal CreatedBy, DateTime CreatedDatetime, decimal ModifiedBy, DateTime ModifiedDatetime)
        {
            SqlParameter prmScheduleID = new SqlParameter("@ScheduleID", SqlDbType.Decimal);
            prmScheduleID.Value = (ScheduleID);
            SqlParameter prmActivityListDetailID = new SqlParameter("@ActivityListDetailID", SqlDbType.Decimal);
            prmActivityListDetailID.Value = (ActivityListDetailID);
            SqlParameter prmSSTID = new SqlParameter("@SSTID", SqlDbType.Decimal);
            prmSSTID.Value = (SSTID);
            SqlParameter prmScheduleDatetime = new SqlParameter("@ScheduleDatetime", SqlDbType.SmallDateTime);
            prmScheduleDatetime.Value = ScheduleDatetime;
            SqlParameter prmJobTypeID = new SqlParameter("@JobTypeID", SqlDbType.Decimal);
            prmJobTypeID.Value = (JobTypeID);


            SqlParameter prmJobTypeServerPath = new SqlParameter("@JobTypeServerPath", SqlDbType.VarChar, 150);
            prmJobTypeServerPath.Value = JobTypeServerPath;
            SqlParameter prmJobTypeAgentPath = new SqlParameter("@JobTypeAgentPath", SqlDbType.VarChar, 150);
            prmJobTypeAgentPath.Value = JobTypeAgentPath;

            SqlParameter prmCreatedBy = new SqlParameter("@CreatedBy", SqlDbType.Decimal);
            prmCreatedBy.Value = CreatedBy;
            SqlParameter prmCreatedDatetime = new SqlParameter("@CreatedDatetime", SqlDbType.SmallDateTime);
            prmCreatedDatetime.Value = CreatedDatetime;
            SqlParameter prmModifiedBy = new SqlParameter("@ModifiedBy", SqlDbType.Decimal);
            prmModifiedBy.Value = ModifiedBy;
            SqlParameter prmModifiedDatetime = new SqlParameter("@ModifiedDatetime", SqlDbType.SmallDateTime);
            prmModifiedDatetime.Value = ModifiedDatetime;
            int x = DataAccess.Execute("sp_Insert_JobScheduleDetails_CDMS", prmScheduleID, prmActivityListDetailID, prmSSTID, prmScheduleDatetime, prmJobTypeID, prmJobTypeServerPath, prmJobTypeAgentPath,
                prmCreatedBy, prmCreatedDatetime, prmModifiedBy, prmModifiedDatetime);
            return x;

        }

        public int sp_Modify_JobScheduleDetails_CDMS(decimal ScheduleID, decimal ActivityListDetailID, decimal SSTID, DateTime ScheduleDatetime, decimal JobTypeID, string JobTypeServerPath, string JobTypeAgentPath,
               decimal ModifiedBy, DateTime ModifiedDatetime, decimal ScheduleDetailsID)
        {
            SqlParameter prmScheduleID = new SqlParameter("@ScheduleID", SqlDbType.Decimal);
            prmScheduleID.Value = (ScheduleID);
            SqlParameter prmActivityListDetailID = new SqlParameter("@ActivityListDetailID", SqlDbType.Decimal);
            prmActivityListDetailID.Value = (ActivityListDetailID);
            SqlParameter prmSSTID = new SqlParameter("@SSTID", SqlDbType.Decimal);
            prmSSTID.Value = (SSTID);
            SqlParameter prmScheduleDatetime = new SqlParameter("@ScheduleDatetime", SqlDbType.SmallDateTime);
            prmScheduleDatetime.Value = ScheduleDatetime;
            SqlParameter prmJobTypeID = new SqlParameter("@JobTypeID", SqlDbType.Decimal);
            prmJobTypeID.Value = (JobTypeID);


            SqlParameter prmJobTypeServerPath = new SqlParameter("@JobTypeServerPath", SqlDbType.VarChar, 150);
            prmJobTypeServerPath.Value = JobTypeServerPath;
            SqlParameter prmJobTypeAgentPath = new SqlParameter("@JobTypeAgentPath", SqlDbType.VarChar, 150);
            prmJobTypeAgentPath.Value = JobTypeAgentPath;

            SqlParameter prmModifiedBy = new SqlParameter("@ModifiedBy", SqlDbType.Decimal);
            prmModifiedBy.Value = ModifiedBy;
            SqlParameter prmModifiedDatetime = new SqlParameter("@ModifiedDatetime", SqlDbType.SmallDateTime);
            prmModifiedDatetime.Value = ModifiedDatetime;
            SqlParameter prmScheduleDetailsID = new SqlParameter("@ScheduleDetailsID", SqlDbType.Decimal);
            prmScheduleDetailsID.Value = (ScheduleDetailsID);
            int x = DataAccess.Execute("sp_Modify_JobScheduleDetails_CDMS", prmScheduleID, prmActivityListDetailID, prmSSTID, prmScheduleDatetime, prmJobTypeID, prmJobTypeServerPath, prmJobTypeAgentPath,
                prmModifiedBy, prmModifiedDatetime, prmScheduleDetailsID);
            return x;

        }

        public int sp_Delete_JobScheduleDetails_CDMS(decimal ScheduleDetailsID)
        {
            SqlParameter prmScheduleDetailsID = new SqlParameter("@ScheduleDetailsID", SqlDbType.Decimal);
            prmScheduleDetailsID.Value = ScheduleDetailsID;
            int x = DataAccess.Execute("sp_Delete_JobScheduleDetails_CDMS", prmScheduleDetailsID);
            return x;
        }

        #endregion


        #region JobScheduleMaster

        public int sp_Insert_JobScheduleMaster_CDMS(string ScheduleName, decimal SSTActivityListID, DateTime ScheduleDatetime, decimal JobTypeID, string ScheduleType, string RepeatType, string ScheduleRemarks,
           decimal CreatedBy, DateTime CreatedDatetime, decimal ModifiedBy, DateTime ModifiedDatetime)
        {
            SqlParameter prmScheduleName = new SqlParameter("@ScheduleName", SqlDbType.VarChar, 50);
            prmScheduleName.Value = ScheduleName;
            SqlParameter prmSSTActivityListID = new SqlParameter("@SSTActivityListID", SqlDbType.Decimal);
            prmSSTActivityListID.Value = SSTActivityListID;
            SqlParameter prmScheduleDatetime = new SqlParameter("@ScheduleDatetime", SqlDbType.SmallDateTime);
            prmScheduleDatetime.Value = ScheduleDatetime;
            SqlParameter prmJobTypeID = new SqlParameter("@JobTypeID", SqlDbType.Decimal);
            prmJobTypeID.Value = JobTypeID;
            SqlParameter prmScheduleType = new SqlParameter("@ScheduleType", SqlDbType.VarChar, 50);
            prmScheduleType.Value = ScheduleType;
            SqlParameter prmRepeatType = new SqlParameter("@RepeatType", SqlDbType.VarChar, 50);
            prmRepeatType.Value = RepeatType;
            SqlParameter prmScheduleRemarks = new SqlParameter("@ScheduleRemarks", SqlDbType.VarChar, 50);
            prmScheduleRemarks.Value = ScheduleRemarks;

            SqlParameter prmCreatedBy = new SqlParameter("@CreatedBy", SqlDbType.Decimal);
            prmCreatedBy.Value = CreatedBy;
            SqlParameter prmCreatedDatetime = new SqlParameter("@CreatedDatetime", SqlDbType.SmallDateTime);
            prmCreatedDatetime.Value = CreatedDatetime;
            SqlParameter prmModifiedBy = new SqlParameter("@ModifiedBy", SqlDbType.Decimal);
            prmModifiedBy.Value = ModifiedBy;
            SqlParameter prmModifiedDatetime = new SqlParameter("@ModifiedDatetime", SqlDbType.SmallDateTime);
            prmModifiedDatetime.Value = ModifiedDatetime;
            int x = DataAccess.Execute("sp_Insert_JobScheduleMaster_CDMS", prmScheduleName, prmSSTActivityListID, prmScheduleDatetime, prmJobTypeID, prmScheduleType, prmRepeatType, prmScheduleRemarks, prmCreatedBy, prmCreatedDatetime, prmModifiedBy, prmModifiedDatetime);
            return x;

        }

        public int sp_Modify_JobScheduleMaster_CDMS(string ScheduleName, decimal SSTActivityListID, DateTime ScheduleDatetime, decimal JobTypeID, string ScheduleType, string RepeatType, string ScheduleRemarks,
           decimal ModifiedBy, DateTime ModifiedDatetime, decimal ScheduleID)
        {
            SqlParameter prmScheduleName = new SqlParameter("@ScheduleName", SqlDbType.VarChar, 50);
            prmScheduleName.Value = ScheduleName;
            SqlParameter prmSSTActivityListID = new SqlParameter("@SSTActivityListID", SqlDbType.Decimal);
            prmSSTActivityListID.Value = SSTActivityListID;
            SqlParameter prmScheduleDatetime = new SqlParameter("@ScheduleDatetime", SqlDbType.SmallDateTime);
            prmScheduleDatetime.Value = ScheduleDatetime;
            SqlParameter prmJobTypeID = new SqlParameter("@JobTypeID", SqlDbType.Decimal);
            prmJobTypeID.Value = JobTypeID;
            SqlParameter prmScheduleType = new SqlParameter("@ScheduleType", SqlDbType.VarChar, 50);
            prmScheduleType.Value = ScheduleType;
            SqlParameter prmRepeatType = new SqlParameter("@RepeatType", SqlDbType.VarChar, 50);
            prmRepeatType.Value = RepeatType;
            SqlParameter prmScheduleRemarks = new SqlParameter("@ScheduleRemarks", SqlDbType.VarChar, 50);
            prmScheduleRemarks.Value = ScheduleRemarks;
            SqlParameter prmModifiedBy = new SqlParameter("@ModifiedBy", SqlDbType.Decimal);
            prmModifiedBy.Value = ModifiedBy;
            SqlParameter prmModifiedDatetime = new SqlParameter("@ModifiedDatetime", SqlDbType.SmallDateTime);
            prmModifiedDatetime.Value = ModifiedDatetime;
            SqlParameter prmScheduleID = new SqlParameter("@ScheduleID", SqlDbType.Decimal);
            prmScheduleID.Value = (ScheduleID);

            int x = DataAccess.Execute("sp_Modify_JobScheduleMaster_CDMS", prmScheduleName, prmSSTActivityListID, prmScheduleDatetime, prmJobTypeID, prmScheduleType, prmRepeatType, prmScheduleRemarks, prmModifiedBy, prmModifiedDatetime, prmScheduleID);
            return x;

        }

        public int sp_Delete_JobScheduleMaster_CDMS(decimal ScheduleID)
        {
            SqlParameter prmScheduleID = new SqlParameter("@ScheduleID", SqlDbType.Decimal);
            prmScheduleID.Value = ScheduleID;
            int x = DataAccess.Execute("sp_Delete_JobScheduleMaster_CDMS", prmScheduleID);
            return x;
        }

        #endregion


        #region JobTypeMaster
        public int sp_Insert_JobTypeMaster_CDMS(string JobType, string JobTypeDescription, decimal CreatedBy, DateTime CreatedDatetime, decimal ModifiedBy, DateTime ModifiedDatetime)
        {
            SqlParameter prmJobType = new SqlParameter("@JobType", SqlDbType.VarChar, 50);
            prmJobType.Value = JobType;
            SqlParameter prmJobTypeDescription = new SqlParameter("@JobTypeDescription", SqlDbType.VarChar, 150);
            prmJobTypeDescription.Value = JobTypeDescription;

            SqlParameter prmCreatedBy = new SqlParameter("@CreatedBy", SqlDbType.Decimal);
            prmCreatedBy.Value = CreatedBy;
            SqlParameter prmCreatedDatetime = new SqlParameter("@CreatedDatetime", SqlDbType.SmallDateTime);
            prmCreatedDatetime.Value = CreatedDatetime;
            SqlParameter prmModifiedBy = new SqlParameter("@ModifiedBy", SqlDbType.Decimal);
            prmModifiedBy.Value = ModifiedBy;
            SqlParameter prmModifiedDatetime = new SqlParameter("@ModifiedDatetime", SqlDbType.SmallDateTime);
            prmModifiedDatetime.Value = ModifiedDatetime;
            int x = DataAccess.Execute("sp_Insert_JobTypeMaster_CDMS", prmJobType, prmJobTypeDescription, prmCreatedBy, prmCreatedDatetime, prmModifiedBy, prmModifiedDatetime);
            return x;

        }

        public int sp_Modify_JobTypeMaster_CDMS(string JobType, string JobTypeDescription, decimal ModifiedBy, DateTime ModifiedDatetime, decimal JobTypeID)
        {
            SqlParameter prmJobType = new SqlParameter("@JobType", SqlDbType.VarChar, 50);
            prmJobType.Value = JobType;
            SqlParameter prmJobTypeDescription = new SqlParameter("@JobTypeDescription", SqlDbType.VarChar, 150);
            prmJobTypeDescription.Value = JobTypeDescription;
            SqlParameter prmModifiedBy = new SqlParameter("@ModifiedBy", SqlDbType.Decimal);
            prmModifiedBy.Value = ModifiedBy;
            SqlParameter prmModifiedDatetime = new SqlParameter("@ModifiedDatetime", SqlDbType.SmallDateTime);
            prmModifiedDatetime.Value = ModifiedDatetime;
            SqlParameter prmJobTypeID = new SqlParameter("@JobTypeID", SqlDbType.Decimal);
            prmJobTypeID.Value = (JobTypeID);

            int x = DataAccess.Execute("sp_Modify_JobTypeMaster_CDMS", prmJobType, prmJobTypeDescription, prmModifiedBy, prmModifiedDatetime, prmJobTypeID);
            return x;

        }

        public int sp_Delete_JobTypeMaster_CDMS(decimal JobTypeID)
        {
            SqlParameter prmJobTypeID = new SqlParameter("@JobTypeID", SqlDbType.Decimal);
            prmJobTypeID.Value = JobTypeID;
            int x = DataAccess.Execute("sp_Delete_JobTypeMaster_CDMS", prmJobTypeID);
            return x;
        }


        #endregion


        #region SSTActivityGroupList

        public int sp_Insert_SSTActivityGroupList_CDMS(string ActivityGroupName, string ActivityGroupDescription, decimal CreatedBy, DateTime CreatedDatetime, decimal ModifiedBy, DateTime ModifiedDatetime)
        {
            SqlParameter prmActivityGroupName = new SqlParameter("@ActivityGroupName", SqlDbType.VarChar, 50);
            prmActivityGroupName.Value = ActivityGroupName;
            SqlParameter prmActivityGroupDescription = new SqlParameter("@ActivityGroupDescription", SqlDbType.VarChar, 150);
            prmActivityGroupDescription.Value = ActivityGroupDescription;

            SqlParameter prmCreatedBy = new SqlParameter("@CreatedBy", SqlDbType.Decimal);
            prmCreatedBy.Value = CreatedBy;
            SqlParameter prmCreatedDatetime = new SqlParameter("@CreatedDatetime", SqlDbType.SmallDateTime);
            prmCreatedDatetime.Value = CreatedDatetime;
            SqlParameter prmModifiedBy = new SqlParameter("@ModifiedBy", SqlDbType.Decimal);
            prmModifiedBy.Value = ModifiedBy;
            SqlParameter prmModifiedDatetime = new SqlParameter("@ModifiedDatetime", SqlDbType.SmallDateTime);
            prmModifiedDatetime.Value = ModifiedDatetime;
            int x = DataAccess.Execute("sp_Insert_SSTActivityGroupList_CDMS", prmActivityGroupName, prmActivityGroupDescription, prmCreatedBy, prmCreatedDatetime, prmModifiedBy, prmModifiedDatetime);
            return x;

        }

        public int sp_Modify_SSTActivityGroupList_CDMS(string ActivityGroupName, string ActivityGroupDescription, decimal ModifiedBy, DateTime ModifiedDatetime, decimal SSTActivityListID)
        {
            SqlParameter prmActivityGroupName = new SqlParameter("@ActivityGroupName", SqlDbType.VarChar, 50);
            prmActivityGroupName.Value = ActivityGroupName;
            SqlParameter prmActivityGroupDescription = new SqlParameter("@ActivityGroupDescription", SqlDbType.VarChar, 150);
            prmActivityGroupDescription.Value = ActivityGroupDescription;
            SqlParameter prmModifiedBy = new SqlParameter("@ModifiedBy", SqlDbType.Decimal);
            prmModifiedBy.Value = ModifiedBy;
            SqlParameter prmModifiedDatetime = new SqlParameter("@ModifiedDatetime", SqlDbType.SmallDateTime);
            prmModifiedDatetime.Value = ModifiedDatetime;
            SqlParameter prmSSTActivityListID = new SqlParameter("@SSTActivityListID", SqlDbType.Decimal);
            prmSSTActivityListID.Value = (SSTActivityListID);

            int x = DataAccess.Execute("sp_Modify_SSTActivityGroupList_CDMS", prmActivityGroupName, prmActivityGroupDescription, prmModifiedBy, prmModifiedDatetime, prmSSTActivityListID);
            return x;

        }

        public int sp_Delete_SSTActivityGroupList_CDMS(decimal SSTActivityListID)
        {
            SqlParameter prmSSTActivityListID = new SqlParameter("@SSTActivityListID", SqlDbType.Decimal);
            prmSSTActivityListID.Value = SSTActivityListID;
            int x = DataAccess.Execute("sp_Delete_SSTActivityGroupList_CDMS", prmSSTActivityListID);
            return x;
        }

        #endregion


        #region SSTActivityGroupListDetails

        public int sp_Insert_SSTActivityGroupListDetails_CDMS(decimal SSTActivityListID, decimal SSTBankID, decimal SSTRegionID, decimal SSTBranchID, decimal SSTGroupID, decimal SSTTypeID,
          decimal SSTLinkTypeID, decimal SSTLinkServiceProvideID, decimal SSTID, decimal CreatedBy, DateTime CreatedDatetime, decimal ModifiedBy, DateTime ModifiedDatetime)
        {
            SqlParameter prmSSTActivityListID = new SqlParameter("@SSTActivityListID", SqlDbType.Decimal);
            prmSSTActivityListID.Value = SSTActivityListID;
            SqlParameter prmSSTBankID = new SqlParameter("@SSTBankID", SqlDbType.Decimal);
            prmSSTBankID.Value = SSTBankID;
            SqlParameter prmSSTRegionID = new SqlParameter("@SSTRegionID", SqlDbType.Decimal);
            prmSSTRegionID.Value = SSTRegionID;
            SqlParameter prmSSTBranchID = new SqlParameter("@SSTBranchID", SqlDbType.Decimal);
            prmSSTBranchID.Value = SSTBranchID;
            SqlParameter prmSSTGroupID = new SqlParameter("@SSTGroupID", SqlDbType.Decimal);
            prmSSTGroupID.Value = SSTGroupID;
            SqlParameter prmSSTTypeID = new SqlParameter("@SSTTypeID", SqlDbType.Decimal);
            prmSSTTypeID.Value = SSTTypeID;
            SqlParameter prmSSTLinkTypeID = new SqlParameter("@SSTLinkTypeID", SqlDbType.Decimal);
            prmSSTLinkTypeID.Value = SSTLinkTypeID;
            SqlParameter prmSSTLinkServiceProvideID = new SqlParameter("@SSTLinkServiceProvideID", SqlDbType.Decimal);
            prmSSTLinkServiceProvideID.Value = SSTLinkServiceProvideID;
            SqlParameter prmSSTID = new SqlParameter("@SSTID", SqlDbType.Decimal);
            prmSSTID.Value = SSTID;
            SqlParameter prmCreatedBy = new SqlParameter("@CreatedBy", SqlDbType.Decimal);
            prmCreatedBy.Value = CreatedBy;
            SqlParameter prmCreatedDatetime = new SqlParameter("@CreatedDatetime", SqlDbType.SmallDateTime);
            prmCreatedDatetime.Value = CreatedDatetime;
            SqlParameter prmModifiedBy = new SqlParameter("@ModifiedBy", SqlDbType.Decimal);
            prmModifiedBy.Value = ModifiedBy;
            SqlParameter prmModifiedDatetime = new SqlParameter("@ModifiedDatetime", SqlDbType.SmallDateTime);
            prmModifiedDatetime.Value = ModifiedDatetime;
            int x = DataAccess.Execute("sp_Insert_SSTActivityGroupListDetails_CDMS", prmSSTActivityListID, prmSSTBankID, prmSSTRegionID, prmSSTBranchID, prmSSTGroupID, prmSSTTypeID, prmSSTLinkTypeID, prmSSTLinkServiceProvideID, prmSSTID, prmCreatedBy, prmCreatedDatetime, prmModifiedBy, prmModifiedDatetime);
            return x;

        }

        public int sp_Modify_SSTActivityGroupListDetails_CDMS(decimal SSTActivityListID, decimal SSTBankID, decimal SSTRegionID, decimal SSTBranchID, decimal SSTGroupID, decimal SSTTypeID,
          decimal SSTLinkTypeID, decimal SSTLinkServiceProvideID, decimal SSTID, decimal ModifiedBy, DateTime ModifiedDatetime, decimal ActivityListDetailID)
        {
            SqlParameter prmSSTActivityListID = new SqlParameter("@SSTActivityListID", SqlDbType.Decimal);
            prmSSTActivityListID.Value = SSTActivityListID;
            SqlParameter prmSSTBankID = new SqlParameter("@SSTBankID", SqlDbType.Decimal);
            prmSSTBankID.Value = SSTBankID;
            SqlParameter prmSSTRegionID = new SqlParameter("@SSTRegionID", SqlDbType.Decimal);
            prmSSTRegionID.Value = SSTRegionID;
            SqlParameter prmSSTBranchID = new SqlParameter("@SSTBranchID", SqlDbType.Decimal);
            prmSSTBranchID.Value = SSTBranchID;
            SqlParameter prmSSTGroupID = new SqlParameter("@SSTGroupID", SqlDbType.Decimal);
            prmSSTGroupID.Value = SSTGroupID;
            SqlParameter prmSSTTypeID = new SqlParameter("@SSTTypeID", SqlDbType.Decimal);
            prmSSTTypeID.Value = SSTTypeID;
            SqlParameter prmSSTLinkTypeID = new SqlParameter("@SSTLinkTypeID", SqlDbType.Decimal);
            prmSSTLinkTypeID.Value = SSTLinkTypeID;
            SqlParameter prmSSTLinkServiceProvideID = new SqlParameter("@SSTLinkServiceProvideID", SqlDbType.Decimal);
            prmSSTLinkServiceProvideID.Value = SSTLinkServiceProvideID;
            SqlParameter prmSSTID = new SqlParameter("@SSTID", SqlDbType.Decimal);
            prmSSTID.Value = SSTID;
            SqlParameter prmModifiedBy = new SqlParameter("@ModifiedBy", SqlDbType.Decimal);
            prmModifiedBy.Value = ModifiedBy;
            SqlParameter prmModifiedDatetime = new SqlParameter("@ModifiedDatetime", SqlDbType.SmallDateTime);
            prmModifiedDatetime.Value = ModifiedDatetime;
            SqlParameter prmActivityListDetailID = new SqlParameter("@ActivityListDetailID", SqlDbType.Decimal);
            prmActivityListDetailID.Value = ActivityListDetailID;
            int x = DataAccess.Execute("sp_Modify_SSTActivityGroupListDetails_CDMS", prmSSTActivityListID, prmSSTBankID, prmSSTRegionID, prmSSTBranchID, prmSSTGroupID, prmSSTTypeID, prmSSTLinkTypeID,
                prmSSTLinkServiceProvideID, prmSSTID, prmModifiedBy, prmModifiedDatetime, prmActivityListDetailID);
            return x;

        }

        public int sp_Delete_SSTActivityGroupListDetails_CDMS(decimal ActivityListDetailID)
        {
            SqlParameter prmActivityListDetailID = new SqlParameter("@ActivityListDetailID", SqlDbType.Decimal);
            prmActivityListDetailID.Value = ActivityListDetailID;
            int x = DataAccess.Execute("sp_Delete_SSTActivityGroupListDetails_CDMS", prmActivityListDetailID);
            return x;
        }

        #endregion


        #region SSTGroupMappingDetails

        public int sp_Insert_SSTGroupMappingDetails_CDMS(decimal SSTGroupID, decimal SSTID, decimal CreatedBy, DateTime CreatedDatetime, decimal ModifiedBy, DateTime ModifiedDatetime)
        {
            SqlParameter prmSSTGroupID = new SqlParameter("@SSTGroupID", SqlDbType.Decimal);
            prmSSTGroupID.Value = SSTGroupID;
            SqlParameter prmSSTID = new SqlParameter("@SSTID", SqlDbType.Decimal);
            prmSSTID.Value = SSTID;

            SqlParameter prmCreatedBy = new SqlParameter("@CreatedBy", SqlDbType.Decimal);
            prmCreatedBy.Value = CreatedBy;
            SqlParameter prmCreatedDatetime = new SqlParameter("@CreatedDatetime", SqlDbType.SmallDateTime);
            prmCreatedDatetime.Value = CreatedDatetime;
            SqlParameter prmModifiedBy = new SqlParameter("@ModifiedBy", SqlDbType.Decimal);
            prmModifiedBy.Value = ModifiedBy;
            SqlParameter prmModifiedDatetime = new SqlParameter("@ModifiedDatetime", SqlDbType.SmallDateTime);
            prmModifiedDatetime.Value = ModifiedDatetime;
            int x = DataAccess.Execute("sp_Insert_SSTGroupMappingDetails_CDMS", prmSSTGroupID, prmSSTID, prmCreatedBy, prmCreatedDatetime, prmModifiedBy, prmModifiedDatetime);
            return x;

        }

        public int sp_Modify_SSTGroupMappingDetails_CDMS(decimal SSTGroupID, decimal SSTID, decimal ModifiedBy, DateTime ModifiedDatetime, decimal SSTGroupDetailsID)
        {
            SqlParameter prmSSTGroupID = new SqlParameter("@SSTGroupID", SqlDbType.Decimal);
            prmSSTGroupID.Value = SSTGroupID;
            SqlParameter prmSSTID = new SqlParameter("@SSTID", SqlDbType.Decimal);
            prmSSTID.Value = SSTID;
            SqlParameter prmModifiedBy = new SqlParameter("@ModifiedBy", SqlDbType.Decimal);
            prmModifiedBy.Value = ModifiedBy;
            SqlParameter prmModifiedDatetime = new SqlParameter("@ModifiedDatetime", SqlDbType.SmallDateTime);
            prmModifiedDatetime.Value = ModifiedDatetime;
            SqlParameter prmSSTGroupDetailsID = new SqlParameter("@SSTGroupDetailsID", SqlDbType.Decimal);
            prmSSTGroupDetailsID.Value = SSTGroupDetailsID;

            int x = DataAccess.Execute("sp_Modify_SSTGroupMappingDetails_CDMS", prmSSTGroupID, prmSSTID, prmModifiedBy, prmModifiedDatetime, prmSSTGroupDetailsID);
            return x;

        }

        public int sp_Delete_SSTGroupMappingDetails_CDMS(decimal SSTGroupDetailsID)
        {
            SqlParameter prmSSTGroupDetailsID = new SqlParameter("@SSTGroupDetailsID", SqlDbType.Decimal);
            prmSSTGroupDetailsID.Value = SSTGroupDetailsID;
            int x = DataAccess.Execute("sp_Delete_SSTGroupMappingDetails_CDMS", prmSSTGroupDetailsID);
            return x;
        }


        #endregion


        #endregion
    }
}
