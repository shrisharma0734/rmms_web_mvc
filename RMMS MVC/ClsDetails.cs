﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Text;
using System.IO;

/// <summary>
/// Summary description for ClsDetails
/// </summary>
public static class ClsDetails
{

    static string TRANS_LOG_PATH = @"D:/PUSH_LOGS";

    #region Transaction Log

    public static void WriteToTransactionLog(StringBuilder strLog)
    {
        StringBuilder swTemp;
        DateTime dtToday = DateTime.Now.Date;
        if (!System.IO.Directory.Exists(TRANS_LOG_PATH))
        {
            System.IO.Directory.CreateDirectory(TRANS_LOG_PATH);
        }
        StreamWriter swGeneralLog = new StreamWriter(TRANS_LOG_PATH + @"\PUSH_LOG_" + dtToday.ToString("dd-MM-yyyy") + ".log", true);
        try
        {
            if (!System.IO.File.Exists(TRANS_LOG_PATH + @"\PUSH_LOG_" + dtToday.ToString("dd-MM-yyyy") + ".log"))
            {
                swGeneralLog = new StreamWriter(TRANS_LOG_PATH + @"\PUSH_LOG_" + dtToday.ToString("dd-MM-yyyy") + ".log", true);
            }
            swGeneralLog.Write(strLog);
            swGeneralLog.Close();

        }
        catch (Exception exc)
        {

            swTemp = new StringBuilder();
            string strBl = "Error in write_to_transaction_log : " + exc.Message + " Stack Trace : " + exc.StackTrace;
            swGeneralLog.AutoFlush = true;
            swTemp.Append("\r\n\r\n" + DateTime.Now.ToString() + " " + strBl);
            //swTemp.Append("\r\n\r\n" + DateTime.Now.ToString() + " millisecond:" + DateTime.Now.Millisecond.ToString() + "       " + paramValue);
            swGeneralLog.Write(swTemp);
            swGeneralLog.Close();
        }
        finally
        {
        }
    }

    public static void writeStringTotransactionLog(string message)
    {
        try
        {
            StringBuilder sb = new StringBuilder();
            sb.AppendLine(DateTime.Now.ToString() + " : " + message);
            WriteToTransactionLog(sb);
        }
        catch (Exception Ex)
        {
            //Application.Exit();
        }
    }

    #endregion
}