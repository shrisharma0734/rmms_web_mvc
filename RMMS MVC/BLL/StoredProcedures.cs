﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Configuration;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Data.SqlClient;
using FORBES.DAE;
using System.Data.SqlTypes;
using RMMS_MVC.MVCActionStoreProcedure;

namespace RMMS_MVC.BLL
{
    public class BLogic
    {
        public BLogic() { }
        public DataTable GetSSTProfileBranchDrd(string RegionlistId)
        {
            SqlParameter prmRegionListId = new SqlParameter("@RegionListId", SqlDbType.VarChar, 500);
            prmRegionListId.Value = RegionlistId;
            return DataAccess.GetFromDataTable("Sp_ReportSSTProfilefillBranch ", prmRegionListId);
        }

        #region Login Page

        public DataTable GetLoginUser(string UserName, string Password)
        {
            SqlParameter prmUserName = new SqlParameter("@UserLoginName", SqlDbType.VarChar, 50);
            prmUserName.Value = UserName;
            SqlParameter prmPassword = new SqlParameter("@UserLoginPassword", SqlDbType.VarChar, 20);
            prmPassword.Value = Password;
            return DataAccess.GetFromDataTable("GetLoginUser", prmUserName, prmPassword);
        }
        #endregion
        #region Home Page
        public DataTable SP_RetrieveDefaultPageName(decimal UserID)
        {
            SqlParameter prmUserID = new SqlParameter("@UserID", SqlDbType.Decimal);
            prmUserID.Value = UserID;
            return DataAccess.GetFromDataTable("SP_RetrieveDefaultPageName", prmUserID);
        }
        public DataTable sp_GetNodes1(decimal RoleID, decimal UserID)
        {
            SqlParameter prmLogInRoleID = new SqlParameter("@RoleID", SqlDbType.Decimal);
            prmLogInRoleID.Value = RoleID;
            SqlParameter prmUserID = new SqlParameter("@UserID", SqlDbType.Decimal);
            prmUserID.Value = UserID;

            return DataAccess.GetFromDataTable("sp_GetNodes1", prmLogInRoleID, prmUserID);
        }
        public DataTable sp_GetNodes1_2(decimal RoleID, decimal UserID, decimal parentID)//Retrieves all the node names from the Menu Table based on role.
        {
            SqlParameter prmLogInRoleID = new SqlParameter("@RoleID", SqlDbType.Decimal);
            prmLogInRoleID.Value = RoleID;
            SqlParameter prmUserID = new SqlParameter("@UserID", SqlDbType.Decimal);
            prmUserID.Value = UserID;
            SqlParameter prmparentID = new SqlParameter("@parentID", SqlDbType.Decimal);
            prmparentID.Value = parentID;

            return DataAccess.GetFromDataTable("sp_GetNodes1_2", prmLogInRoleID, prmUserID, prmparentID);
        }
        # endregion
        #region UserRole
        public DataTable sp_EditRoleMaster()
        {
            return DataAccess.GetFromDataTable("sp_EditRoleMaster");
        }
        public DataTable SP_RoleCount(decimal RoleID)
        {
            SqlParameter prmRoleID = new SqlParameter("@RoleID", SqlDbType.Decimal);
            prmRoleID.Value = RoleID;
            return DataAccess.GetFromDataTable("SP_RoleCount", prmRoleID);
        }
        public int sp_DeleteUserRole(decimal RoleID)
        {
            SqlParameter prmRoleID = new SqlParameter("@RoleID", SqlDbType.Decimal);
            prmRoleID.Value = RoleID;
            int x = DataAccess.Execute("sp_DeleteUserRole", prmRoleID);
            return x;
        }
        public DataTable SP_FillUpdateUserRole(decimal RoleID)
        {
            SqlParameter prmRoleID = new SqlParameter("@RoleID", SqlDbType.Decimal);
            prmRoleID.Value = RoleID;
            return DataAccess.GetFromDataTable("SP_FillUpdateUserRole", prmRoleID);
        }
        public int SP_InsertRoleMaster(string RoleName, string RoleDescription, decimal CreatedBy, decimal ModifiedBy)
        {
            SqlParameter prmRoleName = new SqlParameter("@RoleName", SqlDbType.VarChar, 50);
            prmRoleName.Value = RoleName;
            SqlParameter prmRoleDescription = new SqlParameter("@RoleDescription", SqlDbType.VarChar, 150);
            prmRoleDescription.Value = RoleDescription;
            SqlParameter prmCreatedBy = new SqlParameter("@CreatedBy", SqlDbType.Decimal);
            prmCreatedBy.Value = CreatedBy;
            SqlParameter prmModifiedBy = new SqlParameter("@ModifiedBy", SqlDbType.Decimal);
            prmModifiedBy.Value = ModifiedBy;
            int x = DataAccess.Execute("SP_InsertRoleMaster", prmRoleName, prmRoleDescription, prmCreatedBy, prmModifiedBy);
            return x;
        }
        public int SP_UpdateUserRole(string RoleName, string RoleDescription, decimal ModifiedBy, decimal RoleID)
        {
            SqlParameter prmRoleName = new SqlParameter("@RoleName", SqlDbType.VarChar, 50);
            prmRoleName.Value = RoleName;
            SqlParameter prmRoleDescription = new SqlParameter("@RoleDescription ", SqlDbType.VarChar, 150);
            prmRoleDescription.Value = RoleDescription;
            SqlParameter prmModifiedBy = new SqlParameter("@ModifiedBy", SqlDbType.Decimal);
            prmModifiedBy.Value = ModifiedBy;
            SqlParameter prmRoleID = new SqlParameter("@RoleID", SqlDbType.Decimal);
            prmRoleID.Value = (RoleID);
            int x = DataAccess.Execute("SP_UpdateUserRole", prmRoleName, prmRoleDescription, prmModifiedBy, prmRoleID);
            return x;
        }
        #endregion
        #region Password Change
        public DataTable sp_GetUserOldPwd(decimal UserID, string useroldPwd) // Get the User Old current passowrd .
        {
            SqlParameter prmUserID = new SqlParameter("@UserID", SqlDbType.Decimal);
            prmUserID.Value = UserID;

            SqlParameter prmuserPwd = new SqlParameter("@userOldPwd", SqlDbType.VarChar);
            prmuserPwd.Value = useroldPwd;
            return DataAccess.GetFromDataTable("sp_GetUserOldPwdHistory", prmUserID, prmuserPwd);
        }
        public DataTable Sp_GetPswChangeddaycount(string UserExpiryDay)
        {
            SqlParameter prmUserExpday = new SqlParameter("@UserLoginName", SqlDbType.VarChar, 20);
            prmUserExpday.Value = UserExpiryDay;
            return DataAccess.GetFromDataTable("Sp_UserExpiredayCount", prmUserExpday);

        }
        public DataTable sp_GetUserdetails(decimal UserID) // Select the User information from UserMaster to fill the User Update page.
        {
            SqlParameter prmUserID = new SqlParameter("@UserID", SqlDbType.Decimal);
            prmUserID.Value = UserID;
            return DataAccess.GetFromDataTable("sp_GetUserdetails", prmUserID);
        }
        public int insertpwd(string Userpwd, decimal userID) //Insert the password on Passowrd History Table 
        {
            SqlParameter prmUserpwd = new SqlParameter("@Userpwd", SqlDbType.VarChar);
            prmUserpwd.Value = Userpwd;
            SqlParameter prmUserID = new SqlParameter("@UserID", SqlDbType.Decimal);
            prmUserID.Value = userID;

            int x = DataAccess.Execute("sp_Insert_UserPwdHistory_CDMS", prmUserpwd, prmUserID);
            return x;
        }
        #endregion

        #region EditUserMaster
        public DataTable sp_EditUser()
        {
            return DataAccess.GetFromDataTable("sp_EditUser");

        }
        public DataTable SP_Fill_Role()
        {
            return DataAccess.GetFromDataTable("SP_Fill_Role");

        }
        public DataTable sp_UserAccess(decimal UserID) // Selecting UserID from UserAccess Table to check the avialabilty of a User, for deleting that User.
        {
            SqlParameter prmUserID = new SqlParameter("@UserID", SqlDbType.Decimal);
            prmUserID.Value = UserID;
            return DataAccess.GetFromDataTable("sp_UserAccess", prmUserID);
        }
        public int sp_DeleteUser(decimal UserID) // Deleting the User 
        {
            SqlParameter prmUserID = new SqlParameter("@UserID", SqlDbType.Decimal);
            prmUserID.Value = UserID;
            int x = DataAccess.Execute("sp_DeleteUser", prmUserID);
            return x;
        }
        #endregion
        #region UserMaster
        public DataTable SP_UserRole() // Selects all the roles from Role table and fills it in the drop down list in the User page.
        {
            return DataAccess.GetFromDataTable("SP_UserRole");
        }
        public DataTable SP_DefaultPage() // Selects all the roles from Role table and fills it in the drop down list in the User page.
        {
            return DataAccess.GetFromDataTable("SP_DefaultPage");
        }
        //Marshal
        public DataTable SP_Region() // Selects all the roles from Role table and fills it in the drop down list in the User page.
        {
            return DataAccess.GetFromDataTable("SP_Region");
        }
        //Marshal
        public DataTable SP_Branch(int RegionId) // Selects all the roles from Role table and fills it in the drop down list in the User page.
        {
            SqlParameter prmRegionId = new SqlParameter("@RegionId", SqlDbType.Int);
            prmRegionId.Value = RegionId;
            return DataAccess.GetFromDataTable("SP_Branch", prmRegionId);
        }
        public DataTable SP_FillUpdatePage(decimal UserID) // Select the User information from UserMaster to fill the User Update page.
        {
            SqlParameter prmUserID = new SqlParameter("@UserID", SqlDbType.Decimal);
            prmUserID.Value = UserID;
            return DataAccess.GetFromDataTable("SP_FillUpdatePage", prmUserID);
        }
        public DataTable Sp_CheckLoginName(string UserLoginName) // 
        {
            SqlParameter prmUserLoginName = new SqlParameter("@UserLoginName", SqlDbType.VarChar, 50);
            prmUserLoginName.Value = UserLoginName;
            return DataAccess.GetFromDataTable("Sp_CheckLoginName", prmUserLoginName);
        }
        public int SP_InsertUser(decimal RoleID, string UserLoginName, string UserFullName, string UserLoginPassword, string UserEmailID, string UserContactNo, string DefaultPage, bool UserStatus, decimal CreatedBy, DateTime CreatedDatetime, decimal ModifiedBy, DateTime ModifiedDatetime, int RegionID, string BranchId) // Inserting the new User
        {
            SqlParameter prmRoleID = new SqlParameter("@RoleID", SqlDbType.Decimal);
            prmRoleID.Value = (RoleID);
            SqlParameter prmUserLoginName = new SqlParameter("@UserLoginName", SqlDbType.VarChar, 50);
            prmUserLoginName.Value = UserLoginName;
            SqlParameter prmUserFullName = new SqlParameter("@UserFullName", SqlDbType.VarChar, 50);
            prmUserFullName.Value = UserFullName;
            SqlParameter prmUserLoginPassword = new SqlParameter("@UserLoginPassword", SqlDbType.VarChar, 20);
            prmUserLoginPassword.Value = UserLoginPassword;
            SqlParameter prmUserEmailID = new SqlParameter("@UserEmailID", SqlDbType.VarChar, 50);
            prmUserEmailID.Value = UserEmailID;
            SqlParameter prmUserContactNo = new SqlParameter("@UserContactNo", SqlDbType.VarChar, 14);
            prmUserContactNo.Value = UserContactNo;
            SqlParameter prmDefaultPage = new SqlParameter("@DefaultPage", SqlDbType.VarChar, 100);
            prmDefaultPage.Value = DefaultPage;

            SqlParameter prmUserStatus = new SqlParameter("@UserStatus", SqlDbType.Bit);
            prmUserStatus.Value = UserStatus;
            SqlParameter prmCreatedBy = new SqlParameter("@CreatedBy", SqlDbType.Decimal);
            prmCreatedBy.Value = CreatedBy;
            SqlParameter prmCreatedDatetime = new SqlParameter("@CreatedDatetime", SqlDbType.SmallDateTime);
            prmCreatedDatetime.Value = CreatedDatetime;
            SqlParameter prmModifiedBy = new SqlParameter("@ModifiedBy", SqlDbType.Decimal);
            prmModifiedBy.Value = ModifiedBy;
            SqlParameter prmModifiedDatetime = new SqlParameter("@ModifiedDatetime", SqlDbType.SmallDateTime);
            prmModifiedDatetime.Value = ModifiedDatetime;
            SqlParameter prmRegionId = new SqlParameter("@RegionId", SqlDbType.Int);
            prmRegionId.Value = RegionID;
            SqlParameter prmBranchId = new SqlParameter("@BranchId", SqlDbType.NVarChar, 300);
            prmBranchId.Value = BranchId;
            int x = DataAccess.Execute("sp_Insert_UserMaster1_CDMS", prmRoleID, prmUserLoginName, prmUserFullName, prmUserLoginPassword, prmUserEmailID, prmUserContactNo, prmDefaultPage, prmUserStatus, prmCreatedBy, prmCreatedDatetime, prmModifiedBy, prmModifiedDatetime, prmRegionId, prmBranchId);

            return x;
        }
        public DataTable sp_CheckUserLoginNameForUpdate(decimal UserID, string UserLoginName) // 
        {
            SqlParameter prmUserID = new SqlParameter("@UserID", SqlDbType.Decimal);
            prmUserID.Value = UserID;
            SqlParameter prmUserLoginName = new SqlParameter("@UserLoginName", SqlDbType.VarChar, 50);
            prmUserLoginName.Value = UserLoginName;
            return DataAccess.GetFromDataTable("sp_CheckUserLoginNameForUpdate", prmUserID, prmUserLoginName);
        }
        public int Sp_UpdateUser(decimal RoleID, string UserLoginName, string UserFullName, string UserLoginPassword, string UserEmailID, string UserContactNo, string DefaultPage, bool UserStatus, decimal ModifiedBy, DateTime ModifiedDatetime, decimal UserID, int RegionID)// Updating the User
        {
            SqlParameter prmRoleID = new SqlParameter("@RoleID", SqlDbType.Decimal);
            prmRoleID.Value = (RoleID);
            SqlParameter prmUserLoginName = new SqlParameter("@UserLoginName", SqlDbType.VarChar, 50);
            prmUserLoginName.Value = UserLoginName;
            SqlParameter prmUserFullName = new SqlParameter("@UserFullName", SqlDbType.VarChar, 50);
            prmUserFullName.Value = UserFullName;
            SqlParameter prmUserLoginPassword = new SqlParameter("@UserLoginPassword", SqlDbType.VarChar, 20);
            prmUserLoginPassword.Value = UserLoginPassword;
            SqlParameter prmUserEmailID = new SqlParameter("@UserEmailID", SqlDbType.VarChar, 50);
            prmUserEmailID.Value = UserEmailID;
            SqlParameter prmUserContactNo = new SqlParameter("@UserContactNo", SqlDbType.VarChar, 14);
            prmUserContactNo.Value = UserContactNo;
            SqlParameter prmDefaultPage = new SqlParameter("@DefaultPage", SqlDbType.VarChar, 100);
            prmDefaultPage.Value = DefaultPage;

            SqlParameter prmUserStatus = new SqlParameter("@UserStatus", SqlDbType.Bit);
            prmUserStatus.Value = UserStatus;
            SqlParameter prmModifiedBy = new SqlParameter("@ModifiedBy", SqlDbType.Decimal);
            prmModifiedBy.Value = ModifiedBy;
            SqlParameter prmModifiedDatetime = new SqlParameter("@ModifiedDatetime", SqlDbType.SmallDateTime);
            prmModifiedDatetime.Value = ModifiedDatetime;
            SqlParameter prmUserID = new SqlParameter("@UserID", SqlDbType.Decimal);
            prmUserID.Value = (UserID);
            SqlParameter prmRegionID = new SqlParameter("@RegionID", SqlDbType.Int);
            prmRegionID.Value = RegionID;
            int x = DataAccess.Execute("Sp_UpdateUser", prmRoleID, prmUserLoginName, prmUserFullName, prmUserLoginPassword, prmUserEmailID, prmUserContactNo, prmDefaultPage, prmUserStatus, prmModifiedBy, prmModifiedDatetime, prmUserID, prmRegionID);
            return x;
        }
        #endregion
        #region EditCity
        public DataTable SP_EditCity(string ExecQuary) // Fills the Update page.
        {
            SqlParameter prmExecQuary = new SqlParameter("@ExecQuary", SqlDbType.VarChar, 6000);
            prmExecQuary.Value = ExecQuary;
            return DataAccess.GetFromDataTable("SP_EditCity", prmExecQuary);
        }
        public DataTable SP_GetCityCount()
        {
            return DataAccess.GetFromDataTable("SP_GetCityCount");
        }
        public DataTable SP_GetCityID(decimal CityID)
        {
            SqlParameter prmCityID = new SqlParameter("@CityID", SqlDbType.Decimal);
            prmCityID.Value = CityID;
            return DataAccess.GetFromDataTable("SP_GetCityID", prmCityID);
        }
        public int SP_DeleteCity(decimal CityID)
        {
            SqlParameter prmCityID = new SqlParameter("@CityID", SqlDbType.Decimal);
            prmCityID.Value = CityID;
            int x = DataAccess.Execute("SP_DeleteCity", prmCityID);
            return x;
        }
        #endregion
        #region CityMaster
        public DataTable Sp_FillUpdateCity(decimal CityID) // 
        {
            SqlParameter prmCityID = new SqlParameter("@CityID", SqlDbType.Decimal);
            prmCityID.Value = CityID;
            return DataAccess.GetFromDataTable("SP_FillUpdateCity", prmCityID);
        }

        public DataTable FillZone()  //vaibhav
        {
            return DataAccess.GetFromDataTable("Sp_FillZone");
        }
        public DataTable FillZone(int BranchID)  //vaibhav branch Id wise
        {
            SqlParameter prmBranchID = new SqlParameter("@BranchID", SqlDbType.Decimal);
            prmBranchID.Value = BranchID;
            return DataAccess.GetFromDataTable("Sp_FillZoneBranchIdwise", prmBranchID);
        }

        public int SP_PinCode(string CityName, decimal PinCode)
        {
            SqlParameter prmCityName = new SqlParameter("@city ", SqlDbType.VarChar, 50);
            prmCityName.Value = CityName;
            SqlParameter prmPinCode = new SqlParameter("@PinCode", SqlDbType.Decimal);
            prmPinCode.Value = PinCode;
            int x = DataAccess.GetInt32("SP_PinCode", prmCityName, prmPinCode);
            return x;
        }
        public int Sp_InsertCity(string ZoneName, string CityName, string State, decimal PinCode, decimal CreatedBy, DateTime CreatedDatetime, decimal ModifiedBy, DateTime ModifiedDatetime) // Inserting the new City
        {
            SqlParameter prmZoneName = new SqlParameter("@ZoneName", SqlDbType.VarChar, 50);
            prmZoneName.Value = ZoneName;
            SqlParameter prmCityName = new SqlParameter("@CityName ", SqlDbType.VarChar, 50);
            prmCityName.Value = CityName;
            SqlParameter prmState = new SqlParameter("@State", SqlDbType.VarChar, 50);
            prmState.Value = State;
            SqlParameter prmPinCode = new SqlParameter("@PinCode", SqlDbType.Decimal);
            prmPinCode.Value = (PinCode);
            SqlParameter prmCreatedBy = new SqlParameter("@CreatedBy", SqlDbType.Decimal);
            prmCreatedBy.Value = CreatedBy;
            SqlParameter prmCreatedDatetime = new SqlParameter("@CreatedDatetime", SqlDbType.SmallDateTime);
            prmCreatedDatetime.Value = CreatedDatetime;
            SqlParameter prmModifiedBy = new SqlParameter("@ModifiedBy", SqlDbType.Decimal);
            prmModifiedBy.Value = ModifiedBy;
            SqlParameter prmModifiedDatetime = new SqlParameter("@ModifiedDatetime", SqlDbType.SmallDateTime);
            prmModifiedDatetime.Value = ModifiedDatetime;
            int x = DataAccess.Execute("Sp_InsertCity", prmZoneName, prmCityName, prmState, prmPinCode, prmCreatedBy, prmCreatedDatetime, prmModifiedBy, prmModifiedDatetime);
            return x;
        }
        public int sp_GetProblemCount(string Problem, string ProblemCode)
        {
            SqlParameter prmProblem = new SqlParameter("@Problem ", SqlDbType.VarChar, 500);
            prmProblem.Value = Problem;
            SqlParameter prmProblemCode = new SqlParameter("@ProblemCode", SqlDbType.VarChar);
            prmProblemCode.Value = ProblemCode;

            int x = DataAccess.GetInt32("sp_getProblemMastercount", prmProblem, prmProblemCode);
            return x;
        }
        public int sp_GetCityMasterCount(string CityName, decimal PinCode, decimal CityID)
        {
            SqlParameter prmCityName = new SqlParameter("@CityName ", SqlDbType.VarChar, 50);
            prmCityName.Value = CityName;
            SqlParameter prmPinCode = new SqlParameter("@PinCode", SqlDbType.Decimal);
            prmPinCode.Value = PinCode;
            SqlParameter prmCityID = new SqlParameter("@CityID", SqlDbType.Decimal);
            prmCityID.Value = CityID;
            int x = DataAccess.GetInt32("sp_GetCityMasterCount", prmCityName, prmPinCode, prmCityID);
            return x;
        }
        public int Sp_UpdateCity(string ZoneName, string CityName, string State, decimal PinCode, decimal ModifiedBy, DateTime ModifiedDatetime, decimal CityID)
        {
            SqlParameter prmZoneName = new SqlParameter("@ZoneName", SqlDbType.VarChar, 50);
            prmZoneName.Value = ZoneName;
            SqlParameter prmCityName = new SqlParameter("@CityName", SqlDbType.VarChar, 50);
            prmCityName.Value = CityName;
            SqlParameter prmState = new SqlParameter("@State", SqlDbType.VarChar, 50);
            prmState.Value = State;
            SqlParameter prmPinCode = new SqlParameter("@PinCode", SqlDbType.Decimal);
            prmPinCode.Value = (PinCode);
            SqlParameter prmModifiedBy = new SqlParameter("@ModifiedBy", SqlDbType.Decimal);
            prmModifiedBy.Value = ModifiedBy;
            SqlParameter prmModifiedDatetime = new SqlParameter("@ModifiedDatetime", SqlDbType.SmallDateTime);
            prmModifiedDatetime.Value = ModifiedDatetime;
            SqlParameter prmCityID = new SqlParameter("@CityID", SqlDbType.Decimal);
            prmCityID.Value = (CityID);
            int x = DataAccess.Execute("Sp_UpdateCity", prmZoneName, prmCityName, prmState, prmPinCode, prmModifiedBy, prmModifiedDatetime, prmCityID);
            return x;
        }
        #endregion
        #region EditRegion
        //public DataTable SP_EditRegion()
        //{
        //    return DataAccess.GetFromDataTable("SP_EditRegion");
        //}
        //shrikant
        public DataTable SP_EditRegion(decimal RegionID)
        {
            SqlParameter prmRegionID = new SqlParameter("@RegionID", SqlDbType.Decimal);
            prmRegionID.Value = RegionID;
            return DataAccess.GetFromDataTable("SP_EditRegion", prmRegionID);
        }

        public DataTable SP_GetRegionID(decimal RegionID)
        {
            SqlParameter prmRegionID = new SqlParameter("@RegionID", SqlDbType.Decimal);
            prmRegionID.Value = RegionID;
            return DataAccess.GetFromDataTable("SP_GetRegionID", prmRegionID);
        }
        public int SP_DeleteRegion(decimal RegionID)
        {
            SqlParameter prmRegionID = new SqlParameter("@RegionID", SqlDbType.Decimal);
            prmRegionID.Value = RegionID;
            int x = DataAccess.Execute("SP_DeleteRegion", prmRegionID);
            return x;
        }
        #endregion
        #region BankRegionMaster
        public DataTable SP_FillBankName()
        {
            return DataAccess.GetFromDataTable("SP_FillBankName");
        }
        public DataTable SP_FillUpdateRegion(decimal RegionID)
        {
            SqlParameter prmRegionID = new SqlParameter("@RegionID", SqlDbType.Decimal);
            prmRegionID.Value = RegionID;
            return DataAccess.GetFromDataTable("SP_FillUpdateRegion", prmRegionID);
        }
        public int SP_GetRegionMasterCount(decimal BankID, string RegionName)
        {
            SqlParameter prmBankID = new SqlParameter("@BankID", SqlDbType.Decimal);
            prmBankID.Value = BankID;
            SqlParameter prmRegionName = new SqlParameter("@RegionName", SqlDbType.VarChar, 50);
            prmRegionName.Value = RegionName;
            int x = DataAccess.GetInt32("SP_GetRegionMasterCount", prmBankID, prmRegionName);
            return x;
        }
        public int SP_InsertBankRegionMaster_CDMS(decimal BankID, string RegionName, string RegionDescription, decimal CreatedBy, DateTime CreatedDatetime, decimal ModifiedBy, DateTime ModifiedDatetime)
        {
            SqlParameter prmBankID = new SqlParameter("@BankID", SqlDbType.Decimal);
            prmBankID.Value = (BankID);
            SqlParameter prmRegionName = new SqlParameter("@RegionName", SqlDbType.VarChar, 50);
            prmRegionName.Value = RegionName;
            SqlParameter prmRegionDescription = new SqlParameter("@RegionDescription", SqlDbType.VarChar, 150);
            prmRegionDescription.Value = RegionDescription;
            SqlParameter prmCreatedBy = new SqlParameter("@CreatedBy", SqlDbType.Decimal);
            prmCreatedBy.Value = CreatedBy;
            SqlParameter prmCreatedDatetime = new SqlParameter("@CreatedDatetime", SqlDbType.SmallDateTime);
            prmCreatedDatetime.Value = CreatedDatetime;
            SqlParameter prmModifiedBy = new SqlParameter("@ModifiedBy", SqlDbType.Decimal);
            prmModifiedBy.Value = ModifiedBy;
            SqlParameter prmModifiedDatetime = new SqlParameter("@ModifiedDatetime", SqlDbType.SmallDateTime);
            prmModifiedDatetime.Value = ModifiedDatetime;
            int x = DataAccess.Execute("SP_InsertBankRegionMaster_CDMS", prmBankID, prmRegionName, prmRegionDescription, prmCreatedBy, prmCreatedDatetime, prmModifiedBy, prmModifiedDatetime);
            return x;

        }
        public int SP_UpdateRegion(decimal BankID, string RegionName, string RegionDescription, decimal ModifiedBy, DateTime ModifiedDatetime, decimal RegionID)
        {
            SqlParameter prmBankID = new SqlParameter("@BankID", SqlDbType.Decimal);
            prmBankID.Value = (BankID);
            SqlParameter prmRegionName = new SqlParameter("@RegionName", SqlDbType.VarChar, 50);
            prmRegionName.Value = RegionName;
            SqlParameter prmRegionDescription = new SqlParameter("@RegionDescription", SqlDbType.VarChar, 150);
            prmRegionDescription.Value = RegionDescription;
            SqlParameter prmModifiedBy = new SqlParameter("@ModifiedBy", SqlDbType.Decimal);
            prmModifiedBy.Value = ModifiedBy;
            SqlParameter prmModifiedDatetime = new SqlParameter("@ModifiedDatetime", SqlDbType.SmallDateTime);
            prmModifiedDatetime.Value = ModifiedDatetime;
            SqlParameter prmRegionID = new SqlParameter("@RegionID", SqlDbType.Decimal);
            prmRegionID.Value = (RegionID);
            int x = DataAccess.Execute("SP_UpdateRegion", prmBankID, prmRegionName, prmRegionDescription, prmModifiedBy, prmModifiedDatetime, prmRegionID);
            return x;
        }
        #endregion
        #region EditBank
        public DataTable SP_EditBank()
        {
            return DataAccess.GetFromDataTable("SP_EditBank");
        }
        public DataTable SP_GetBankID(decimal BankID)
        {
            SqlParameter prmBankID = new SqlParameter("@BankID", SqlDbType.Decimal);
            prmBankID.Value = BankID;
            return DataAccess.GetFromDataTable("SP_GetBankID", prmBankID);
        }
        public int SP_DeleteBank(decimal BankID)
        {
            SqlParameter prmBankID = new SqlParameter("@BankID", SqlDbType.Decimal);
            prmBankID.Value = BankID;
            int x = DataAccess.Execute("SP_DeleteBank", prmBankID);
            return x;
        }
        #endregion
        #region BankMaster
        public DataTable SP_FillCityName()
        {
            return DataAccess.GetFromDataTable("SP_FillCityName");
        }
        public DataTable SP_FillUpdateBank(decimal BankID)
        {
            SqlParameter prmBankID = new SqlParameter("@BankID", SqlDbType.Decimal);
            prmBankID.Value = BankID;
            return DataAccess.GetFromDataTable("SP_FillUpdateBank", prmBankID);
        }
        public int SP_GetBankMasterCount(string BankName)
        {
            SqlParameter prmBankName = new SqlParameter("@BankName", SqlDbType.VarChar, 50);
            prmBankName.Value = BankName;
            int x = DataAccess.GetInt32("SP_GetBankMasterCount", prmBankName);
            return x;
        }
        public int SP_Insert_BankMaster_CDMS(string BankName, string BankShotName, string Bank_HO_Address1, string Bank_HO_Address2, decimal CityID, string Bank_ContactName, string Bank_ContactNo1, string Bank_ContactNo2, string Bank_EmailID1, string Bank_EmailID2, string Bank_FTPAddress1, string Bank_FTPAddress2, decimal Bank_FTPPort1, decimal Bank_FTPPort2, string Bank_FTP1UserID, string Bank_FTP2UserID, string Bank_FTP1Password, string Bank_FTP2Password, string Bank_ExtrctdFileLoc, decimal CreatedBy, DateTime CreatedDatetime, decimal ModifiedBy, DateTime ModifiedDatetime)
        {
            SqlParameter prmBankName = new SqlParameter("@BankName", SqlDbType.VarChar, 50);
            prmBankName.Value = BankName;
            SqlParameter prmBankShotName = new SqlParameter("@BankShotName", SqlDbType.VarChar, 50);
            prmBankShotName.Value = BankShotName;
            SqlParameter prmBank_HO_Address1 = new SqlParameter("@Bank_HO_Address1", SqlDbType.VarChar, 300);
            prmBank_HO_Address1.Value = Bank_HO_Address1;
            SqlParameter prmBank_HO_Address2 = new SqlParameter("@Bank_HO_Address2", SqlDbType.VarChar, 300);
            prmBank_HO_Address2.Value = Bank_HO_Address2;
            SqlParameter prmCityID = new SqlParameter("@CityID", SqlDbType.Decimal);
            prmCityID.Value = Convert.ToInt32(CityID);
            SqlParameter prmBank_ContactName = new SqlParameter("@Bank_ContactName", SqlDbType.VarChar, 50);
            prmBank_ContactName.Value = Bank_ContactName;
            SqlParameter prmBank_ContactNo1 = new SqlParameter("@Bank_ContactNo1", SqlDbType.VarChar, 15);
            prmBank_ContactNo1.Value = Bank_ContactNo1;
            SqlParameter prmBank_ContactNo2 = new SqlParameter("@Bank_ContactNo2", SqlDbType.VarChar, 15);
            prmBank_ContactNo2.Value = Bank_ContactNo2;
            SqlParameter prmBank_EmailID1 = new SqlParameter("@Bank_EmailID1", SqlDbType.VarChar, 50);
            prmBank_EmailID1.Value = Bank_EmailID1;
            SqlParameter prmBank_EmailID2 = new SqlParameter("@Bank_EmailID2", SqlDbType.VarChar, 50);
            prmBank_EmailID2.Value = Bank_EmailID2;
            SqlParameter prmBank_FTPAddress1 = new SqlParameter("@Bank_FTPAddress1", SqlDbType.VarChar, 300);
            prmBank_FTPAddress1.Value = Bank_FTPAddress1;
            SqlParameter prmBank_FTPAddress2 = new SqlParameter("@Bank_FTPAddress2", SqlDbType.VarChar, 300);
            prmBank_FTPAddress2.Value = Bank_FTPAddress2;
            SqlParameter prmBank_FTPPort1 = new SqlParameter("@Bank_FTPPort1", SqlDbType.Decimal);
            prmBank_FTPPort1.Value = Convert.ToInt32(Bank_FTPPort1);
            SqlParameter prmBank_FTPPort2 = new SqlParameter("@Bank_FTPPort2", SqlDbType.Decimal);
            prmBank_FTPPort2.Value = Convert.ToInt32(Bank_FTPPort2);
            SqlParameter prmBank_FTP1UserID = new SqlParameter("@Bank_FTP1UserID", SqlDbType.VarChar, 50);
            prmBank_FTP1UserID.Value = Bank_FTP1UserID;
            SqlParameter prmBank_FTP2UserID = new SqlParameter("@Bank_FTP2UserID", SqlDbType.VarChar, 50);
            prmBank_FTP2UserID.Value = Bank_FTP2UserID;
            SqlParameter prmBank_FTP1Password = new SqlParameter("@Bank_FTP1Password", SqlDbType.VarChar, 20);
            prmBank_FTP1Password.Value = Bank_FTP1Password;
            SqlParameter prmBank_FTP2Password = new SqlParameter("@Bank_FTP2Password", SqlDbType.VarChar, 20);
            prmBank_FTP2Password.Value = Bank_FTP2Password;
            SqlParameter prmBank_ExtractedFile = new SqlParameter("@Bank_ExtrctdFileLoc", SqlDbType.VarChar, 300);
            prmBank_ExtractedFile.Value = Bank_ExtrctdFileLoc;
            SqlParameter prmCreatedBy = new SqlParameter("CreatedBy", SqlDbType.Decimal);
            prmCreatedBy.Value = Convert.ToInt32(CreatedBy);
            SqlParameter prmCreatedDatetime = new SqlParameter("@CreatedDatetime", SqlDbType.SmallDateTime);
            prmCreatedDatetime.Value = CreatedDatetime;
            SqlParameter prmModifiedBy = new SqlParameter("@ModifiedBy", SqlDbType.Decimal);
            prmModifiedBy.Value = Convert.ToInt32(ModifiedBy);
            SqlParameter prmModifiedDatetime = new SqlParameter("@ModifiedDatetime", SqlDbType.SmallDateTime);
            prmModifiedDatetime.Value = ModifiedDatetime;
            int x = DataAccess.Execute("SP_Insert_BankMaster_CDMS", prmBankName, prmBankShotName, prmBank_HO_Address1, prmBank_HO_Address2, prmCityID, prmBank_ContactName, prmBank_ContactNo1, prmBank_ContactNo2, prmBank_EmailID1, prmBank_EmailID2, prmBank_FTPAddress1, prmBank_FTPAddress2, prmBank_FTPPort1, prmBank_FTPPort2, prmBank_FTP1UserID, prmBank_FTP2UserID, prmBank_FTP1Password, prmBank_FTP2Password, prmBank_ExtractedFile, prmCreatedBy, prmCreatedDatetime, prmModifiedBy, prmModifiedDatetime);
            return x;

        }
        public int Sp_UpdateBank(decimal CityID, string BankName, string BankShotName, string Bank_HO_Address1, string Bank_HO_Address2, string Bank_ContactName, string Bank_ContactNo1, string Bank_ContactNo2, string Bank_EmailID1, string Bank_EmailID2, string Bank_FTPAddress1, string Bank_FTPAddress2, decimal Bank_FTPPort1, decimal Bank_FTPPort2, string Bank_FTP1UserID, string Bank_FTP2UserID, string Bank_FTP1Password, string Bank_FTP2Password, string Bank_ExtrctdFileLoc, decimal ModifiedBy, DateTime ModifiedDatetime, decimal BankID)
        {
            SqlParameter prmCityID = new SqlParameter("@CityID", SqlDbType.Decimal);
            prmCityID.Value = (CityID);
            SqlParameter prmBankName = new SqlParameter("@BankName", SqlDbType.VarChar, 50);
            prmBankName.Value = BankName;
            SqlParameter prmBankShotName = new SqlParameter("@BankShotName", SqlDbType.VarChar, 50);
            prmBankShotName.Value = BankShotName;
            SqlParameter prmBank_HO_Address1 = new SqlParameter("@Bank_HO_Address1", SqlDbType.VarChar, 300);
            prmBank_HO_Address1.Value = Bank_HO_Address1;
            SqlParameter prmBank_HO_Address2 = new SqlParameter("@Bank_HO_Address2", SqlDbType.VarChar, 300);
            prmBank_HO_Address2.Value = Bank_HO_Address2;
            SqlParameter prmBank_ContactName = new SqlParameter("@Bank_ContactName", SqlDbType.VarChar, 50);
            prmBank_ContactName.Value = Bank_ContactName;
            SqlParameter prmBank_ContactNo1 = new SqlParameter("@Bank_ContactNo1", SqlDbType.VarChar, 15);
            prmBank_ContactNo1.Value = Bank_ContactNo1;
            SqlParameter prmBank_ContactNo2 = new SqlParameter("@Bank_ContactNo2", SqlDbType.VarChar, 15);
            prmBank_ContactNo2.Value = Bank_ContactNo2;
            SqlParameter prmBank_EmailID1 = new SqlParameter("@Bank_EmailID1", SqlDbType.VarChar, 50);
            prmBank_EmailID1.Value = Bank_EmailID1;
            SqlParameter prmBank_EmailID2 = new SqlParameter("@Bank_EmailID2", SqlDbType.VarChar, 50);
            prmBank_EmailID2.Value = Bank_EmailID2;
            SqlParameter prmBank_FTPAddress1 = new SqlParameter("@Bank_FTPAddress1", SqlDbType.VarChar, 300);
            prmBank_FTPAddress1.Value = Bank_FTPAddress1;
            SqlParameter prmBank_FTPAddress2 = new SqlParameter("@Bank_FTPAddress2", SqlDbType.VarChar, 300);
            prmBank_FTPAddress2.Value = Bank_FTPAddress2;
            SqlParameter prmBank_FTPPort1 = new SqlParameter("@Bank_FTPPort1", SqlDbType.Decimal);
            prmBank_FTPPort1.Value = Bank_FTPPort1;
            SqlParameter prmBank_FTPPort2 = new SqlParameter("@Bank_FTPPort2", SqlDbType.Decimal);
            prmBank_FTPPort2.Value = Bank_FTPPort2;
            SqlParameter prmBank_FTP1UserID = new SqlParameter("@Bank_FTP1UserID", SqlDbType.VarChar, 50);
            prmBank_FTP1UserID.Value = Bank_FTP1UserID;
            SqlParameter prmBank_FTP2UserID = new SqlParameter("@Bank_FTP2UserID", SqlDbType.VarChar, 50);
            prmBank_FTP2UserID.Value = Bank_FTP2UserID;
            SqlParameter prmBank_FTP1Password = new SqlParameter("@Bank_FTP1Password", SqlDbType.VarChar, 20);
            prmBank_FTP1Password.Value = Bank_FTP1Password;
            SqlParameter prmBank_FTP2Password = new SqlParameter("@Bank_FTP2Password", SqlDbType.VarChar, 20);
            prmBank_FTP2Password.Value = Bank_FTP2Password;
            SqlParameter prmBank_ExtrctdFileLoc = new SqlParameter("@Bank_ExtrctdFileLoc", SqlDbType.VarChar, 300);
            prmBank_ExtrctdFileLoc.Value = Bank_ExtrctdFileLoc;
            SqlParameter prmModifiedBy = new SqlParameter("@ModifiedBy", SqlDbType.Decimal);
            prmModifiedBy.Value = ModifiedBy;
            SqlParameter prmModifiedDatetime = new SqlParameter("@ModifiedDatetime", SqlDbType.SmallDateTime);
            prmModifiedDatetime.Value = ModifiedDatetime;
            SqlParameter prmBankID = new SqlParameter("@BankID", SqlDbType.Decimal);
            prmBankID.Value = (BankID);
            int x = DataAccess.Execute("Sp_UpdateBank", prmCityID, prmBankName, prmBankShotName, prmBank_HO_Address1, prmBank_HO_Address2, prmBank_ContactName, prmBank_ContactNo1, prmBank_ContactNo2, prmBank_EmailID1, prmBank_EmailID2, prmBank_FTPAddress1, prmBank_FTPAddress2, prmBank_FTPPort1, prmBank_FTPPort2, prmBank_FTP1UserID, prmBank_FTP2UserID, prmBank_FTP1Password, prmBank_FTP2Password, prmBank_ExtrctdFileLoc, prmModifiedBy, prmModifiedDatetime, prmBankID);
            return x;
        }
        #endregion
        #region EditBranch
        public DataTable SP_EditBranch(string ExecQuary) // Fills the GridView on the Edit page.
        {
            SqlParameter prmExecQuary = new SqlParameter("@ExecQuary", SqlDbType.VarChar, 6000);
            prmExecQuary.Value = ExecQuary;

            return DataAccess.GetFromDataTable("SP_EditBranch", prmExecQuary);
        }

        //Taufiq 05052015 To search and Reset gridview
        //public DataTable SP_EditBranch(string ExecQuary) 
        //{
        //    SqlParameter prmExecQuary = new SqlParameter("@ExecQuary", SqlDbType.VarChar, 6000);
        //    prmExecQuary.Value = ExecQuary;

        //    return DataAccess.GetFromDataTable("SP_EditBranch", prmExecQuary);
        //}
        public DataTable SP_GetBranchIDs(decimal BranchID)
        {
            SqlParameter prmBranchID = new SqlParameter("@BranchID", SqlDbType.Decimal);
            prmBranchID.Value = BranchID;
            return DataAccess.GetFromDataTable("SP_GetBranchIDs", prmBranchID);
        }
        public int SP_DeleteBranchMaster(decimal BranchID)
        {
            SqlParameter prmBranchID = new SqlParameter("@BranchID", SqlDbType.Decimal);
            prmBranchID.Value = BranchID;
            int x = DataAccess.Execute("SP_DeleteBranchMaster", prmBranchID);
            return x;
        }
        #endregion
        #region BankBranch
        public DataTable Sp_GetBankNames()
        {
            return DataAccess.GetFromDataTable("Sp_GetBankNames");
        }
        public DataTable SP_FillRegionName()
        {
            return DataAccess.GetFromDataTable("SP_FillRegionName");
        }
        public DataTable SP_FillUpdateBranch(decimal BranchID)
        {
            SqlParameter prmBranchID = new SqlParameter("@BranchID", SqlDbType.Decimal);
            prmBranchID.Value = BranchID;
            return DataAccess.GetFromDataTable("SP_FillUpdateBranch", prmBranchID);
        }
        public DataTable SP_FillRegionName4Bank(decimal BankID)
        {
            SqlParameter prmBankID = new SqlParameter("@BankID", SqlDbType.Decimal);
            prmBankID.Value = BankID;
            return DataAccess.GetFromDataTable("SP_FillRegionName4Bank", prmBankID);
        }
        public DataTable SP_CheckBranchForSave(decimal BankID, string BranchCode)
        {
            SqlParameter prmBankID = new SqlParameter("@BankID", SqlDbType.Decimal);
            prmBankID.Value = BankID;
            SqlParameter prmBranchCode = new SqlParameter("@BranchCode", SqlDbType.VarChar, 30);
            prmBranchCode.Value = BranchCode;
            return DataAccess.GetFromDataTable("SP_CheckBranchForSave", prmBankID, prmBranchCode);
        }
        public int SP_InsertBranchMaster(decimal BankID, decimal RegionID, string BranchName, string BranchCode, string BranchAddress1, string BranchAddress2, decimal CityID, string Locality, decimal CreatedBy, DateTime CreatedDatetime, decimal ModifiedBy, DateTime ModifiedDatetime)
        {
            SqlParameter prmBankID = new SqlParameter("@BankID", SqlDbType.Decimal);
            prmBankID.Value = BankID;
            SqlParameter prmRegionID = new SqlParameter("@RegionID", SqlDbType.Decimal);
            prmRegionID.Value = RegionID;
            SqlParameter prmBranchName = new SqlParameter("@BranchName", SqlDbType.VarChar, 50);
            prmBranchName.Value = BranchName;
            SqlParameter prmBranchCode = new SqlParameter("@BranchCode", SqlDbType.VarChar, 30);
            prmBranchCode.Value = BranchCode;
            SqlParameter prmBranchAddress1 = new SqlParameter("@BranchAddress1", SqlDbType.VarChar, 300);
            prmBranchAddress1.Value = BranchAddress1;
            SqlParameter prmBranchAddress2 = new SqlParameter("@BranchAddress2", SqlDbType.VarChar, 300);
            prmBranchAddress2.Value = BranchAddress2;
            SqlParameter prmCityID = new SqlParameter("@CityID", SqlDbType.Decimal);
            prmCityID.Value = CityID;//Locality
            SqlParameter prmLocality = new SqlParameter("@Locality", SqlDbType.VarChar, 50);
            prmLocality.Value = Locality;
            SqlParameter prmCreatedBy = new SqlParameter("@CreatedBy", SqlDbType.Decimal);
            prmCreatedBy.Value = CreatedBy;
            SqlParameter prmCreatedDatetime = new SqlParameter("@CreatedDatetime", SqlDbType.SmallDateTime);
            prmCreatedDatetime.Value = CreatedDatetime;
            SqlParameter prmModifiedBy = new SqlParameter("@ModifiedBy", SqlDbType.Decimal);
            prmModifiedBy.Value = ModifiedBy;
            SqlParameter prmModifiedDatetime = new SqlParameter("@ModifiedDatetime", SqlDbType.SmallDateTime);
            prmModifiedDatetime.Value = ModifiedDatetime;
            int x = DataAccess.Execute("SP_InsertBranchMaster", prmBankID, prmRegionID, prmBranchName, prmBranchCode, prmBranchAddress1, prmBranchAddress2, prmCityID, prmLocality, prmCreatedBy, prmCreatedDatetime, prmModifiedBy, prmModifiedDatetime);
            return x;
        }
        public DataTable SP_CheckBranchForUpdate(decimal BranchID, decimal BankID, string BranchCode)
        {
            SqlParameter prmBranchID = new SqlParameter("@BranchID", SqlDbType.Decimal);
            prmBranchID.Value = BranchID;
            SqlParameter prmBankID = new SqlParameter("@BankID", SqlDbType.Decimal);
            prmBankID.Value = BankID;
            SqlParameter prmBranchCode = new SqlParameter("@BranchCode", SqlDbType.VarChar, 50);
            prmBranchCode.Value = BranchCode;
            return DataAccess.GetFromDataTable("SP_CheckBranchForUpdate", prmBranchID, prmBankID, prmBranchCode);

        }
        public int SP_UpdateBankBranch(decimal RegionID, string BranchName, string BranchCode, string BranchAddress1, string BranchAddress2, decimal CityID, string Locality, decimal ModifiedBy, DateTime ModifiedDatetime, decimal BranchID)
        {
            SqlParameter prmRegionID = new SqlParameter("@RegionID", SqlDbType.Decimal);
            prmRegionID.Value = (RegionID);
            SqlParameter prmBranchName = new SqlParameter("@BranchName", SqlDbType.VarChar, 50);
            prmBranchName.Value = BranchName;
            SqlParameter prmBranchCode = new SqlParameter("@BranchCode", SqlDbType.VarChar, 30);
            prmBranchCode.Value = BranchCode;
            SqlParameter prmBranchAddress1 = new SqlParameter("@BranchAddress1", SqlDbType.VarChar, 300);
            prmBranchAddress1.Value = BranchAddress1;
            SqlParameter prmBranchAddress2 = new SqlParameter("@BranchAddress2", SqlDbType.VarChar, 300);
            prmBranchAddress2.Value = BranchAddress2;
            SqlParameter prmCityID = new SqlParameter("@CityID", SqlDbType.Decimal);
            prmCityID.Value = (CityID);
            SqlParameter prmLocality = new SqlParameter("@Locality", SqlDbType.VarChar, 50);
            prmLocality.Value = Locality;
            SqlParameter prmModifiedBy = new SqlParameter("@ModifiedBy", SqlDbType.Decimal);
            prmModifiedBy.Value = (ModifiedBy);
            SqlParameter prmModifiedDatetime = new SqlParameter("@ModifiedDatetime", SqlDbType.SmallDateTime);
            prmModifiedDatetime.Value = (ModifiedDatetime);
            SqlParameter prmBranchID = new SqlParameter("@BranchID", SqlDbType.Decimal);
            prmBranchID.Value = (BranchID);
            int x = DataAccess.Execute("SP_UpdateBankBranch", prmRegionID, prmBranchName, prmBranchCode, prmBranchAddress1, prmBranchAddress2, prmCityID, prmLocality, prmModifiedBy, prmModifiedDatetime, prmBranchID);
            return x;
        }
        public DataTable SP_FillRegionNameOnBank(decimal BankID)
        {
            SqlParameter prmBankID = new SqlParameter("@BankID", SqlDbType.Decimal);
            prmBankID.Value = BankID;
            return DataAccess.GetFromDataTable("SP_FillRegionNameOnBank", prmBankID);
        }
        #endregion
        #region Edit SST Link Type
        public DataTable SP_EditSST_LINK()
        {
            return DataAccess.GetFromDataTable("SP_EditSST_LINK");
        }
        public DataTable SP_GetLinkTypeID(decimal LinkTypeID)
        {
            SqlParameter prmLinkTypeID = new SqlParameter("@LinkTypeID", SqlDbType.Decimal);
            prmLinkTypeID.Value = LinkTypeID;
            return DataAccess.GetFromDataTable("SP_GetLinkTypeID", prmLinkTypeID);
        }
        public int SP_DeleteSSTType(decimal LinkTypeID)
        {
            SqlParameter prmLinkTypeID = new SqlParameter("@LinkTypeID", SqlDbType.Decimal);
            prmLinkTypeID.Value = LinkTypeID;
            int x = DataAccess.Execute("SP_DeleteSSTType", prmLinkTypeID);
            return x;
        }
        #endregion
        #region SST Link Type
        public DataTable SP_FillUpdateSSTLink(decimal LinkTypeID)
        {
            SqlParameter prmLinkTypeID = new SqlParameter("@LinkTypeID", SqlDbType.Decimal);
            prmLinkTypeID.Value = LinkTypeID;
            return DataAccess.GetFromDataTable("SP_FillUpdateSSTLink", prmLinkTypeID);
        }
        public int SP_GetLinkTypeMasterCount(string LinkType)
        {
            SqlParameter prmLinkType = new SqlParameter("@LinkType", SqlDbType.VarChar, 50);
            prmLinkType.Value = LinkType;
            int x = DataAccess.GetInt32("SP_GetLinkTypeMasterCount", prmLinkType);
            return x;
        }
        public int SP_Insert_SSTLinkType_CDMS(string LinkType, string LinkTypeDescription, decimal CreatedBy, DateTime CreatedDatetime, decimal ModifiedBy, DateTime ModifiedDatetime)
        {
            SqlParameter prmLinkType = new SqlParameter("@LinkType", SqlDbType.VarChar, 50);
            prmLinkType.Value = LinkType;
            SqlParameter prmLinkTypeDescription = new SqlParameter("@LinkTypeDescription ", SqlDbType.VarChar, 150);
            prmLinkTypeDescription.Value = LinkTypeDescription;
            SqlParameter prmCreatedBy = new SqlParameter("@CreatedBy", SqlDbType.Decimal);
            prmCreatedBy.Value = CreatedBy;
            SqlParameter prmCreatedDatetime = new SqlParameter("@CreatedDatetime", SqlDbType.SmallDateTime);
            prmCreatedDatetime.Value = CreatedDatetime;
            SqlParameter prmModifiedBy = new SqlParameter("@ModifiedBy", SqlDbType.Decimal);
            prmModifiedBy.Value = ModifiedBy;
            SqlParameter prmModifiedDatetime = new SqlParameter("@ModifiedDatetime", SqlDbType.SmallDateTime);
            prmModifiedDatetime.Value = ModifiedDatetime;
            int x = DataAccess.Execute("SP_Insert_SSTLinkType_CDMS", prmLinkType, prmLinkTypeDescription, prmCreatedBy, prmCreatedDatetime, prmModifiedBy, prmModifiedDatetime);
            return x;

        }
        public int SP_UpdateSSTLink(string LinkType, string LinkTypeDescription, decimal ModifiedBy, DateTime ModifiedDatetime, decimal LinkTypeID)
        {
            SqlParameter prmLinkType = new SqlParameter("@LinkType", SqlDbType.VarChar, 50);
            prmLinkType.Value = LinkType;
            SqlParameter prmLinkTypeDescription = new SqlParameter("@LinkTypeDescription", SqlDbType.VarChar, 150);
            prmLinkTypeDescription.Value = LinkTypeDescription;
            SqlParameter prmModifiedBy = new SqlParameter("@ModifiedBy", SqlDbType.Decimal);
            prmModifiedBy.Value = ModifiedBy;
            SqlParameter prmModifiedDatetime = new SqlParameter("@ModifiedDatetime", SqlDbType.SmallDateTime);
            prmModifiedDatetime.Value = ModifiedDatetime;
            SqlParameter prmLinkTypeID = new SqlParameter("@LinkTypeID", SqlDbType.Decimal);
            prmLinkTypeID.Value = (LinkTypeID);
            int x = DataAccess.Execute("SP_UpdateSSTLink", prmLinkType, prmLinkTypeDescription, prmModifiedBy, prmModifiedDatetime, prmLinkTypeID);
            return x;
        }
        #endregion
        #region Link Provider
        public DataTable SP_GetLinkProviderID(decimal LinkProviderID)
        {
            SqlParameter prmLinkProviderID = new SqlParameter("@LinkProviderID", SqlDbType.Decimal);
            prmLinkProviderID.Value = LinkProviderID;
            return DataAccess.GetFromDataTable("SP_GetLinkProviderID", prmLinkProviderID);
        }
        public int SP_DeleteLinkProvider(decimal LinkProviderID)
        {
            SqlParameter prmLinkProviderID = new SqlParameter("@LinkProviderID", SqlDbType.Decimal);
            prmLinkProviderID.Value = LinkProviderID;
            int x = DataAccess.Execute("SP_DeleteLinkProvider", prmLinkProviderID);
            return x;
        }
        public DataTable SP_EditSST_LinkProvider()
        {
            return DataAccess.GetFromDataTable("SP_EditSST_LinkProvider");
        }
        #endregion
        #region SST Link Provider
        public DataTable SP_FillUPdate_SSTLINKProvider(decimal LinkProviderID)
        {
            SqlParameter prmLinkProviderID = new SqlParameter("@LinkProviderID", SqlDbType.Decimal);
            prmLinkProviderID.Value = LinkProviderID;
            return DataAccess.GetFromDataTable("SP_FillUPdate_SSTLINKProvider", prmLinkProviderID);
        }
        public int SP_GetSSTProviderMasterCount(string LinkProviderName)
        {
            SqlParameter prmLinkProviderName = new SqlParameter("@LinkProviderName", SqlDbType.VarChar, 50);
            prmLinkProviderName.Value = LinkProviderName;
            int x = DataAccess.GetInt32("SP_GetSSTProviderMasterCount", prmLinkProviderName);
            return x;
        }
        public int SP_Insert_SSTLinkProvider_CDMS(string LinkProviderName, string LinkProviderDescription, decimal CreatedBy, DateTime CreatedDatetime, decimal ModifiedBy, DateTime ModifiedDatetime)
        {
            SqlParameter prmLinkProviderName = new SqlParameter("@LinkProviderName", SqlDbType.VarChar, 50);
            prmLinkProviderName.Value = LinkProviderName;
            SqlParameter prmLinkProviderDescription = new SqlParameter("@LinkProviderDescription ", SqlDbType.VarChar, 150);
            prmLinkProviderDescription.Value = LinkProviderDescription;
            SqlParameter prmCreatedBy = new SqlParameter("@CreatedBy", SqlDbType.Decimal);
            prmCreatedBy.Value = CreatedBy;
            SqlParameter prmCreatedDatetime = new SqlParameter("@CreatedDatetime", SqlDbType.SmallDateTime);
            prmCreatedDatetime.Value = CreatedDatetime;
            SqlParameter prmModifiedBy = new SqlParameter("@ModifiedBy", SqlDbType.Decimal);
            prmModifiedBy.Value = ModifiedBy;
            SqlParameter prmModifiedDatetime = new SqlParameter("@ModifiedDatetime", SqlDbType.SmallDateTime);
            prmModifiedDatetime.Value = ModifiedDatetime;
            int x = DataAccess.Execute("SP_Insert_SSTLinkProvider_CDMS", prmLinkProviderName, prmLinkProviderDescription, prmCreatedBy, prmCreatedDatetime, prmModifiedBy, prmModifiedDatetime);
            return x;

        }
        public int SP_UpdateSSTLINK_Provider(string LinkProviderName, string LinkProviderDescription, decimal ModifiedBy, DateTime ModifiedDatetime, decimal LinkProviderID)
        {
            SqlParameter prmLinkProviderName = new SqlParameter("@LinkProviderName", SqlDbType.VarChar, 50);
            prmLinkProviderName.Value = LinkProviderName;
            SqlParameter prmLinkProviderDescription = new SqlParameter("@LinkProviderDescription ", SqlDbType.VarChar, 150);
            prmLinkProviderDescription.Value = LinkProviderDescription;
            SqlParameter prmModifiedBy = new SqlParameter("@ModifiedBy", SqlDbType.Decimal);
            prmModifiedBy.Value = ModifiedBy;
            SqlParameter prmModifiedDatetime = new SqlParameter("@ModifiedDatetime", SqlDbType.SmallDateTime);
            prmModifiedDatetime.Value = ModifiedDatetime;
            SqlParameter prmLinkProviderID = new SqlParameter("@LinkProviderID", SqlDbType.Decimal);
            prmLinkProviderID.Value = (LinkProviderID);
            int x = DataAccess.Execute("SP_UpdateSSTLINK_Provider", prmLinkProviderName, prmLinkProviderDescription, prmModifiedBy, prmModifiedDatetime, prmLinkProviderID);
            return x;
        }
        #endregion
        #region Edit SST Type
        public DataTable SP_EditSST_Type()
        {
            return DataAccess.GetFromDataTable("SP_EditSST_Type");
        }
        public DataTable SP_GetSSTTYPEID(decimal SST_TypeID)
        {
            SqlParameter prmSST_TypeID = new SqlParameter("@SST_TypeID", SqlDbType.Decimal);
            prmSST_TypeID.Value = SST_TypeID;
            return DataAccess.GetFromDataTable("SP_GetSSTTYPEID", prmSST_TypeID);
        }
        public int SP_DeleteSSTTypeID(decimal SST_TypeID)
        {
            SqlParameter prmSST_TypeID = new SqlParameter("@SST_TypeID", SqlDbType.Decimal);
            prmSST_TypeID.Value = SST_TypeID;
            int x = DataAccess.Execute("SP_DeleteSSTTypeID", prmSST_TypeID);
            return x;
        }
        #endregion
        #region SST Type
        public DataTable SP_FillUpdateSSTType(decimal SST_TypeID)
        {
            SqlParameter prmSST_TypeID = new SqlParameter("@SST_TypeID", SqlDbType.Decimal);
            prmSST_TypeID.Value = SST_TypeID;
            return DataAccess.GetFromDataTable("SP_FillUpdateSSTType", prmSST_TypeID);
        }
        public int SP_GetSSTTypeMasterCount(string SST_TYPE, string SST_Make)
        {
            SqlParameter prmSST_TYPE = new SqlParameter("@SST_TYPE", SqlDbType.VarChar, 50);
            prmSST_TYPE.Value = SST_TYPE;
            SqlParameter prmSST_Make = new SqlParameter("@SST_Make", SqlDbType.VarChar, 300);
            prmSST_Make.Value = SST_Make;
            int x = DataAccess.GetInt32("SP_GetSSTTypeMasterCount", prmSST_TYPE, prmSST_Make);
            return x;
        }
        public int SP_Insert_SSTTypeMaster_CDMS(string SST_TYPE, string SST_TypeDescription, string SST_Make, string EDCPath_OnSST, string IconPath_OnSST,
            string MoviePath_OnSST, string AudioPath_OnSST, string TraceFilePath_OnSST, string EDCFileName_OnSST, string EDCBackupPath_OnSST, string RestartAppPath_OnSST,
            string RestartAppnPathName, string RestartServiceName,
            decimal CreatedBy, DateTime CreatedDatetime, decimal ModifiedBy, DateTime ModifiedDatetime, string UpdateQuary)
        {
            SqlParameter prmSST_TYPE = new SqlParameter("@SST_TYPE", SqlDbType.VarChar, 50);
            prmSST_TYPE.Value = SST_TYPE;
            SqlParameter prmSST_TypeDescription = new SqlParameter("@SST_TypeDescription ", SqlDbType.VarChar, 150);
            prmSST_TypeDescription.Value = SST_TypeDescription;
            SqlParameter prmSST_Make = new SqlParameter("@SST_Make ", SqlDbType.VarChar, 300);
            prmSST_Make.Value = SST_Make;
            SqlParameter prmEDCPath_OnSST = new SqlParameter("@EDCPath_OnSST ", SqlDbType.VarChar, 500);
            prmEDCPath_OnSST.Value = EDCPath_OnSST;
            SqlParameter prmIconPath_OnSST = new SqlParameter("@IconPath_OnSST ", SqlDbType.VarChar, 500);
            prmIconPath_OnSST.Value = IconPath_OnSST;
            SqlParameter prmMoviePath_OnSST = new SqlParameter("@MoviePath_OnSST ", SqlDbType.VarChar, 500);
            prmMoviePath_OnSST.Value = MoviePath_OnSST;
            SqlParameter prmAudioPath_OnSST = new SqlParameter("@AudioPath_OnSST ", SqlDbType.VarChar, 500);
            prmAudioPath_OnSST.Value = AudioPath_OnSST;
            SqlParameter prmTraceFilePath_OnSST = new SqlParameter("@TraceFilePath_OnSST ", SqlDbType.VarChar, 500);
            prmTraceFilePath_OnSST.Value = TraceFilePath_OnSST;
            SqlParameter prmEDCFileName_OnSST = new SqlParameter("@EDCFileName_OnSST ", SqlDbType.VarChar, 500);
            prmEDCFileName_OnSST.Value = EDCFileName_OnSST;
            SqlParameter prmEDCBackupPath_OnSST = new SqlParameter("@EDCBackupPath_OnSST ", SqlDbType.VarChar, 500);
            prmEDCBackupPath_OnSST.Value = EDCBackupPath_OnSST;
            SqlParameter prmRestartAppPath_OnSST = new SqlParameter("@RestartAppPath_OnSST ", SqlDbType.VarChar, 500);
            prmRestartAppPath_OnSST.Value = RestartAppPath_OnSST;

            SqlParameter prmRestartAppnPathName = new SqlParameter("@RestartAppnPathName ", SqlDbType.VarChar, 500);
            prmRestartAppnPathName.Value = RestartAppnPathName;
            SqlParameter prmRestartServiceName = new SqlParameter("@RestartServiceName ", SqlDbType.VarChar, 500);
            prmRestartServiceName.Value = RestartServiceName;


            SqlParameter prmCreatedBy = new SqlParameter("@CreatedBy", SqlDbType.Decimal);
            prmCreatedBy.Value = CreatedBy;
            SqlParameter prmCreatedDatetime = new SqlParameter("@CreatedDatetime", SqlDbType.SmallDateTime);
            prmCreatedDatetime.Value = CreatedDatetime;
            SqlParameter prmModifiedBy = new SqlParameter("@ModifiedBy", SqlDbType.Decimal);
            prmModifiedBy.Value = ModifiedBy;
            SqlParameter prmModifiedDatetime = new SqlParameter("@ModifiedDatetime", SqlDbType.SmallDateTime);
            prmModifiedDatetime.Value = ModifiedDatetime;

            SqlParameter prmUpdateQuary = new SqlParameter("@UpdateQuary", SqlDbType.VarChar, 6000);
            prmUpdateQuary.Value = UpdateQuary;

            int x = DataAccess.Execute("SP_Insert_SSTTypeMaster_CDMS", prmSST_TYPE, prmSST_TypeDescription, prmSST_Make, prmEDCPath_OnSST, prmIconPath_OnSST, prmMoviePath_OnSST, prmAudioPath_OnSST, prmTraceFilePath_OnSST, prmEDCFileName_OnSST, prmEDCBackupPath_OnSST, prmRestartAppPath_OnSST, prmRestartAppnPathName, prmRestartServiceName, prmCreatedBy, prmCreatedDatetime, prmModifiedBy, prmModifiedDatetime, prmUpdateQuary);
            return x;

        }
        public DataTable SP_UpdateCheckSSTType(decimal SST_TypeID, string SST_TYPE, string SST_Make)
        {
            SqlParameter prmSST_TypeID = new SqlParameter("@SST_TypeID", SqlDbType.Decimal);
            prmSST_TypeID.Value = SST_TypeID;

            SqlParameter prmSST_TYPE = new SqlParameter("@SST_TYPE", SqlDbType.VarChar, 50);
            prmSST_TYPE.Value = SST_TYPE;
            SqlParameter prmSST_Make = new SqlParameter("@SST_Make", SqlDbType.VarChar, 300);
            prmSST_Make.Value = SST_Make;
            return DataAccess.GetFromDataTable("SP_UpdateCheckSSTType", prmSST_TypeID, prmSST_TYPE, prmSST_Make);
        }
        public int SP_UpdateSSTTypeMaster_CDMS(string SST_TYPE, string SST_TypeDescription, string SST_Make, string EDCPath_OnSST, string IconPath_OnSST, string MoviePath_OnSST, string AudioPath_OnSST, string TraceFilePath_OnSST, string EDCFileName_OnSST, string EDCBackupPath_OnSST, string RestartAppPath_OnSST,
             string RestartAppnPathName, string RestartServiceName,
             decimal ModifiedBy, DateTime ModifiedDatetime, decimal SST_TypeID, string UpdateQuary)
        {
            SqlParameter prmUpdateQuary = new SqlParameter("@UpdateQuary", SqlDbType.VarChar, 6000);
            prmUpdateQuary.Value = UpdateQuary;
            SqlParameter prmSST_TYPE = new SqlParameter("@SST_TYPE", SqlDbType.VarChar, 50);
            prmSST_TYPE.Value = SST_TYPE;
            SqlParameter prmSST_TypeDescription = new SqlParameter("@SST_TypeDescription ", SqlDbType.VarChar, 150);
            prmSST_TypeDescription.Value = SST_TypeDescription;
            SqlParameter prmSST_Make = new SqlParameter("@SST_Make ", SqlDbType.VarChar, 300);
            prmSST_Make.Value = SST_Make;
            SqlParameter prmEDCPath_OnSST = new SqlParameter("@EDCPath_OnSST ", SqlDbType.VarChar, 500);
            prmEDCPath_OnSST.Value = EDCPath_OnSST;
            SqlParameter prmIconPath_OnSST = new SqlParameter("@IconPath_OnSST ", SqlDbType.VarChar, 500);
            prmIconPath_OnSST.Value = IconPath_OnSST;
            SqlParameter prmMoviePath_OnSST = new SqlParameter("@MoviePath_OnSST ", SqlDbType.VarChar, 500);
            prmMoviePath_OnSST.Value = MoviePath_OnSST;
            SqlParameter prmAudioPath_OnSST = new SqlParameter("@AudioPath_OnSST ", SqlDbType.VarChar, 500);
            prmAudioPath_OnSST.Value = AudioPath_OnSST;
            SqlParameter prmTraceFilePath_OnSST = new SqlParameter("@TraceFilePath_OnSST ", SqlDbType.VarChar, 500);
            prmTraceFilePath_OnSST.Value = TraceFilePath_OnSST;
            SqlParameter prmEDCFileName_OnSST = new SqlParameter("@EDCFileName_OnSST ", SqlDbType.VarChar, 500);
            prmEDCFileName_OnSST.Value = EDCFileName_OnSST;
            SqlParameter prmEDCBackupPath_OnSST = new SqlParameter("@EDCBackupPath_OnSST ", SqlDbType.VarChar, 500);
            prmEDCBackupPath_OnSST.Value = EDCBackupPath_OnSST;
            SqlParameter prmRestartAppPath_OnSST = new SqlParameter("@RestartAppPath_OnSST ", SqlDbType.VarChar, 500);
            prmRestartAppPath_OnSST.Value = RestartAppPath_OnSST;
            SqlParameter prmRestartAppnPathName = new SqlParameter("@RestartAppnPathName ", SqlDbType.VarChar, 500);
            prmRestartAppnPathName.Value = RestartAppnPathName;

            SqlParameter prmRestartServiceName = new SqlParameter("@RestartServiceName ", SqlDbType.VarChar, 500);
            prmRestartServiceName.Value = RestartServiceName;

            SqlParameter prmModifiedBy = new SqlParameter("@ModifiedBy", SqlDbType.Decimal);
            prmModifiedBy.Value = ModifiedBy;
            SqlParameter prmModifiedDatetime = new SqlParameter("@ModifiedDatetime", SqlDbType.SmallDateTime);
            prmModifiedDatetime.Value = ModifiedDatetime;
            SqlParameter prmSST_TypeID = new SqlParameter("@SST_TypeID", SqlDbType.Decimal);
            prmSST_TypeID.Value = (SST_TypeID);
            int x = DataAccess.Execute("SP_UpdateSSTTypeMaster_CDMS", prmSST_TYPE, prmSST_TypeDescription, prmSST_Make, prmEDCPath_OnSST, prmIconPath_OnSST, prmMoviePath_OnSST, prmAudioPath_OnSST, prmTraceFilePath_OnSST, prmEDCFileName_OnSST, prmEDCBackupPath_OnSST, prmRestartAppPath_OnSST, prmRestartAppnPathName, prmRestartServiceName, prmModifiedBy, prmModifiedDatetime, prmSST_TypeID, prmUpdateQuary);
            return x;
        }
        #endregion
        #region Report SSTType
        //public DataTable GetFaultyTerminalSSTTypewise()
        //{
        //    return DataAccess.GetFromDataTable("SP_GetFaultyTerminalSSTTypewise");
        //}
        public DataTable GetFaultyTerminalSSTTypewise(Decimal RegionID, Decimal BranchId)
        {
            SqlParameter prmRegionID = new SqlParameter("@RegionID", SqlDbType.Decimal);
            prmRegionID.Value = RegionID;
            SqlParameter prmBranchId = new SqlParameter("@BranchID", SqlDbType.Decimal);
            prmBranchId.Value = BranchId;
            return DataAccess.GetFromDataTable("SP_GetFaultyTerminalSSTTypewise", prmRegionID, prmBranchId);
        }

        //public DataTable GetTicketDetailSSTTypewise(decimal SST_TypeID)
        //{
        //    SqlParameter prmSST_TypeID = new SqlParameter("SST_TypeID", SqlDbType.Decimal, 18);
        //    prmSST_TypeID.Value = SST_TypeID;
        //    return DataAccess.GetFromDataTable("SP_GetTicketDetail_SSTTypewise", prmSST_TypeID);
        //}


        //public DataTable GetTicketDetailSSTTypewise(decimal RegionID, decimal BranchId, decimal SST_TypeID)
        //{
        //    SqlParameter prmRegionID = new SqlParameter("@RegionID", SqlDbType.Decimal);
        //    prmRegionID.Value = RegionID;
        //    SqlParameter prmBranchId = new SqlParameter("@BranchId", SqlDbType.Decimal);
        //    prmBranchId.Value = BranchId;
        //    SqlParameter prmSST_TypeID = new SqlParameter("@SST_TypeID", SqlDbType.Decimal);
        //    prmSST_TypeID.Value = SST_TypeID;
        //    return DataAccess.GetFromDataTable("SP_GetTicketDetail_SSTTypewise", prmRegionID, prmSST_TypeID, prmBranchId);
        //}

        public DataTable TestGetTerminalSSTTypewise(Decimal RegionID, decimal BranchId, decimal SSTTypeID)
        {
            SqlParameter prmRegionID = new SqlParameter("@RegionID", SqlDbType.Decimal);
            prmRegionID.Value = RegionID;
            SqlParameter prmBranchId = new SqlParameter("@BranchId", SqlDbType.Decimal);
            prmBranchId.Value = BranchId;
            SqlParameter prmSST_TypeID = new SqlParameter("@SSTTypeID", SqlDbType.Decimal);
            prmSST_TypeID.Value = SSTTypeID;
            return DataAccess.GetFromDataTable("SP_TestGetTerminalSSTTypewise", prmRegionID, prmSST_TypeID, prmBranchId);
        }
        public DataTable GetTicketDetailSSTTypewise(Decimal RegionID, Decimal BranchId, decimal SST_TypeID)
        {
            SqlParameter prmRegionID = new SqlParameter("@RegionID", SqlDbType.Decimal);
            prmRegionID.Value = RegionID;
            SqlParameter prmBranchId = new SqlParameter("@BranchId", SqlDbType.Decimal);
            prmBranchId.Value = BranchId;
            SqlParameter prmSST_TypeID = new SqlParameter("@SST_TypeID", SqlDbType.Decimal);
            prmSST_TypeID.Value = SST_TypeID;
            return DataAccess.GetFromDataTable("SP_GetTicketDetail_SSTTypewise", prmRegionID, prmSST_TypeID, prmBranchId);
        }


        #endregion
        #region Edit SST
        public DataTable SP_EditSST1()
        {
            return DataAccess.GetFromDataTable("SP_EditSST1");
        }
        public DataTable SP_FillSST_Type() // 
        {
            return DataAccess.GetFromDataTable("SP_FillSST_Type");
        }
        public DataTable SP_GetSSTID(decimal SSTID)
        {
            SqlParameter prmSSTID = new SqlParameter("@SSTID", SqlDbType.Decimal);
            prmSSTID.Value = SSTID;
            return DataAccess.GetFromDataTable("SP_GetSSTID", prmSSTID);
        }
        public int SP_DeleteSSTMaster(decimal SSTID)
        {
            SqlParameter prmSSTID = new SqlParameter("@SSTID", SqlDbType.Decimal);
            prmSSTID.Value = SSTID;
            int x = DataAccess.Execute("SP_DeleteSSTMaster", prmSSTID);
            return x;
        }
        public DataTable SP_FillBranchNameOnBank_Region(decimal BankID, decimal RegionID)
        {
            SqlParameter prmBankID = new SqlParameter("@BankID", SqlDbType.Decimal);
            prmBankID.Value = BankID;
            SqlParameter prmRegionID = new SqlParameter("@RegionID", SqlDbType.Decimal);
            prmRegionID.Value = RegionID;
            return DataAccess.GetFromDataTable("SP_FillBranchNameOnBank_Region", prmBankID, prmRegionID);
        }

        public int SP_GetTerminalCount()
        {
            int x = DataAccess.GetInt32("SP_GetTerminalCount");
            return x;
        }

        #endregion
        #region SST Profile
        public DataTable SP_GetSSTGroupForSSTProfile1()
        {
            return DataAccess.GetFromDataTable("SP_GetSSTGroupForSSTProfile1");
        }
        public DataTable SP_FillSST_LinkTypeMaster() //
        {
            return DataAccess.GetFromDataTable("SP_FillSST_LinkTypeMaster");
        }
        public DataTable SP_SST_LinkServiceProvider() //
        {
            return DataAccess.GetFromDataTable("SP_SST_LinkServiceProvider");
        }
        public DataTable SP_FillUpdateSST1(decimal SSTID)
        {
            SqlParameter prmSSTID = new SqlParameter("@SSTID", SqlDbType.Decimal);
            prmSSTID.Value = SSTID;
            return DataAccess.GetFromDataTable("SP_FillUpdateSST1", prmSSTID);
        }
        public DataTable SP_GetTop5Activity(string SST_TerminalID)
        {
            SqlParameter prmSST_TerminalID = new SqlParameter("@SST_TerminalID", SqlDbType.VarChar, 25);
            prmSST_TerminalID.Value = SST_TerminalID;
            return DataAccess.GetFromDataTable("SP_GetTop5Activity", prmSST_TerminalID);

        }
        public DataTable SP_GetSSTGroupsForEditSSTProfile(decimal SSTID)
        {
            SqlParameter prmSSTID = new SqlParameter("@SSTID", SqlDbType.VarChar, 50);
            prmSSTID.Value = SSTID;
            return DataAccess.GetFromDataTable("SP_GetSSTGroupsForEditSSTProfile", prmSSTID);
        }
        public int SP_GetSSTMasterCount(string SST_TerminalID)
        {
            SqlParameter prmSST_TerminalID = new SqlParameter("@SST_TerminalID", SqlDbType.VarChar, 25);
            prmSST_TerminalID.Value = SST_TerminalID;
            int x = DataAccess.GetInt32("SP_GetSSTMasterCount", prmSST_TerminalID);
            return x;
        }
        // Created by: Salini Sunil
        // Date: 19/03/2012
        // Edited on : 11/05/2012
        //Description: Inserts values to the SST_Master table.--DateTime SST_StatusChangeDate,string SST_AgentConnectivityDate,
        // decimal SSTLinkTypeID, decimal SSTLinkServiceProvideID,
        public int SP_Insert_SSTMaster_CDMS
                                               (
                                                 decimal SSTBankID, decimal SSTRegionID, decimal SSTBranchID, decimal SSTTypeID, decimal SSTLinkTypeID,
                                                 string SST_Name, string SST_TerminalID, string SST_ORGIP, string SST_NAT_IP, decimal SST_AgentPort, string SST_SiteAddress, string SST_ServiceArea, bool SST_Status,
                                                 bool SST_AgentConnectivity_Status, string Reason, bool isSunday, string sundayFTime,
                                                 string sundayTTime, bool isMonday, string mondayFTime, string mondayTTime, bool isTuesday, string tuesdayFTime, string tuesdayTTime,
                                                 bool isWednesday, string wednesdayFTime, string wednesdayTTime, bool isThursday, string thursdayFTime, string thursdayTTime, bool isFriday,
                                                 string fridayFTime, string fridayTTime, bool isSaturday, string saturdayFTime, string saturdayTTime,
            decimal VendorMonitoring, decimal VendorSLM, decimal VendorFLM, decimal VendorCash, decimal VendorNetwork, decimal VendorUPS, decimal Vendorconsumable, decimal VendorAC, decimal VendorTIS,
            decimal VendorHouseKeeping, decimal VendorHelpDesk, decimal VendorOther, decimal CreatedBy, decimal ModifiedBy)
        {
            /* From the front end it (ie, FromTime & ToTime) gets either empty string or 
             * a time based on the selection of check boxes. If the time is an empty string 
             * it inserts a null value to the datetime field in the DB  otherwise it appends 
             * the time with a default date and inserts it to the DB*/

            DateTime sundayFromTime = DateTime.Parse("1/1/2001 12:00:00 AM");
            DateTime sundayToTime = DateTime.Parse("1/1/2001 12:00:00 AM");

            DateTime mondayFromTime = DateTime.Parse("1/1/2001 12:00:00 AM");
            DateTime mondayToTime = DateTime.Parse("1/1/2001 12:00:00 AM");

            DateTime tuesdayFromTime = DateTime.Parse("1/1/2001 12:00:00 AM");
            DateTime tuesdayToTime = DateTime.Parse("1/1/2001 12:00:00 AM");

            DateTime wednesdayFromTime = DateTime.Parse("1/1/2001 12:00:00 AM");
            DateTime wednesdayToTime = DateTime.Parse("1/1/2001 12:00:00 AM");

            DateTime thursdayFromTime = DateTime.Parse("1/1/2001 12:00:00 AM");
            DateTime thursdayToTime = DateTime.Parse("1/1/2001 12:00:00 AM");

            DateTime fridayFromTime = DateTime.Parse("1/1/2001 12:00:00 AM");
            DateTime fridayToTime = DateTime.Parse("1/1/2001 12:00:00 AM");

            DateTime saturdayFromTime = DateTime.Parse("1/1/2001 12:00:00 AM");
            DateTime saturdayToTime = DateTime.Parse("1/1/2001 12:00:00 AM");


            SqlParameter prmSunday_FromTime = new SqlParameter("@Sunday_FromTime", SqlDbType.DateTime);
            SqlParameter prmSunday_ToTime = new SqlParameter("@Sunday_ToTime", SqlDbType.DateTime);
            SqlParameter prmMonday_FromTime = new SqlParameter("@Monday_FromTime", SqlDbType.DateTime);
            SqlParameter prmMonday_ToTime = new SqlParameter("@Monday_ToTime", SqlDbType.DateTime);
            SqlParameter prmTuesday_FromTime = new SqlParameter("@Tuesday_FromTime", SqlDbType.DateTime);
            SqlParameter prmTuesday_ToTime = new SqlParameter("@Tuesday_ToTime", SqlDbType.SmallDateTime);
            SqlParameter prmWednesday_FromTime = new SqlParameter("@Wednesday_FromTime", SqlDbType.DateTime);
            SqlParameter prmWednesday_ToTime = new SqlParameter("@Wednesday_ToTime", SqlDbType.DateTime);
            SqlParameter prmThursday_FromTime = new SqlParameter("@Thursday_FromTime", SqlDbType.DateTime);
            SqlParameter prmThursday_ToTime = new SqlParameter("@Thursday_ToTime", SqlDbType.DateTime);
            SqlParameter prmFriday_FromTime = new SqlParameter("@Friday_FromTime", SqlDbType.DateTime);
            SqlParameter prmFriday_ToTime = new SqlParameter("@Friday_ToTime", SqlDbType.DateTime);
            SqlParameter prmSaturday_FromTime = new SqlParameter("@Saturday_FromTime", SqlDbType.DateTime);
            SqlParameter prmSaturday_ToTime = new SqlParameter("@Saturday_ToTime", SqlDbType.DateTime);
            if (sundayFTime == string.Empty)
            {

                prmSunday_FromTime.Value = null;

            }
            else
            {
                string ft = sundayFromTime.ToShortDateString() + " " + sundayFTime;
                sundayFromTime = Convert.ToDateTime(ft);
                prmSunday_FromTime.Value = sundayFromTime;
            }
            if (sundayTTime == string.Empty)
            {

                prmSunday_ToTime.Value = null;

            }
            else
            {
                //  sundayToTime = sundayToTime.Date;
                string tt = sundayToTime.ToShortDateString() + " " + sundayTTime;
                sundayToTime = Convert.ToDateTime(tt);
                prmSunday_ToTime.Value = sundayToTime;
            }
            if (mondayFTime == string.Empty)
            {

                prmMonday_FromTime.Value = null;

            }
            else
            {
                // mondayFromTime = mondayFromTime.Date;
                string ft = mondayFromTime.ToShortDateString() + " " + mondayFTime;
                mondayFromTime = Convert.ToDateTime(ft);
                prmMonday_FromTime.Value = mondayFromTime;
            }
            if (mondayTTime == string.Empty)
            {

                prmMonday_ToTime.Value = null;

            }
            else
            {
                // mondayToTime = mondayToTime.Date;
                string tt = mondayToTime.ToShortDateString() + " " + mondayTTime;
                mondayToTime = Convert.ToDateTime(tt);
                prmMonday_ToTime.Value = mondayToTime;
            }


            if (tuesdayFTime == string.Empty)
            {
                prmTuesday_FromTime.Value = null;

            }
            else
            {
                //tuesdayFromTime = tuesdayFromTime.Date;
                string ft = tuesdayFromTime.ToShortDateString() + " " + tuesdayFTime;
                tuesdayFromTime = Convert.ToDateTime(ft);
                prmTuesday_FromTime.Value = tuesdayFromTime;
            }

            if (tuesdayTTime == string.Empty)
            {

                prmTuesday_ToTime.Value = null;

            }
            else
            {
                //  tuesdayToTime = tuesdayToTime.Date;
                string tt = tuesdayToTime.ToShortDateString() + " " + tuesdayTTime;
                tuesdayToTime = Convert.ToDateTime(tt);
                prmTuesday_ToTime.Value = tuesdayToTime;
            }


            if (wednesdayFTime == string.Empty)
            {

                prmWednesday_FromTime.Value = null;

            }
            else
            {
                // wednesdayFromTime = wednesdayFromTime.Date;
                string ft = wednesdayFromTime.ToShortDateString() + " " + wednesdayFTime;
                wednesdayFromTime = Convert.ToDateTime(ft);
                prmWednesday_FromTime.Value = wednesdayFromTime;
            }

            if (wednesdayTTime == string.Empty)
            {

                prmWednesday_ToTime.Value = null;

            }
            else
            {
                //  wednesdayToTime = wednesdayToTime.Date;
                string tt = wednesdayToTime.ToShortDateString() + " " + wednesdayTTime;
                wednesdayToTime = Convert.ToDateTime(tt);
                prmWednesday_ToTime.Value = wednesdayToTime;
            }

            if (thursdayFTime == string.Empty)
            {

                prmThursday_FromTime.Value = null;

            }
            else
            {

                string ft = thursdayFromTime.ToShortDateString() + " " + thursdayFTime;
                thursdayFromTime = Convert.ToDateTime(ft);
                prmThursday_FromTime.Value = thursdayFromTime;
            }

            if (thursdayTTime == string.Empty)
            {

                prmThursday_ToTime.Value = null;

            }
            else
            {
                //thursdayToTime = thursdayToTime.Date;
                string tt = thursdayToTime.ToShortDateString() + " " + thursdayTTime;
                thursdayToTime = Convert.ToDateTime(tt);
                prmThursday_ToTime.Value = thursdayToTime;
            }

            if (fridayFTime == string.Empty)
            {

                prmFriday_FromTime.Value = null;

            }
            else
            {
                //  fridayFromTime = fridayFromTime.Date;
                string ft = fridayFromTime.ToShortDateString() + " " + fridayFTime;
                fridayFromTime = Convert.ToDateTime(ft);
                prmFriday_FromTime.Value = fridayFromTime;
            }
            if (fridayTTime == string.Empty)
            {

                prmFriday_ToTime.Value = null;

            }
            else
            {
                //  fridayToTime = fridayToTime.Date;
                string tt = fridayToTime.ToShortDateString() + " " + fridayTTime;
                fridayToTime = Convert.ToDateTime(tt);
                prmFriday_ToTime.Value = fridayToTime;
            }
            if (saturdayFTime == string.Empty)
            {

                prmSaturday_FromTime.Value = null;

            }
            else
            {
                // saturdayFromTime = sundayFromTime.Date;
                string ft = saturdayFromTime.ToShortDateString() + " " + saturdayFTime;
                saturdayFromTime = Convert.ToDateTime(ft);
                prmSaturday_FromTime.Value = saturdayFromTime;
            }
            if (saturdayTTime == string.Empty)
            {

                prmSaturday_ToTime.Value = null;

            }
            else
            {
                // saturdayToTime = saturdayToTime.Date;
                string tt = saturdayToTime.ToShortDateString() + " " + saturdayTTime;
                saturdayToTime = Convert.ToDateTime(tt);
                prmSaturday_ToTime.Value = saturdayToTime;
            }
            SqlParameter prmSSTBankID = new SqlParameter("@SSTBankID", SqlDbType.Decimal);
            prmSSTBankID.Value = SSTBankID;
            SqlParameter prmSSTRegionID = new SqlParameter("@SSTRegionID", SqlDbType.Decimal);
            prmSSTRegionID.Value = SSTRegionID;
            SqlParameter prmSSTBranchID = new SqlParameter("@SSTBranchID", SqlDbType.Decimal);
            prmSSTBranchID.Value = SSTBranchID;
            SqlParameter prmSSTTypeID = new SqlParameter("@SSTTypeID", SqlDbType.Decimal);
            prmSSTTypeID.Value = SSTTypeID;
            SqlParameter prmSSTLinkTypeID = new SqlParameter("@SSTLinkTypeID", SqlDbType.Decimal);
            prmSSTLinkTypeID.Value = SSTLinkTypeID;
            //SqlParameter prmSSTLinkServiceProvideID = new SqlParameter("@SSTLinkServiceProvideID", SqlDbType.Decimal);
            //prmSSTLinkServiceProvideID.Value = SSTLinkServiceProvideID;
            SqlParameter prmSST_Name = new SqlParameter("@SST_Name", SqlDbType.VarChar, 50);
            prmSST_Name.Value = SST_Name;
            SqlParameter prmSST_TerminalID = new SqlParameter("@SST_TerminalID", SqlDbType.VarChar, 50);
            prmSST_TerminalID.Value = SST_TerminalID;
            SqlParameter prmSST_ORGIP = new SqlParameter("@SST_ORGIP", SqlDbType.VarChar, 50);
            prmSST_ORGIP.Value = SST_ORGIP;
            SqlParameter prmSST_NAT_IP = new SqlParameter("@SST_NAT_IP", SqlDbType.VarChar, 50);
            prmSST_NAT_IP.Value = SST_NAT_IP;
            SqlParameter prmSST_AgentPort = new SqlParameter("@SST_AgentPort", SqlDbType.Decimal);
            prmSST_AgentPort.Value = SST_AgentPort;
            SqlParameter prmSST_SiteAddress = new SqlParameter("@SST_SiteAddress", SqlDbType.VarChar, 50);
            prmSST_SiteAddress.Value = SST_SiteAddress;
            SqlParameter prmSST_Status = new SqlParameter("@SST_Status", SqlDbType.Bit);
            prmSST_Status.Value = SST_Status;

            SqlParameter prmSST_ServiceArea = new SqlParameter("@SST_ServiceArea ", SqlDbType.VarChar, 25);
            prmSST_ServiceArea.Value = SST_ServiceArea;
            SqlParameter prmSST_AgentConnectivity_Status = new SqlParameter("@SST_AgentConnectivity_Status", SqlDbType.Bit);
            prmSST_AgentConnectivity_Status.Value = SST_AgentConnectivity_Status;
            SqlParameter prmReason = new SqlParameter("@Reason", SqlDbType.VarChar, 50);
            prmReason.Value = Reason;
            SqlParameter prmIsSunday = new SqlParameter("@AccTime_Sunday", SqlDbType.Bit);
            prmIsSunday.Value = isSunday;
            SqlParameter prmIsMonday = new SqlParameter("@AccTime_Monday", SqlDbType.Bit);
            prmIsMonday.Value = isMonday;
            SqlParameter prmIsTuesday = new SqlParameter("@AccTime_Tuesday", SqlDbType.Bit);
            prmIsTuesday.Value = isTuesday;
            SqlParameter prmIswednesday = new SqlParameter("@AccTime_Wednesday", SqlDbType.Bit);
            prmIswednesday.Value = isWednesday;
            SqlParameter prmIsThursday = new SqlParameter("@AccTime_Thursday", SqlDbType.Bit);
            prmIsThursday.Value = isThursday;
            SqlParameter prmIsFriday = new SqlParameter("@AccTime_Friday", SqlDbType.Bit);
            prmIsFriday.Value = isFriday;
            SqlParameter prmIsSaturday = new SqlParameter("@AccTime_Saturday", SqlDbType.Bit);
            prmIsSaturday.Value = isSaturday;
            SqlParameter prmVendorMonitoring = new SqlParameter("@VendorMonitoring", SqlDbType.Decimal);
            prmVendorMonitoring.Value = VendorMonitoring;
            SqlParameter prmVendorSLM = new SqlParameter("@VendorSLM", SqlDbType.Decimal);
            prmVendorSLM.Value = VendorSLM;
            SqlParameter prmVendorFLM = new SqlParameter("@VendorFLM", SqlDbType.Decimal);
            prmVendorFLM.Value = VendorFLM;
            SqlParameter prmVendorCash = new SqlParameter("@VendorCash", SqlDbType.Decimal);
            prmVendorCash.Value = VendorCash;
            SqlParameter prmVendorNetwork = new SqlParameter("@VendorNetwork", SqlDbType.Decimal);
            prmVendorNetwork.Value = VendorNetwork;
            SqlParameter prmVendorUPS = new SqlParameter("@VendorUPS", SqlDbType.Decimal);
            prmVendorUPS.Value = VendorUPS;
            SqlParameter prmVendorconsumable = new SqlParameter("@Vendorconsumable", SqlDbType.Decimal);
            prmVendorconsumable.Value = Vendorconsumable;
            SqlParameter prmVendorAC = new SqlParameter("@VendorAC", SqlDbType.Decimal);
            prmVendorAC.Value = VendorAC;
            SqlParameter prmVendorTIS = new SqlParameter("@VendorTIS", SqlDbType.Decimal);
            prmVendorTIS.Value = VendorTIS;
            SqlParameter prmVendorHouseKeeping = new SqlParameter("@VendorHouseKeeping", SqlDbType.Decimal);
            prmVendorHouseKeeping.Value = VendorHouseKeeping;
            SqlParameter prmVendorHelpDesk = new SqlParameter("@VendorHelpDesk", SqlDbType.Decimal);
            prmVendorHelpDesk.Value = VendorHelpDesk;
            SqlParameter prmVendorOther = new SqlParameter("@VendorOther", SqlDbType.Decimal);
            prmVendorOther.Value = VendorOther;
            SqlParameter prmCreatedBy = new SqlParameter("@CreatedBy", SqlDbType.Decimal);
            prmCreatedBy.Value = CreatedBy;
            SqlParameter prmModifiedBy = new SqlParameter("@ModifiedBy", SqlDbType.Decimal);
            prmModifiedBy.Value = ModifiedBy;

            int result = DataAccess.Execute
                   (
                    "SP_Insert_SSTMaster_CDMS",
                    prmSSTBankID, prmSSTRegionID, prmSSTBranchID, prmSSTTypeID, prmSSTLinkTypeID,
                    prmSST_Name, prmSST_TerminalID, prmSST_ORGIP, prmSST_NAT_IP, prmSST_AgentPort, prmSST_SiteAddress, prmSST_ServiceArea, prmSST_Status,
                    prmSST_AgentConnectivity_Status, prmReason, prmIsSunday, prmSunday_FromTime, prmSunday_ToTime, prmIsMonday, prmMonday_FromTime, prmMonday_ToTime, prmIsTuesday,
                    prmTuesday_FromTime, prmTuesday_ToTime, prmIswednesday, prmWednesday_FromTime, prmWednesday_ToTime, prmIsThursday, prmThursday_FromTime, prmThursday_ToTime,
                    prmIsFriday, prmFriday_FromTime, prmFriday_ToTime, prmIsSaturday, prmSaturday_FromTime, prmSaturday_ToTime, prmVendorMonitoring, prmVendorSLM, prmVendorFLM,
                    prmVendorCash, prmVendorNetwork, prmVendorUPS, prmVendorconsumable, prmVendorAC, prmVendorTIS, prmVendorHouseKeeping, prmVendorHelpDesk, prmVendorOther, prmCreatedBy, prmModifiedBy
                    );
            return result;

        }

        public int GetSSTIDForSSTProfile(string SST_TerminalID)
        {
            SqlParameter prmSST_TerminalID = new SqlParameter("@SST_TerminalID", SqlDbType.VarChar, 50);
            prmSST_TerminalID.Value = SST_TerminalID;
            return DataAccess.GetInt32("GetSSTIDForSSTProfile", prmSST_TerminalID);
        }
        public decimal SP_RetuenSSTActivityListID(string ActivityGroupName, decimal CreatedBy, decimal ModifiedBy)
        {
            SqlParameter prmActivityGroupName = new SqlParameter("@ActivityGroupName", SqlDbType.VarChar, 50);
            prmActivityGroupName.Value = ActivityGroupName;
            SqlParameter prmCreatedBy = new SqlParameter("@CreatedBy", SqlDbType.Decimal);
            prmCreatedBy.Value = CreatedBy;
            SqlParameter prmModifiedBy = new SqlParameter("@ModifiedBy", SqlDbType.Decimal);
            prmModifiedBy.Value = CreatedBy;
            decimal SSTActivityListID = DataAccess.GetInt32("SP_RetuenSSTActivityListID", prmActivityGroupName, prmCreatedBy, prmModifiedBy);
            return SSTActivityListID;
        }
        public int sp_Insert_SSTGroupMappingDetails_CDMS(decimal SSTGroupID, decimal SSTBankID, string SST_TerminalID, decimal SSTTypeID, decimal SSTLinkTypeID, decimal SSTLinkServiceProvideID, decimal SSTRegionID, decimal SSTBranchID, decimal SSTID, decimal CreatedBy, decimal ModifiedBy, decimal SSTActivityListID)
        {

            SqlParameter prmSSTActivityListID = new SqlParameter("@SSTActivityListID", SqlDbType.VarChar, 50);
            prmSSTActivityListID.Value = SSTActivityListID;
            SqlParameter prmSSTGroupID = new SqlParameter("@SSTGroupID", SqlDbType.Decimal);
            prmSSTGroupID.Value = SSTGroupID;
            SqlParameter prmSSTBankID = new SqlParameter("@SSTBankID", SqlDbType.Decimal);
            prmSSTBankID.Value = SSTBankID;
            SqlParameter prmSST_TerminalID = new SqlParameter("@SST_TerminalID", SqlDbType.VarChar, 50);
            prmSST_TerminalID.Value = SST_TerminalID;
            SqlParameter prmSSTTypeID = new SqlParameter("@SSTTypeID", SqlDbType.Decimal);
            prmSSTTypeID.Value = SSTTypeID;
            SqlParameter prmLinkType = new SqlParameter("@SSTLinkTypeID", SqlDbType.Decimal);
            prmLinkType.Value = SSTLinkTypeID;
            SqlParameter prmLinkProviderName = new SqlParameter("@SSTLinkServiceProvideID", SqlDbType.Decimal);
            prmLinkProviderName.Value = SSTLinkServiceProvideID;
            SqlParameter prmSSTRegionID = new SqlParameter("@SSTRegionID", SqlDbType.Decimal);
            prmSSTRegionID.Value = SSTRegionID;
            SqlParameter prmSSTBranchID = new SqlParameter("@SSTBranchID", SqlDbType.Decimal);
            prmSSTBranchID.Value = SSTBranchID;
            SqlParameter prmSSTID = new SqlParameter("@SSTID", SqlDbType.Decimal);
            prmSSTID.Value = SSTID;
            SqlParameter prmCreatedBy = new SqlParameter("@CreatedBy", SqlDbType.Decimal);
            prmCreatedBy.Value = CreatedBy;
            SqlParameter prmModifiedBy = new SqlParameter("@ModifiedBy", SqlDbType.Decimal);
            prmModifiedBy.Value = ModifiedBy;
            int result = DataAccess.Execute("sp_Insert_SSTGroupMappingDetails_CDMS", prmSSTActivityListID, prmSSTGroupID, prmSSTBankID, prmSST_TerminalID, prmSSTTypeID, prmLinkType, prmLinkProviderName, prmSSTRegionID, prmSSTBranchID, prmSSTID, prmCreatedBy, prmModifiedBy);
            return result;
        }
        public int SP_GetSSTMasterCountForUpdate(decimal SSTID, string SST_TerminalID)
        {

            SqlParameter prmSSTID = new SqlParameter("@SSTID", SqlDbType.Decimal);
            prmSSTID.Value = SSTID;
            SqlParameter prmSST_TerminalID = new SqlParameter("@SST_TerminalID", SqlDbType.VarChar, 50);
            prmSST_TerminalID.Value = SST_TerminalID;
            int x = DataAccess.GetInt32("SP_GetSSTMasterCountForUpdate", prmSSTID, prmSST_TerminalID);
            return x;
        }
        public int SP_UpdateSSTMaster
                             (
                               decimal SSTBankID, decimal SSTRegionID, decimal SSTBranchID, decimal SSTTypeID, decimal SSTLinkTypeID, decimal SSTLinkServiceProvideID, string SST_Name, string SST_TerminalID, string SST_ORGIP, string SST_NAT_IP, decimal SST_AgentPort, string SST_SiteAddress, string SST_ServiceArea, bool SST_Status, string SST_StatusChangeDate, string SST_AgentConnectivityDate, bool SST_AgentConnectivity_Status, string Reason,
                               bool isSunday, string sundayFTime, string sundayTTime, bool isMonday, string mondayFTime, string mondayTTime, bool isTuesday, string tuesdayFTime, string tuesdayTTime,
                               bool isWednesday, string wednesdayFTime, string wednesdayTTime, bool isThursday, string thursdayFTime, string thursdayTTime, bool isFriday,
                               string fridayFTime, string fridayTTime, bool isSaturday, string saturdayFTime, string saturdayTTime, decimal VendorMonitoring, decimal VendorSLM, decimal VendorFLM, decimal VendorCash, decimal VendorNetwork, decimal VendorUPS, decimal Vendorconsumable, decimal VendorAC, decimal VendorTIS,
                               decimal VendorHouseKeeping, decimal VendorHelpDesk, decimal VendorOther, decimal ModifiedBy, decimal SSTID
                           )
        {


            DateTime sundayFromTime = DateTime.Parse("1/1/2001 12:00:00 AM");
            DateTime sundayToTime = DateTime.Parse("1/1/2001 12:00:00 AM");
            DateTime mondayFromTime = DateTime.Parse("1/1/2001 12:00:00 AM");
            DateTime tuesdayFromTime = DateTime.Parse("1/1/2001 12:00:00 AM");
            DateTime tuesdayToTime = DateTime.Parse("1/1/2001 12:00:00 AM");
            DateTime wednesdayFromTime = DateTime.Parse("1/1/2001 12:00:00 AM");
            DateTime thursdayFromTime = DateTime.Parse("1/1/2001 12:00:00 AM");
            DateTime fridayFromTime = DateTime.Parse("1/1/2001 12:00:00 AM");
            DateTime saturdayFromTime = DateTime.Parse("1/1/2001 12:00:00 AM");
            DateTime mondayToTime = DateTime.Parse("1/1/2001 12:00:00 AM");
            DateTime wednesdayToTime = DateTime.Parse("1/1/2001 12:00:00 AM");
            DateTime thursdayToTime = DateTime.Parse("1/1/2001 12:00:00 AM");
            DateTime fridayToTime = DateTime.Parse("1/1/2001 12:00:00 AM");
            DateTime saturdayToTime = DateTime.Parse("1/1/2001 12:00:00 AM");
            SqlParameter prmSunday_FromTime = new SqlParameter("@Sunday_FromTime", SqlDbType.DateTime);
            SqlParameter prmSunday_ToTime = new SqlParameter("@Sunday_ToTime", SqlDbType.DateTime);
            SqlParameter prmMonday_FromTime = new SqlParameter("@Monday_FromTime", SqlDbType.DateTime);
            SqlParameter prmMonday_ToTime = new SqlParameter("@Monday_ToTime", SqlDbType.DateTime);
            SqlParameter prmTuesday_FromTime = new SqlParameter("@Tuesday_FromTime", SqlDbType.DateTime);
            SqlParameter prmTuesday_ToTime = new SqlParameter("@Tuesday_ToTime", SqlDbType.DateTime);
            SqlParameter prmWednesday_FromTime = new SqlParameter("@Wednesday_FromTime", SqlDbType.DateTime);
            SqlParameter prmWednesday_ToTime = new SqlParameter("@Wednesday_ToTime", SqlDbType.DateTime);
            SqlParameter prmThursday_FromTime = new SqlParameter("@Thursday_FromTime", SqlDbType.DateTime);
            SqlParameter prmThursday_ToTime = new SqlParameter("@Thursday_ToTime", SqlDbType.DateTime);
            SqlParameter prmFriday_FromTime = new SqlParameter("@Friday_FromTime", SqlDbType.DateTime);
            SqlParameter prmFriday_ToTime = new SqlParameter("@Friday_ToTime", SqlDbType.DateTime);
            SqlParameter prmSaturday_FromTime = new SqlParameter("@Saturday_FromTime", SqlDbType.DateTime);
            SqlParameter prmSaturday_ToTime = new SqlParameter("@Saturday_ToTime", SqlDbType.DateTime);
            if (sundayFTime == string.Empty)
            {
                prmSunday_FromTime.Value = null;
            }
            else
            {
                string ft = sundayFromTime.ToShortDateString() + " " + sundayFTime;
                sundayFromTime = Convert.ToDateTime(ft);
                prmSunday_FromTime.Value = sundayFromTime;
            }
            if (sundayTTime == string.Empty)
            {
                prmSunday_ToTime.Value = null;
            }
            else
            {
                string tt = sundayToTime.ToShortDateString() + " " + sundayTTime;
                sundayToTime = Convert.ToDateTime(tt);
                prmSunday_ToTime.Value = sundayToTime;
            }
            if (mondayFTime == string.Empty)
            {
                prmMonday_FromTime.Value = null;
            }
            else
            {
                string ft = mondayFromTime.ToShortDateString() + " " + mondayFTime;
                mondayFromTime = Convert.ToDateTime(ft);
                prmMonday_FromTime.Value = mondayFromTime;
            }
            if (mondayTTime == string.Empty)
            {
                prmMonday_ToTime.Value = null;
            }
            else
            {
                string ft = mondayToTime.ToShortDateString() + " " + mondayTTime;
                mondayToTime = Convert.ToDateTime(ft);
                prmMonday_ToTime.Value = mondayToTime;
            }
            if (tuesdayFTime == string.Empty)
            {
                prmTuesday_FromTime.Value = null;
            }
            else
            {
                string ft = tuesdayFromTime.ToShortDateString() + " " + tuesdayFTime;
                tuesdayFromTime = Convert.ToDateTime(ft);
                prmTuesday_FromTime.Value = tuesdayFromTime;
            }

            if (tuesdayTTime == string.Empty)
            {
                prmTuesday_ToTime.Value = null;
            }
            else
            {
                string tt = tuesdayToTime.ToShortDateString() + " " + tuesdayTTime;
                tuesdayToTime = Convert.ToDateTime(tt);
                prmTuesday_ToTime.Value = tuesdayToTime;
            }
            if (wednesdayFTime == string.Empty)
            {
                prmWednesday_FromTime.Value = null;
            }
            else
            {
                string ft = wednesdayFromTime.ToShortDateString() + " " + wednesdayFTime;
                wednesdayFromTime = Convert.ToDateTime(ft);
                prmWednesday_FromTime.Value = wednesdayFromTime;
            }
            if (wednesdayTTime == string.Empty)
            {
                prmWednesday_ToTime.Value = null;
            }
            else
            {
                string tt = wednesdayToTime.ToShortDateString() + " " + wednesdayTTime;
                wednesdayToTime = Convert.ToDateTime(tt);
                prmWednesday_ToTime.Value = wednesdayToTime;
            }
            if (thursdayFTime == string.Empty)
            {
                prmThursday_FromTime.Value = null;
            }
            else
            {
                string ft = thursdayFromTime.ToShortDateString() + " " + thursdayFTime;
                thursdayFromTime = Convert.ToDateTime(ft);
                prmThursday_FromTime.Value = thursdayFromTime;
            }
            if (thursdayTTime == string.Empty)
            {
                prmThursday_ToTime.Value = null;
            }
            else
            {
                string tt = thursdayToTime.ToShortDateString() + " " + thursdayTTime;
                thursdayToTime = Convert.ToDateTime(tt);
                prmThursday_ToTime.Value = thursdayToTime;
            }
            if (fridayFTime == string.Empty)
            {
                prmFriday_FromTime.Value = null;
            }
            else
            {
                string ft = fridayFromTime.ToShortDateString() + " " + fridayFTime;
                fridayFromTime = Convert.ToDateTime(ft);
                prmFriday_FromTime.Value = fridayFromTime;
            }
            if (fridayTTime == string.Empty)
            {
                prmFriday_ToTime.Value = null;
            }
            else
            {
                string tt = fridayToTime.ToShortDateString() + " " + fridayTTime;
                fridayToTime = Convert.ToDateTime(tt);
                prmFriday_ToTime.Value = fridayToTime;
            }
            if (saturdayFTime == string.Empty)
            {
                prmSaturday_FromTime.Value = null;
            }
            else
            {
                string ft = saturdayFromTime.ToShortDateString() + " " + saturdayFTime;
                saturdayFromTime = Convert.ToDateTime(ft);
                prmSaturday_FromTime.Value = saturdayFromTime;
            }
            if (saturdayTTime == string.Empty)
            {
                prmSaturday_ToTime.Value = null;
            }
            else
            {
                string tt = saturdayToTime.ToShortDateString() + " " + saturdayTTime;
                saturdayToTime = Convert.ToDateTime(tt);
                prmSaturday_ToTime.Value = saturdayToTime;
            }

            SqlParameter prmSST_AgentConnectivityDate;
            SqlParameter prmSSTBankID = new SqlParameter("@SSTBankID", SqlDbType.Decimal);
            prmSSTBankID.Value = SSTBankID;
            SqlParameter prmSSTRegionID = new SqlParameter("@SSTRegionID", SqlDbType.Decimal);
            prmSSTRegionID.Value = SSTRegionID;
            SqlParameter prmSSTBranchID = new SqlParameter("@SSTBranchID", SqlDbType.Decimal);
            prmSSTBranchID.Value = SSTBranchID;
            SqlParameter prmSSTTypeID = new SqlParameter("@SSTTypeID", SqlDbType.Decimal);
            prmSSTTypeID.Value = SSTTypeID;
            SqlParameter prmSSTLinkTypeID = new SqlParameter("@SSTLinkTypeID", SqlDbType.Decimal);
            prmSSTLinkTypeID.Value = SSTLinkTypeID;
            //SqlParameter prmSSTLinkServiceProvideID = new SqlParameter("@SSTLinkServiceProvideID", SqlDbType.Decimal);
            //prmSSTLinkServiceProvideID.Value = SSTLinkServiceProvideID;
            SqlParameter prmSST_Name = new SqlParameter("@SST_Name", SqlDbType.VarChar, 50);
            prmSST_Name.Value = SST_Name;
            SqlParameter prmSST_TerminalID = new SqlParameter("@SST_TerminalID", SqlDbType.VarChar, 50);
            prmSST_TerminalID.Value = SST_TerminalID;
            SqlParameter prmSST_ORGIP = new SqlParameter("@SST_ORGIP", SqlDbType.VarChar, 15);
            if (Convert.ToString(SST_ORGIP) == "")
            {
                prmSST_ORGIP.IsNullable = true;
                prmSST_ORGIP.Value = DBNull.Value;
            }
            else
            {
                prmSST_ORGIP.Value = SST_ORGIP;
            }
            SqlParameter prmSST_NAT_IP = new SqlParameter("@SST_NAT_IP", SqlDbType.VarChar, 15);
            prmSST_NAT_IP.Value = SST_NAT_IP;
            SqlParameter prmSST_AgentPort = new SqlParameter("@SST_AgentPort", SqlDbType.Decimal);
            prmSST_AgentPort.Value = SST_AgentPort;
            SqlParameter prmSST_SiteAddress = new SqlParameter("@SST_SiteAddress", SqlDbType.VarChar, 50);
            prmSST_SiteAddress.Value = SST_SiteAddress;
            SqlParameter prmSST_Status = new SqlParameter("@SST_Status", SqlDbType.Bit);
            prmSST_Status.Value = SST_Status;

            SqlParameter prmSST_ServiceArea = new SqlParameter("@SST_ServiceArea", SqlDbType.VarChar, 50);
            prmSST_ServiceArea.Value = SST_ServiceArea;
            SqlParameter prmSST_StatusChangeDate = new SqlParameter("@SST_StatusChangeDate", SqlDbType.SmallDateTime);
            if (SST_StatusChangeDate == "")
            {
                prmSST_StatusChangeDate.Value = DBNull.Value;
            }
            else
            {
                prmSST_StatusChangeDate.Value = Convert.ToDateTime(SST_StatusChangeDate);
            }

            prmSST_AgentConnectivityDate = new SqlParameter("@SST_AgentConnectivityDate", SqlDbType.SmallDateTime);

            if (SST_AgentConnectivityDate == "")
            {
                prmSST_AgentConnectivityDate.Value = DBNull.Value;
            }
            else
            {
                prmSST_AgentConnectivityDate.Value = Convert.ToDateTime(SST_StatusChangeDate);
            }
            SqlParameter prmSST_AgentConnectivity_Status = new SqlParameter("@SST_AgentConnectivity_Status", SqlDbType.Bit);
            prmSST_AgentConnectivity_Status.Value = SST_AgentConnectivity_Status;
            SqlParameter prmReason = new SqlParameter("@Reason", SqlDbType.VarChar, 50);
            prmReason.Value = Reason;
            SqlParameter prmIsSunday = new SqlParameter("@AccTime_Sunday", SqlDbType.Bit);
            prmIsSunday.Value = isSunday;
            SqlParameter prmIsMonday = new SqlParameter("@AccTime_Monday", SqlDbType.Bit);
            prmIsMonday.Value = isMonday;
            SqlParameter prmIsTuesday = new SqlParameter("@AccTime_Tuesday", SqlDbType.Bit);
            prmIsTuesday.Value = isTuesday;
            SqlParameter prmIswednesday = new SqlParameter("@AccTime_Wednesday", SqlDbType.Bit);
            prmIswednesday.Value = isWednesday;
            SqlParameter prmIsThursday = new SqlParameter("@AccTime_Thursday", SqlDbType.Bit);
            prmIsThursday.Value = isThursday;
            SqlParameter prmIsFriday = new SqlParameter("@AccTime_Friday", SqlDbType.Bit);
            prmIsFriday.Value = isFriday;
            SqlParameter prmIsSaturday = new SqlParameter("@AccTime_Saturday", SqlDbType.Bit);
            prmIsSaturday.Value = isSaturday;

            SqlParameter prmVendorMonitoring = new SqlParameter("@VendorMonitoring", SqlDbType.Decimal);
            prmVendorMonitoring.Value = VendorMonitoring;
            SqlParameter prmVendorSLM = new SqlParameter("@VendorSLM", SqlDbType.Decimal);
            prmVendorSLM.Value = VendorSLM;
            SqlParameter prmVendorFLM = new SqlParameter("@VendorFLM", SqlDbType.Decimal);
            prmVendorFLM.Value = VendorFLM;
            SqlParameter prmVendorCash = new SqlParameter("@VendorCash", SqlDbType.Decimal);
            prmVendorCash.Value = VendorCash;
            SqlParameter prmVendorNetwork = new SqlParameter("@VendorNetwork", SqlDbType.Decimal);
            prmVendorNetwork.Value = VendorNetwork;
            SqlParameter prmVendorUPS = new SqlParameter("@VendorUPS", SqlDbType.Decimal);
            prmVendorUPS.Value = VendorUPS;
            SqlParameter prmVendorconsumable = new SqlParameter("@Vendorconsumable", SqlDbType.Decimal);
            prmVendorconsumable.Value = Vendorconsumable;
            SqlParameter prmVendorAC = new SqlParameter("@VendorAC", SqlDbType.Decimal);
            prmVendorAC.Value = VendorAC;
            SqlParameter prmVendorTIS = new SqlParameter("@VendorTIS", SqlDbType.Decimal);
            prmVendorTIS.Value = VendorTIS;
            SqlParameter prmVendorHouseKeeping = new SqlParameter("@VendorHouseKeeping", SqlDbType.Decimal);
            prmVendorHouseKeeping.Value = VendorHouseKeeping;
            SqlParameter prmVendorHelpDesk = new SqlParameter("@VendorHelpDesk", SqlDbType.Decimal);
            prmVendorHelpDesk.Value = VendorHelpDesk;
            SqlParameter prmVendorOther = new SqlParameter("@VendorOther", SqlDbType.Decimal);
            prmVendorOther.Value = VendorOther;


            SqlParameter prmModifiedBy = new SqlParameter("@ModifiedBy", SqlDbType.Decimal);
            prmModifiedBy.Value = ModifiedBy;
            SqlParameter prmSSTID = new SqlParameter("@SSTID", SqlDbType.Decimal);
            prmSSTID.Value = (SSTID);
            int x = DataAccess.Execute
                                     ("SP_UpdateSSTMaster",
                                     prmSSTBankID, prmSSTRegionID, prmSSTBranchID, prmSSTTypeID, prmSSTLinkTypeID,
                                     prmSST_Name, prmSST_TerminalID, prmSST_ORGIP, prmSST_NAT_IP, prmSST_AgentPort, prmSST_SiteAddress, prmSST_ServiceArea, prmSST_Status,
                                     prmSST_StatusChangeDate, prmSST_AgentConnectivityDate, prmSST_AgentConnectivity_Status, prmReason, prmIsSunday, prmSunday_FromTime, prmSunday_ToTime, prmIsMonday, prmMonday_FromTime, prmMonday_ToTime, prmIsTuesday,
                                     prmTuesday_FromTime, prmTuesday_ToTime, prmIswednesday, prmWednesday_FromTime, prmWednesday_ToTime, prmIsThursday, prmThursday_FromTime, prmThursday_ToTime,
                                     prmIsFriday, prmFriday_FromTime, prmFriday_ToTime, prmIsSaturday, prmSaturday_FromTime, prmSaturday_ToTime, prmVendorMonitoring, prmVendorSLM, prmVendorFLM,
                                     prmVendorCash, prmVendorNetwork, prmVendorUPS, prmVendorconsumable, prmVendorAC, prmVendorTIS, prmVendorHouseKeeping, prmVendorHelpDesk, prmVendorOther, prmModifiedBy, prmSSTID
                                     );
            return x;
        }


















        public int SP_Update_SSTGroupMappingDetails_CDMSForSSTProfile(decimal SSTGroupID, decimal SSTBankID, string SST_TerminalID, decimal SSTTypeID, decimal SSTLinkTypeID, decimal SSTLinkServiceProvideID, decimal SSTRegionID, decimal SSTBranchID, decimal ModifiedBy, decimal CreatedBy, decimal SSTID, string ActivityGroupName, decimal SSTActivityListID)
        {
            int result = 1;

            SqlParameter prmSSTGroupID = new SqlParameter("@SSTGroupID", SqlDbType.Decimal);
            prmSSTGroupID.Value = SSTGroupID;
            SqlParameter prmSSTID = new SqlParameter("@SSTID", SqlDbType.Decimal);
            prmSSTID.Value = SSTID;
            SqlParameter prmSSTBankID = new SqlParameter("@SSTBankID", SqlDbType.Decimal);
            prmSSTBankID.Value = SSTBankID;
            SqlParameter prmSST_TerminalID = new SqlParameter("@SST_TerminalID", SqlDbType.VarChar, 25);
            prmSST_TerminalID.Value = SST_TerminalID;
            SqlParameter prmSSTTypeID = new SqlParameter("@SSTTypeID", SqlDbType.Decimal);
            prmSSTTypeID.Value = SSTTypeID;
            SqlParameter prmLinkType = new SqlParameter("@SSTLinkTypeID", SqlDbType.Decimal);
            prmLinkType.Value = SSTLinkTypeID;
            SqlParameter prmLinkProviderName = new SqlParameter("@SSTLinkServiceProvideID", SqlDbType.Decimal);
            prmLinkProviderName.Value = SSTLinkServiceProvideID;//
            SqlParameter prmSSTRegionID = new SqlParameter("@SSTRegionID", SqlDbType.Decimal);
            prmSSTRegionID.Value = SSTRegionID;
            SqlParameter prmSSTBranchID = new SqlParameter("@SSTBranchID", SqlDbType.Decimal);
            prmSSTBranchID.Value = SSTBranchID;
            SqlParameter prmModifiedBy = new SqlParameter("@ModifiedBy", SqlDbType.Decimal);
            prmModifiedBy.Value = ModifiedBy;
            SqlParameter prmCreatedBy = new SqlParameter("@CreatedBy", SqlDbType.Decimal);
            prmCreatedBy.Value = CreatedBy;
            SqlParameter prmActivityGroupName = new SqlParameter("@ActivityGroupName", SqlDbType.VarChar, 25);
            prmActivityGroupName.Value = ActivityGroupName;
            SqlParameter prmSSTActivityListID = new SqlParameter("@SSTActivityListID", SqlDbType.VarChar, 25);
            prmSSTActivityListID.Value = SSTActivityListID;
            result = DataAccess.Execute("SP_Update_SSTGroupMappingDetails_CDMSForSSTProfile", prmSSTGroupID, prmSSTBankID, prmSST_TerminalID, prmSSTTypeID, prmLinkType, prmLinkProviderName, prmSSTRegionID, prmSSTBranchID, prmModifiedBy, prmCreatedBy, prmSSTID, prmActivityGroupName, prmSSTActivityListID);
            return result;
        }
        public int SP_DeleteSSTGroup_Table_All(decimal SSTGroupID, decimal SSTID)
        {
            SqlParameter prmSSTGroupID = new SqlParameter("@SSTGroupID", SqlDbType.Decimal);
            prmSSTGroupID.Value = SSTGroupID;
            SqlParameter prmSSTID = new SqlParameter("@SSTID", SqlDbType.Decimal);
            prmSSTID.Value = SSTID;
            int x = DataAccess.Execute("SP_DeleteSSTGroup_Table_All", prmSSTGroupID, prmSSTID);
            return x;
        }
        #endregion
        #region Pull EJ SST Profile
        public int SP_AddNewActivityGroupEJ(string ActivityGroupName, string ActivityGroupDescription, bool ReadOnly, decimal CreatedBy, decimal ModifiedBy)
        {
            SqlParameter prmActivityGroupName = new SqlParameter("@ActivityGroupName", SqlDbType.VarChar, 50);
            prmActivityGroupName.Value = ActivityGroupName;
            SqlParameter prmActivityGroupDescription = new SqlParameter("@ActivityGroupDescription", SqlDbType.VarChar, 50);
            prmActivityGroupDescription.Value = ActivityGroupDescription;
            SqlParameter prmReadOnly = new SqlParameter("@ReadOnly", SqlDbType.Bit);
            prmReadOnly.Value = ReadOnly;
            SqlParameter prmCreatedBy = new SqlParameter("@CreatedBy", SqlDbType.Decimal);
            prmCreatedBy.Value = CreatedBy;
            SqlParameter prmModifiedBy = new SqlParameter("@ModifiedBy", SqlDbType.Decimal);
            prmModifiedBy.Value = ModifiedBy;
            int result = DataAccess.Execute("SP_AddNewActivityGroup", prmActivityGroupName, prmActivityGroupDescription, prmReadOnly, prmCreatedBy, prmModifiedBy);
            return result;
        }

        #endregion
        #region Vendor Master
        public int SP_InsertVendorInfo(string VendorName, string VendorType, string VendorCode, string VendorRegNumber, string VendorAddress, decimal VendorCity, string VendorContactName1, string VendorContactName2, string VendorContactNumber1, string VendorContactNumber2, string VendorEmailID1, string VendorEmailID2, string DistanceFromSite, string OtherInforamtion, decimal LinkType, decimal CreatedBy, decimal ModifiedBy)
        {
            int result = 0;
            SqlParameter prmVendorName = new SqlParameter("@VendorName", SqlDbType.VarChar, 50);
            prmVendorName.Value = VendorName;
            SqlParameter prmVendorType = new SqlParameter("@VendorType", SqlDbType.VarChar, 50);
            prmVendorType.Value = VendorType;
            SqlParameter prmVendorCode = new SqlParameter("@VendorCode", SqlDbType.VarChar, 50);
            prmVendorCode.Value = VendorCode;
            SqlParameter prmVendorRegNumber = new SqlParameter("@VendorRegNumber", SqlDbType.VarChar, 50);
            prmVendorRegNumber.Value = VendorRegNumber;
            SqlParameter prmVendorAddress = new SqlParameter("@VendorAddress", SqlDbType.VarChar, 300);
            prmVendorAddress.Value = VendorAddress;
            SqlParameter prmVendorCity = new SqlParameter("@VendorCity", SqlDbType.Decimal);
            prmVendorCity.Value = VendorCity;
            SqlParameter prmVendorContactName1 = new SqlParameter("@VendorContactName1", SqlDbType.VarChar, 50);
            prmVendorContactName1.Value = VendorContactName1;
            SqlParameter prmVendorContactName2 = new SqlParameter("@VendorContactName2", SqlDbType.VarChar, 50);
            prmVendorContactName2.Value = VendorContactName2;
            SqlParameter prmVendorContactNumber1 = new SqlParameter("@VendorContactNumber1", SqlDbType.VarChar, 15);
            prmVendorContactNumber1.Value = VendorContactNumber1;
            SqlParameter prmVendorContactNumber2 = new SqlParameter("@VendorContactNumber2", SqlDbType.VarChar, 15);
            prmVendorContactNumber2.Value = VendorContactNumber2;
            SqlParameter prmVendorEmailID1 = new SqlParameter("@VendorEmailID1", SqlDbType.VarChar, 50);
            prmVendorEmailID1.Value = VendorEmailID1;
            SqlParameter prmVendorEmailID2 = new SqlParameter("@VendorEmailID2", SqlDbType.VarChar, 50);
            prmVendorEmailID2.Value = VendorEmailID2;
            SqlParameter prmDistanceFromSite = new SqlParameter("@DistanceFromSite", SqlDbType.VarChar, 50);
            prmDistanceFromSite.Value = DistanceFromSite;
            SqlParameter prmOtherInforamtion = new SqlParameter("@OtherInforamtion", SqlDbType.VarChar, 100);
            prmOtherInforamtion.Value = OtherInforamtion;
            SqlParameter prmLinkType = new SqlParameter("@LinkType", SqlDbType.Decimal);
            prmLinkType.Value = LinkType;
            SqlParameter prmCreatedBy = new SqlParameter("@CreatedBy", SqlDbType.Decimal);
            prmCreatedBy.Value = CreatedBy;
            SqlParameter prmModifiedBy = new SqlParameter("@ModifiedBy", SqlDbType.Decimal);
            prmModifiedBy.Value = ModifiedBy;
            result = DataAccess.Execute("SP_InsertVendorInfo", prmVendorName, prmVendorType, prmVendorCode, prmVendorRegNumber, prmVendorAddress,
                                        prmVendorCity, prmVendorContactName1, prmVendorContactName2, prmVendorContactNumber1, prmVendorContactNumber2,
                                        prmVendorEmailID1, prmVendorEmailID2, prmDistanceFromSite, prmOtherInforamtion, prmLinkType, prmCreatedBy
                                        , prmModifiedBy);
            return result;
        }
        public DataTable SP_IsUniqueVendorInfo(string vendorName, string vendorType)
        {
            SqlParameter prmVendorName = new SqlParameter("@VendorName", SqlDbType.VarChar, 50);
            prmVendorName.Value = vendorName;
            SqlParameter prmVendorType = new SqlParameter("@VendorType", SqlDbType.VarChar, 50);
            prmVendorType.Value = vendorType;
            return DataAccess.GetFromDataTable("SP_IsUniqueVendorInfo", prmVendorName, prmVendorType);
        }
        public DataTable SP_IsUniqueVendorInfo_Update(string vendorName, string vendorType, decimal vendorID)
        {
            SqlParameter prmVendorName = new SqlParameter("@VendorName", SqlDbType.VarChar, 50);
            prmVendorName.Value = vendorName;
            SqlParameter prmVendorType = new SqlParameter("@VendorType", SqlDbType.VarChar, 50);
            prmVendorType.Value = vendorType;
            SqlParameter prmVendorID = new SqlParameter("@VendorID", SqlDbType.Decimal, 4);
            prmVendorID.Value = vendorID;
            return DataAccess.GetFromDataTable("SP_IsUniqueVendorInfo_Update", prmVendorName, prmVendorType, prmVendorID);
        }
        public int SP_UpdateVendorInfo(decimal vendorID, string VendorName, string VendorType, string VendorCode, string VendorRegNumber, string VendorAddress, decimal VendorCity, string VendorContactName1, string VendorContactName2, string VendorContactNumber1, string VendorContactNumber2, string VendorEmailID1, string VendorEmailID2, string DistanceFromSite, string OtherInforamtion, decimal LinkType, decimal ModifiedBy)
        {
            SqlParameter prmLinkType = new SqlParameter("@LinkType", SqlDbType.Decimal);
            prmLinkType.Value = LinkType;
            int result = 0;
            SqlParameter prmVendorID = new SqlParameter("@VendorID", SqlDbType.Decimal);
            prmVendorID.Value = vendorID;
            SqlParameter prmVendorName = new SqlParameter("@VendorName", SqlDbType.VarChar, 50);
            prmVendorName.Value = VendorName;
            SqlParameter prmVendorType = new SqlParameter("@VendorType", SqlDbType.VarChar, 50);
            prmVendorType.Value = VendorType;
            SqlParameter prmVendorCode = new SqlParameter("@VendorCode", SqlDbType.VarChar, 50);
            prmVendorCode.Value = VendorCode;
            SqlParameter prmVendorRegNumber = new SqlParameter("@VendorRegNumber", SqlDbType.VarChar, 50);
            prmVendorRegNumber.Value = VendorRegNumber;
            SqlParameter prmVendorAddress = new SqlParameter("@VendorAddress", SqlDbType.VarChar, 300);
            prmVendorAddress.Value = VendorAddress;
            SqlParameter prmVendorCity = new SqlParameter("@VendorCity", SqlDbType.Decimal);
            prmVendorCity.Value = VendorCity;
            SqlParameter prmVendorContactName1 = new SqlParameter("@VendorContactName1", SqlDbType.VarChar, 50);
            prmVendorContactName1.Value = VendorContactName1;
            SqlParameter prmVendorContactName2 = new SqlParameter("@VendorContactName2", SqlDbType.VarChar, 50);
            prmVendorContactName2.Value = VendorContactName2;
            SqlParameter prmVendorContactNumber1 = new SqlParameter("@VendorContactNumber1", SqlDbType.VarChar, 15);
            prmVendorContactNumber1.Value = VendorContactNumber1;
            SqlParameter prmVendorContactNumber2 = new SqlParameter("@VendorContactNumber2", SqlDbType.VarChar, 15);
            prmVendorContactNumber2.Value = VendorContactNumber2;
            SqlParameter prmVendorEmailID1 = new SqlParameter("@VendorEmailID1", SqlDbType.VarChar, 50);
            prmVendorEmailID1.Value = VendorEmailID1;
            SqlParameter prmVendorEmailID2 = new SqlParameter("@VendorEmailID2", SqlDbType.VarChar, 50);
            prmVendorEmailID2.Value = VendorEmailID2;
            SqlParameter prmDistanceFromSite = new SqlParameter("@DistanceFromSite", SqlDbType.VarChar, 50);
            prmDistanceFromSite.Value = DistanceFromSite;
            SqlParameter prmOtherInforamtion = new SqlParameter("@OtherInforamtion", SqlDbType.VarChar, 100);
            prmOtherInforamtion.Value = OtherInforamtion;
            SqlParameter prmModifiedBy = new SqlParameter("@ModifiedBy", SqlDbType.Decimal);
            prmModifiedBy.Value = ModifiedBy;
            result = DataAccess.Execute("SP_UpdateVendorInfo", prmVendorID, prmVendorName, prmVendorType, prmVendorCode, prmVendorRegNumber, prmVendorAddress,
                                        prmVendorCity, prmVendorContactName1, prmVendorContactName2, prmVendorContactNumber1, prmVendorContactNumber2,
                                        prmVendorEmailID1, prmVendorEmailID2, prmDistanceFromSite, prmOtherInforamtion, prmLinkType, prmModifiedBy);
            return result;
        }
        public DataTable SP_GetVendorInfo_Update(decimal vendorID)
        {
            SqlParameter prmVendorId = new SqlParameter("@VendorID", SqlDbType.Decimal);
            prmVendorId.Value = vendorID;
            return DataAccess.GetFromDataTable("SP_GetVendorInfo_Update", prmVendorId);
        }

        #endregion
        #region EditVendor
        public DataTable SP_GetVendorInfoAll(string SqlQuary)
        {
            SqlParameter prmSqlQuary = new SqlParameter("@ExecQuary", SqlDbType.VarChar, 6000);
            prmSqlQuary.Value = SqlQuary;

            return DataAccess.GetFromDataTable("SP_GetVendorInfoAll", prmSqlQuary);
        }
        public int SP_CheckDependancyVendorMaster(decimal vendorID)
        {
            SqlParameter prmVendorID = new SqlParameter("@VendorID", SqlDbType.Decimal);
            prmVendorID.Value = vendorID;
            int id = DataAccess.Execute("SP_CheckDependancyVendorMaster", prmVendorID);
            return id;
        }
        public int SP_DeleteVendorInfo(decimal vendorID)
        {
            SqlParameter prmVendorID = new SqlParameter("@VendorID", SqlDbType.Decimal);
            prmVendorID.Value = vendorID;
            int res = DataAccess.Execute("SP_DeleteVendorInfo", prmVendorID);
            return res;
        }
        #endregion
        /********************************* GAURAV *********************************/

        #region Device Type Stored Procedure

        public DataTable ShowDeviceType()
        {
            DataTable dtDevType = new DataTable();
            dtDevType = DataAccess.GetFromDataTable("SP_ShowDeviceTypeTable");
            return dtDevType;
        }
        public DataTable DeleteDeviceType(decimal DevTypeID)
        {
            DataTable dtDevType = new DataTable();
            SqlParameter prmDevTypeID = new SqlParameter("@DevTypeID", SqlDbType.Decimal, 4);
            prmDevTypeID.Value = DevTypeID;
            dtDevType = DataAccess.GetFromDataTable("SP_DeleteDeviceType", prmDevTypeID);
            return dtDevType;
        }
        public DataTable FillDeviceType(decimal DevTypeID)
        {
            DataTable dtDevType = new DataTable();
            SqlParameter prmDevTypeID = new SqlParameter("@DevTypeID", SqlDbType.Decimal, 4);
            prmDevTypeID.Value = DevTypeID;
            dtDevType = DataAccess.GetFromDataTable("SP_FillDeviceType", prmDevTypeID);
            return dtDevType;
        }
        public int DeviceTypeTable_Update(decimal DevTypeID, string DevType, int SST_TypeID, string DevMake, string Description, decimal CreatedBy, decimal ModifiedBy)
        {
            SqlParameter prmDevTypeID = new SqlParameter("@DeviceTypeID", SqlDbType.Decimal, 4);
            prmDevTypeID.Value = DevTypeID;
            SqlParameter prmDevTyp = new SqlParameter("@DeviceType", SqlDbType.VarChar, 50);
            prmDevTyp.Value = DevType;
            SqlParameter prmDevMake = new SqlParameter("@DeviceMake", SqlDbType.VarChar, 150);
            prmDevMake.Value = DevMake;
            SqlParameter prmDescription = new SqlParameter("@Description", SqlDbType.VarChar, 300);
            prmDescription.Value = Description;
            SqlParameter prmCreatedBy = new SqlParameter("@CreatedBy", SqlDbType.Decimal, 4);
            prmCreatedBy.Value = CreatedBy;
            SqlParameter prmModifiedBy = new SqlParameter("@ModifiedBy", SqlDbType.Decimal, 4);
            prmModifiedBy.Value = ModifiedBy;
            SqlParameter prmSST_TypeID = new SqlParameter("@SST_TypeID", SqlDbType.Int);
            prmSST_TypeID.Value = SST_TypeID;
            return DataAccess.Execute("SP_UpdateDeviceTypeMasterTable", prmDevTypeID, prmDevTyp, prmSST_TypeID, prmDevMake, prmDescription, prmCreatedBy, prmModifiedBy);
        }
        public DataTable DeviceTypeTable_Insert(string DevType, int SST_TypeID, string DevMake, string Description, decimal CreatedBy, decimal ModifiedBy)
        {
            DataTable dtDevTyp = new DataTable();
            SqlParameter prmDevTyp = new SqlParameter("@DeviceType", SqlDbType.VarChar, 50);
            prmDevTyp.Value = DevType;
            SqlParameter prmDevMake = new SqlParameter("@DeviceMake", SqlDbType.VarChar, 150);
            prmDevMake.Value = DevMake;
            SqlParameter prmDescription = new SqlParameter("@Description", SqlDbType.VarChar, 300);
            prmDescription.Value = Description;
            SqlParameter prmCreatedBy = new SqlParameter("@CreatedBy", SqlDbType.Decimal, 4);
            prmCreatedBy.Value = CreatedBy;
            SqlParameter prmModifiedBy = new SqlParameter("@ModifiedBy", SqlDbType.Decimal, 4);
            prmModifiedBy.Value = ModifiedBy;
            SqlParameter prmSST_TypeID = new SqlParameter("@SST_TypeID", SqlDbType.Int);
            prmSST_TypeID.Value = SST_TypeID;
            dtDevTyp = DataAccess.GetFromDataTable("SP_InsertInDeviceTypeMasterTable", prmDevTyp, prmSST_TypeID, prmDevMake, prmDescription, prmCreatedBy, prmModifiedBy);
            return dtDevTyp;
        }

        #endregion

        #region Device Stored Procedure

        public DataTable FillDeviceType()
        {
            DataTable dtDevType = new DataTable();
            dtDevType = DataAccess.GetFromDataTable("SP_Fill_DeviceType");
            return dtDevType;
        }
        public DataTable DeleteDevice(decimal DeviceID)
        {
            DataTable dtDevice = new DataTable();
            SqlParameter prmDeviceID = new SqlParameter("@DeviceId", SqlDbType.Decimal, 4);
            prmDeviceID.Value = DeviceID;
            dtDevice = DataAccess.GetFromDataTable("SP_Delete_DeviceMaster", prmDeviceID);
            return dtDevice;
        }
        public DataTable ShowDeviceInfo()
        {
            return DataAccess.GetFromDataTable("SP_Show_Device_Info");
        }
        public DataTable DeviceTable_Insert(string Device, decimal DeviceTypeID, string Description, decimal CreatedBy, decimal ModifiedBy)
        {
            DataTable dtDevice = new DataTable();
            SqlParameter prmDevice = new SqlParameter("@Device", SqlDbType.VarChar, 1000);
            prmDevice.Value = Device;
            SqlParameter prmDeviceTypeID = new SqlParameter("@DeviceTypeID", SqlDbType.Decimal, 4);
            prmDeviceTypeID.Value = DeviceTypeID;
            SqlParameter prmDescription = new SqlParameter("@Description", SqlDbType.VarChar, 1000);
            prmDescription.Value = Description;
            SqlParameter prmCreatedBy = new SqlParameter("@CreatedBy", SqlDbType.Decimal, 4);
            prmCreatedBy.Value = CreatedBy;
            SqlParameter prmModifiedBy = new SqlParameter("@ModifiedBy", SqlDbType.Decimal, 4);
            prmModifiedBy.Value = ModifiedBy;
            dtDevice = DataAccess.GetFromDataTable("SP_Insert_DeviceMaster", prmDevice, prmDeviceTypeID, prmDescription, prmCreatedBy, prmModifiedBy);
            return dtDevice;
        }
        public int DeviceTable_Update(decimal DeviceID, string Device, decimal DeviceTypeID, string Description, decimal ModifiedBy)
        {
            SqlParameter prmDeviceID = new SqlParameter("@DeviceID", SqlDbType.Decimal, 4);
            prmDeviceID.Value = DeviceID;
            SqlParameter prmDevice = new SqlParameter("@Device", SqlDbType.VarChar, 1000);
            prmDevice.Value = Device;
            SqlParameter prmDeviceTypeID = new SqlParameter("@DeviceTypeID", SqlDbType.Decimal, 4);
            prmDeviceTypeID.Value = DeviceTypeID;
            SqlParameter prmDescription = new SqlParameter("@Description", SqlDbType.VarChar, 1000);
            prmDescription.Value = Description;
            SqlParameter prmModifiedBy = new SqlParameter("@ModifiedBy", SqlDbType.Decimal, 4);
            prmModifiedBy.Value = ModifiedBy;
            int UpdtRslt = DataAccess.Execute("SP_Update_DeviceMaster", prmDeviceID, prmDevice, prmDeviceTypeID, prmDescription, prmModifiedBy);
            return UpdtRslt;
        }
        public DataTable FillDevice(decimal DeviceID)
        {
            DataTable dtDevice = new DataTable();
            SqlParameter prmDeviceID = new SqlParameter("@DeviceID", SqlDbType.Decimal, 4);
            prmDeviceID.Value = DeviceID;
            dtDevice = DataAccess.GetFromDataTable("SP_Fill_DeviceInfo", prmDeviceID);
            return dtDevice;
        }

        #endregion

        #region Device Status Type Stored Procedure

        public DataTable ShowDeviceStatusType()
        {
            DataTable dtDevType = new DataTable();
            dtDevType = DataAccess.GetFromDataTable("SP_ShowDeviceStatusType");
            return dtDevType;
        }
        public DataTable DeleteDeviceStatusType(decimal DeviceStausTypeID)
        {
            DataTable dtDeviceStatusType = new DataTable();
            SqlParameter prmDeviceTypeID = new SqlParameter("@StatusTypeID", SqlDbType.Decimal, 4);
            prmDeviceTypeID.Value = DeviceStausTypeID;
            dtDeviceStatusType = DataAccess.GetFromDataTable("SP_Delete_DeviceStatusType", prmDeviceTypeID);
            return dtDeviceStatusType;
        }
        public DataTable DeviceStatusTypeTable_Insert(string StatusType, string Description, decimal CreatedBy, decimal ModifiedBy)
        {
            DataTable dtDevTyp = new DataTable();
            SqlParameter prmStatusType = new SqlParameter("@StatusType", SqlDbType.VarChar, 100);
            prmStatusType.Value = StatusType;

            SqlParameter prmDescription = new SqlParameter("@Description", SqlDbType.VarChar, 300);
            prmDescription.Value = Description;
            SqlParameter prmCreatedBy = new SqlParameter("@CreatedBy", SqlDbType.Decimal, 4);
            prmCreatedBy.Value = CreatedBy;
            SqlParameter prmModifiedBy = new SqlParameter("@ModifiedBy", SqlDbType.Decimal, 4);
            prmModifiedBy.Value = ModifiedBy;
            dtDevTyp = DataAccess.GetFromDataTable("SP_Insert_DeviceStatusType", prmStatusType, prmDescription, prmCreatedBy, prmModifiedBy);
            return dtDevTyp;
        }
        public DataTable FillDeviceStatusType(decimal StatusTypeID)
        {
            DataTable dtDeviceStatusType = new DataTable();
            SqlParameter prmStatusTypeID = new SqlParameter("@StatusTypeId", SqlDbType.Decimal, 4);
            prmStatusTypeID.Value = StatusTypeID;
            dtDeviceStatusType = DataAccess.GetFromDataTable("SP_Fill_DeviceStatusType", prmStatusTypeID);
            return dtDeviceStatusType;
        }
        public int DeviceStatusTypeTable_Update(decimal StatusTypeID, string StatusType, string Description, decimal ModifiedBy)
        {
            SqlParameter prmStatusTypeID = new SqlParameter("@StatusTypeId", SqlDbType.Decimal, 4);
            prmStatusTypeID.Value = StatusTypeID;

            SqlParameter prmStatusType = new SqlParameter("@StatusType", SqlDbType.VarChar, 100);
            prmStatusType.Value = StatusType;

            SqlParameter prmDescription = new SqlParameter("@Description", SqlDbType.VarChar, 300);
            prmDescription.Value = Description;

            SqlParameter prmModifiedBy = new SqlParameter("@ModifiedBy", SqlDbType.Decimal, 4);
            prmModifiedBy.Value = ModifiedBy;

            return DataAccess.Execute("SP_Update_DeviceStatusType", prmStatusTypeID, prmStatusType, prmDescription, prmModifiedBy);
        }
        #endregion

        #region Device Status Stored Procedure

        public DataTable FillDevices(decimal DeviceTypeID)
        {
            DataTable dtDevice = new DataTable();
            SqlParameter prmDeviceTypeID = new SqlParameter("@DeviceTypeID", SqlDbType.Decimal, 4);
            prmDeviceTypeID.Value = DeviceTypeID;
            dtDevice = DataAccess.GetFromDataTable("SP_Fill_Devices", prmDeviceTypeID);
            return dtDevice;
        }
        public DataTable FillStatusType()
        {
            return DataAccess.GetFromDataTable("SP_FillDeviceTypeStatus");
        }

        public DataTable ShowDeviceStatus(string SelectStatement)
        {
            SqlParameter prmStatement = new SqlParameter("@SelectStmt", SqlDbType.VarChar, 2000);
            prmStatement.Value = SelectStatement;
            return DataAccess.GetFromDataTable("Sp_Show_DeviceStatus", prmStatement);
        }

        public DataTable DeleteDeviceStatus(decimal StatusID)
        {
            SqlParameter prmStatusID = new SqlParameter("@DeviceStatusID", SqlDbType.Decimal, 4);
            prmStatusID.Value = StatusID;
            return DataAccess.GetFromDataTable("SP_Delete_DeviceStatus", prmStatusID);
        }

        public DataTable ShowDeviceStatusInfo(decimal StatusID)
        {
            SqlParameter prmStatusID = new SqlParameter("@DeviceStatusID", SqlDbType.Decimal, 4);
            prmStatusID.Value = StatusID;
            return DataAccess.GetFromDataTable("SP_Show_DeviceStatusInfo", prmStatusID);
        }

        public DataTable InsertInDeviceStatus(decimal DevTypeID, decimal DevID, decimal StatusTypeID, decimal NoRecur, decimal CreatedBy, decimal ModifiedBy, bool CreateTT, bool CloseTT,
          string Status, string Description, string Severity, bool IgnoreTT)
        {

            SqlParameter prmDeviceTypeID = new SqlParameter("@DeviceTypeID", SqlDbType.Decimal, 4);
            prmDeviceTypeID.Value = DevTypeID;

            SqlParameter prmDeviceTID = new SqlParameter("@DeviceID", SqlDbType.Decimal, 4);
            prmDeviceTID.Value = DevID;

            SqlParameter prmStatusTypeID = new SqlParameter("@StatusTypeID", SqlDbType.Decimal, 4);
            prmStatusTypeID.Value = StatusTypeID;

            SqlParameter prmNoRecur = new SqlParameter("@No_Recurrences", SqlDbType.Decimal, 4);
            prmNoRecur.Value = NoRecur;

            SqlParameter prmCreatedBy = new SqlParameter("@CreatedBy", SqlDbType.Decimal, 4);
            prmCreatedBy.Value = CreatedBy;

            SqlParameter prmModifiedBy = new SqlParameter("@ModifiedBy", SqlDbType.Decimal, 4);
            prmModifiedBy.Value = ModifiedBy;

            SqlParameter prmStatus = new SqlParameter("@Status", SqlDbType.VarChar, 500);
            prmStatus.Value = Status;

            SqlParameter prmDescription = new SqlParameter("@Description", SqlDbType.VarChar, 300);
            prmDescription.Value = Description;

            SqlParameter prmSeverity = new SqlParameter("@Severity", SqlDbType.VarChar, 50);
            prmSeverity.Value = Severity;


            SqlParameter prmCreateTT = new SqlParameter("@CreateTT", SqlDbType.Bit);
            prmCreateTT.Value = CreateTT;

            SqlParameter prmCloseTT = new SqlParameter("@CloseTT", SqlDbType.Bit);
            prmCloseTT.Value = CloseTT;
            SqlParameter prmIgnoreTT = new SqlParameter("@IgnoreTT", SqlDbType.Bit);
            prmIgnoreTT.Value = IgnoreTT;

            return DataAccess.GetFromDataTable("SP_Insert_DeviceStatus", prmCloseTT, prmCreateTT, prmSeverity, prmDescription, prmStatus, prmModifiedBy, prmCreatedBy, prmNoRecur, prmStatusTypeID
              , prmDeviceTID, prmDeviceTypeID, prmIgnoreTT);
        }



        public int UpdateDeviceStatus(decimal DevStatusID, decimal DevTypeID, decimal DevID, decimal StatusTypeID, decimal NoRecur, decimal ModifiedBy, bool CreateTT, bool CloseTT,
       string Status, string Description, string Severity, bool IgnoreTT)
        {

            SqlParameter prmDevStatusID = new SqlParameter("@DeviceStatusID", SqlDbType.Decimal, 4);
            prmDevStatusID.Value = DevStatusID;

            SqlParameter prmDeviceTypeID = new SqlParameter("@DeviceTypeID", SqlDbType.Decimal, 4);
            prmDeviceTypeID.Value = DevTypeID;

            SqlParameter prmDeviceTID = new SqlParameter("@DeviceID", SqlDbType.Decimal, 4);
            prmDeviceTID.Value = DevID;

            SqlParameter prmStatusTypeID = new SqlParameter("@StatusTypeID", SqlDbType.Decimal, 4);
            prmStatusTypeID.Value = StatusTypeID;

            SqlParameter prmNoRecur = new SqlParameter("@No_Recurrences", SqlDbType.Decimal, 4);
            prmNoRecur.Value = NoRecur;


            SqlParameter prmModifiedBy = new SqlParameter("@ModifiedBy", SqlDbType.Decimal, 4);
            prmModifiedBy.Value = ModifiedBy;

            SqlParameter prmStatus = new SqlParameter("@Status", SqlDbType.VarChar, 500);
            prmStatus.Value = Status;

            SqlParameter prmDescription = new SqlParameter("@Description", SqlDbType.VarChar, 300);
            prmDescription.Value = Description;

            SqlParameter prmSeverity = new SqlParameter("@Severity", SqlDbType.VarChar, 50);
            prmSeverity.Value = Severity;


            SqlParameter prmCreateTT = new SqlParameter("@CreateTT", SqlDbType.Bit);
            prmCreateTT.Value = CreateTT;

            SqlParameter prmCloseTT = new SqlParameter("@CloseTT", SqlDbType.Bit);
            prmCloseTT.Value = CloseTT;
            SqlParameter prmIgnoreTT = new SqlParameter("@IgnoreTT", SqlDbType.Bit);
            prmIgnoreTT.Value = IgnoreTT;


            return DataAccess.Execute("SP_Update_DeviceStatus", prmDevStatusID, prmCloseTT, prmCreateTT, prmSeverity, prmDescription, prmStatus, prmModifiedBy, prmNoRecur, prmStatusTypeID
              , prmDeviceTID, prmDeviceTypeID, prmIgnoreTT);
        }

        #endregion

        #region Problem Category Storeds Procedure

        public DataTable ShowProblem()
        {
            return DataAccess.GetFromDataTable("SP_Show_ProblemCategory");
        }
        public DataTable DeleteProblemCategory(decimal ProblemID)
        {
            SqlParameter prmProblemID = new SqlParameter("@ProblemID", SqlDbType.Decimal, 4);
            prmProblemID.Value = ProblemID;
            return DataAccess.GetFromDataTable("SP_Delete_ProblemCategory", prmProblemID);
        }
        public DataTable InsertProblemCategory(string Problem, string Description, string ColorCode, decimal CreatedBy)
        {
            SqlParameter prmCreatedBy = new SqlParameter("@UserID", SqlDbType.Decimal, 4);
            prmCreatedBy.Value = CreatedBy;
            SqlParameter prmProblem = new SqlParameter("@Problem", SqlDbType.VarChar, 50);
            prmProblem.Value = Problem;
            SqlParameter prmDescription = new SqlParameter("@Description", SqlDbType.VarChar, 300);
            prmDescription.Value = Description;
            SqlParameter prmColorCode = new SqlParameter("@ProblemColorCode", SqlDbType.VarChar, 500);
            prmColorCode.Value = ColorCode;
            return DataAccess.GetFromDataTable("SP_Insert_ProblemCategory", prmCreatedBy, prmProblem, prmDescription, prmColorCode);
        }
        public DataTable FillProblemCategory(decimal ProblemID)
        {
            SqlParameter prmProblemID = new SqlParameter("@ProblemID", SqlDbType.Decimal, 4);
            prmProblemID.Value = ProblemID;
            return DataAccess.GetFromDataTable("SP_Fil_ProblemCategoryInfo", prmProblemID);
        }
        public int UpdateProblemCategory(decimal ProblemID, string Problem, string Description, string ColorCode, decimal ModifiedBy)
        {
            SqlParameter prmProblemID = new SqlParameter("@ProblemID", SqlDbType.Decimal, 4);
            prmProblemID.Value = ProblemID;
            SqlParameter prmModifiedBy = new SqlParameter("@ModifiedBy", SqlDbType.Decimal, 4);
            prmModifiedBy.Value = ModifiedBy;
            SqlParameter prmProblem = new SqlParameter("@Problem", SqlDbType.VarChar, 1000);
            prmProblem.Value = Problem;
            SqlParameter prmDescription = new SqlParameter("@Description", SqlDbType.VarChar, 1000);
            prmDescription.Value = Description;
            SqlParameter prmColorCode = new SqlParameter("@ColorCode", SqlDbType.VarChar, 10);
            prmColorCode.Value = ColorCode;
            return DataAccess.Execute("SP_Update_ProblemCategoryInfo", prmModifiedBy, prmProblem, prmDescription, prmProblemID, prmColorCode);
        }

        #endregion


        #region Problem Master Stored Procedure

        public DataTable FillDeviceStatus(decimal StatusTypeID, decimal DevTypeID, decimal DevID)
        {
            SqlParameter prmDevTypeID = new SqlParameter("@DevTypeID", SqlDbType.Decimal, 4);
            prmDevTypeID.Value = DevTypeID;
            SqlParameter prmDevID = new SqlParameter("@DevID", SqlDbType.Decimal, 4);
            prmDevID.Value = DevID;
            SqlParameter prmStatusTypeID = new SqlParameter("@DevStatusTypeID", SqlDbType.Decimal, 4);
            prmStatusTypeID.Value = StatusTypeID;

            return DataAccess.GetFromDataTable("SP_Fill_DeviceStatus", prmDevTypeID, prmDevID, prmStatusTypeID);
        }
        public DataTable FillStatusType(decimal DevTypeID, decimal DevID)
        {
            SqlParameter prmDevTypeID = new SqlParameter("@DevTypeID", SqlDbType.Decimal, 4);
            prmDevTypeID.Value = DevTypeID;
            SqlParameter prmDevID = new SqlParameter("@DevID", SqlDbType.Decimal, 4);
            prmDevID.Value = DevID;
            return DataAccess.GetFromDataTable("SP_FillStatusTypeInfo", prmDevTypeID, prmDevID);
        }
        public DataTable InsertProblemMaster(string Problem, string ProblemCode, string Description, string Severity, decimal ProblemCategaryID, decimal DeviceTypeID,
            decimal DeviceID, decimal StatusTypeID, decimal StatusID, string Resolution, string ColorCode, string Icon, bool DownTime, decimal UserID)
        {
            SqlParameter prmProblemCategaryID = new SqlParameter("@ProblemCategoryID", SqlDbType.Decimal, 4);
            prmProblemCategaryID.Value = ProblemCategaryID;
            SqlParameter prmDeviceTypeID = new SqlParameter("@DeviceTypeID", SqlDbType.Decimal, 4);
            prmDeviceTypeID.Value = DeviceTypeID;
            SqlParameter prmDeviceID = new SqlParameter("@DeviceID", SqlDbType.Decimal, 4);
            prmDeviceID.Value = DeviceID;
            SqlParameter prmStatusTypeID = new SqlParameter("@DeviceStatusTypeID", SqlDbType.Decimal, 4);
            prmStatusTypeID.Value = StatusTypeID;
            SqlParameter prmStatusID = new SqlParameter("@DeviceStatusID", SqlDbType.Decimal, 4);
            prmStatusID.Value = StatusID;
            SqlParameter prmProblem = new SqlParameter("@Problem", SqlDbType.VarChar, 500);
            prmProblem.Value = Problem;
            SqlParameter prmProblemCode = new SqlParameter("@ProblemCode", SqlDbType.VarChar, 50);
            prmProblemCode.Value = ProblemCode;
            SqlParameter prmDescription = new SqlParameter("@ProblemDecsription", SqlDbType.VarChar, 500);
            prmDescription.Value = Description;
            SqlParameter prmSeverity = new SqlParameter("@ProblemSeverity", SqlDbType.VarChar, 500);
            prmSeverity.Value = Severity;
            SqlParameter prmResolution = new SqlParameter("@ProblemResolution", SqlDbType.VarChar, 250);
            prmResolution.Value = Resolution;
            SqlParameter prmColorCode = new SqlParameter("@ProblemColorCode", SqlDbType.VarChar, 500);
            prmColorCode.Value = ColorCode;
            SqlParameter prmIcon = new SqlParameter("@ProblemIcon", SqlDbType.VarChar, 500);
            prmIcon.Value = Icon;
            SqlParameter prmDownTime = new SqlParameter("@CalculateDownTime", SqlDbType.Bit);
            prmDownTime.Value = DownTime;
            SqlParameter prmUserID = new SqlParameter("@UserID", SqlDbType.Decimal, 4);
            prmUserID.Value = UserID;

            return DataAccess.GetFromDataTable("SP_Insert_ProblemMaster", prmUserID, prmDownTime, prmIcon, prmColorCode, prmResolution, prmSeverity, prmDescription, prmProblemCode,
                prmProblem, prmStatusID, prmStatusTypeID, prmDeviceID, prmDeviceTypeID, prmProblemCategaryID);

        }

        public DataTable ShowProblemDetails(string SelectStatement)
        {
            SqlParameter prmStatement = new SqlParameter("@SelectStmt", SqlDbType.VarChar, 2000);
            prmStatement.Value = SelectStatement;
            return DataAccess.GetFromDataTable("SP_Show_ProblemMaster", prmStatement);
        }

        public DataTable DeleteProblem(decimal ProblemID)
        {
            SqlParameter prmID = new SqlParameter("@ProblemID", SqlDbType.Decimal, 4);
            prmID.Value = ProblemID;
            return DataAccess.GetFromDataTable("SP_Delete_ProblemMaster", prmID);
        }

        public DataTable FillProblemMaster(decimal ProblemID)
        {
            SqlParameter prmID = new SqlParameter("@ProblemID", SqlDbType.Decimal, 4);
            prmID.Value = ProblemID;
            return DataAccess.GetFromDataTable("SP_Fill_ProblemMaster", prmID);
        }


        public DataTable UpdateProblemMaster(string Problem, string ProblemCode, string Description, string Severity, decimal ProblemCategaryID, decimal DeviceTypeID,
       decimal DeviceID, decimal StatusTypeID, decimal StatusID, string Resolution, string ColorCode, string Icon, bool DownTime, decimal UserID, decimal ProblemID)
        {
            SqlParameter prmProblemID = new SqlParameter("@ProblemID", SqlDbType.Decimal, 4);
            prmProblemID.Value = ProblemID;
            SqlParameter prmProblemCategaryID = new SqlParameter("@ProblemCategoryID", SqlDbType.Decimal, 4);
            prmProblemCategaryID.Value = ProblemCategaryID;
            SqlParameter prmDeviceTypeID = new SqlParameter("@DeviceTypeID", SqlDbType.Decimal, 4);
            prmDeviceTypeID.Value = DeviceTypeID;
            SqlParameter prmDeviceID = new SqlParameter("@DeviceID", SqlDbType.Decimal, 4);
            prmDeviceID.Value = DeviceID;
            SqlParameter prmStatusTypeID = new SqlParameter("@DeviceStatusTypeID", SqlDbType.Decimal, 4);
            prmStatusTypeID.Value = StatusTypeID;
            SqlParameter prmStatusID = new SqlParameter("@DeviceStatusID", SqlDbType.Decimal, 4);
            prmStatusID.Value = StatusID;
            SqlParameter prmProblem = new SqlParameter("@Problem", SqlDbType.VarChar, 500);
            prmProblem.Value = Problem;
            SqlParameter prmProblemCode = new SqlParameter("@ProblemCode", SqlDbType.VarChar, 50);
            prmProblemCode.Value = ProblemCode;
            SqlParameter prmDescription = new SqlParameter("@ProblemDecsription", SqlDbType.VarChar, 500);
            prmDescription.Value = Description;
            SqlParameter prmSeverity = new SqlParameter("@ProblemSeverity", SqlDbType.VarChar, 500);
            prmSeverity.Value = Severity;
            SqlParameter prmResolution = new SqlParameter("@ProblemResolution", SqlDbType.VarChar, 250);
            prmResolution.Value = Resolution;
            SqlParameter prmColorCode = new SqlParameter("@ProblemColorCode", SqlDbType.VarChar, 500);
            prmColorCode.Value = ColorCode;
            SqlParameter prmIcon = new SqlParameter("@ProblemIcon", SqlDbType.VarChar, 500);
            prmIcon.Value = Icon;
            SqlParameter prmDownTime = new SqlParameter("@CalculateDownTime", SqlDbType.Bit);
            prmDownTime.Value = DownTime;
            SqlParameter prmUserID = new SqlParameter("@UserID", SqlDbType.Decimal, 4);
            prmUserID.Value = UserID;

            return DataAccess.GetFromDataTable("SP_Upadte_ProblemMaster", prmUserID, prmDownTime, prmIcon, prmColorCode, prmResolution, prmSeverity, prmDescription, prmProblemCode,
                prmProblem, prmStatusID, prmStatusTypeID, prmDeviceID, prmDeviceTypeID, prmProblemCategaryID, prmProblemID);

        }
        #endregion

        #region Remotely Restart

        public DataTable SP_FillGroups()
        {
            return DataAccess.GetFromDataTable("SP_FillGroups");
        }
        public DataTable SP_FillAllTerminal(decimal SSTActivityGroupListId)
        {
            SqlParameter prmSSTActivityGroupListId = new SqlParameter("@SSTActivityListID", SqlDbType.Decimal);
            prmSSTActivityGroupListId.Value = SSTActivityGroupListId;
            DataTable dt = new DataTable();
            dt = DataAccess.GetFromDataTable("SP_FillAllTerminal", prmSSTActivityGroupListId);
            return dt;
        }
        public DataTable SP_FillAllBranch()//
        {
            return DataAccess.GetFromDataTable("SP_FillAllBranch");
        }
        public DataTable SP_FillAllRegions()//
        {
            return DataAccess.GetFromDataTable("SP_FillAllRegions");
        }
        public DataTable SP_RemotlyRestart_ShowTerminalDetails()//
        {
            return DataAccess.GetFromDataTable("SP_RemotlyRestart_ShowTerminalDetails");
        }

        public DataTable SP_RemotlyRestart_ShowSelectedTerminalDetails(string selTerminal)//
        {
            DataTable dt = new DataTable();
            using (SqlConnection cn = new SqlConnection(ConfigurationManager.ConnectionStrings["strConn"].ToString()))
            {
                cn.Open();
                SqlCommand cmd = new SqlCommand(selTerminal);
                cmd.Connection = cn;
                SqlDataAdapter da = new SqlDataAdapter();
                da.SelectCommand = cmd;
                da.Fill(dt);

            }

            return dt;
            //SqlParameter prmTerminals = new SqlParameter("@TerminalList",SqlDbType.VarChar,1000);
            //prmTerminals.Value = selTerminal;

            //return DataAccess.GetFromDataTable("SP_RemotlyRestart_ShowSelectedTerminalDetails",prmTerminals);
        }
        public DataTable SP_RemotelyRestart_Filter(string Banks, string BankSurchOp, string Regions, string RegionSurchOp, string Branchs,
                                     string BranchSurchOp,
                                   string SSTTYpes, string SSTTypeSurchOp,
                                   string SSTs, string SSTTerminalSurchOp, string RegionAndOrClause, string BranchAndORClause,
                                   string SSTTypeAndORClause,
                                     string SSTTerminalAndOrClause, string Groups, string GroupSurchOp,
                                     string GroupAndOrClause, string IPAdress, string IpAdressSurchOp, string IPAdressAndOr,
                                     string SSTServiceAreas, string SSTServiceAreasSurchOp, string SSTServiceAreasAndOrClause)//, string TerminalidCondi, string TerminalidCondOp)
        {

            SqlParameter prmSSTServiceAreas = new SqlParameter("@SSTServiceAreas", SqlDbType.VarChar, 1000);
            prmSSTServiceAreas.Value = SSTServiceAreas;
            SqlParameter prmSSTServiceAreasSurchOp = new SqlParameter("@SSTServiceAreasSurchOp", SqlDbType.VarChar, 1000);
            prmSSTServiceAreasSurchOp.Value = SSTServiceAreasSurchOp;
            SqlParameter prmSSTServiceAreasAndOrClause = new SqlParameter("@SSTServiceAreasAndOrClause", SqlDbType.VarChar, 1000);
            prmSSTServiceAreasAndOrClause.Value = SSTServiceAreasAndOrClause;

            SqlParameter prmSSTTYpes = new SqlParameter("@SSTTYpes", SqlDbType.VarChar, 1000);
            prmSSTTYpes.Value = SSTTYpes;
            SqlParameter prmSSTTypeSurchOp = new SqlParameter("@SSTTypeSurchOp", SqlDbType.VarChar, 1000);
            prmSSTTypeSurchOp.Value = SSTTypeSurchOp;
            SqlParameter prmSSTTypeAndORClause = new SqlParameter("@SSTTypeAndORClause", SqlDbType.VarChar, 1000);
            prmSSTTypeAndORClause.Value = SSTTypeAndORClause;
            SqlParameter prmGroups = new SqlParameter("@Groups", SqlDbType.VarChar, 1000);
            prmGroups.Value = Groups;
            SqlParameter prmGroupSurchOp = new SqlParameter("@GroupSurchOp", SqlDbType.VarChar, 1000);
            prmGroupSurchOp.Value = GroupSurchOp;
            SqlParameter prmGroupAndOrClause = new SqlParameter("@GroupAndOrClause", SqlDbType.VarChar, 1000);
            prmGroupAndOrClause.Value = GroupAndOrClause;
            SqlParameter prmBanks = new SqlParameter("@Banks", SqlDbType.VarChar, 1000);
            prmBanks.Value = Banks;
            SqlParameter prmBankSurchOp = new SqlParameter("@BankSurchOp", SqlDbType.VarChar, 1000);
            prmBankSurchOp.Value = BankSurchOp;
            SqlParameter prmRegions = new SqlParameter("@Regions", SqlDbType.VarChar, 1000);
            prmRegions.Value = Regions;
            SqlParameter prmRegionSurchOp = new SqlParameter("@RegionSurchOp", SqlDbType.VarChar, 1000);
            prmRegionSurchOp.Value = RegionSurchOp;
            SqlParameter prmBranchs = new SqlParameter("@Branchs", SqlDbType.VarChar, 1000);
            prmBranchs.Value = Branchs;
            SqlParameter prmBranchSurchOp = new SqlParameter("@BranchSurchOp", SqlDbType.VarChar, 1000);
            prmBranchSurchOp.Value = BranchSurchOp;
            SqlParameter prmSSTTerminal = new SqlParameter("@SSTIDs", SqlDbType.VarChar, 1000);
            prmSSTTerminal.Value = SSTs;
            SqlParameter prmSSTerminalSurchOp = new SqlParameter("@SSTIDSurchOp", SqlDbType.VarChar, 1000);
            prmSSTerminalSurchOp.Value = SSTTerminalSurchOp;
            SqlParameter prmRegionAndOr = new SqlParameter("@RegionsAndORClause", SqlDbType.VarChar, 1000);
            prmRegionAndOr.Value = RegionAndOrClause;
            SqlParameter prmBranchAndOr = new SqlParameter("@BranchAndORClause", SqlDbType.VarChar, 1000);
            prmBranchAndOr.Value = BranchAndORClause;
            SqlParameter prmSSTTerminalAndORClause = new SqlParameter("@SSTTerminalAndORClause", SqlDbType.VarChar, 1000);
            prmSSTTerminalAndORClause.Value = SSTTerminalAndOrClause;
            SqlParameter prmIpAdress = new SqlParameter("@Ipaddess", SqlDbType.VarChar, 1000);
            prmIpAdress.Value = IPAdress;
            SqlParameter prmIpAdressSurchOp = new SqlParameter("@Ipadressurchop", SqlDbType.VarChar, 1000);
            prmIpAdressSurchOp.Value = IpAdressSurchOp;
            SqlParameter prmIpadressAndORClause = new SqlParameter("@IpAddressAndORClause", SqlDbType.VarChar, 1000);
            prmIpadressAndORClause.Value = IPAdressAndOr;

            DataTable dt = new DataTable();
            dt = DataAccess.GetFromDataTable("SP_RemotelyRestart_Filter", prmBanks, prmBankSurchOp, prmRegions, prmRegionSurchOp, prmBranchs,
                                             prmBranchSurchOp, prmSSTTYpes, prmSSTTypeSurchOp, prmSSTTerminal, prmSSTerminalSurchOp,
                                             prmRegionAndOr, prmBranchAndOr, prmSSTTypeAndORClause, prmSSTTerminalAndORClause, prmGroups, prmGroupAndOrClause,
                                             prmGroupSurchOp, prmIpAdress, prmIpAdressSurchOp, prmIpadressAndORClause, prmSSTServiceAreas, prmSSTServiceAreasSurchOp, prmSSTServiceAreasAndOrClause);
            dt.Columns.Add("Response");

            return dt;
        }
        public DataTable SP_FillAllTerminalIDs()//
        {
            return DataAccess.GetFromDataTable("SP_ShowAllTerminalIDs");
        }
        public DataTable SP_FillRegions(string OpSign, string SurchValue)
        {
            SqlParameter prmOpSign = new SqlParameter("@OpSign", SqlDbType.VarChar, 10);
            prmOpSign.Value = OpSign;
            SqlParameter prmSurchValue = new SqlParameter("@SurchValue", SqlDbType.VarChar, 1000);
            prmSurchValue.Value = SurchValue;
            return DataAccess.GetFromDataTable("SP_FillRegions", prmOpSign, prmSurchValue);
        }
        public DataTable SP_FillBranchs(string BankOpSign, string BankSurchValue, string RegionOpSign, string RegionSurchValue)
        {
            SqlParameter prmBankOpSign = new SqlParameter("@BankOpSign", SqlDbType.VarChar, 10);
            prmBankOpSign.Value = BankOpSign;
            SqlParameter prmBankSurchValue = new SqlParameter("@BankSurchValue", SqlDbType.VarChar, 1000);
            prmBankSurchValue.Value = BankSurchValue;
            SqlParameter prmRegionOpSign = new SqlParameter("@RegionOpSign", SqlDbType.VarChar, 10);
            prmRegionOpSign.Value = RegionOpSign;
            SqlParameter prmRegionSurchValue = new SqlParameter("@RegionSurchValue", SqlDbType.VarChar, 1000);
            prmRegionSurchValue.Value = RegionSurchValue;
            return DataAccess.GetFromDataTable("SP_FillBranchs", prmBankOpSign, prmBankSurchValue, prmRegionOpSign, prmRegionSurchValue);
        }

        public DataTable SP_Show_RemotlyRestart_SSTName(string Banks, string BankSurchOp, string Regions, string RegionSurchOp, string Branchs,
                                         string BranchSurchOp, string SSTTYpes, string SSTTypeSurchOp,
                                          string SSTServiceAreas, string SSTServiceAreasSurchOp,
            string Groups, string GroupSurchOp, string IpAdress, string IpadressUrchOp)//, string TerminalidCondi, string TerminalidCondOp)
        {

            SqlParameter prmSSTServiceAreas = new SqlParameter("@SSTServiceAreas", SqlDbType.VarChar, 1000);
            prmSSTServiceAreas.Value = SSTServiceAreas;
            SqlParameter prmSSTServiceAreasSurchOp = new SqlParameter("@SSTServiceAreasSurchOp", SqlDbType.VarChar, 1000);
            prmSSTServiceAreasSurchOp.Value = SSTServiceAreasSurchOp;

            SqlParameter prmGroups = new SqlParameter("@Groups", SqlDbType.VarChar, 1000);
            prmGroups.Value = Groups;
            SqlParameter prmGroupSurchOp = new SqlParameter("@GroupSurchOp", SqlDbType.VarChar, 1000);
            prmGroupSurchOp.Value = GroupSurchOp;
            SqlParameter prmBanks = new SqlParameter("@Banks", SqlDbType.VarChar, 1000);
            prmBanks.Value = Banks;
            SqlParameter prmBankSurchOp = new SqlParameter("@BankSurchOp", SqlDbType.VarChar, 1000);
            prmBankSurchOp.Value = BankSurchOp;
            SqlParameter prmRegions = new SqlParameter("@Regions", SqlDbType.VarChar, 1000);
            prmRegions.Value = Regions;
            SqlParameter prmRegionSurchOp = new SqlParameter("@RegionSurchOp", SqlDbType.VarChar, 1000);
            prmRegionSurchOp.Value = RegionSurchOp;
            SqlParameter prmBranchs = new SqlParameter("@Branchs", SqlDbType.VarChar, 1000);
            prmBranchs.Value = Branchs;
            SqlParameter prmBranchSurchOp = new SqlParameter("@BranchSurchOp", SqlDbType.VarChar, 1000);
            prmBranchSurchOp.Value = BranchSurchOp;
            SqlParameter prmSSTTYpes = new SqlParameter("@SSTTYpes", SqlDbType.VarChar, 1000);
            prmSSTTYpes.Value = SSTTYpes;
            SqlParameter prmSSTTypeSurchOp = new SqlParameter("@SSTTypeSurchOp", SqlDbType.VarChar, 1000);
            prmSSTTypeSurchOp.Value = SSTTypeSurchOp;

            SqlParameter prmIpAdress = new SqlParameter("@Ipadderss", SqlDbType.VarChar, 1000);
            prmIpAdress.Value = IpAdress;
            SqlParameter prmIpAdressSurchOp = new SqlParameter("@IpAdressSurchOp", SqlDbType.VarChar, 1000);
            prmIpAdressSurchOp.Value = IpadressUrchOp;
            //SqlParameter TerminalidCond = new SqlParameter("@TerminalCondition", SqlDbType.VarChar, 1000)
            //TerminalidCond.Value = TerminalidCondi;
            //SqlParameter TerminalidCondop = new SqlParameter("@TerminalConditionop", SqlDbType.VarChar, 1000);
            //TerminalidCondop.Value = TerminalidCondOp;

            DataTable dt = new DataTable();
            dt = DataAccess.GetFromDataTable("SP_Show_RemotlyRestart_SSTName", prmBanks, prmBankSurchOp, prmRegions, prmRegionSurchOp, prmBranchs, prmBranchSurchOp, prmSSTTYpes, prmSSTTypeSurchOp, prmSSTServiceAreas, prmSSTServiceAreasSurchOp, prmGroups, prmGroupSurchOp, prmIpAdress, prmIpAdressSurchOp);//, TerminalidCond, TerminalidCondop);
            return dt;
        }

        public DataTable GetTerminalIDlist()
        {
            DataTable dt = new DataTable();

            dt = DataAccess.GetFromDataTable("sp_getlistTerminalid_activity");
            return dt;
        }


        public DataTable GetTerminalIPlist()
        {
            DataTable dt = new DataTable();

            dt = DataAccess.GetFromDataTable("sp_getlistTerminalip_activity");
            return dt;
        }


        public decimal DoRemotelyRestart(decimal SSTID, string GroupName, decimal UserID)
        {
            DataTable dt = new DataTable();
            SqlParameter prmSSTID = new SqlParameter("@SSTID", SqlDbType.Decimal, 4);
            prmSSTID.Value = SSTID;
            SqlParameter prmName = new SqlParameter("@Name", SqlDbType.VarChar, 1000);
            prmName.Value = GroupName;
            SqlParameter prmUserID = new SqlParameter("@UserID", SqlDbType.Decimal, 4);
            prmUserID.Value = UserID;
            dt = DataAccess.GetFromDataTable("SP_DoRemotelyRestart", prmName, prmUserID, prmSSTID);

            if (dt != null)
            {
                DataRow dr = dt.Rows[0];
                return decimal.Parse(dr["ScheduleID"].ToString());
            }
            else
                return 0.0M;
        }
        public DataTable GetRemotelyRestartResult(string strScheduleIDList)
        {
            SqlParameter prmResult = new SqlParameter("@ScheduleIDList", SqlDbType.VarChar, 5000);
            prmResult.Value = strScheduleIDList;
            return DataAccess.GetFromDataTable("SP_RemotelyAurorestart_Restult", prmResult);
        }
        //vaibhav
        public DataTable GetRemotelyRestartResultforFailed(string strScheduleIDList)
        {
            SqlParameter prmResult = new SqlParameter("@ScheduleIDList", SqlDbType.VarChar, 5000);
            prmResult.Value = strScheduleIDList;
            return DataAccess.GetFromDataTable("SP_RemotelyAurorestart_RestultforFailed", prmResult);
        }
        public DataTable GetRestartReport(DateTime FromDate, DateTime ToDate)
        {
            SqlParameter prmFromDate = new SqlParameter("@FromDate", SqlDbType.DateTime);
            prmFromDate.Value = FromDate;
            SqlParameter prmToDate = new SqlParameter("@ToDate", SqlDbType.DateTime);
            prmToDate.Value = ToDate;
            return DataAccess.GetFromDataTable("SP_RemotelyRestart_Report", prmFromDate, prmToDate);
        }
        #endregion

        #region Trouble Ticket Details
        public DataTable SP_ShowRepeatcountDetails(string TerminalID, string problm, string Device)
        {
            SqlParameter prmTerminalID = new SqlParameter("@TermialID", SqlDbType.VarChar, 50);
            prmTerminalID.Value = TerminalID;
            SqlParameter prmProblem = new SqlParameter("@ProblemName", SqlDbType.VarChar, 50);
            prmProblem.Value = problm;
            SqlParameter prmDevice = new SqlParameter("@DeviceStatusType", SqlDbType.VarChar, 50);
            prmDevice.Value = Device;
            return DataAccess.GetFromDataTable("SP_ShowRepeatcountDetails", prmTerminalID, prmProblem, prmDevice);
        }

        public DataTable SP_TroubleTicketDetail(string SearchCondition)
        {
            SqlParameter prmSearchCondition = new SqlParameter("@Condition", SqlDbType.VarChar, 1000);
            prmSearchCondition.Value = SearchCondition;
            return DataAccess.GetFromDataTable("SP_TroubleTicketDetail", prmSearchCondition);
        }
        public DataTable SP_TroubleTicketDetailcountNew(string SearchCondition)
        {
            SqlParameter prmSearchCondition = new SqlParameter("@Condition", SqlDbType.VarChar, 1000);
            prmSearchCondition.Value = SearchCondition;
            //SqlParameter prmDays = new SqlParameter("@Days", SqlDbType.Decimal);
            //prmDays.Value = Days;
            return DataAccess.GetFromDataTable("SP_TroubleTicketDetail_New", prmSearchCondition);
        }

        //public DataTable SP_GetSSTMachineDetails(string SearchCondition)
        //{
        //    SqlParameter prmSearchCondition = new SqlParameter("@Condition", SqlDbType.VarChar, 1000);
        //    prmSearchCondition.Value = SearchCondition;
        //    //SqlParameter prmDays = new SqlParameter("@Days", SqlDbType.Decimal);
        //    //prmDays.Value = Days;

        //    return DataAccess.GetFromDataTable("Get_SSTMachineDetails", prmSearchCondition);
        //}


        //Prajyoti
        public DataTable sp_monitor_status_Search(string SearchCondition)
        {
            SqlParameter prmSearchCondition = new SqlParameter("@Condition", SqlDbType.VarChar, 1000);
            prmSearchCondition.Value = SearchCondition;


            return DataAccess.GetFromDataTable("sp_monitor_status_Search", prmSearchCondition);
        }
        //
        //SSK bIOS Report edit by shrikant sharma
        public DataTable GetReportsskbios()
        {
            return DataAccess.GetFromDataTable("SP_SSKBIOSReport");
        }
        //SSK BIOS Report end
        public DataTable SP_GetSSTMachineDetails(string SearchCondition, string region, string branchid)
        {
            SqlParameter prmSearchCondition = new SqlParameter("@Condition", SqlDbType.VarChar, 1000);
            prmSearchCondition.Value = SearchCondition;
            SqlParameter prmregionId = new SqlParameter("@RegionId", SqlDbType.VarChar, 1000);
            prmregionId.Value = region;
            SqlParameter prmbranchid = new SqlParameter("@BranchId", SqlDbType.VarChar, 1000);
            prmbranchid.Value = branchid;

            return DataAccess.GetFromDataTable("Get_SSTMachineDetails", prmSearchCondition, prmregionId, prmbranchid);
        }

        //for switch
        public DataTable SP_GetSSTMachineDetails_switch(string SearchCondition, string region, string branchid)
        {
            SqlParameter prmSearchCondition = new SqlParameter("@Condition", SqlDbType.VarChar, 1000);
            prmSearchCondition.Value = SearchCondition;
            SqlParameter prmregionId = new SqlParameter("@RegionId", SqlDbType.VarChar, 1000);
            prmregionId.Value = region;
            SqlParameter prmbranchid = new SqlParameter("@BranchId", SqlDbType.VarChar, 1000);
            prmbranchid.Value = branchid;

            return DataAccess.GetFromDataTable("Get_SSTMachineDetails_Switch", prmSearchCondition, prmregionId, prmbranchid);
        }
        //switch end
        public DataTable SP_GetSSTTYPe(string SearchCondition)
        {
            return DataAccess.GetFromDataTable("SP_Fill_SST_Type");
        }
        public DataTable Sp_RepeatCount(DateTime strFromDate, DateTime strToDate, string DeviceIDList)
        {
            SqlParameter prmDeviceIDList = new SqlParameter("@DeviceID", SqlDbType.VarChar, 1000);
            prmDeviceIDList.Value = DeviceIDList;
            SqlParameter prmstrFromDate = new SqlParameter("@ProblemDateTime", SqlDbType.VarChar, 1000);
            prmstrFromDate.Value = strFromDate;

            SqlParameter prmtodate = new SqlParameter("@ProblemDateTime", SqlDbType.VarChar, 1000);
            prmtodate.Value = strToDate;

            return DataAccess.GetFromDataTable("Sp_RepeatCount", prmstrFromDate, prmtodate, prmDeviceIDList);
        }
        public DataTable sp_RetriveDeviceList()
        {
            return DataAccess.GetFromDataTable("sp_RetriveDeviceList");
        }
        public DataTable ShowComments(decimal TicketID)
        {
            SqlParameter prmTicketID = new SqlParameter("@TicketID", SqlDbType.Decimal);
            prmTicketID.Value = TicketID;
            return DataAccess.GetFromDataTable("SP_ShowCommentsDetails", prmTicketID);
        }
        public DataTable ShowVenderType()
        {
            return DataAccess.GetFromDataTable("SP_ShowVenderType");
        }
        public DataTable ShowVenderName(string VenderType)
        {
            SqlParameter prmVenderType = new SqlParameter("@VenderType", SqlDbType.VarChar, 500);
            prmVenderType.Value = VenderType;
            return DataAccess.GetFromDataTable("SP_ShowVenderName", prmVenderType);
        }
        public int InsertComment(decimal TicketID, string Comment, string Buisness_Comment, string CallStage, decimal UserID, decimal VendorID)
        {
            SqlParameter prmTicketID = new SqlParameter("@TicketID", SqlDbType.Decimal);
            prmTicketID.Value = TicketID;
            SqlParameter prmComment = new SqlParameter("@Comment", SqlDbType.VarChar, 500);
            prmComment.Value = Comment;
            SqlParameter prmBuisness_Comment = new SqlParameter("@Buisness_Comment", SqlDbType.VarChar, 500);
            prmBuisness_Comment.Value = Buisness_Comment;
            SqlParameter prmCallStage = new SqlParameter("@CallStage", SqlDbType.VarChar, 500);
            prmCallStage.Value = CallStage;
            SqlParameter prmUserID = new SqlParameter("@UserID", SqlDbType.Decimal);
            prmUserID.Value = UserID;
            SqlParameter prmVendorID = new SqlParameter("@VendorID", SqlDbType.Decimal);
            prmVendorID.Value = VendorID;
            return DataAccess.Execute("SP_Insert_Comment", prmTicketID, prmComment, prmBuisness_Comment, prmCallStage, prmUserID, prmVendorID);
        }
        public int UpdateComment(decimal TicketID, string Comment, string CallStage, decimal UserID, decimal VendorID, decimal CommentID)
        {
            SqlParameter prmTCommentID = new SqlParameter("@CommentID", SqlDbType.Decimal);
            prmTCommentID.Value = CommentID;
            SqlParameter prmTicketID = new SqlParameter("@TicketID", SqlDbType.Decimal);
            prmTicketID.Value = TicketID;
            SqlParameter prmComment = new SqlParameter("@Comment", SqlDbType.VarChar, 500);
            prmComment.Value = Comment;
            SqlParameter prmCallStage = new SqlParameter("@CallStage", SqlDbType.VarChar, 500);
            prmCallStage.Value = CallStage;
            SqlParameter prmUserID = new SqlParameter("@UserID", SqlDbType.Decimal);
            prmUserID.Value = UserID;
            SqlParameter prmVendorID = new SqlParameter("@VendorID", SqlDbType.Decimal);
            prmVendorID.Value = VendorID;
            return DataAccess.Execute("SP_Update_Comment", prmTicketID, prmComment, prmCallStage, prmUserID, prmVendorID, prmTCommentID);
        }
        public DataTable FillCommentDetails(decimal CommentID)
        {
            SqlParameter prmCommentID = new SqlParameter("@CommentID", SqlDbType.Decimal);
            prmCommentID.Value = CommentID;
            return DataAccess.GetFromDataTable("SP_FillCommentDetails", prmCommentID);
        }
        public DataTable GetVendorType(string VendorName)
        {
            SqlParameter prmVendorName = new SqlParameter("@VendorName", SqlDbType.VarChar, 500);
            prmVendorName.Value = VendorName;
            return DataAccess.GetFromDataTable("GEtVendorName", prmVendorName);
        }
        public DataTable GetTerminalInfo(string TerminalID)
        {
            SqlParameter prmTerminalID = new SqlParameter("@TerminalID", SqlDbType.VarChar, 50);
            prmTerminalID.Value = TerminalID;
            return DataAccess.GetFromDataTable("SP_GetTerminalInfo", prmTerminalID);
        }
        public DataTable ShowTerminalProblem(string TerminalID)
        {
            SqlParameter prmTerminalID = new SqlParameter("@TermialID", SqlDbType.VarChar, 50);
            prmTerminalID.Value = TerminalID;
            return DataAccess.GetFromDataTable("SP_ShowTermiailProblem", prmTerminalID);
        }
        public DataTable SP_ShowTermiailBriefTicketWise(string TerminalID, string TicketID)
        {
            SqlParameter prmTerminalID = new SqlParameter("@TermialID", SqlDbType.VarChar, 50);
            prmTerminalID.Value = TerminalID;
            SqlParameter prmTicketID = new SqlParameter("@TicketID", SqlDbType.VarChar, 50);
            prmTicketID.Value = TicketID;
            return DataAccess.GetFromDataTable("SP_ShowTermiailBriefTicketWise", prmTerminalID, prmTicketID);
        }

        public DataTable sp_monitor_status()
        {
            string constring = System.Configuration.ConfigurationManager.ConnectionStrings["strConn"].ConnectionString;

            DecryptTheString(constring);
            SqlConnection con = new SqlConnection(DecryptTheString(constring));
            SqlCommand cmd = new SqlCommand();
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.CommandText = "sp_monitor_status";
            cmd.Connection = con;
            //SqlParameter prmTerminalID = new SqlParameter("@TermialID", SqlDbType.VarChar, 50);
            //prmTerminalID.Value = TerminalID;
            return DataAccess.GetFromDataTable("sp_monitor_status");
        }
        public DataTable sp_monitor_status_CDrive(string TerminalID)
        {
            string constring = System.Configuration.ConfigurationManager.ConnectionStrings["strConn"].ConnectionString;

            DecryptTheString(constring);
            SqlConnection con = new SqlConnection(DecryptTheString(constring));
            SqlCommand cmd = new SqlCommand();
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.CommandText = "sp_monitor_status_CDrive";
            cmd.Connection = con;
            SqlParameter prmTerminalID = new SqlParameter("@TerminalID", SqlDbType.VarChar, 50);
            prmTerminalID.Value = TerminalID;
            return DataAccess.GetFromDataTable("sp_monitor_status_CDrive", prmTerminalID);
        }
        public DataTable sp_monitor_status_DDrive(string TerminalID)
        {
            string constring = System.Configuration.ConfigurationManager.ConnectionStrings["strConn"].ConnectionString;

            DecryptTheString(constring);
            SqlConnection con = new SqlConnection(DecryptTheString(constring));
            SqlCommand cmd = new SqlCommand();
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.CommandText = "sp_monitor_status_DDrive";
            cmd.Connection = con;
            SqlParameter prmTerminalID = new SqlParameter("@TerminalID", SqlDbType.VarChar, 50);
            prmTerminalID.Value = TerminalID;
            return DataAccess.GetFromDataTable("sp_monitor_status_DDrive", prmTerminalID);
        }
        public DataTable SP_GetSSTMachineDetails_Emitra(string SearchCondition, string region, string branchid)
        {
            SqlParameter prmSearchCondition = new SqlParameter("@Condition", SqlDbType.VarChar, 1000);
            prmSearchCondition.Value = SearchCondition;
            SqlParameter prmregionId = new SqlParameter("@RegionId", SqlDbType.VarChar, 1000);
            prmregionId.Value = region;
            SqlParameter prmbranchid = new SqlParameter("@BranchId", SqlDbType.VarChar, 1000);
            prmbranchid.Value = branchid;

            return DataAccess.GetFromDataTable("Get_SSTMachineDetails_Emitra", prmSearchCondition, prmregionId, prmbranchid);
        }


        public DataTable SP_Get_SSTMachineDetails_Emitra(string SearchCondition, string region, string branchid)
        {
            SqlParameter prmSearchCondition = new SqlParameter("@Condition", SqlDbType.VarChar, 1000);
            prmSearchCondition.Value = SearchCondition;
            SqlParameter prmregionId = new SqlParameter("@RegionId", SqlDbType.VarChar, 1000);
            prmregionId.Value = region;
            SqlParameter prmbranchid = new SqlParameter("@BranchId", SqlDbType.VarChar, 1000);
            prmbranchid.Value = branchid;

            return DataAccess.GetFromDataTable("Get_SSTMachineDetails_Emitra", prmSearchCondition, prmregionId, prmbranchid);
        }

        public DataTable ShowComment(decimal TicketID)
        {
            SqlParameter prmTicketID = new SqlParameter("@TicketID", SqlDbType.Decimal);
            prmTicketID.Value = TicketID;
            return DataAccess.GetFromDataTable("SP_ShowComments", prmTicketID);
        }
        public DataTable GetTerminalCounts()
        {
            return DataAccess.GetFromDataTable("SP_TerminalsCount");
        }
        public DataTable GetProblemCatagoryCount()
        {
            return DataAccess.GetFromDataTable("SP_GetProblemStatus");
        }
        public DataTable TroubleTicketDetailReport(string SearchCondition)
        {
            SqlParameter prmSearchCondition = new SqlParameter("@Condition", SqlDbType.VarChar, 1000);
            prmSearchCondition.Value = SearchCondition;
            return DataAccess.GetFromDataTable("SP_TroubleTicketDetail_Report", prmSearchCondition);
        }


        public DataTable OverallTroubleTicketDetailReport(string SearchCondition)
        {
            SqlParameter prmSearchCondition = new SqlParameter("@Condition", SqlDbType.VarChar, 1000);
            prmSearchCondition.Value = SearchCondition;
            return DataAccess.GetFromDataTable("Get_SSTMachineDetailsReport", prmSearchCondition);
        }
        #endregion

        #region Esacalation Master

        public DataTable GEtRegionsWithBanks()
        {
            return DataAccess.GetFromDataTable("SP_GetRegionsOfBank");
        }
        public DataTable GetBranchWithBanks(decimal RegionID)
        {
            SqlParameter prmRegionID = new SqlParameter("@RegionID", SqlDbType.Decimal, 4);
            prmRegionID.Value = RegionID;
            return DataAccess.GetFromDataTable("SP_BranchesOfBanks", prmRegionID);
        }
        public DataTable GetGroups()
        {
            return DataAccess.GetFromDataTable("Sp_GetGroups");
        }
        public DataTable GetDeviceType()
        {
            return DataAccess.GetFromDataTable("SP_GetDeviceTypes");
        }
        public DataTable GetDevices(decimal DeviceTypeID)
        {
            SqlParameter prmDeviceTypeID = new SqlParameter("@DeviceTypeID", SqlDbType.Decimal);
            prmDeviceTypeID.Value = DeviceTypeID;
            return DataAccess.GetFromDataTable("SP_GetDevices", prmDeviceTypeID);
        }
        public DataTable GetDeviceStatusType()
        {
            return DataAccess.GetFromDataTable("Sp_GetDeviceStatusType");
        }
        public DataTable GetDeviceStatus(decimal DevTypeID, decimal DevID, decimal DevStatusTypeId)
        {
            string SelectStatement = "SELECT DeviceStatus_SHMS.DeviceStatus + ' (' + DeviceMaster_SHMS.DeviceName + ')' AS 'Status', DeviceStatus_SHMS.DeviceStatusID as 'StatusID' FROM DeviceStatus_SHMS INNER JOIN DeviceStatusType_SHMS ON DeviceStatus_SHMS.StatusTypeID = DeviceStatusType_SHMS.StatusTypeId INNER JOIN DeviceMaster_SHMS ON DeviceStatus_SHMS.DeviceID = DeviceMaster_SHMS.DeviceID INNER JOIN DeviceTypeMaster_SHMS ON DeviceStatus_SHMS.DeviceTypeID = DeviceTypeMaster_SHMS.DeviceTypeID WHERE (DeviceStatus_SHMS.DeviceStatusID <> 0)";

            if (DevTypeID != 0.0M)
                SelectStatement += " and DeviceStatus_SHMS.DeviceTypeID=" + DevTypeID;

            if (DevID != 0.0M)
                SelectStatement += " and DeviceStatus_SHMS.DeviceID=" + DevID;

            if (DevStatusTypeId != 0.0M)
                SelectStatement += " and DeviceStatus_SHMS.StatusTypeID=" + DevStatusTypeId;


            SelectStatement += " ORDER BY  DeviceStatus_SHMS.DeviceStatus ";
            SqlParameter prmSelectStatement = new SqlParameter("@SelectStatement", SqlDbType.VarChar, 1000);

            prmSelectStatement.Value = SelectStatement;
            return DataAccess.GetFromDataTable("Sp_GetDeviceStatus", prmSelectStatement);
        }
        public DataTable GetProblem(decimal DevTypeID, decimal DevID, decimal StatusTypeID, decimal StatusID)
        {
            //string SelectStatement = "Select  ProblemID,Problem FROM   ProblemMaster_SHMS  ";

            string SelectStatement = "SELECT ProblemMaster_SHMS.ProblemID as ProblemID,ProblemMaster_SHMS.Problem +' ('+ DeviceMaster_SHMS.DeviceName+')' as Problem FROM  ProblemMaster_SHMS INNER JOIN DeviceMaster_SHMS ON (ProblemMaster_SHMS.DeviceID = DeviceMaster_SHMS.DeviceID AND ProblemMaster_SHMS.DeviceTypeID = DeviceMaster_SHMS.DeviceTypeID)";

            if (DevTypeID != 0.0M || DevID != 0.0M || StatusTypeID != 0.0M || StatusID != 0.0M)
            {
                SelectStatement += " where ";
                if (DevTypeID != 0.0M)
                    SelectStatement += " ProblemMaster_SHMS.DeviceTypeID =" + DevTypeID;

                if (DevTypeID != 0.0M && DevID != 0.0M)
                    SelectStatement += " and ProblemMaster_SHMS. DeviceID =" + DevID;
                else if (DevID != 0.0M)
                    SelectStatement += " ProblemMaster_SHMS.DeviceID =" + DevID;

                if ((DevTypeID != 0.0M || DevID != 0.0M) && StatusTypeID != 0.0M)
                    SelectStatement += " and ProblemMaster_SHMS.DeviceStatusTypeID =" + StatusTypeID;
                else if (StatusTypeID != 0.0M)
                    SelectStatement += "  ProblemMaster_SHMS.DeviceStatusTypeID =" + StatusTypeID;

                if ((DevTypeID != 0.0M || DevID != 0.0M || StatusTypeID != 0.0M) && StatusID != 0.0M)
                    SelectStatement += " and ProblemMaster_SHMS.DeviceStatusID =" + StatusID;
                else if (StatusID != 0.0M)
                    SelectStatement += "  ProblemMaster_SHMS.DeviceStatusID =" + StatusID;
            }

            SqlParameter prmSelectStatement = new SqlParameter("@SelectStatement", SqlDbType.VarChar, 1000);
            prmSelectStatement.Value = SelectStatement;

            return DataAccess.GetFromDataTable("Sp_Problem", prmSelectStatement);
        }
        public DataTable EscalationTable_Insert(decimal BranchID, decimal RegionID, decimal GroupID, decimal DeviceTypeID, decimal DeviceID,
            decimal DeviceStatusTypeID, decimal DeviceStatusID, decimal ProblemID, decimal TimeInMin, string EscColor, decimal EscLevel,
            string EscEmailID1, string EscEmailID2, string EscEmailID3, string EscEmailID4, string EscEmailID5, string EscEmailID6,
            string EscMobile1, string EscMobile2, string EscMobile3, string EscMobile4, string EscMobile5, string EscMobile6,
            decimal CreatedBy, decimal ModifiedBy)
        {
            //  DataTable dtEscTable = new DataTable();
            SqlParameter prmdBranchID = new SqlParameter("@BranchID", SqlDbType.Decimal, 4);
            prmdBranchID.Value = BranchID;

            SqlParameter prmRegionID = new SqlParameter("@RegionID", SqlDbType.Decimal, 4);
            prmRegionID.Value = RegionID;

            SqlParameter prmGroupID = new SqlParameter("@GroupID", SqlDbType.Decimal, 4);
            prmGroupID.Value = GroupID;

            SqlParameter prmDeviceTypeID = new SqlParameter("@DeviceTypeID", SqlDbType.Decimal, 4);
            prmDeviceTypeID.Value = DeviceTypeID;
            SqlParameter prmDeviceID = new SqlParameter("@DeviceID", SqlDbType.Decimal, 4);
            prmDeviceID.Value = DeviceID;
            SqlParameter prmDeviceStatusTypeID = new SqlParameter("@DeviceStatusTypeID", SqlDbType.Decimal, 4);
            prmDeviceStatusTypeID.Value = DeviceStatusTypeID;
            SqlParameter prmDeviceStatusID = new SqlParameter("@DeviceStatusID", SqlDbType.Decimal, 4);
            prmDeviceStatusID.Value = DeviceStatusID;
            SqlParameter prmProblemID = new SqlParameter("@ProblemID", SqlDbType.Decimal, 4);
            prmProblemID.Value = ProblemID;
            SqlParameter prmTimeInMin = new SqlParameter("@TimeInMin", SqlDbType.Decimal, 4);
            prmTimeInMin.Value = TimeInMin;
            SqlParameter prmEscColor = new SqlParameter("@EscColor", SqlDbType.VarChar, 1000);
            prmEscColor.Value = EscColor;
            SqlParameter prmEscLevel = new SqlParameter("@EscLevel", SqlDbType.Decimal, 4);
            prmEscLevel.Value = EscLevel;
            SqlParameter prmEscEmailID1 = new SqlParameter("@EscEmailID1", SqlDbType.VarChar, 1000);
            prmEscEmailID1.Value = EscEmailID1;
            SqlParameter prmEscEmailID2 = new SqlParameter("@EscEmailID2", SqlDbType.VarChar, 1000);
            prmEscEmailID2.Value = EscEmailID2;
            SqlParameter prmEscEmailID3 = new SqlParameter("@EscEmailID3", SqlDbType.VarChar, 1000);
            prmEscEmailID3.Value = EscEmailID3;
            SqlParameter prmEscEmailID4 = new SqlParameter("@EscEmailID4", SqlDbType.VarChar, 1000);
            prmEscEmailID4.Value = EscEmailID4;
            SqlParameter prmEscEmailID5 = new SqlParameter("@EscEmailID5", SqlDbType.VarChar, 1000);
            prmEscEmailID5.Value = EscEmailID5;
            SqlParameter prmEscEmailID6 = new SqlParameter("@EscEmailID6", SqlDbType.VarChar, 1000);
            prmEscEmailID6.Value = EscEmailID6;
            SqlParameter prmEscMobile1 = new SqlParameter("@EscMobile1", SqlDbType.VarChar, 1000);
            prmEscMobile1.Value = EscMobile1;
            SqlParameter prmEscMobile2 = new SqlParameter("@EscMobile2", SqlDbType.VarChar, 1000);
            prmEscMobile2.Value = EscMobile2;
            SqlParameter prmEscMobile3 = new SqlParameter("@EscMobile3", SqlDbType.VarChar, 1000);
            prmEscMobile3.Value = EscMobile3;
            SqlParameter prmEscMobile4 = new SqlParameter("@EscMobile4", SqlDbType.VarChar, 1000);
            prmEscMobile4.Value = EscMobile4;
            SqlParameter prmEscMobile5 = new SqlParameter("@EscMobile5", SqlDbType.VarChar, 1000);
            prmEscMobile5.Value = EscMobile5;
            SqlParameter prmEscMobile6 = new SqlParameter("@EscMobile6", SqlDbType.VarChar, 1000);
            prmEscMobile6.Value = EscMobile6;
            SqlParameter prmCreatedBy = new SqlParameter("@CreatedBy", SqlDbType.Decimal, 4);
            prmCreatedBy.Value = CreatedBy;
            SqlParameter prmModifiedBy = new SqlParameter("@ModifiedBy", SqlDbType.Decimal, 4);
            prmModifiedBy.Value = ModifiedBy;
            return DataAccess.GetFromDataTable("Sp_InsertEsclationInfo", prmdBranchID, prmRegionID, prmGroupID, prmDeviceTypeID, prmDeviceID, prmDeviceStatusTypeID, prmDeviceStatusID, prmProblemID, prmTimeInMin, prmEscColor, prmEscLevel, prmEscEmailID1, prmEscEmailID2, prmEscEmailID3, prmEscEmailID4, prmEscEmailID5, prmEscEmailID6, prmEscMobile1, prmEscMobile2, prmEscMobile3, prmEscMobile4, prmEscMobile5, prmEscMobile6, prmCreatedBy, prmModifiedBy);
        }
        public DataTable SP_ShowEscalationDetails(string SelectQuary)
        {
            SqlParameter prmSelectQuary = new SqlParameter("@SelectQuary", SqlDbType.VarChar, 3000);
            prmSelectQuary.Value = SelectQuary;
            return DataAccess.GetFromDataTable("SP_ShowEscalationDetails", prmSelectQuary);
        }

        public int DeleteEscalation(decimal EscID)
        {
            SqlParameter prmEscID = new SqlParameter("@EscID", SqlDbType.Decimal, 4);
            prmEscID.Value = EscID;
            return DataAccess.Execute("SP_Delete_Escalation", prmEscID);

        }

        public DataTable FillEsc(decimal EscID)
        {
            SqlParameter prmEscID = new SqlParameter("@EscID", SqlDbType.Decimal, 4);
            prmEscID.Value = EscID;
            return DataAccess.GetFromDataTable("SP_ShowEsc", prmEscID);

        }

        public DataTable UpadteEsc(decimal EscID, decimal BranchID, decimal RegionID, decimal GroupID, decimal DeviceTypeID, decimal DeviceID,
           decimal DeviceStatusTypeID, decimal DeviceStatusID, decimal ProblemID, decimal TimeInMin, string EscColor, decimal EscLevel,
           string EscEmailID1, string EscEmailID2, string EscEmailID3, string EscEmailID4, string EscEmailID5, string EscEmailID6,
           string EscMobile1, string EscMobile2, string EscMobile3, string EscMobile4, string EscMobile5, string EscMobile6,
           decimal ModifiedBy)
        {
            //  DataTable dtEscTable = new DataTable();
            SqlParameter prmdBranchID = new SqlParameter("@BranchID", SqlDbType.Decimal, 4);
            prmdBranchID.Value = BranchID;

            SqlParameter prmRegionID = new SqlParameter("@RegionID", SqlDbType.Decimal, 4);
            prmRegionID.Value = RegionID;

            SqlParameter prmGroupID = new SqlParameter("@GroupID", SqlDbType.Decimal, 4);
            prmGroupID.Value = GroupID;

            SqlParameter prmDeviceTypeID = new SqlParameter("@DeviceTypeID", SqlDbType.Decimal, 4);
            prmDeviceTypeID.Value = DeviceTypeID;
            SqlParameter prmDeviceID = new SqlParameter("@DeviceID", SqlDbType.Decimal, 4);
            prmDeviceID.Value = DeviceID;
            SqlParameter prmDeviceStatusTypeID = new SqlParameter("@DeviceStatusTypeID", SqlDbType.Decimal, 4);
            prmDeviceStatusTypeID.Value = DeviceStatusTypeID;
            SqlParameter prmDeviceStatusID = new SqlParameter("@DeviceStatusID", SqlDbType.Decimal, 4);
            prmDeviceStatusID.Value = DeviceStatusID;
            SqlParameter prmProblemID = new SqlParameter("@ProblemID", SqlDbType.Decimal, 4);
            prmProblemID.Value = ProblemID;
            SqlParameter prmTimeInMin = new SqlParameter("@TimeInMin", SqlDbType.Decimal, 4);
            prmTimeInMin.Value = TimeInMin;
            SqlParameter prmEscColor = new SqlParameter("@EscColor", SqlDbType.VarChar, 1000);
            prmEscColor.Value = EscColor;
            SqlParameter prmEscLevel = new SqlParameter("@EscLevel", SqlDbType.Decimal, 4);
            prmEscLevel.Value = EscLevel;
            SqlParameter prmEscEmailID1 = new SqlParameter("@EscEmailID1", SqlDbType.VarChar, 1000);
            prmEscEmailID1.Value = EscEmailID1;
            SqlParameter prmEscEmailID2 = new SqlParameter("@EscEmailID2", SqlDbType.VarChar, 1000);
            prmEscEmailID2.Value = EscEmailID2;
            SqlParameter prmEscEmailID3 = new SqlParameter("@EscEmailID3", SqlDbType.VarChar, 1000);
            prmEscEmailID3.Value = EscEmailID3;
            SqlParameter prmEscEmailID4 = new SqlParameter("@EscEmailID4", SqlDbType.VarChar, 1000);
            prmEscEmailID4.Value = EscEmailID4;
            SqlParameter prmEscEmailID5 = new SqlParameter("@EscEmailID5", SqlDbType.VarChar, 1000);
            prmEscEmailID5.Value = EscEmailID5;
            SqlParameter prmEscEmailID6 = new SqlParameter("@EscEmailID6", SqlDbType.VarChar, 1000);
            prmEscEmailID6.Value = EscEmailID6;
            SqlParameter prmEscMobile1 = new SqlParameter("@EscMobile1", SqlDbType.VarChar, 1000);
            prmEscMobile1.Value = EscMobile1;
            SqlParameter prmEscMobile2 = new SqlParameter("@EscMobile2", SqlDbType.VarChar, 1000);
            prmEscMobile2.Value = EscMobile2;
            SqlParameter prmEscMobile3 = new SqlParameter("@EscMobile3", SqlDbType.VarChar, 1000);
            prmEscMobile3.Value = EscMobile3;
            SqlParameter prmEscMobile4 = new SqlParameter("@EscMobile4", SqlDbType.VarChar, 1000);
            prmEscMobile4.Value = EscMobile4;
            SqlParameter prmEscMobile5 = new SqlParameter("@EscMobile5", SqlDbType.VarChar, 1000);
            prmEscMobile5.Value = EscMobile5;
            SqlParameter prmEscMobile6 = new SqlParameter("@EscMobile6", SqlDbType.VarChar, 1000);
            prmEscMobile6.Value = EscMobile6;

            SqlParameter prmModifiedBy = new SqlParameter("@UserID", SqlDbType.Decimal, 4);
            prmModifiedBy.Value = ModifiedBy;
            SqlParameter prmEscID = new SqlParameter("@EscID", SqlDbType.Decimal, 4);
            prmEscID.Value = EscID;
            return DataAccess.GetFromDataTable("Sp_UpdateEsc", prmEscID, prmdBranchID, prmRegionID, prmGroupID, prmDeviceTypeID,
                prmDeviceID, prmDeviceStatusTypeID, prmDeviceStatusID, prmProblemID, prmTimeInMin, prmEscColor,
                prmEscLevel, prmEscEmailID1, prmEscEmailID2, prmEscEmailID3, prmEscEmailID4, prmEscEmailID5,
                prmEscEmailID6, prmEscMobile1, prmEscMobile2, prmEscMobile3, prmEscMobile4, prmEscMobile5,
                prmEscMobile6, prmModifiedBy);

        }

        #endregion

        #region Report Region

        //public DataTable GetSSTCountRegionWise(decimal SelectTerminalType)
        //{
        //    SqlParameter prmSelectTerminalType = new SqlParameter("@SST_TypeID", SqlDbType.Decimal);
        //    prmSelectTerminalType.Value = SelectTerminalType;
        //    return DataAccess.GetFromDataTable("SP_GetRegionwiseInfo", prmSelectTerminalType);
        //}
        public DataTable GetSSTCountRegionWise(decimal RegionID, decimal BranchId)
        {
            SqlParameter prmRegionID = new SqlParameter("@RegionId", SqlDbType.Decimal);
            prmRegionID.Value = RegionID;
            SqlParameter prmBranchId = new SqlParameter("@BranchId", SqlDbType.Decimal);
            prmBranchId.Value = BranchId;

            return DataAccess.GetFromDataTable("SP_GetRegionwiseInfo", prmRegionID, prmBranchId);
        }

        public DataTable GetTicketDetailRegionwise(decimal RegionID, decimal SelectTerminalType)
        {
            SqlParameter prmRegionID = new SqlParameter("@regionID", SqlDbType.Decimal, 18);
            prmRegionID.Value = RegionID;
            SqlParameter prmSelectTerminalType = new SqlParameter("@SST_TypeID", SqlDbType.Decimal);
            prmSelectTerminalType.Value = SelectTerminalType;

            return DataAccess.GetFromDataTable("SP_GetTicketDetailRegionwise", prmRegionID, prmSelectTerminalType);
        }


        //public DataTable Sevirity()
        //{
        //    return DataAccess.GetFromDataTable("SP_GetTicketCountBySeverity");
        //}
        public DataTable Sevirity(decimal SelectTerminalType)
        {
            SqlParameter prmSelectTerminalType = new SqlParameter("@SST_TypeID", SqlDbType.Decimal);
            prmSelectTerminalType.Value = SelectTerminalType;
            return DataAccess.GetFromDataTable("SP_GetTicketCountBySeverity", prmSelectTerminalType);
        }


        #endregion

        #region Report Branch

        public DataTable GetSSTCountBranchWise()
        {
            return DataAccess.GetFromDataTable("SP_GetTerminalCountBranchwise");
        }

        public DataTable GetTicketDetailBranchwise(decimal BranchID)
        {
            SqlParameter prmBranchID = new SqlParameter("BranchID", SqlDbType.Decimal, 18);
            prmBranchID.Value = BranchID;
            return DataAccess.GetFromDataTable("SP_GetTicketDetailBranchwise", prmBranchID);
        }

        public DataTable GetTicketDetailCriticalwise(decimal ProblemCategoryId, decimal BankID)
        {
            SqlParameter prmProblemCategoryId = new SqlParameter("ProblemCategoryId", SqlDbType.Decimal, 18);
            prmProblemCategoryId.Value = ProblemCategoryId;
            SqlParameter prmBankID = new SqlParameter("BankID", SqlDbType.Decimal, 18);
            prmBankID.Value = BankID;

            return DataAccess.GetFromDataTable("SP_GetTicketDetailCriticalwise", prmProblemCategoryId, prmBankID);
        }
        public DataTable GetTicketDetailCriticalwise_NETWORKZONE(decimal ProblemCategoryId, decimal RegionID)
        {
            SqlParameter prmProblemCategoryId = new SqlParameter("ProblemCategoryId", SqlDbType.Decimal, 18);
            prmProblemCategoryId.Value = ProblemCategoryId;
            SqlParameter prmRegionID = new SqlParameter("RegionID", SqlDbType.Decimal, 18);
            prmRegionID.Value = RegionID;
            return DataAccess.GetFromDataTable("SP_GetTicketDetailCriticalwise_NETWORKZONE", prmProblemCategoryId, prmRegionID);
        }

        public DataTable GetNetworkCount()
        {
            return DataAccess.GetFromDataTable("SP_GetTerminalCountByNetwork");
        }
        public DataTable GetNetworkCount1()
        {
            return DataAccess.GetFromDataTable("SP_GetTerminalCountByNetwork_New");
        }
        public DataTable GetNetworkWiseZoneCount()
        {
            return DataAccess.GetFromDataTable("SP_GetTerminalCountByNetworkZoneWise");
        }
        //public DataTable GetNetworkCount1()
        //{
        //    return DataAccess.GetFromDataTable("SP_GetTerminalCountByNetwork_New");
        //}

        public DataTable GetNetworkBindChartOnLoad()
        {
            return DataAccess.GetFromDataTable("SP_GetTerminalCountByNetwork_NewBindChart_ONLOAD");
        }
        public DataTable GetNetworkZONEBindChartOnLoad()
        {
            return DataAccess.GetFromDataTable("SP_GetTerminalCountByNetworkZONE_BindChart_ONLOAD");
        }
        public DataTable GetNetworkBindChart(decimal BankID)
        {
            SqlParameter prmBankID = new SqlParameter("BankID", SqlDbType.Decimal, 18);
            prmBankID.Value = BankID;

            return DataAccess.GetFromDataTable("SP_GetTerminalCountByNetwork_NewBindChart", prmBankID);
        }
        public DataTable GetNetworkZoNEBindChart(decimal RegionID)
        {
            SqlParameter prmRegionID = new SqlParameter("RegionID", SqlDbType.Decimal, 18);
            prmRegionID.Value = RegionID;

            return DataAccess.GetFromDataTable("SP_GetTerminalCountByNetwork_ZoneBindChart", prmRegionID);
        }
        #region Report CallStage
        public DataTable GetTerminalCallStageCount()
        {
            return DataAccess.GetFromDataTable("SP_GetTerminalCallStageCount");
        }
        public DataTable GetTerminalCallStageDetail(String ProblemStatus)
        {
            SqlParameter prmProbleStatus = new SqlParameter("@ProblemStatus", SqlDbType.VarChar, 50);
            prmProbleStatus.Value = ProblemStatus;
            return DataAccess.GetFromDataTable("Sp_GetProblemStatusTerminalWise", prmProbleStatus);
        }

        #endregion



        #endregion

        #region Report Fault

        public DataTable GetFaultyTerminalCount()
        {
            return DataAccess.GetFromDataTable("SP_GetTerminalCountByFault");
        }

        public DataTable GetTicketDetailFaultwise(decimal ProblemID)
        {
            SqlParameter prmProblemID = new SqlParameter("@ProblemID", SqlDbType.Decimal, 18);
            prmProblemID.Value = ProblemID;
            return DataAccess.GetFromDataTable("SP_GetTicketDetailFaultwise", prmProblemID);
        }

        public DataTable ProblemCategory()
        {
            return DataAccess.GetFromDataTable("SP_GetTicketCountByProblemCategory");
        }


        #endregion

        #region Report Device

        public DataTable GetFaultyTerminalCountDevicewise()
        {
            return DataAccess.GetFromDataTable("SP_GetTerminalCountDevciewise");
        }
        public DataTable GetTicketDetailDevicewise(decimal DeviceID)
        {
            SqlParameter prmDeviceID = new SqlParameter("@DeviceID", SqlDbType.Decimal, 18);
            prmDeviceID.Value = DeviceID;
            return DataAccess.GetFromDataTable("SP_GetTicketDetailDevicewise", prmDeviceID);
        }
        public DataTable DeviceType()
        {
            return DataAccess.GetFromDataTable("SP_GetTicketCountByDevicetype");
        }

        #endregion

        #region Report Device Status Type

        public DataTable GetDeviceStatusTypeTerminalCount()
        {
            return DataAccess.GetFromDataTable("SP_GetTerminalCountDevcieStatusTypewise");
        }
        public DataTable GetTicketDetailDeviceStatusTypewise(decimal StatusTypeID)
        {
            SqlParameter prmStatusTypeID = new SqlParameter("@StatusTypeID", SqlDbType.Decimal, 18);
            prmStatusTypeID.Value = StatusTypeID;
            return DataAccess.GetFromDataTable("SP_GetTicketDetailDeviceStatusTypewise", prmStatusTypeID);
        }
        public DataTable CallStage()
        {
            return DataAccess.GetFromDataTable("SP_GetTerminalCounbyCallStatge");
        }

        #endregion

        #region Close Trouble Ticket Details

        public DataTable Close_TroubleTicketDetail(string SearchCondition)
        {
            SqlParameter prmSearchCondition = new SqlParameter("@Condition", SqlDbType.VarChar, 1000);
            prmSearchCondition.Value = SearchCondition;
            return DataAccess.GetFromDataTable("SP_CloseTroubleTicketDetail", prmSearchCondition);
        }

        public DataTable Close_TroubleTicketDetailReport(string SearchCondition)
        {
            SqlParameter prmSearchCondition = new SqlParameter("@Condition", SqlDbType.VarChar, 1000);
            prmSearchCondition.Value = SearchCondition;
            return DataAccess.GetFromDataTable("SP_CloseTroubleTicketDetailReport", prmSearchCondition);
        }

        public int InsertCommentForConfirmedClose(decimal TicketID, string Comment, decimal UserID, string Buisness_Comment)
        {
            SqlParameter prmTicketID = new SqlParameter("@TicketID", SqlDbType.Decimal);
            prmTicketID.Value = TicketID;
            SqlParameter prmComment = new SqlParameter("@Comment", SqlDbType.VarChar, 500);
            prmComment.Value = Comment;
            SqlParameter prmUserID = new SqlParameter("@UserID", SqlDbType.Decimal);
            prmUserID.Value = UserID;
            SqlParameter prmBuisness_Comment = new SqlParameter("@Buisness_Comment", SqlDbType.VarChar, 500);
            prmBuisness_Comment.Value = Buisness_Comment;

            return DataAccess.Execute("SP_Insert_CommentForClose", prmTicketID, prmComment, prmUserID, prmBuisness_Comment);
        }
        public DataTable GetTerminalCountsForCloseticket()
        {
            return DataAccess.GetFromDataTable("SP_TerminalsCountForColse");
        }
        public DataTable ProbleCategaryForCloseticket()
        {
            return DataAccess.GetFromDataTable("SP_GetProblemStatusForClose");
        }
        #endregion

        #region SST Group

        public DataTable SP_FillUpdateSSTGroup(decimal SSTGroupID)
        {
            SqlParameter prmSSTGroupID = new SqlParameter("@SSTGroupID", SqlDbType.Decimal);
            prmSSTGroupID.Value = SSTGroupID;
            return DataAccess.GetFromDataTable("SP_FillUpdateSSTGroup", prmSSTGroupID);
        }

        public int SP_GetSSTGroupMasterCount(string SSTGroupName)
        {
            SqlParameter prmSSTGroupName = new SqlParameter("@SSTGroupName", SqlDbType.VarChar, 50);
            prmSSTGroupName.Value = SSTGroupName;
            int x = DataAccess.GetInt32("SP_GetSSTGroupMasterCount", prmSSTGroupName);
            return x;
        }

        public int SP_InsertSSTGroup(string SSTGroupName, string SSTGroupDescription, decimal CreatedBy, DateTime CreatedDatetime, decimal ModifiedBy, DateTime ModifiedDatetime) // Inserting the new City
        {

            SqlParameter prmSSTGroupName = new SqlParameter("@SSTGroupName", SqlDbType.VarChar, 50);
            prmSSTGroupName.Value = SSTGroupName;
            SqlParameter prmSSTGroupDescription = new SqlParameter("@SSTGroupDescription ", SqlDbType.VarChar, 150);
            prmSSTGroupDescription.Value = SSTGroupDescription;
            SqlParameter prmCreatedBy = new SqlParameter("@CreatedBy", SqlDbType.Decimal);
            prmCreatedBy.Value = CreatedBy;
            SqlParameter prmCreatedDatetime = new SqlParameter("@CreatedDatetime", SqlDbType.SmallDateTime);
            prmCreatedDatetime.Value = CreatedDatetime;
            SqlParameter prmModifiedBy = new SqlParameter("@ModifiedBy", SqlDbType.Decimal);
            prmModifiedBy.Value = ModifiedBy;
            SqlParameter prmModifiedDatetime = new SqlParameter("@ModifiedDatetime", SqlDbType.SmallDateTime);
            prmModifiedDatetime.Value = ModifiedDatetime;
            int x = DataAccess.Execute("SP_InsertSSTGroup", prmSSTGroupName, prmSSTGroupDescription, prmCreatedBy, prmCreatedDatetime, prmModifiedBy, prmModifiedDatetime);
            return x;
        }

        public int SP_UpdateSSTGroup(string SSTGroupName, string SSTGroupDescription, decimal ModifiedBy, DateTime ModifiedDatetime, decimal SSTGroupID)
        {
            SqlParameter prmSSTGroupName = new SqlParameter("@SSTGroupName", SqlDbType.VarChar, 50);
            prmSSTGroupName.Value = SSTGroupName;
            SqlParameter prmSSTGroupDescription = new SqlParameter("@SSTGroupDescription", SqlDbType.VarChar, 150);
            prmSSTGroupDescription.Value = SSTGroupDescription;
            SqlParameter prmModifiedBy = new SqlParameter("@ModifiedBy", SqlDbType.Decimal);
            prmModifiedBy.Value = ModifiedBy;
            SqlParameter prmModifiedDatetime = new SqlParameter("@ModifiedDatetime", SqlDbType.SmallDateTime);
            prmModifiedDatetime.Value = ModifiedDatetime;
            SqlParameter prmSSTGroupID = new SqlParameter("@SSTGroupID", SqlDbType.Decimal);
            prmSSTGroupID.Value = (SSTGroupID);
            int x = DataAccess.Execute("SP_UpdateSSTGroup", prmSSTGroupName, prmSSTGroupDescription, prmModifiedBy, prmModifiedDatetime, prmSSTGroupID);
            return x;
        }

        public DataTable SP_EditSSTGroup()
        {
            return DataAccess.GetFromDataTable("SP_EditSSTGroup");
        }

        public DataTable SP_GetSSTGroupID(decimal SSTGroupID)
        {
            SqlParameter prmSSTGroupID = new SqlParameter("@SSTGroupID", SqlDbType.Decimal);
            prmSSTGroupID.Value = SSTGroupID;
            return DataAccess.GetFromDataTable("SP_GetSSTGroupID", prmSSTGroupID);
        }

        public int SP_DeleteSSTGroup(decimal SSTGroupID)
        {
            SqlParameter prmSSTGroupID = new SqlParameter("@SSTGroupID", SqlDbType.Decimal);
            prmSSTGroupID.Value = SSTGroupID;
            int x = DataAccess.Execute("SP_DeleteSSTGroup", prmSSTGroupID);
            return x;
        }

        #endregion

        #region Follow Up Detail

        public DataTable SP_FollowUpDetails(string SearchCondition)
        {
            SqlParameter prmSearchCondition = new SqlParameter("@Condition", SqlDbType.VarChar, 1000);
            prmSearchCondition.Value = SearchCondition;
            return DataAccess.GetFromDataTable("SP_DisplayFollowupDetails_RepeatCount", prmSearchCondition);
        }

        public DataTable SP_FollowUpDetailsReport(string SearchCondition)
        {
            SqlParameter prmSearchCondition = new SqlParameter("@Condition", SqlDbType.VarChar, 1000);
            prmSearchCondition.Value = SearchCondition;
            return DataAccess.GetFromDataTable("SP_DisplayFollowupDetailsReport", prmSearchCondition);
        }

        #endregion

        #region Open Trouble Tickets Details

        //public DataTable OpenTroubleTicketDetail(string SearchCondition)
        //{
        //    SqlParameter prmSearchCondition = new SqlParameter("@Condition", SqlDbType.VarChar, 1000);
        //    prmSearchCondition.Value = SearchCondition;
        //    return DataAccess.GetFromDataTable("SP_OpenTroubleTicketDetail_RepeatCount", prmSearchCondition);
        //}
        public DataTable OpenTroubleTicketDetail(string SearchCondition, decimal region, decimal BranchId)
        {
            SqlParameter prmSearchCondition = new SqlParameter("@Condition", SqlDbType.VarChar, 1000);
            prmSearchCondition.Value = SearchCondition;
            SqlParameter prmregionId = new SqlParameter("@RegionId", SqlDbType.Decimal);
            prmregionId.Value = region;
            SqlParameter prmBranchId = new SqlParameter("@BranchId", SqlDbType.Decimal);
            prmBranchId.Value = BranchId;
            return DataAccess.GetFromDataTable("SP_OpenTroubleTicketDetail_RepeatCount", prmSearchCondition, prmregionId, prmBranchId);
        }

        public DataTable OpenTroubleTicketDetailReport(string SearchCondition)
        {
            SqlParameter prmSearchCondition = new SqlParameter("@Condition", SqlDbType.VarChar, 1000);
            prmSearchCondition.Value = SearchCondition;
            return DataAccess.GetFromDataTable("SP_OpenTroubleTicketDetail_Report", prmSearchCondition);
        }

        #endregion

        #region Restart Windows Service
        public decimal SP_DoAppnRemotelyRestart(decimal SSTID, string GroupName, decimal UserID)
        {
            DataTable dt = new DataTable();
            SqlParameter prmSSTID = new SqlParameter("@SSTID", SqlDbType.Decimal, 4);
            prmSSTID.Value = SSTID;
            SqlParameter prmName = new SqlParameter("@Name", SqlDbType.VarChar, 1000);
            prmName.Value = GroupName;
            SqlParameter prmUserID = new SqlParameter("@UserID", SqlDbType.Decimal, 4);
            prmUserID.Value = UserID;
            dt = DataAccess.GetFromDataTable("SP_DoAppnRemotelyRestart", prmName, prmUserID, prmSSTID);
            if (dt != null)
            {
                DataRow dr = dt.Rows[0];
                return decimal.Parse(dr["ScheduleID"].ToString());
            }
            else
                return 0.0M;
        }

        public decimal WindowsServiceRestart(decimal SSTID, string GroupName, decimal UserID)
        {
            DataTable dt = new DataTable();
            SqlParameter prmSSTID = new SqlParameter("@SSTID", SqlDbType.Decimal, 4);
            prmSSTID.Value = SSTID;
            SqlParameter prmName = new SqlParameter("@Name", SqlDbType.VarChar, 1000);
            prmName.Value = GroupName;
            SqlParameter prmUserID = new SqlParameter("@UserID", SqlDbType.Decimal, 4);
            prmUserID.Value = UserID;
            dt = DataAccess.GetFromDataTable("SP_DoRestartService", prmName, prmUserID, prmSSTID);

            if (dt != null)
            {
                DataRow dr = dt.Rows[0];
                return decimal.Parse(dr["ScheduleID"].ToString());
            }
            else
                return 0.0M;
        }

        #endregion

        #region Get System Perofrmance Of SST

        public decimal GetSystemPerformaceOfSST(decimal SSTID, string GroupName, decimal UserID)
        {
            DataTable dt = new DataTable();
            SqlParameter prmSSTID = new SqlParameter("@SSTID", SqlDbType.Decimal, 4);
            prmSSTID.Value = SSTID;
            SqlParameter prmName = new SqlParameter("@Name", SqlDbType.VarChar, 1000);
            prmName.Value = GroupName;
            SqlParameter prmUserID = new SqlParameter("@UserID", SqlDbType.Decimal, 4);
            prmUserID.Value = UserID;
            dt = DataAccess.GetFromDataTable("SP_DoSystemPerformanceOfSST", prmName, prmUserID, prmSSTID);

            if (dt != null)
            {
                DataRow dr = dt.Rows[0];
                return decimal.Parse(dr["ScheduleID"].ToString());
            }
            else
                return 0.0M;
        }
        public DataTable GetSystemPerformaceResult(string strScheduleIDList)
        {
            SqlParameter prmResult = new SqlParameter("@ScheduleIDList", SqlDbType.VarChar, 5000);
            prmResult.Value = strScheduleIDList;
            return DataAccess.GetFromDataTable("SP_SSTPerformance_Restult", prmResult);
        }

        public DataTable GetSSTPerformanceXml(string TerminalID, decimal ScheduleID)
        {
            SqlParameter prmTerminalID = new SqlParameter("@TerminalID", SqlDbType.VarChar, 1000);
            prmTerminalID.Value = TerminalID;
            SqlParameter prmScheduleID = new SqlParameter("@ScheduleID", SqlDbType.Decimal, 4);
            prmScheduleID.Value = ScheduleID;
            return DataAccess.GetFromDataTable("SP_GetSSTPerformanceInfoXml", prmTerminalID, prmScheduleID);
        }


        #endregion

        #region Pull File

        public decimal DoPullFile(decimal SSTID, string GroupName, decimal UserID)
        {
            DataTable dt = new DataTable();
            SqlParameter prmSSTID = new SqlParameter("@SSTID", SqlDbType.Decimal, 4);
            prmSSTID.Value = SSTID;
            SqlParameter prmName = new SqlParameter("@Name", SqlDbType.VarChar, 1000);
            prmName.Value = GroupName;
            SqlParameter prmUserID = new SqlParameter("@UserID", SqlDbType.Decimal, 4);
            prmUserID.Value = UserID;
            dt = DataAccess.GetFromDataTable("SP_DoPullFile", prmName, prmUserID, prmSSTID);

            if (dt != null)
            {
                DataRow dr = dt.Rows[0];
                return decimal.Parse(dr["ScheduleID"].ToString());
            }
            else
                return 0.0M;
        }


        public decimal DoScreenShot(decimal SSTID, string GroupName, decimal UserID)
        {
            DataTable dt = new DataTable();
            SqlParameter prmSSTID = new SqlParameter("@SSTID", SqlDbType.Decimal, 4);
            prmSSTID.Value = SSTID;
            SqlParameter prmName = new SqlParameter("@Name", SqlDbType.VarChar, 1000);
            prmName.Value = GroupName;
            SqlParameter prmUserID = new SqlParameter("@UserID", SqlDbType.Decimal, 4);
            prmUserID.Value = UserID;
            dt = DataAccess.GetFromDataTable("SP_DoScreenShot", prmName, prmUserID, prmSSTID);

            if (dt != null)
            {
                DataRow dr = dt.Rows[0];
                return decimal.Parse(dr["ScheduleID"].ToString());
            }
            else
                return 0.0M;
        }

        public DataTable PullFileResultforFailed(string strScheduleIDList)
        {
            SqlParameter prmResult = new SqlParameter("@ScheduleIDList", SqlDbType.VarChar, 5000);
            prmResult.Value = strScheduleIDList;
            return DataAccess.GetFromDataTable("SP_PullFile_RestultforFailed", prmResult);
        }
        public DataTable PullFileResult(string strScheduleIDList)
        {
            SqlParameter prmResult = new SqlParameter("@ScheduleIDList", SqlDbType.VarChar, 5000);
            prmResult.Value = strScheduleIDList;
            return DataAccess.GetFromDataTable("SP_PullFile_Restult", prmResult);
        }
        public DataTable SP_ScreenShot_Restult(string strScheduleIDList)
        {
            SqlParameter prmResult = new SqlParameter("@ScheduleIDList", SqlDbType.VarChar, 5000);
            prmResult.Value = strScheduleIDList;
            return DataAccess.GetFromDataTable("SP_ScreenShot_Restult", prmResult);
        }
        public DataTable SP_ScreenShot_RestultforFailed(string strScheduleIDList)
        {
            SqlParameter prmResult = new SqlParameter("@ScheduleIDList", SqlDbType.VarChar, 5000);
            prmResult.Value = strScheduleIDList;
            return DataAccess.GetFromDataTable("SP_ScreenShot_RestultforFailed", prmResult);
        }

        public DataTable GetBankExtractPath(string TerminalID)
        {
            SqlParameter prmTerminalID = new SqlParameter("@TerminalID", SqlDbType.VarChar, 100);
            prmTerminalID.Value = TerminalID;
            return DataAccess.GetFromDataTable("SP_GetBankExtractPath", prmTerminalID);
        }
        #endregion

        #region Push File


        public DataTable SP_GetSSTTypeForPush(decimal SST_TypeID)
        {
            SqlParameter prmSST_TypeID = new SqlParameter("@SSTTypeID", SqlDbType.Decimal);
            prmSST_TypeID.Value = SST_TypeID;
            return DataAccess.GetFromDataTable("SP_GetSSTTypeForPush", prmSST_TypeID);
        }

        public decimal DoPushFile(decimal SSTID, string GroupName, decimal UserID)
        {
            DataTable dt = new DataTable();
            SqlParameter prmSSTID = new SqlParameter("@SSTID", SqlDbType.Decimal, 4);
            prmSSTID.Value = SSTID;
            SqlParameter prmName = new SqlParameter("@Name", SqlDbType.VarChar, 1000);
            prmName.Value = GroupName;
            SqlParameter prmUserID = new SqlParameter("@UserID", SqlDbType.Decimal, 4);
            prmUserID.Value = UserID;
            dt = DataAccess.GetFromDataTable("SP_DoPushFile", prmName, prmUserID, prmSSTID);

            if (dt != null)
            {
                DataRow dr = dt.Rows[0];
                return decimal.Parse(dr["ScheduleID"].ToString());
            }
            else
                return 0.0M;
        }

        #endregion

        #region run Self Diagnostic

        public decimal RunSelfDiagonstic(decimal SSTID, string GroupName, decimal UserID)
        {
            DataTable dt = new DataTable();
            SqlParameter prmSSTID = new SqlParameter("@SSTID", SqlDbType.Decimal, 4);
            prmSSTID.Value = SSTID;
            SqlParameter prmName = new SqlParameter("@Name", SqlDbType.VarChar, 1000);
            prmName.Value = GroupName;
            SqlParameter prmUserID = new SqlParameter("@UserID", SqlDbType.Decimal, 4);
            prmUserID.Value = UserID;
            dt = DataAccess.GetFromDataTable("SP_DoSelfDiagnostic", prmName, prmUserID, prmSSTID);

            if (dt != null)
            {
                DataRow dr = dt.Rows[0];
                return decimal.Parse(dr["ScheduleID"].ToString());
            }
            else
                return 0.0M;
        }

        public DataTable SelfDiagnosticResult(string strScheduleIDList)
        {
            SqlParameter prmResult = new SqlParameter("@ScheduleIDList", SqlDbType.VarChar, 5000);
            prmResult.Value = strScheduleIDList;
            return DataAccess.GetFromDataTable("SP_SelfDiagnostic_Restult", prmResult);
        }

        #endregion

        #region SST Group Mapping

        public DataTable SP_MappingCascading(int SST_BankID, int SST_RegionID, int SST_BranchID)
        {
            SqlParameter prmSSTBankID = new SqlParameter("@SSTBankID", SqlDbType.Int);
            prmSSTBankID.Value = SST_BankID;
            SqlParameter prmSSTRegionID = new SqlParameter("@SSTRegionID", SqlDbType.Int);
            prmSSTRegionID.Value = SST_RegionID;
            SqlParameter prmSSTBranchID = new SqlParameter("@SSTBranchID", SqlDbType.Int);
            prmSSTBranchID.Value = (SST_BranchID);
            return DataAccess.GetFromDataTable("SP_MappingCascading_nogroup", prmSSTBankID, prmSSTBranchID, prmSSTRegionID);
        }
        public DataTable SP_MappingCascading(int SST_BankID, int SST_RegionID, int SST_BranchID, int SSTgroupID)
        {
            SqlParameter prmSSTBankID = new SqlParameter("@SSTBankID", SqlDbType.Int);
            prmSSTBankID.Value = SST_BankID;
            SqlParameter prmSSTRegionID = new SqlParameter("@SSTRegionID", SqlDbType.Int);
            prmSSTRegionID.Value = SST_RegionID;
            SqlParameter prmSSTBranchID = new SqlParameter("@SSTBranchID", SqlDbType.Int);
            prmSSTBranchID.Value = (SST_BranchID);
            SqlParameter prmSSTgroupID = new SqlParameter("@SSTgroupID", SqlDbType.Int);
            prmSSTgroupID.Value = (SSTgroupID);
            return DataAccess.GetFromDataTable("SP_MappingCascading", prmSSTBankID, prmSSTBranchID, prmSSTRegionID, prmSSTgroupID);
        }
        public int InsertSSTGroupTable(decimal SSTRegionID, decimal SSTBankID, decimal SSTBranchID, decimal SSTGroupID, decimal SSTID, string SST_TerminalID, decimal SST_Type, decimal SST_LinkType, decimal Link_ProviderName, decimal CreatedBy, decimal ModifiedBy)
        {
            SqlParameter prmsstRegion = new SqlParameter("@SSTRegionID", SqlDbType.Decimal);
            prmsstRegion.Value = SSTRegionID;
            SqlParameter prmsstBank = new SqlParameter("@SSTBankID", SqlDbType.Decimal);
            prmsstBank.Value = SSTBankID;
            SqlParameter prmsstBranch = new SqlParameter("@SSTBranchID", SqlDbType.Decimal);
            prmsstBranch.Value = (SSTBranchID);
            SqlParameter prmsstGroupID = new SqlParameter("@SSTGroupID", SqlDbType.Decimal);
            prmsstGroupID.Value = (SSTGroupID);
            SqlParameter prmsstID = new SqlParameter("@SSTID", SqlDbType.Decimal);
            prmsstID.Value = (SSTID);
            SqlParameter prmSST_TerminalID = new SqlParameter("@SST_TerminalID", SqlDbType.VarChar, 50);
            prmSST_TerminalID.Value = SST_TerminalID;
            SqlParameter prmSST_Type = new SqlParameter("@SSTTypeID", SqlDbType.Decimal);
            prmSST_Type.Value = SST_Type;
            SqlParameter prmSST_LinkType = new SqlParameter("@SSTLinkTypeID", SqlDbType.Decimal);
            prmSST_LinkType.Value = SST_LinkType;
            SqlParameter prmLink_ProviderName = new SqlParameter("@SSTLinkServiceProvideID", SqlDbType.Decimal);
            prmLink_ProviderName.Value = Link_ProviderName;
            SqlParameter prmCreatedBy = new SqlParameter("@CreatedBy", SqlDbType.Decimal);
            prmCreatedBy.Value = CreatedBy;
            SqlParameter prmModifiedBy = new SqlParameter("@ModifiedBy", SqlDbType.Decimal);
            prmModifiedBy.Value = ModifiedBy;
            int x = DataAccess.Execute("SP_Insert_SST_Group_Table", prmsstRegion, prmsstBank, prmsstBranch, prmsstGroupID, prmsstID, prmSST_TerminalID,
                prmSST_Type, prmSST_LinkType, prmLink_ProviderName, prmCreatedBy, prmModifiedBy);
            return x;
        }
        public int SP_InsertSSTActivityGroup_Details(decimal SSTBankID, decimal SSTRegionID, decimal SSTBranchID, decimal SSTGroupID, decimal SSTTypeID, decimal SSTLinkTypeID, decimal SSTLinkServiceProvideID, decimal SSTID, string SST_TerminalID, decimal CreatedBy, decimal ModifiedBy)
        {
            SqlParameter prmSSTBankID = new SqlParameter("@SSTBankID", SqlDbType.Decimal);
            prmSSTBankID.Value = SSTBankID;
            SqlParameter prmSSTRegionID = new SqlParameter("@SSTRegionID", SqlDbType.Decimal);
            prmSSTRegionID.Value = SSTRegionID;
            SqlParameter prmSSTBranchID = new SqlParameter("@SSTBranchID", SqlDbType.Decimal);
            prmSSTBranchID.Value = SSTBranchID;
            SqlParameter prmSSTGroupID = new SqlParameter("@SSTGroupID", SqlDbType.Decimal);
            prmSSTGroupID.Value = SSTGroupID;
            SqlParameter prmSSTTypeID = new SqlParameter("@SSTTypeID", SqlDbType.Decimal);
            prmSSTTypeID.Value = SSTTypeID;
            SqlParameter prmSSTLinkTypeID = new SqlParameter("@SSTLinkTypeID", SqlDbType.Decimal);
            prmSSTLinkTypeID.Value = SSTLinkTypeID;
            SqlParameter prmSSTLinkServiceProvideID = new SqlParameter("@SSTLinkServiceProvideID", SqlDbType.Decimal);
            prmSSTLinkServiceProvideID.Value = SSTLinkServiceProvideID;
            SqlParameter prmSSTID = new SqlParameter("@SSTID", SqlDbType.Decimal);
            prmSSTID.Value = SSTID;
            SqlParameter prmTerminalID = new SqlParameter("@SST_TerminalID", SqlDbType.VarChar, 50);
            prmTerminalID.Value = SST_TerminalID;
            SqlParameter prmCreatedBy = new SqlParameter("@CreatedBy", SqlDbType.Decimal);
            prmCreatedBy.Value = CreatedBy;
            SqlParameter prmModifiedBy = new SqlParameter("@ModifiedBy", SqlDbType.Decimal);
            prmModifiedBy.Value = ModifiedBy;
            int x = DataAccess.Execute("SP_InsertSSTActivityGroup_Details", prmSSTBankID, prmSSTRegionID, prmSSTBranchID, prmSSTGroupID, prmSSTTypeID, prmSSTLinkTypeID, prmSSTLinkServiceProvideID, prmSSTID, prmTerminalID, prmCreatedBy, prmModifiedBy);
            return x;
        }
        public DataTable SP_GetSSTGroup()
        {
            return DataAccess.GetFromDataTable("SP_GetSSTGroup");
        }
        public DataTable SP_FillGridView(decimal SSTGroupID)
        {
            SqlParameter prmsstgroupID = new SqlParameter("@SSTGroupID", SqlDbType.Decimal);
            prmsstgroupID.Value = (@SSTGroupID);
            return DataAccess.GetFromDataTable("SP_FillGridView", prmsstgroupID);
        }
        public int SP_DeleteChkheader_ActivityGroupList(decimal SSTGroupID)
        {
            SqlParameter prmSSTGroupID = new SqlParameter("@SSTGroupID ", SqlDbType.Decimal);
            prmSSTGroupID.Value = SSTGroupID;
            int x = DataAccess.Execute("SP_DeleteChkheader_ActivityGroupList", prmSSTGroupID);
            return x;
        }
        public int sp_Delete_SSTActivityGroupListDetails_CDMS(decimal SSTID, decimal SSTGroupID)
        {
            SqlParameter prmsSSTID = new SqlParameter("@SSTID", SqlDbType.Decimal);
            prmsSSTID.Value = SSTID;
            SqlParameter prmSSTGroupID = new SqlParameter("@SSTGroupID", SqlDbType.Decimal);
            prmSSTGroupID.Value = SSTGroupID;
            int x = DataAccess.Execute("sp_Delete_SSTActivityGroupListDetails_CDMS", prmsSSTID, prmSSTGroupID);
            return x;
        }
        public int SP_DeleteSSTGroup_Table(decimal SSTGroupID, decimal SSTID)
        {
            SqlParameter prmSSTGroupID = new SqlParameter("@SSTGroupID", SqlDbType.Decimal);
            prmSSTGroupID.Value = SSTGroupID;
            SqlParameter prmsSSTID = new SqlParameter("@SSTID", SqlDbType.Decimal);
            prmsSSTID.Value = SSTID;
            int x = DataAccess.Execute("SP_DeleteSSTGroup_Table", prmSSTGroupID, prmsSSTID);
            return x;
        }
        public int SP_DeleteChkheader_ActivityGroupListS(decimal SSTGroupID, string SSTGroupName)
        {
            SqlParameter prmSSTGroupID = new SqlParameter("@SSTGroupID", SqlDbType.Decimal);
            prmSSTGroupID.Value = SSTGroupID;
            SqlParameter prmSSTGroupName = new SqlParameter("@SSTGroupName", SqlDbType.VarChar, 50);
            prmSSTGroupName.Value = SSTGroupName;
            int x = DataAccess.Execute("SP_DeleteChkheader_ActivityGroupList", prmSSTGroupID, prmSSTGroupName);
            return x;
        }
        public int SP_CheckTerminalID(string SST_TerminalID, decimal sstGroupID)
        {
            SqlParameter prmterminalID = new SqlParameter("@SST_TerminalID", SqlDbType.NVarChar, 50);
            prmterminalID.Value = SST_TerminalID;
            SqlParameter prmsstGroupID = new SqlParameter("@SST_sstGroupID", SqlDbType.Decimal, 50);
            prmsstGroupID.Value = sstGroupID;
            int x = DataAccess.GetInt32("SP_CheckTerminalID", prmterminalID, prmsstGroupID);
            return x;
        }
        public DataTable SP_CountEditSSTGroupMapping() // Fills the Update page.
        {
            return DataAccess.GetFromDataTable("SP_CountEditSSTGroupMapping");
        }
        #endregion

        #region Trouble Ticket Details For Branch


        public DataTable TroubleTicketDerailForBranch(string SearchCondition)
        {
            SqlParameter prmCondition = new SqlParameter("@Condition", SqlDbType.VarChar, 5000);
            prmCondition.Value = SearchCondition;

            return DataAccess.GetFromDataTable("SP_TroubleTicketDetailForBranch_RepeatCount", prmCondition);
        }

        public DataTable FillBranchForTroubleTicket()
        {
            return DataAccess.GetFromDataTable("SP_FillBranch");
        }


        public DataTable FillDeviceForBranch()
        {
            return DataAccess.GetFromDataTable("SP_FillDevice");
        }


        public DataTable FillFaults(string strDeviceIDlist)
        {

            SqlParameter prmDev = new SqlParameter("@DevIDList", SqlDbType.VarChar, 5000);
            prmDev.Value = strDeviceIDlist;

            return DataAccess.GetFromDataTable("SP_FillFaults", prmDev);
        }


        public DataTable GetTerminalCountsForBranch(string Str)
        {
            SqlParameter prmDev = new SqlParameter("@select", SqlDbType.VarChar, 6000);
            prmDev.Value = Str;
            return DataAccess.GetFromDataTable("SP_TerminalsCountForBranch", prmDev);
        }


        public DataTable TroubleTicketDetailReportForBranch(string SearchCondition)
        {
            SqlParameter prmSearchCondition = new SqlParameter("@Condition", SqlDbType.VarChar, 1000);
            prmSearchCondition.Value = SearchCondition;
            return DataAccess.GetFromDataTable("SP_TroubleTicketDetailForBranch_Report", prmSearchCondition);
        }

        #endregion

        #region User Access System

        public void GetUserAccessRightsForUser(out bool IsView, out bool IsAdd, out bool IsEdit, out bool IsDelete, int UserID, int PageID)
        {
            IsView = false; IsAdd = false; IsEdit = false; IsDelete = false;

            SqlParameter prmUserID = new SqlParameter("@UserID", SqlDbType.Int, 4);
            prmUserID.Value = UserID;

            SqlParameter prmPageID = new SqlParameter("@PageID", SqlDbType.Int, 4);
            prmPageID.Value = PageID;

            DataTable dtUserAccess = new DataTable();
            dtUserAccess = DataAccess.GetFromDataTable("SP_UserAccessRightsForPage", prmUserID, prmPageID);

            foreach (DataRow drUserAccess in dtUserAccess.Rows)
            {
                IsView = Convert.ToBoolean(drUserAccess["View"]);
                IsAdd = Convert.ToBoolean(drUserAccess["Add"]);
                IsEdit = Convert.ToBoolean(drUserAccess["Edit"]);
                IsDelete = Convert.ToBoolean(drUserAccess["Delete"]);
            }
        }

        public DataTable GetMenuList()
        {

            return DataAccess.GetFromDataTable("SP_GetMenuList");
        }
        public DataTable SP_GetMenuListByRole(decimal RoleID)
        {
            SqlParameter prmRoleID = new SqlParameter("@RoleID", SqlDbType.Int, 4);
            prmRoleID.Value = RoleID;
            return DataAccess.GetFromDataTable("SP_GetMenuListByRole", prmRoleID);
        }
        public DataTable SP_GetMenuListONSubmit(int RoleID, int LoginUserID)
        {
            SqlParameter prmRoleID = new SqlParameter("@RoleID", SqlDbType.Int, 4);
            prmRoleID.Value = RoleID;
            SqlParameter prmLoginUserID = new SqlParameter("@UserID", SqlDbType.Int, 4);
            prmLoginUserID.Value = LoginUserID;
            return DataAccess.GetFromDataTable("SP_GetMenuListONSubmit", prmRoleID, prmLoginUserID);
        }

        public DataTable GetUserList(int RoleID, int LoginUserID)
        {
            SqlParameter prmRoleID = new SqlParameter("@RoleID", SqlDbType.Int, 4);
            prmRoleID.Value = RoleID;
            SqlParameter prmLoginUserID = new SqlParameter("@LoginUserID", SqlDbType.Int, 4);
            prmLoginUserID.Value = LoginUserID;
            return DataAccess.GetFromDataTable("SP_GEtUserList", prmRoleID, prmLoginUserID);
        }

        public int InsertInUserAccess(int RoleID, int UserID, int ParentMenuID, int MenuID, bool IsView, bool IsAdd, bool IsEdit, bool IsDelete)
        {
            SqlParameter prmRoleID = new SqlParameter("@RoleID", SqlDbType.Int, 4);
            prmRoleID.Value = RoleID;
            SqlParameter prmUserID = new SqlParameter("@UserID", SqlDbType.Int, 4);
            prmUserID.Value = UserID;

            SqlParameter prmParentMenuID = new SqlParameter("@ParentMenuID", SqlDbType.Int, 4);
            prmParentMenuID.Value = ParentMenuID;

            SqlParameter prmMenuID = new SqlParameter("@MenuID", SqlDbType.Int, 4);
            prmMenuID.Value = MenuID;
            SqlParameter prmView = new SqlParameter("@View", SqlDbType.Bit);
            prmView.Value = IsView;
            SqlParameter prmAdd = new SqlParameter("@Add", SqlDbType.Bit);
            prmAdd.Value = IsAdd;
            SqlParameter prmEdit = new SqlParameter("@Edit", SqlDbType.Bit);
            prmEdit.Value = IsEdit;
            SqlParameter prmDelete = new SqlParameter("@Delete", SqlDbType.Bit);
            prmDelete.Value = IsDelete;
            return DataAccess.Execute("SP_UserAccessSystemTransaction", prmParentMenuID, prmRoleID, prmUserID, prmMenuID, prmView, prmAdd, prmEdit, prmDelete);
        }

        public DataTable UserAccessPrmission(int RoleID, int UserID)
        {
            SqlParameter prmRoleID = new SqlParameter("@RoleID", SqlDbType.Int, 4);
            prmRoleID.Value = RoleID;
            SqlParameter prmUserID = new SqlParameter("@UserID", SqlDbType.Int, 4);
            prmUserID.Value = UserID;
            return DataAccess.GetFromDataTable("SP_GetUserAccessPermision", prmRoleID, prmUserID);
        }

        #endregion

        #region Reports

        public DataTable GetReportDetail(DateTime fromdate, DateTime todate, Decimal SelectBankId, decimal SelectSST, decimal RegionId, decimal BranchId)
        {

            SqlParameter prmfromdate = new SqlParameter("@fromDate", SqlDbType.DateTime);
            prmfromdate.Value = fromdate;
            SqlParameter prmtodate = new SqlParameter("@todate", SqlDbType.DateTime);
            prmtodate.Value = todate;
            SqlParameter prmSelectBankId = new SqlParameter("@BankId", SqlDbType.Decimal);
            prmSelectBankId.Value = SelectBankId;
            SqlParameter prmSelectSST = new SqlParameter("@SST_TypeID", SqlDbType.Decimal);
            prmSelectSST.Value = SelectSST;
            SqlParameter prmRegionId = new SqlParameter("@RegionId", SqlDbType.Decimal);
            prmRegionId.Value = RegionId;
            SqlParameter prmBranchId = new SqlParameter("@BranchId", SqlDbType.Decimal);
            prmBranchId.Value = BranchId;
            return DataAccess.GetFromDataTable("Sp_TroubLeTicketReportDetail", prmfromdate, prmtodate, prmSelectBankId, prmSelectSST, prmRegionId, prmBranchId);
        }
        public DataTable GetSummaryReport(DateTime fromdateSummary, DateTime todateSummary, Decimal SelectBankId, decimal SelectTerminal, decimal RegionId, decimal BranchId)
        {

            SqlParameter prmfromdateSummary = new SqlParameter("@SummryFromDate", SqlDbType.DateTime);
            prmfromdateSummary.Value = fromdateSummary;
            SqlParameter prmtodateSummary = new SqlParameter("@SummaryToDate", SqlDbType.DateTime);
            prmtodateSummary.Value = todateSummary;
            SqlParameter prmSelectBankId = new SqlParameter("@BankId", SqlDbType.Decimal);
            prmSelectBankId.Value = SelectBankId;
            SqlParameter prmSelectTerminal = new SqlParameter("@SST_TypeID", SqlDbType.Decimal);
            prmSelectTerminal.Value = SelectTerminal;
            SqlParameter prmRegionId = new SqlParameter("@RegionId", SqlDbType.Decimal);
            prmRegionId.Value = RegionId;
            SqlParameter prmBranchId = new SqlParameter("@BranchId", SqlDbType.Decimal);
            prmBranchId.Value = BranchId;
            return DataAccess.GetFromDataTable("Sp_SummaryReport", prmfromdateSummary, prmtodateSummary, prmSelectBankId, prmSelectTerminal, prmRegionId, prmBranchId);
        }
        public DataTable GetSummryProblemStatus(DateTime fromdateSummryStatus, DateTime todateSummryStatus, Decimal SelectBankId, decimal SelectTerminal, decimal RegionId, decimal BranchId)
        {
            SqlParameter prmfromdateSummryStatus = new SqlParameter("SummryStatusFromDate", SqlDbType.DateTime);
            prmfromdateSummryStatus.Value = fromdateSummryStatus;
            SqlParameter prmtodateSummryStatus = new SqlParameter("SummaryStatusToDate", SqlDbType.DateTime);
            prmtodateSummryStatus.Value = todateSummryStatus;
            SqlParameter prmSelectBankId = new SqlParameter("@BankId", SqlDbType.Decimal);
            prmSelectBankId.Value = SelectBankId;
            SqlParameter prmSelectTerminal = new SqlParameter("@SST_TypeID", SqlDbType.Decimal);
            prmSelectTerminal.Value = SelectTerminal;
            SqlParameter prmRegionId = new SqlParameter("@RegionId", SqlDbType.Decimal);
            prmRegionId.Value = RegionId;
            SqlParameter prmBranchId = new SqlParameter("@BranchId", SqlDbType.Decimal);
            prmBranchId.Value = BranchId;
            return DataAccess.GetFromDataTable("Sp_SummaryReportProblemStatus", prmfromdateSummryStatus, prmtodateSummryStatus, prmSelectBankId, prmSelectTerminal, prmRegionId, prmBranchId);
        }
        public DataTable GetRportDropDown()
        {
            return DataAccess.GetFromDataTable("Sp_FillReportDropDown");
        }

        //anagha 
        //public DataTable GetRportDropDowncircle()
        //{
        //    return DataAccess.GetFromDataTable("Sp_FillReportDropDowncircle");
        //}
        public DataTable GetBranchesCHK()
        {
            return DataAccess.GetFromDataTable("Sp_FillBranchNameChkList");
        }

        public DataTable dtFillReportDeviceList()
        {
            return DataAccess.GetFromDataTable("Sp_FillReportDeviceDropDown");
        }
        public DataTable GetCloseDeatilReport(DateTime fromdate, DateTime todate, Decimal BankId, Decimal SelectSST, decimal RegionId, decimal BranchId)
        {
            SqlParameter prmfromdate = new SqlParameter("@fromDate", SqlDbType.DateTime);
            prmfromdate.Value = fromdate;
            SqlParameter prmtodate = new SqlParameter("@todate", SqlDbType.DateTime);
            prmtodate.Value = todate;
            SqlParameter prmBankId = new SqlParameter("@BankId", SqlDbType.Decimal);
            prmBankId.Value = BankId;
            SqlParameter prmSST_TypeID = new SqlParameter("@SST_TypeID", SqlDbType.Decimal);
            prmSST_TypeID.Value = SelectSST;
            SqlParameter prmRegionId = new SqlParameter("@RegionId", SqlDbType.Decimal);
            prmRegionId.Value = RegionId;
            SqlParameter prmBranchId = new SqlParameter("@BranchId", SqlDbType.Decimal);
            prmBranchId.Value = BranchId;
            return DataAccess.GetFromDataTable("Sp_CloseTroubleTicketReportDetail", prmfromdate, prmtodate, prmBankId, prmSST_TypeID, prmRegionId, prmBranchId);
        }

        public DataTable GetCloseSummaryReport(DateTime fromdate, DateTime todate, Decimal BankId, decimal SelectSST, decimal RegionId, decimal BranchId)
        {
            SqlParameter prmSummryfromdate = new SqlParameter("@SummryFromDate", SqlDbType.DateTime);
            prmSummryfromdate.Value = fromdate;
            SqlParameter prmSummrytodate = new SqlParameter("@SummaryToDate", SqlDbType.DateTime);
            prmSummrytodate.Value = todate;
            SqlParameter prmBankId = new SqlParameter("@BankId", SqlDbType.Decimal);
            prmBankId.Value = BankId;
            SqlParameter prmSelectSST = new SqlParameter("@SST_TypeID", SqlDbType.Decimal);
            prmSelectSST.Value = SelectSST;
            SqlParameter prmRegionId = new SqlParameter("@RegionId", SqlDbType.Decimal);
            prmRegionId.Value = RegionId;
            SqlParameter prmBranchId = new SqlParameter("@BranchId", SqlDbType.Decimal);
            prmBranchId.Value = BranchId;
            return DataAccess.GetFromDataTable("Sp_SummaryReportClose", prmSummryfromdate, prmSummrytodate, prmBankId, prmSelectSST, prmRegionId, prmBranchId);
        }

        //public DataTable GetCloseDeatilReport(DateTime fromdate, DateTime todate, Decimal BankId, Decimal SelectSST)
        //{
        //    SqlParameter prmfromdate = new SqlParameter("@fromDate", SqlDbType.DateTime);
        //    prmfromdate.Value = fromdate;
        //    SqlParameter prmtodate = new SqlParameter("@todate", SqlDbType.DateTime);
        //    prmtodate.Value = todate;
        //    SqlParameter prmBankId = new SqlParameter("@BankId", SqlDbType.Decimal);
        //    prmBankId.Value = BankId;
        //    SqlParameter prmSST_TypeID = new SqlParameter("@SST_TypeID", SqlDbType.Decimal);
        //    prmSST_TypeID.Value = SelectSST;

        //    return DataAccess.GetFromDataTable("Sp_CloseTroubleTicketReportDetail", prmfromdate, prmtodate, prmBankId, prmSST_TypeID);
        //}
        ////public DataTable GetCloseSummaryReport(DateTime fromdate, DateTime todate, Decimal BankId, decimal SelectSST)
        ////{
        ////    SqlParameter prmSummryfromdate = new SqlParameter("@SummryFromDate", SqlDbType.DateTime);
        ////    prmSummryfromdate.Value = fromdate;
        ////    SqlParameter prmSummrytodate = new SqlParameter("@SummaryToDate", SqlDbType.DateTime);
        ////    prmSummrytodate.Value = todate;
        ////    SqlParameter prmBankId = new SqlParameter("@BankId", SqlDbType.Decimal);
        ////    prmBankId.Value = BankId;
        ////    SqlParameter prmSelectSST = new SqlParameter("@SST_TypeID", SqlDbType.Decimal);
        ////    prmSelectSST.Value = SelectSST;
        ////    return DataAccess.GetFromDataTable("Sp_SummaryReportClose", prmSummryfromdate, prmSummrytodate, prmBankId, prmSelectSST);
        ////}
        public DataTable GetAvailabilityReport(string dateDay, string SelectDate, Decimal BankId)
        {
            SqlParameter prmdateDay = new SqlParameter("@Day", SqlDbType.VarChar, 50);
            prmdateDay.Value = dateDay;
            SqlParameter prmSelectDate = new SqlParameter("@TTDate", SqlDbType.VarChar, 50);
            prmSelectDate.Value = SelectDate;
            SqlParameter prmBankId = new SqlParameter("@BankId", SqlDbType.Decimal);
            prmBankId.Value = BankId;

            return DataAccess.GetFromDataTable("SP_GetTerminalAvailability_Daywise2", prmdateDay, prmSelectDate, prmBankId);


        }
        public DataTable GetBranchWeeklyRpt(string fromdate, string todate, Decimal BankId, String E)
        {
            SqlParameter prmFromDate = new SqlParameter("@fromDate", SqlDbType.VarChar, 50);
            prmFromDate.Value = fromdate;
            SqlParameter prmToDate = new SqlParameter("@todate", SqlDbType.VarChar, 50);
            prmToDate.Value = todate;
            SqlParameter prmBankId = new SqlParameter("@BankId", SqlDbType.Decimal);
            prmBankId.Value = BankId;

            SqlParameter prmBranchId = new SqlParameter("@BranchID", SqlDbType.VarChar, 500);
            prmBranchId.Value = E;
            //return DataAccess.GetFromDataTable("Sp_BranchWiseWeeklyTransationReport", prmFromDate, prmToDate, prmBankId, prmBranchId);
            return DataAccess.GetFromDataTable("Sp_BranchWiseWeeklyTransationReport_test", prmFromDate, prmToDate, prmBankId, prmBranchId);


        }

        public DataTable GetChronicReport(DateTime fromdate, DateTime todate, string DeivceIDList, Decimal BankId)
        {
            SqlParameter prmfromdate = new SqlParameter("@fromDate", SqlDbType.DateTime);
            prmfromdate.Value = fromdate;
            SqlParameter prmtodate = new SqlParameter("@todate", SqlDbType.DateTime);
            prmtodate.Value = todate;
            SqlParameter prmDeivceIDList = new SqlParameter("@DeviceIdList", SqlDbType.VarChar, 5000);
            prmDeivceIDList.Value = DeivceIDList;
            SqlParameter prmBankId = new SqlParameter("@BankId", SqlDbType.Decimal);
            prmBankId.Value = BankId;
            return DataAccess.GetFromDataTable("Sp_ChronicFaultReport", prmfromdate, prmtodate, prmDeivceIDList, prmBankId);
        }
        public DataTable GetCityReport(string StateName)
        {
            SqlParameter prmStateName = new SqlParameter("@StateName", SqlDbType.VarChar, 50
                );
            prmStateName.Value = StateName;
            return DataAccess.GetFromDataTable("Sp_ReportCityMaster", prmStateName);
        }
        public DataTable GetCityCountReport(string StateName)
        {
            SqlParameter prmStateName = new SqlParameter("@StateName", SqlDbType.VarChar, 50);
            prmStateName.Value = StateName;
            return DataAccess.GetFromDataTable("Sp_ReportCountCityMaster", prmStateName);
        }
        public DataTable GetBankReport()
        {
            return DataAccess.GetFromDataTable("Sp_ReportBankMaster");
        }
        public DataTable GetBankCountReport()
        {
            return DataAccess.GetFromDataTable("Sp_ReportCountBankMaster");
        }
        public DataTable GetRegionReport()
        {
            return DataAccess.GetFromDataTable("Sp_ReportRegionMaster");
        }
        public DataTable GetRegionCountReport()
        {
            return DataAccess.GetFromDataTable("Sp_ReportCountRegionMaster");
        }
        public DataTable GetBankBranchReport(Decimal RegionId)
        {
            SqlParameter prmRegionId = new SqlParameter("@RegionId ", SqlDbType.Decimal);
            prmRegionId.Value = RegionId;
            return DataAccess.GetFromDataTable("Sp_ReportBankBranch", prmRegionId);
        }
        public DataTable GetCountBankBranchReport(Decimal RegionId)
        {
            SqlParameter prmRegionId = new SqlParameter("@RegionId ", SqlDbType.Decimal);
            prmRegionId.Value = RegionId;
            return DataAccess.GetFromDataTable("Sp_ReportCountBankBranch", prmRegionId);
        }
        public DataTable GetSSTLinkProviderReport()
        {
            return DataAccess.GetFromDataTable("Sp_ReportSSTLinkProvider");
        }
        public DataTable GetSSTLinkProviderCount()
        {
            return DataAccess.GetFromDataTable("Sp_ReportCountSSTLinkProvider");
        }
        public DataTable GetRptStateList()
        {
            return DataAccess.GetFromDataTable("Sp_ReportFillState");
        }
        public DataTable GetRptBranchList()
        {
            return DataAccess.GetFromDataTable("Sp_ReportFillZoneName");
        }
        public DataTable GetVendortReport(string VendorType)
        {
            SqlParameter prmVendorType = new SqlParameter("@VendorType", SqlDbType.VarChar, 50);
            prmVendorType.Value = VendorType;
            return DataAccess.GetFromDataTable("Sp_ReportVendor", prmVendorType);
        }
        public DataTable GetVendorCountReport(string VendorType)
        {
            SqlParameter prmVendorType = new SqlParameter("@VendorType", SqlDbType.VarChar, 50);
            prmVendorType.Value = VendorType;
            return DataAccess.GetFromDataTable("Sp_ReportCountVendor", prmVendorType);
        }
        public DataTable GetFillVendorTypeList()
        {
            return DataAccess.GetFromDataTable("Sp_ReportFillVendorType");
        }
        public DataTable GetSSTTerminalFillDrd(string BranchIdlist)
        {
            SqlParameter prmBranchIdlist = new SqlParameter("@SSTBranchIDList", SqlDbType.VarChar, 500);
            prmBranchIdlist.Value = BranchIdlist;
            return DataAccess.GetFromDataTable("Sp_ReportFillSSTTerminal", prmBranchIdlist);
        }
        public DataTable GetSSTCityDrd(string strRegionId)
        {
            SqlParameter prmstrRegionId = new SqlParameter("@RegionIdList", SqlDbType.VarChar, 500);
            prmstrRegionId.Value = strRegionId;
            return DataAccess.GetFromDataTable("Sp_ReportCity", prmstrRegionId);
        }
        public DataTable GetSSTRegionDrd(Decimal BankId)
        {
            SqlParameter prmBankId = new SqlParameter("@BankId", SqlDbType.Decimal);
            prmBankId.Value = BankId;
            return DataAccess.GetFromDataTable("Sp_ReportFillRegion", prmBankId);
        }
        public DataTable GetSSTBranchDrd(string CityListId)
        {
            SqlParameter prmCityListId = new SqlParameter("@CityListId", SqlDbType.VarChar, 500);
            prmCityListId.Value = CityListId;
            return DataAccess.GetFromDataTable("Sp_ReportfillBranch ", prmCityListId);
        }
        public DataTable GetBankLst()
        {
            return DataAccess.GetFromDataTable("Sp_ReportFillBankLst");
        }
        public DataTable GetRegionList()
        {
            return DataAccess.GetFromDataTable("Sp_ReportFillRegionGroupwise");
        }
        public DataTable GetGroupWiseReport(Decimal BankId, string StrRegionList, string StrCityList, string StrBranchList, string StrSStTerminalList, DateTime fromdate, DateTime todate)
        {
            SqlParameter prmBankId = new SqlParameter("@BankId", SqlDbType.Decimal);
            prmBankId.Value = BankId;
            SqlParameter prmStrRegionList = new SqlParameter("@strRegionId", SqlDbType.VarChar, 500);
            prmStrRegionList.Value = StrRegionList;
            SqlParameter prmStrCityList = new SqlParameter("@strCityId", SqlDbType.VarChar, 500);
            prmStrCityList.Value = StrCityList;
            SqlParameter prmStrBranchList = new SqlParameter("@strBranchId", SqlDbType.VarChar, 500);
            prmStrBranchList.Value = StrBranchList;
            SqlParameter prmStrSStTerminalList = new SqlParameter("@strSSTTerminalId", SqlDbType.VarChar, 500);
            prmStrSStTerminalList.Value = StrSStTerminalList;

            SqlParameter prmfromdate = new SqlParameter("@fromDate", SqlDbType.DateTime);
            prmfromdate.Value = fromdate;
            SqlParameter prmtodate = new SqlParameter("@todate", SqlDbType.DateTime);
            prmtodate.Value = todate;


            return DataAccess.GetFromDataTable("Sp_ReportGroupWiseRegion", prmBankId, prmStrRegionList, prmStrCityList, prmStrBranchList, prmStrSStTerminalList, prmfromdate, prmtodate);

        }
        public DataTable GetTestReport()
        {
            return DataAccess.GetFromDataTable("Sp_ReportTest1");
        }
        public DataTable GetTerminalGroupWiseReport(string strSqlQuary)
        {
            SqlParameter prmSqlQry = new SqlParameter("@SqlQuary", SqlDbType.VarChar, 4000);
            prmSqlQry.Value = strSqlQuary;
            return DataAccess.GetFromDataTable("SP_ExecuteQuary", prmSqlQry);
        }

        //Nikhil
        public DataTable GetTerminalGroupWiseReportnew(string strSqlQuary, string strSqlQuary2)
        {
            SqlParameter prmSqlQry = new SqlParameter("@SqlQuary", SqlDbType.VarChar, 8000);
            prmSqlQry.Value = strSqlQuary;
            SqlParameter prmSqlQuary2 = new SqlParameter("@SqlQuary2", SqlDbType.VarChar, 8000);
            prmSqlQuary2.Value = strSqlQuary2;
            return DataAccess.GetFromDataTable("SP_ExecuteQuary", prmSqlQry, prmSqlQuary2);
        }
        public DataTable GetCityGroupWiseReport(string strSqlCityReportQuary)
        {
            SqlParameter prmSqlCityReportQuary = new SqlParameter("@SqlCityWiseReport", SqlDbType.VarChar, 8000);
            prmSqlCityReportQuary.Value = strSqlCityReportQuary;
            return DataAccess.GetFromDataTable("Sp_ReportCityWiseGroup", prmSqlCityReportQuary);
        }
        public DataTable GetRegionGroupWiseReport(string strSqlRegionWiseQuary)
        {
            SqlParameter prmSqlRegionReportQuary = new SqlParameter("@SqlRegionWiseReport", SqlDbType.VarChar, 8000);
            prmSqlRegionReportQuary.Value = strSqlRegionWiseQuary;
            return DataAccess.GetFromDataTable("Sp_ReportRegionGroupWise", prmSqlRegionReportQuary);

        }
        public DataTable GetDeviceTypeGroupWiseReport(string strSqlDeviceTypeWiseQuary)
        {
            SqlParameter prmSqlDeviceTypeWiseQuary = new SqlParameter("@SqldeviceTypeWiseReport", SqlDbType.VarChar, 8000);
            prmSqlDeviceTypeWiseQuary.Value = strSqlDeviceTypeWiseQuary;
            return DataAccess.GetFromDataTable("Sp_ReportDeviceTypeGroupWise", prmSqlDeviceTypeWiseQuary);

        }
        public DataTable GetSSTProfileBranchFill(decimal RegionId)
        {
            SqlParameter prmRegionId = new SqlParameter("@RegionId", SqlDbType.Decimal);
            prmRegionId.Value = RegionId;
            return DataAccess.GetFromDataTable("Sp_ReportSSTProfileBranchFill", prmRegionId);

        }
        public DataTable GetSSTProfileTerminalFill(decimal BranchId)
        {
            SqlParameter prmBranchId = new SqlParameter("@BranchId", SqlDbType.Decimal);
            prmBranchId.Value = BranchId;
            return DataAccess.GetFromDataTable("Sp_ReportSSTProfileTerminalFill", prmBranchId);

        }
        public DataTable GetSSTProfileReport(string strSSTProfileReportQuary)
        {
            SqlParameter prmSSTProfileReportQuary = new SqlParameter("@SqlSSTProfileQuary", SqlDbType.VarChar, 5000);
            prmSSTProfileReportQuary.Value = strSSTProfileReportQuary;
            return DataAccess.GetFromDataTable("Sp_ReportSSTProfile", prmSSTProfileReportQuary);
        }

        public DataTable GetSSTProfileReport_1(string strSSTProfileReportQuary_1, string strSSTProfileReportQuary_2)
        {
            SqlParameter prmSSTProfileReportQuary_1 = new SqlParameter("@SqlSSTProfileQuary1", SqlDbType.VarChar, 500);
            prmSSTProfileReportQuary_1.Value = strSSTProfileReportQuary_1;
            SqlParameter prmSSTProfileReportQuary_2 = new SqlParameter("@SqlSSTProfileQuary2", SqlDbType.VarChar, 500);
            prmSSTProfileReportQuary_2.Value = strSSTProfileReportQuary_2;
            return DataAccess.GetFromDataTable("Sp_ReportSSTProfile_1", prmSSTProfileReportQuary_1, prmSSTProfileReportQuary_2);
        }


        public DataTable GetSSTProfileRegionList()
        {
            return DataAccess.GetFromDataTable("Sp_ReportRegionList");
        }
        public DataTable GetDeviceTypeReport()
        {
            return DataAccess.GetFromDataTable("Sp_ReportDeviceTypeMaster");
        }
        public DataTable GetDeviceStatusTypeReport()
        {
            return DataAccess.GetFromDataTable("Sp_ReportDeviceStatusType");
        }
        public DataTable GetProblemCategoryReport()
        {
            return DataAccess.GetFromDataTable("Sp_ReportProblemCategory");
        }
        public DataTable GetSSTTypeReport()
        {
            return DataAccess.GetFromDataTable("Sp_ReportSSTType");
        }
        public DataTable GetSSTLinkTypeReport()
        {
            return DataAccess.GetFromDataTable("Sp_ReportSSTLinkType");
        }

        #endregion
        #region MIS Report
        public DataTable GetSSTGbcbBranchDrd(string RegionIdList)
        {
            SqlParameter prmRegionIdList = new SqlParameter("@RegionIdList", SqlDbType.VarChar, 500);
            prmRegionIdList.Value = RegionIdList;
            return DataAccess.GetFromDataTable("Sp_ReportGbcbFillBranch  ", prmRegionIdList);
        }
        public DataTable GetMisStatementPrintingReport(string strStatementPrinting)
        {
            SqlParameter prmstrStatementPrinting = new SqlParameter("@SqlMISStatementPrintingReport", SqlDbType.VarChar, 5000);
            prmstrStatementPrinting.Value = strStatementPrinting;
            return DataAccess.GetFromDataTable("Sp_ReportMISStatementprinting", prmstrStatementPrinting);

        }
        public DataTable GetMISChequeDepositReport(string strChequeDeposit)
        {
            SqlParameter prmstrChequeDeposit = new SqlParameter("@SqlMISChequeDeposit", SqlDbType.VarChar, 8000);
            prmstrChequeDeposit.Value = strChequeDeposit;
            return DataAccess.GetFromDataTable("Sp_ReportMISChequeDeposit", prmstrChequeDeposit);
        }
        public DataTable GetCashDepositReport(string strCashDeposit)
        {
            SqlParameter prmstrCashDeposit = new SqlParameter("@SqlMISCashDeposit", SqlDbType.VarChar, 8000);
            prmstrCashDeposit.Value = strCashDeposit;
            return DataAccess.GetFromDataTable("Sp_ReportMISCashDeposit", prmstrCashDeposit);

        }
        public DataTable GetPassbookPrintingReport(string strPassbookPrinting)
        {
            SqlParameter prmstrPassbookPrintingReport = new SqlParameter("@SqlMISPSB", SqlDbType.VarChar, 8000);
            prmstrPassbookPrintingReport.Value = strPassbookPrinting;
            return DataAccess.GetFromDataTable("Sp_ReportMISPassbookPrinting", prmstrPassbookPrintingReport);
        }
        public DataTable GetMISSummaryReport(DateTime fromdate, DateTime todate, Decimal BankId, decimal SelectTerminal, int RegionId)
        {
            DataTable dt = new DataTable();
            try
            {
                SqlParameter prmfromdate = new SqlParameter("@fromdate", SqlDbType.DateTime);
                prmfromdate.Value = fromdate;
                SqlParameter prmtodate = new SqlParameter("@todate", SqlDbType.DateTime);
                prmtodate.Value = todate;
                SqlParameter prmBankId = new SqlParameter("@BankId", SqlDbType.Decimal);
                prmBankId.Value = BankId;
                SqlParameter prmSelectTerminal = new SqlParameter("@SST_TypeID", SqlDbType.Decimal);
                prmSelectTerminal.Value = SelectTerminal;
                SqlParameter prmRegionId = new SqlParameter("@RegionId", SqlDbType.Int);
                prmRegionId.Value = RegionId;
                dt = DataAccess.GetFromDataTable("SP_MISSummaryReport", prmfromdate, prmtodate, prmBankId, prmSelectTerminal, prmRegionId);
            }
            catch (Exception ex)
            {
                //ErrHandler.WriteError(ex.ToString());
            }
            return dt;

        }


        #endregion
        #region PNB Report
        public DataTable Sp_TerminalWiseAvailabilityReport(string DateInput, Decimal BankId, Decimal SSTTypeID)
        {
            //SqlParameter prmdateValue = new SqlParameter("@DayVal", SqlDbType.VarChar, 50);
            //prmdateValue.Value = dateValue;
            SqlParameter prmDateInput = new SqlParameter("@DATE_Input", SqlDbType.VarChar, 50);
            prmDateInput.Value = DateInput;
            //SqlParameter prmfromDate = new SqlParameter("@DATE_Input", SqlDbType.VarChar, 50);
            //prmSelectDate.Value = fromDate;
            //SqlParameter prmtoDate = new SqlParameter("@DATE_Input", SqlDbType.VarChar, 50);
            //prmtoDate.Value = toDate;
            SqlParameter prmBankId = new SqlParameter("@BankId", SqlDbType.Decimal);
            prmBankId.Value = BankId;
            SqlParameter prmSSTTypeID = new SqlParameter("@SSTTypeID", SqlDbType.Decimal);
            prmSSTTypeID.Value = SSTTypeID;

            return DataAccess.GetFromDataTable("Sp_TerminalWiseAvailabilityReport", prmDateInput, prmBankId, prmSSTTypeID);

        }
        public DataTable Sp_TerminalWisePBK_Report(string DateInput, Decimal BankId, Decimal SSTTypeID)
        {
            //SqlParameter prmdateValue = new SqlParameter("@DayVal", SqlDbType.VarChar, 50);
            //prmdateValue.Value = dateValue;
            SqlParameter prmDateInput = new SqlParameter("@DATE_Input", SqlDbType.VarChar, 50);
            prmDateInput.Value = DateInput;
            //SqlParameter prmfromDate = new SqlParameter("@DATE_Input", SqlDbType.VarChar, 50);
            //prmSelectDate.Value = fromDate;
            //SqlParameter prmtoDate = new SqlParameter("@DATE_Input", SqlDbType.VarChar, 50);
            //prmtoDate.Value = toDate;
            SqlParameter prmBankId = new SqlParameter("@BankId", SqlDbType.Decimal);
            prmBankId.Value = BankId;
            SqlParameter prmSSTTypeID = new SqlParameter("@SSTTypeID", SqlDbType.Decimal);
            prmSSTTypeID.Value = SSTTypeID;

            return DataAccess.GetFromDataTable("Sp_TerminalWise_PP_Avail_Report", prmDateInput, prmBankId, prmSSTTypeID);

        }

        public DataTable PrintTerminalWiseAvailabilityReport(string DateInput, Decimal BankId, Decimal SSTTypeID)
        {
            DataTable dt = new DataTable();
            try
            {
                using (SqlConnection sqlconn = new SqlConnection(ConfigurationManager.ConnectionStrings.ToString()))
                {

                    sqlconn.Open();
                    SqlCommand sqlcomm = new SqlCommand();
                    sqlcomm.Connection = sqlconn;
                    sqlcomm.CommandType = CommandType.StoredProcedure;
                    sqlcomm.CommandText = "Sp_TerminalWiseAvailabilityReport";
                    sqlcomm.CommandTimeout = 600;
                    //                @RegionName varchar(50),
                    //@BranchName varchar(50),
                    //@BranchCode varchar(30),
                    //@BranchAddress varchar(300),
                    //@CityID numeric(18,0)=1,
                    //@locality varchar(50)
                    sqlcomm.Parameters.Add(new SqlParameter("@DATE_Input", SqlDbType.VarChar)).Value = DateInput;
                    sqlcomm.Parameters.Add(new SqlParameter("@BankId", SqlDbType.Decimal)).Value = BankId;
                    sqlcomm.Parameters.Add(new SqlParameter("@SSTTypeID", SqlDbType.Decimal)).Value = SSTTypeID;
                    //sqlcomm.Parameters.Add(new SqlParameter("@BranchAddress", SqlDbType.VarChar)).Value = dr[dic["Branch Address"]].ToString();
                    ////sqlcomm.Parameters.Add(new SqlParameter("@CityID", SqlDbType.Int)).Value =dic[].ToString();
                    //sqlcomm.Parameters.Add(new SqlParameter("@locality", SqlDbType.VarChar)).Value = dr[dic["Branch Address"]].ToString();

                    SqlDataAdapter da = new SqlDataAdapter(sqlcomm);
                    da.Fill(dt);
                    /*SqlDataReader dr = sqlcomm.ExecuteReader();
                    //while (dr.HasRows)
                    //{
                    while (dr.Read())
                    {

                    }
                    //}
                    sqlcomm.Dispose();
                    sqlconn.Close();*/


                }

            }
            catch (Exception ex)
            {
                //messagebox.show(
            }
            return dt;
        }
        public DataTable Sp_TerminalWiseMFKAvail_Report(string DateInput, Decimal BankId, Decimal SSTTypeID)
        {

            SqlParameter prmDateInput = new SqlParameter("@DATE_Input", SqlDbType.VarChar, 50);
            prmDateInput.Value = DateInput;
            SqlParameter prmBankId = new SqlParameter("@BankId", SqlDbType.Decimal);
            prmBankId.Value = BankId;
            SqlParameter prmSSTTypeID = new SqlParameter("@SSTTypeID", SqlDbType.Decimal);
            prmSSTTypeID.Value = SSTTypeID;
            return DataAccess.GetFromDataTable("Sp_TerminalWiseMFKAvailReport", prmDateInput, prmBankId, prmSSTTypeID);

        }
        public DataTable Sp_TerminalWiseCHKAvail_Report(string DateInput, Decimal BankId, Decimal SSTTypeID)
        {

            SqlParameter prmDateInput = new SqlParameter("@DATE_Input", SqlDbType.VarChar, 50);
            prmDateInput.Value = DateInput;
            SqlParameter prmBankId = new SqlParameter("@BankId", SqlDbType.Decimal);
            prmBankId.Value = BankId;
            SqlParameter prmSSTTypeID = new SqlParameter("@SSTTypeID", SqlDbType.Decimal);
            prmSSTTypeID.Value = SSTTypeID;
            return DataAccess.GetFromDataTable("Sp_TerminalWiseCHKAvailReport", prmDateInput, prmBankId, prmSSTTypeID);

        }
        public DataTable Sp_TerminalWiseQMSAvail_Report(string DateInput, Decimal BankId, Decimal SSTTypeID)
        {
            SqlParameter prmDateInput = new SqlParameter("@DATE_Input", SqlDbType.VarChar, 50);
            prmDateInput.Value = DateInput;
            SqlParameter prmBankId = new SqlParameter("@BankId", SqlDbType.Decimal);
            prmBankId.Value = BankId;
            SqlParameter prmSSTTypeID = new SqlParameter("@SSTTypeID", SqlDbType.Decimal);
            prmSSTTypeID.Value = SSTTypeID;
            return DataAccess.GetFromDataTable("[Sp_TerminalWiseQMS_AvailReport]", prmDateInput, prmBankId, prmSSTTypeID);

        }


        public DataTable FillSSTTypedrdReport()
        {
            return DataAccess.GetFromDataTable("Sp_FillSSTTypeReport");
        }
        public DataTable GetTTHistoryDetailReport(int TroubleTickedID)
        {
            //SqlParameter prmfromdate = new SqlParameter("@fromDate", SqlDbType.DateTime);
            //prmfromdate.Value = fromdate;
            //SqlParameter prmtodate = new SqlParameter("@todate", SqlDbType.DateTime);
            //prmtodate.Value = todate;
            SqlParameter prmTroubleTickedID = new SqlParameter("@TroubleTicketID", SqlDbType.Int);
            prmTroubleTickedID.Value = TroubleTickedID;

            return DataAccess.GetFromDataTable("Sp_TroubLeTicketHistoryReport", prmTroubleTickedID);
        }
        public DataTable GetTTHistorySummaryReport(DateTime fromdate, DateTime todate, Decimal SelectBankId)
        {
            SqlParameter prmfromdate = new SqlParameter("@fromDate", SqlDbType.DateTime);
            prmfromdate.Value = fromdate;
            SqlParameter prmtodate = new SqlParameter("@todate", SqlDbType.DateTime);
            prmtodate.Value = todate;
            SqlParameter prmSelectBankId = new SqlParameter("@BankId", SqlDbType.Decimal);
            prmSelectBankId.Value = SelectBankId;

            return DataAccess.GetFromDataTable("Sp_TroubLeTicketHistorySummaryReport", prmfromdate, prmtodate, prmSelectBankId);

        }
        #endregion
        #region Unbi Report
        public DataTable Sp_PeriodicCDKTerminalAvailReport(string FromDate, string ToDate, Decimal BankId, Decimal SSTTypeID)
        {
            //SqlParameter prmdateValue = new SqlParameter("@DayVal", SqlDbType.VarChar, 50);
            //prmdateValue.Value = dateValue;
            SqlParameter prmFromDate = new SqlParameter("@DATE_Input_From", SqlDbType.VarChar, 50);
            prmFromDate.Value = FromDate;
            SqlParameter prmToDate = new SqlParameter("@DATE_Input_To", SqlDbType.VarChar, 50);
            prmToDate.Value = ToDate;
            //SqlParameter prmfromDate = new SqlParameter("@DATE_Input", SqlDbType.VarChar, 50);
            //prmSelectDate.Value = fromDate;
            //SqlParameter prmtoDate = new SqlParameter("@DATE_Input", SqlDbType.VarChar, 50);
            //prmtoDate.Value = toDate;
            SqlParameter prmBankId = new SqlParameter("@BankId", SqlDbType.Decimal);
            prmBankId.Value = BankId;
            SqlParameter prmSSTTypeID = new SqlParameter("@SSTTypeID", SqlDbType.Decimal);
            prmSSTTypeID.Value = SSTTypeID;

            return DataAccess.GetFromDataTable("Sp_TerminalWiseAvailabilityReport_1", prmFromDate, prmToDate, prmBankId, prmSSTTypeID);

        }
        public DataTable Sp_PeriodicMFKTerminalAvailReport(string FromDate, string ToDate, Decimal BankId, Decimal SSTTypeID)
        {
            //SqlParameter prmdateValue = new SqlParameter("@DayVal", SqlDbType.VarChar, 50);
            //prmdateValue.Value = dateValue;
            SqlParameter prmFromDate = new SqlParameter("@DATE_Input_From", SqlDbType.VarChar, 50);
            prmFromDate.Value = FromDate;
            SqlParameter prmToDate = new SqlParameter("@DATE_Input_To", SqlDbType.VarChar, 50);
            prmToDate.Value = ToDate;
            //SqlParameter prmfromDate = new SqlParameter("@DATE_Input", SqlDbType.VarChar, 50);
            //prmSelectDate.Value = fromDate;
            //SqlParameter prmtoDate = new SqlParameter("@DATE_Input", SqlDbType.VarChar, 50);
            //prmtoDate.Value = toDate;
            SqlParameter prmBankId = new SqlParameter("@BankId", SqlDbType.Decimal);
            prmBankId.Value = BankId;
            SqlParameter prmSSTTypeID = new SqlParameter("@SSTTypeID", SqlDbType.Decimal);
            prmSSTTypeID.Value = SSTTypeID;

            return DataAccess.GetFromDataTable("Sp_TerminalMFKPeriodicAvail_Report", prmFromDate, prmToDate, prmBankId, prmSSTTypeID);

        }
        public DataTable Sp_PeriodicCHKTerminalAvailReport(string FromDate, string ToDate, Decimal BankId, Decimal SSTTypeID)
        {
            //SqlParameter prmdateValue = new SqlParameter("@DayVal", SqlDbType.VarChar, 50);
            //prmdateValue.Value = dateValue;
            SqlParameter prmFromDate = new SqlParameter("@DATE_Input_From", SqlDbType.VarChar, 50);
            prmFromDate.Value = FromDate;
            SqlParameter prmToDate = new SqlParameter("@DATE_Input_To", SqlDbType.VarChar, 50);
            prmToDate.Value = ToDate;
            //SqlParameter prmfromDate = new SqlParameter("@DATE_Input", SqlDbType.VarChar, 50);
            //prmSelectDate.Value = fromDate;
            //SqlParameter prmtoDate = new SqlParameter("@DATE_Input", SqlDbType.VarChar, 50);
            //prmtoDate.Value = toDate;
            SqlParameter prmBankId = new SqlParameter("@BankId", SqlDbType.Decimal);
            prmBankId.Value = BankId;
            SqlParameter prmSSTTypeID = new SqlParameter("@SSTTypeID", SqlDbType.Decimal);
            prmSSTTypeID.Value = SSTTypeID;

            return DataAccess.GetFromDataTable("Sp_TerminalCHKPeriodicAvail_Report", prmFromDate, prmToDate, prmBankId, prmSSTTypeID);

        }
        public DataTable Sp_PeriodicPBKTerminalAvailReport(string FromDate, string ToDate, Decimal BankId, Decimal SSTTypeID)
        {
            //SqlParameter prmdateValue = new SqlParameter("@DayVal", SqlDbType.VarChar, 50);
            //prmdateValue.Value = dateValue;
            SqlParameter prmFromDate = new SqlParameter("@DATE_Input_From", SqlDbType.VarChar, 50);
            prmFromDate.Value = FromDate;
            SqlParameter prmToDate = new SqlParameter("@DATE_Input_To", SqlDbType.VarChar, 50);
            prmToDate.Value = ToDate;
            //SqlParameter prmfromDate = new SqlParameter("@DATE_Input", SqlDbType.VarChar, 50);
            //prmSelectDate.Value = fromDate;
            //SqlParameter prmtoDate = new SqlParameter("@DATE_Input", SqlDbType.VarChar, 50);
            //prmtoDate.Value = toDate;
            SqlParameter prmBankId = new SqlParameter("@BankId", SqlDbType.Decimal);
            prmBankId.Value = BankId;
            SqlParameter prmSSTTypeID = new SqlParameter("@SSTTypeID", SqlDbType.Decimal);
            prmSSTTypeID.Value = SSTTypeID;

            return DataAccess.GetFromDataTable("Sp_TerminalPBKPeriodicAvail_Report", prmFromDate, prmToDate, prmBankId, prmSSTTypeID);

        }



        #endregion



        #region SST Profile - Rework
        // This function should be replaced by the function in the DLL which returns Dataset
        // Reason : As no function which returns Dataset is avialable, I explicitely open the connection here.
        public DataTable SP_FillVendors()
        {

            return DataAccess.GetFromDataTable("SP_FillVendors");
        }
        #endregion
        public DataTable TerminalWiseAvailabilityReport(string DateInput, Decimal BankId, Decimal SSTTypeID, String SpCommandText, decimal RegionId, decimal BranchId)
        //public DataTable TerminalWiseAvailabilityReport(string DateInput, Decimal BankId, Decimal SSTTypeID, String SpCommandText)
        {
            DataTable dt = new DataTable();
            try
            {
                string constring = System.Configuration.ConfigurationManager.ConnectionStrings["strConn"].ConnectionString;

                DecryptTheString(constring);
                using (SqlConnection sqlconn = new SqlConnection(DecryptTheString(constring)))
                //using (SqlConnection sqlconn = new SqlConnection(ConfigurationManager.ConnectionStrings["strConn"].ConnectionString.ToString()))
                {
                    sqlconn.Open();
                    SqlCommand sqlcomm = new SqlCommand();
                    sqlcomm.Connection = sqlconn;
                    sqlcomm.CommandType = CommandType.StoredProcedure;

                    sqlcomm.CommandText = SpCommandText;
                    sqlcomm.CommandTimeout = 600;
                    //@RegionName varchar(50),
                    //@BranchName varchar(50),
                    //@BranchCode varchar(30),
                    //@BranchAddress varchar(300),
                    //@CityID numeric(18,0)=1,
                    //@locality varchar(50)
                    sqlcomm.Parameters.Add(new SqlParameter("@DATE_Input", SqlDbType.VarChar)).Value = DateInput;
                    sqlcomm.Parameters.Add(new SqlParameter("@BankId", SqlDbType.Decimal)).Value = BankId;
                    sqlcomm.Parameters.Add(new SqlParameter("@SSTTypeID", SqlDbType.Decimal)).Value = SSTTypeID;

                    sqlcomm.Parameters.Add(new SqlParameter("@RegionId", SqlDbType.Decimal)).Value = RegionId;
                    //sqlcomm.Parameters.Add(new SqlParameter("@BranchId", SqlDbType.Decimal)).Value = BranchId;

                    //sqlcomm.Parameters.Add(new SqlParameter("@BranchAddress", SqlDbType.VarChar)).Value = dr[dic["Branch Address"]].ToString();
                    ////sqlcomm.Parameters.Add(new SqlParameter("@CityID", SqlDbType.Int)).Value =dic[].ToString();
                    //sqlcomm.Parameters.Add(new SqlParameter("@locality", SqlDbType.VarChar)).Value = dr[dic["Branch Address"]].ToString();

                    SqlDataAdapter da = new SqlDataAdapter(sqlcomm);
                    da.Fill(dt);
                    /*SqlDataReader dr = sqlcomm.ExecuteReader();
                    //while (dr.HasRows)
                    //{
                    while (dr.Read())
                    {
                    }
                    //}
                    sqlcomm.Dispose();
                    sqlconn.Close();*/
                }

            }
            catch (Exception ex)
            {
                //messagebox.show(
            }
            return dt;
        }
        public DataTable Sp_PeriodicTerminalAvailReport(string FromDate, string ToDate, Decimal BankId, Decimal SSTTypeID, string SpCommandText)
        {
            //SqlParameter prmdateValue = new SqlParameter("@DayVal", SqlDbType.VarChar, 50);
            //prmdateValue.Value = dateValue;
            SqlParameter prmFromDate = new SqlParameter("@DATE_Input_From", SqlDbType.VarChar, 50);
            prmFromDate.Value = FromDate;
            SqlParameter prmToDate = new SqlParameter("@DATE_Input_To", SqlDbType.VarChar, 50);
            prmToDate.Value = ToDate;
            //SqlParameter prmfromDate = new SqlParameter("@DATE_Input", SqlDbType.VarChar, 50);
            //prmSelectDate.Value = fromDate;
            //SqlParameter prmtoDate = new SqlParameter("@DATE_Input", SqlDbType.VarChar, 50);
            //prmtoDate.Value = toDate;
            SqlParameter prmBankId = new SqlParameter("@BankId", SqlDbType.Decimal);
            prmBankId.Value = BankId;
            SqlParameter prmSSTTypeID = new SqlParameter("@SSTTypeID", SqlDbType.Decimal);
            prmSSTTypeID.Value = SSTTypeID;

            return DataAccess.GetFromDataTable(SpCommandText, prmFromDate, prmToDate, prmBankId, prmSSTTypeID);
        }

        public DataTable GetMISDetailSummaryReport(DateTime FromDateTime, DateTime ToDateTime, decimal BankId)
        {
            throw new NotImplementedException();
        }

        //Shricant
        public DataTable SP_FillRegionNameOnBankRegionBranch(decimal BankID, decimal RegionID)
        {
            SqlParameter prmBankID = new SqlParameter("@BankID", SqlDbType.Decimal);
            prmBankID.Value = BankID;
            SqlParameter prmRegionID = new SqlParameter("@RegionID", SqlDbType.Decimal);
            prmRegionID.Value = RegionID;
            return DataAccess.GetFromDataTable("SP_FillRegionNameOnBankRegionBranch", prmBankID, prmRegionID);
        }
        //Shricant
        public DataTable SP_EditSST2(decimal RegionID, decimal BranchID)//shrikant
        {
            SqlParameter prmRegionID = new SqlParameter("@RegionID", SqlDbType.Decimal);
            prmRegionID.Value = RegionID;
            SqlParameter prmBranchID = new SqlParameter("@BranchID", SqlDbType.Decimal);
            prmBranchID.Value = BranchID;
            return DataAccess.GetFromDataTable("SP_EditSST2", prmRegionID, prmBranchID);
        }

        public static string DecryptTheString(string strConnection)
        {
            SqlConnectionStringBuilder conStringBuilder = new SqlConnectionStringBuilder(strConnection);
            string InitialCatalogValue = (conStringBuilder.InitialCatalog);
            string DataSourceValue = (conStringBuilder.DataSource);
            string UserIDValue = (conStringBuilder.UserID);
            string PasswordValue = DecodeFrom64(conStringBuilder.Password);
            string returnValue = "Data Source=" + DataSourceValue + ";Initial Catalog=" + InitialCatalogValue + ";User ID=" + UserIDValue + ";Password=" + PasswordValue;
            return returnValue;
        }
        public static string DecodeFrom64(string encodedData)
        {
            try
            {
                byte[] encodedDataAsBytes
                = System.Convert.FromBase64String(encodedData);
                string returnValue =
                   System.Text.ASCIIEncoding.ASCII.GetString(encodedDataAsBytes);
                return returnValue;
            }
            catch (Exception)
            {
                return "Exception";
            }

        }
    }
}

