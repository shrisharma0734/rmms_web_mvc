﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Diagnostics;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;
using System.IO;
using System.Net;
using System.Net.Sockets;
using System.Web.UI.HtmlControls;
using System.Threading;
using TraceWriter;
using System.Xml;
//using BLL;
using RMMS_MVC.MVCMODELS;
using Password;
using Ionic.Zip;
using System.Data;
using System.Web.Mail;
using RMMS_MVC.MVCActionStoreProcedure;

namespace RMMS_MVC.SendService
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the interface name "IService1" in both code and config file together.
    [ServiceContract]
    public interface IService1
    {
                [OperationContract]
        void TestConnectSSTs(decimal ScheduleId);

        [OperationContract]
        void PullFile(decimal ScheduleId);

        [OperationContract]
        void RestartCommand(decimal ScheduleId);
  

        [OperationContract]
        bool IsZipFileExistOnServer(string ZipFilePath);

        [OperationContract]
        void RestartService(decimal ScheduleId);

        [OperationContract]
        void GetSystemPerformanceOfSST(decimal ScheduleId);

        [OperationContract(IsOneWay=true)]
        void GetPullFileFromSST(decimal ScheduleId,string SourceDirectoryPath,string FileName,string WildCard);

        [OperationContract]
        void PushFileOnSST(decimal ScheduleId, string SourceDirectoryPath, string SourceDirOnAgent);

        [OperationContract]
        void SelfDiagnostic(decimal ScheduleId);

      

    }

    // Use a data contract as illustrated in the sample below to add composite types to service operations.

    [DataContract]
    public class PushFile
    {

        public void CreatePushRequestXml(string SourceDir, string SourceDirOnAgent)
        {
           string RequestXmlPath =SourceDir + @"\PushFile.xml";

            XmlDocument xmldoc = new XmlDocument();

            using (XmlTextWriter writer = new XmlTextWriter(RequestXmlPath, System.Text.Encoding.UTF8))
            {
                writer.WriteStartDocument(true);
                writer.Formatting = Formatting.Indented;
                writer.Indentation = 3;
                writer.WriteStartElement("RMMS");
                writer.WriteStartElement("PUSH"); //Operation Type
                writer.WriteStartElement("ContentType");
                writer.WriteString("Push_File");   // Content Type
                writer.WriteEndElement();
                writer.WriteStartElement("FileName");
                writer.WriteString("");
                writer.WriteEndElement();
                writer.WriteStartElement("FilePathPerm");
                writer.WriteString(SourceDirOnAgent);
                writer.WriteEndElement();
                writer.WriteStartElement("FilePathTemp");
                writer.WriteString("");
                writer.WriteEndElement();
                writer.WriteStartElement("ReplaceExisting");
                writer.WriteString("");
                writer.WriteEndElement();
                writer.WriteStartElement("RenameExisting");
                writer.WriteString("");
                writer.WriteEndElement();
                writer.WriteStartElement("RemoveCurrent");
                writer.WriteString("");
                writer.WriteEndElement();
                writer.WriteStartElement("RemoveCurrentAfter");
                writer.WriteString("");
                writer.WriteEndElement();
                writer.WriteStartElement("BackupExisting");
                writer.WriteString("");
                writer.WriteEndElement();
                writer.WriteStartElement("Responses");
                writer.WriteEndElement();
                writer.WriteStartElement("Reserve");
                writer.WriteString("");
                writer.WriteEndElement();
                writer.WriteEndElement();
                writer.WriteEndElement();
                writer.WriteEndDocument();
                writer.Close();
            }
        }

        public void MultiSSTPushOperation(decimal ScheduleId, string SourceDirectoryPath, string SourceDirOnAgent)
        {

            try
            {
                ClsDetails.writeStringTotransactionLog("Inside MultiSSTPushOperation");
                foreach (string _Path in Directory.GetFiles(SourceDirectoryPath, "Push_*"))
                    File.Delete(_Path);
                string RequestXmlPath = SourceDirectoryPath + @"\Push" + "_" + ScheduleId + "_" + DateTime.Now.ToString("ddMMyyyy") + ".xml";
                ClsDetails.writeStringTotransactionLog("RequestXMLPath in MultiSSTPushOperation : " + RequestXmlPath);
                XmlDocument xmldoc = new XmlDocument();
                using (XmlTextWriter writer = new XmlTextWriter(RequestXmlPath, System.Text.Encoding.UTF8))
                {
                    ClsDetails.writeStringTotransactionLog("Creating XML");
                    writer.WriteStartDocument(true);
                    writer.Formatting = Formatting.Indented;
                    writer.Indentation = 3;
                    writer.WriteStartElement("RMMS");
                    writer.WriteStartElement("PUSH"); //Operation Type
                    writer.WriteStartElement("ContentType");
                    writer.WriteString("Push_File");   // Content Type
                    writer.WriteEndElement();
                    writer.WriteStartElement("FileName");
                    writer.WriteString("");
                    writer.WriteEndElement();
                    writer.WriteStartElement("FilePathPerm");
                    writer.WriteString(SourceDirOnAgent);
                    writer.WriteEndElement();
                    writer.WriteStartElement("FilePathTemp");
                    writer.WriteString("");
                    writer.WriteEndElement();
                    writer.WriteStartElement("ReplaceExisting");
                    writer.WriteString("");
                    writer.WriteEndElement();
                    writer.WriteStartElement("RenameExisting");
                    writer.WriteString("");
                    writer.WriteEndElement();
                    writer.WriteStartElement("RemoveCurrent");
                    writer.WriteString("");
                    writer.WriteEndElement();
                    writer.WriteStartElement("RemoveCurrentAfter");
                    writer.WriteString("");
                    writer.WriteEndElement();
                    writer.WriteStartElement("BackupExisting");
                    writer.WriteString("");
                    writer.WriteEndElement();
                    writer.WriteStartElement("Responses");
                    writer.WriteEndElement();
                    writer.WriteStartElement("Reserve");
                    writer.WriteString("");
                    writer.WriteEndElement();
                    writer.WriteEndElement();
                    writer.WriteEndElement();
                    writer.WriteEndDocument();
                    writer.Close();
                    ClsDetails.writeStringTotransactionLog("XML File Write Completed in MultiSSTPushOperation ");
                }
            }
            catch (Exception Ex)
            {
                ClsDetails.writeStringTotransactionLog("Exception Caught after XML Wrinting Fuction in MultiSSTPushOperation" + Ex.Message);
            }

            BLogicS ActionSP = new BLogicS();
            JobActionDetails JobActDet = new JobActionDetails();
            Socket SST_Client = null;
            JobScheduleDetails_CDMS GetScheduleInfo = new JobScheduleDetails_CDMS();
            try
            {
                ClsDetails.writeStringTotransactionLog("Inside MultiSSTPushOperation ");
                SST_Client = new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp);
                GetScheduleInfo.ScheduleID = ScheduleId;
                GetScheduleInfo.GetMultiScheduleInfo();
                ClsDetails.writeStringTotransactionLog("Before Foreach Data");
                foreach (DataRow row in GetScheduleInfo.dtScheduleInfo.Rows)
                {
                    {
                        try
                        {
                            String SST_NAT_IP;
                            decimal SSTID, SST_AgentPort;
                            JobActDet.ScheduleID = ScheduleId;
                            SSTID = decimal.Parse(row["SSTID"].ToString());
                            SST_NAT_IP = Convert.ToString(row["SST_NAT_IP"]);
                            SST_AgentPort = decimal.Parse(row["SST_AgentPort"].ToString());
                            IPAddress[] IP = Dns.GetHostAddresses(SST_NAT_IP);
                            IPEndPoint ipEnd = new IPEndPoint(IP[0], Convert.ToInt32(SST_AgentPort));
                            //Socket SST_Client1 = new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp);
                            //   SST_Client1.Connect(ipEnd);
                            //IAsyncResult result = SST_Client1.BeginConnect(SST_NAT_IP, Convert.ToInt16(SST_AgentPort), null, null);
                            //bool success = result.AsyncWaitHandle.WaitOne(1000, true);


                            //Code Added by Mandar
                            TcpClient clientSocket = new TcpClient();
                            clientSocket = new TcpClient();
                            try
                            {
                                Thread.SpinWait(150000000);
                                clientSocket.SendTimeout = 45 * 1000;
                                clientSocket.Connect(SST_NAT_IP, Convert.ToInt16(SST_AgentPort));
                            }
                            catch (Exception Ex)
                            {
                                ClsDetails.writeStringTotransactionLog("Exception in connection : " + Ex.Message);
                            }
                            //End of code

                            if (clientSocket.Connected == true)
                            {
                                ClsDetails.writeStringTotransactionLog("Async True");
                                if (clientSocket.Connected == true)
                                {
                                    ClsDetails.writeStringTotransactionLog("Client Connected");
                                    string strZipSouceFilePath = string.Empty;
                                    string strZipFileName = string.Empty;
                                    string filePath = string.Empty;

                                    using (ZipFile zip = new ZipFile())
                                    {
                                        ClsDetails.writeStringTotransactionLog("insde Using Zip");
                                        Passwd pwd = new Passwd();
                                        strZipFileName = "PushFile" + "_" + ScheduleId + "_" + DateTime.Now.ToString("ddMMyyyy");
                                        zip.Password = pwd.GetPassword(strZipFileName);
                                        zip.AddDirectory(SourceDirectoryPath);
                                        ClsDetails.writeStringTotransactionLog("Directory Appended");
                                        strZipFileName += ".zip";
                                        strZipSouceFilePath = SourceDirectoryPath + "\\" + strZipFileName;
                                        ClsDetails.writeStringTotransactionLog("ZIP File Name : " + strZipSouceFilePath);
                                        zip.Save(strZipSouceFilePath);
                                        ClsDetails.writeStringTotransactionLog("ZIP File Saved");
                                    }
                                    ClsDetails.writeStringTotransactionLog("Zip Completed");

                                    byte[] fileNameByte = Encoding.ASCII.GetBytes(strZipFileName);
                                    byte[] fileData = File.ReadAllBytes(strZipSouceFilePath);
                                    byte[] SendData = new byte[4 + fileNameByte.Length + fileData.Length];
                                    byte[] fileNameLen = BitConverter.GetBytes(fileNameByte.Length);
                                    fileNameLen.CopyTo(SendData, 0);
                                    fileNameByte.CopyTo(SendData, 4);
                                    ClsDetails.writeStringTotransactionLog("File Conversion to bystes ");
                                    fileData.CopyTo(SendData, 4 + fileNameByte.Length);
                                    try
                                    {
                                        //Code Commented by Mandar
                                        //int index = 0;
                                        //int SendBytes = 0;
                                        //while (index < SendData.Length)
                                        //{
                                        //    if ((index + 8192) < SendData.Length)
                                        //    {
                                        //        SST_Client1.Send(SendData, index,8192, SocketFlags.None);
                                        //        SendBytes = SST_Client1.SendBufferSize;
                                        //        ClsDetails.writeStringTotransactionLog("Data Sent"); 
                                        //    }
                                        //    else
                                        //    {
                                        //        SST_Client1.Send(SendData, index, SendData.Length - index, SocketFlags.None);
                                        //        SendBytes = SST_Client1.SendBufferSize;
                                        //        ClsDetails.writeStringTotransactionLog("Data Sent woth More Size");
                                        //    }
                                        //    index += SendBytes;
                                        //}
                                        NetworkStream serverStream = null;
                                        try
                                        {
                                            serverStream = clientSocket.GetStream();

                                            if (serverStream.CanWrite == true && clientSocket.Connected == true)
                                            {
                                                serverStream.Write(SendData, 0, SendData.Length);
                                            }
                                            serverStream.Flush();
                                            serverStream.Close();

                                           
                                        }
                                        catch (Exception Ex)
                                        {
                                            ClsDetails.writeStringTotransactionLog("Exception While data Transfeer : " + Ex.Message);
                                        }
                                    }
                                    catch (Exception ex)
                                    {
                                        ClsDetails.writeStringTotransactionLog("Exception Caught in Send Received TCP" + ex.Message);
                                    }
                                    //SST_Client1.Shutdown(SocketShutdown.Receive);
                                    //SST_Client1.Disconnect(false);
                                    //---------changes by vaibhav for responce
                                    JobActDet.ActivityDetailID = decimal.Parse(row["ActivityDetailID"].ToString());
                                    JobActDet.ScheduleID = ScheduleId;
                                    JobActDet.ScheduleDatetime = DateTime.Parse(row["ScheduleDatetime"].ToString());
                                    JobActDet.SSTActivityListID = decimal.Parse(row["SSTActivityListID"].ToString());
                                    JobActDet.ActivityListDetailID = decimal.Parse(row["ActivityListDetailID"].ToString());
                                    JobActDet.SSTID = decimal.Parse(row["SSTID"].ToString());
                                    JobActDet.ActualStartDateTime = DateTime.Now;
                                    JobActDet.JobResponseID = 4;//Pending
                                    System.Web.HttpContext.Current.Session["JobResponse"] = "Success";
                                    JobActDet.JobResponseDateTime = DateTime.Now;
                                    JobActDet.RetryCount = 0;
                                    JobActDet.Remarks = string.Empty;
                                    JobActDet.CreatedBy = decimal.Parse(row["CreatedBy"].ToString());
                                    JobActDet.CreatedDatetime = DateTime.Parse(row["CreatedDatetime"].ToString());
                                    JobActDet.ModifiedBy = decimal.Parse(row["ModifiedBy"].ToString());
                                    JobActDet.ModifiedDatetime = DateTime.Now;
                                    ClsDetails.writeStringTotransactionLog("Before SP INsert");
                                    ActionSP.sp_Insert_JobActionDetails_CDMS(JobActDet.ActivityDetailID, JobActDet.ScheduleID, JobActDet.ActualStartDateTime, JobActDet.SSTActivityListID,
                                                                                JobActDet.ActivityListDetailID, JobActDet.SSTID, JobActDet.ActualStartDateTime,
                                                                                JobActDet.JobResponseID, JobActDet.JobResponseDateTime, JobActDet.RetryCount, JobActDet.Remarks,
                                                                                JobActDet.CreatedBy, JobActDet.CreatedDatetime, JobActDet.ModifiedBy, JobActDet.ModifiedDatetime);
                                    //----------------------
                                }
                                else
                                {
                                    ClsDetails.writeStringTotransactionLog("Client Not Connected");
                                    JobActDet.ActivityDetailID = decimal.Parse(row["ActivityDetailID"].ToString());
                                    JobActDet.ScheduleID = ScheduleId;
                                    JobActDet.ScheduleDatetime = DateTime.Parse(row["ScheduleDatetime"].ToString());
                                    JobActDet.SSTActivityListID = decimal.Parse(row["SSTActivityListID"].ToString());
                                    JobActDet.ActivityListDetailID = decimal.Parse(row["ActivityListDetailID"].ToString());
                                    JobActDet.SSTID = decimal.Parse(row["SSTID"].ToString());
                                    JobActDet.ActualStartDateTime = DateTime.Now;
                                    JobActDet.JobResponseID = 2;
                                    System.Web.HttpContext.Current.Session["JobResponse"] = "Failed";
                                    JobActDet.JobResponseDateTime = DateTime.Now;
                                    JobActDet.RetryCount = 0;
                                    JobActDet.Remarks = "Not connected";
                                    JobActDet.CreatedBy = decimal.Parse(row["CreatedBy"].ToString());
                                    JobActDet.CreatedDatetime = DateTime.Parse(row["CreatedDatetime"].ToString());
                                    JobActDet.ModifiedBy = decimal.Parse(row["ModifiedBy"].ToString());
                                    JobActDet.ModifiedDatetime = DateTime.Now;
                                    ClsDetails.writeStringTotransactionLog("Before SP else INsert");
                                    ActionSP.sp_Insert_JobActionDetails_CDMS(JobActDet.ActivityDetailID, JobActDet.ScheduleID, JobActDet.ActualStartDateTime, JobActDet.SSTActivityListID,
                                                                                JobActDet.ActivityListDetailID, JobActDet.SSTID, JobActDet.ActualStartDateTime,
                                                                                JobActDet.JobResponseID, JobActDet.JobResponseDateTime, JobActDet.RetryCount, JobActDet.Remarks,
                                                                                JobActDet.CreatedBy, JobActDet.CreatedDatetime, JobActDet.ModifiedBy, JobActDet.ModifiedDatetime);
                                }
                            }
                            else
                            {
                                ClsDetails.writeStringTotransactionLog("Async false");
                                JobActDet.ActivityDetailID = decimal.Parse(row["ActivityDetailID"].ToString());
                                JobActDet.ScheduleID = ScheduleId;
                                JobActDet.ScheduleDatetime = DateTime.Parse(row["ScheduleDatetime"].ToString());
                                JobActDet.SSTActivityListID = decimal.Parse(row["SSTActivityListID"].ToString());
                                JobActDet.ActivityListDetailID = decimal.Parse(row["ActivityListDetailID"].ToString());
                                JobActDet.SSTID = decimal.Parse(row["SSTID"].ToString());
                                JobActDet.ActualStartDateTime = DateTime.Now;
                                JobActDet.JobResponseID = 2;
                                System.Web.HttpContext.Current.Session["JobResponse"] = "Pending";
                                JobActDet.JobResponseDateTime = DateTime.Now;
                                JobActDet.RetryCount = 0;
                                JobActDet.Remarks = "Not connected";
                                JobActDet.CreatedBy = decimal.Parse(row["CreatedBy"].ToString());
                                JobActDet.CreatedDatetime = DateTime.Parse(row["CreatedDatetime"].ToString());
                                JobActDet.ModifiedBy = decimal.Parse(row["ModifiedBy"].ToString());
                                JobActDet.ModifiedDatetime = DateTime.Now;
                                ActionSP.sp_Insert_JobActionDetails_CDMS(JobActDet.ActivityDetailID, JobActDet.ScheduleID, JobActDet.ActualStartDateTime, JobActDet.SSTActivityListID,
                                                                            JobActDet.ActivityListDetailID, JobActDet.SSTID, JobActDet.ActualStartDateTime,
                                                                            JobActDet.JobResponseID, JobActDet.JobResponseDateTime, JobActDet.RetryCount, JobActDet.Remarks,
                                                                            JobActDet.CreatedBy, JobActDet.CreatedDatetime, JobActDet.ModifiedBy, JobActDet.ModifiedDatetime);
                            }
                        }
                        catch (Exception ex)
                        {

                            ClsDetails.writeStringTotransactionLog("Exception inside foeach data sent" + ex.Message);
                            JobActDet.ActivityDetailID = decimal.Parse(row["ActivityDetailID"].ToString());
                            JobActDet.ScheduleID = ScheduleId;
                            JobActDet.ScheduleDatetime = DateTime.Parse(row["ScheduleDatetime"].ToString());
                            JobActDet.SSTActivityListID = decimal.Parse(row["SSTActivityListID"].ToString());
                            JobActDet.ActivityListDetailID = decimal.Parse(row["ActivityListDetailID"].ToString());
                            JobActDet.SSTID = decimal.Parse(row["SSTID"].ToString());
                            JobActDet.ActualStartDateTime = DateTime.Now;
                            JobActDet.JobResponseID = 2;//Failed
                            System.Web.HttpContext.Current.Session["JobResponse"] = "Failed";
                            JobActDet.JobResponseDateTime = DateTime.Now;
                            JobActDet.RetryCount = 0;
                            JobActDet.Remarks = "Not connected";
                            JobActDet.CreatedBy = decimal.Parse(row["CreatedBy"].ToString());
                            JobActDet.CreatedDatetime = DateTime.Parse(row["CreatedDatetime"].ToString());
                            JobActDet.ModifiedBy = decimal.Parse(row["ModifiedBy"].ToString());
                            JobActDet.ModifiedDatetime = DateTime.Now;

                            ActionSP.sp_Insert_JobActionDetails_CDMS(JobActDet.ActivityDetailID, JobActDet.ScheduleID, JobActDet.ActualStartDateTime, JobActDet.SSTActivityListID,
                                                                        JobActDet.ActivityListDetailID, JobActDet.SSTID, JobActDet.ActualStartDateTime,
                                                                        JobActDet.JobResponseID, JobActDet.JobResponseDateTime, JobActDet.RetryCount, JobActDet.Remarks,
                                                                        JobActDet.CreatedBy, JobActDet.CreatedDatetime, JobActDet.ModifiedBy, JobActDet.ModifiedDatetime);
                            //ActionSP = null;
                        }
                    }
                }
            }
            catch (Exception Ex)
            {
                ClsDetails.writeStringTotransactionLog("Exception after XML writing function" + Ex.Message);
            }
            ClsDetails.writeStringTotransactionLog("Exiting MultiSSTPushOperation");
        }


    }



    [DataContract]
    public class CDMSServer
    {
        private static Object[] obj_ConnStatus;
        private static string[] Conn_IP;
        private static byte[] SendData;
        private static string SendFileName;

        BLogicS ActionSP = new BLogicS();
        JobActionDetails JobActDet = new JobActionDetails();

        public CDMSServer()
        {
            obj_ConnStatus = new Object[10];
            Conn_IP = new string[10];
            SendData = null;
            try
            {
                DirectoryStructure.CreateDirectory(System.Configuration.ConfigurationManager.AppSettings["XMLPullPath"]);
                DirectoryStructure.CreateDirectory(System.Configuration.ConfigurationManager.AppSettings["XMLPullTempSavePath"]);
            }
            catch(Exception)
            { 
            
            }
            //DirectoryStructure.CreateDirectory(ConfigurationSettings.AppSettings["XMLTempSavePath"]);
            //DirectoryStructure.CreateDirectory(ConfigurationSettings.AppSettings["XMLPushPath"]);
        }


        #region  TestConnection

        public void TestConnectingOperation(decimal ScheduleId)
        {
            String SST_NAT_IP;// SST_ORGIP,
            decimal ScheduleID, SSTID, SST_AgentPort;
            JobScheduleDetails_CDMS GetScheduleInfo = new JobScheduleDetails_CDMS();
            try
            {
                GetScheduleInfo.ScheduleID = ScheduleId;
                GetScheduleInfo.fillup();

                ScheduleID = GetScheduleInfo.ScheduleID;
                SSTID = GetScheduleInfo.SSTID;
                SST_NAT_IP = GetScheduleInfo.SST_NAT_IP;
                SST_AgentPort = GetScheduleInfo.SST_AgentPort;

                IPAddress[] IP = Dns.GetHostAddresses(SST_NAT_IP);
                IPEndPoint ipEnd = new IPEndPoint(IP[0], Convert.ToInt32(SST_AgentPort));
                Socket SST_Client = new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp);

                SST_Client.Connect(ipEnd);

                if (SST_Client.Connected)
                {
                    obj_ConnStatus[0] = (object)SST_Client;
                    Conn_IP[0] = SST_NAT_IP + " : " + SST_AgentPort;

                    JobActDet.ActivityDetailID = GetScheduleInfo.ActivityDetailID;
                    JobActDet.ScheduleID = GetScheduleInfo.ScheduleID;
                    JobActDet.ScheduleDatetime = GetScheduleInfo.ScheduleDatetime;
                    JobActDet.SSTActivityListID = GetScheduleInfo.SSTActivityListID;
                    JobActDet.ActivityListDetailID = GetScheduleInfo.ActivityListDetailID;
                    JobActDet.SSTID = GetScheduleInfo.SSTID;
                    JobActDet.ActualStartDateTime = DateTime.Now;
                    JobActDet.JobResponseID = 4;  //Pending
                   // System.Web.HttpContext.Current.Session["JobResponse"] = "Success";
                    //for WAITIMAG by sunita
                  

                    JobActDet.JobResponseDateTime = DateTime.Now;
                    JobActDet.RetryCount = 0;
                    JobActDet.Remarks = "";
                    JobActDet.CreatedBy = GetScheduleInfo.CreatedBy;
                    JobActDet.CreatedDatetime = GetScheduleInfo.CreatedDatetime;
                    JobActDet.ModifiedBy = GetScheduleInfo.ModifiedBy;
                    JobActDet.ModifiedDatetime = DateTime.Now;

                    ActionSP.sp_Insert_JobActionDetails_CDMS(JobActDet.ActivityDetailID, JobActDet.ScheduleID, JobActDet.ActualStartDateTime, JobActDet.SSTActivityListID,
                                                                JobActDet.ActivityListDetailID, JobActDet.SSTID, JobActDet.ActualStartDateTime,
                                                                JobActDet.JobResponseID, JobActDet.JobResponseDateTime, JobActDet.RetryCount, JobActDet.Remarks,
                                                                JobActDet.CreatedBy, JobActDet.CreatedDatetime, JobActDet.ModifiedBy, JobActDet.ModifiedDatetime);
                    ActionSP = null;

                    //  DissConnectingOperation(SST_IP, SST_Port);
                }
            }
            catch (Exception)
            {
                JobActDet.ActivityDetailID = GetScheduleInfo.ActivityDetailID;
                JobActDet.ScheduleID = GetScheduleInfo.ScheduleID;
                JobActDet.ScheduleDatetime = GetScheduleInfo.ScheduleDatetime;
                JobActDet.SSTActivityListID = GetScheduleInfo.SSTActivityListID;
                JobActDet.ActivityListDetailID = GetScheduleInfo.ActivityListDetailID;
                JobActDet.SSTID = GetScheduleInfo.SSTID;
                JobActDet.ActualStartDateTime = DateTime.Now;
                JobActDet.JobResponseID = 2;//Failed
                System.Web.HttpContext.Current.Session["JobResponse"] = "Failed";
                JobActDet.JobResponseDateTime = DateTime.Now;
                JobActDet.RetryCount = 0;
                JobActDet.Remarks = "Not Connected";
                JobActDet.CreatedBy = GetScheduleInfo.CreatedBy;
                JobActDet.CreatedDatetime = GetScheduleInfo.CreatedDatetime;
                JobActDet.ModifiedBy = GetScheduleInfo.ModifiedBy;
                JobActDet.ModifiedDatetime = DateTime.Now;

                ActionSP.sp_Insert_JobActionDetails_CDMS(JobActDet.ActivityDetailID, JobActDet.ScheduleID, JobActDet.ActualStartDateTime, JobActDet.SSTActivityListID,
                                                               JobActDet.ActivityListDetailID, JobActDet.SSTID, JobActDet.ActualStartDateTime,
                                                               JobActDet.JobResponseID, JobActDet.JobResponseDateTime, JobActDet.RetryCount, JobActDet.Remarks,
                                                               JobActDet.CreatedBy, JobActDet.CreatedDatetime, JobActDet.ModifiedBy, JobActDet.ModifiedDatetime);
                ActionSP = null;

            }
        }

        #endregion

        #region TestPull

        public void TestPullOperation(decimal ScheduleId)
        {
            Socket SST_Client = (Socket)obj_ConnStatus[0];
            JobScheduleDetails_CDMS GetScheduleInfo = new JobScheduleDetails_CDMS();
            try
            {
                TestPullRequestXML(ScheduleId);
                if (SST_Client.Connected)
                {
                    string filePath = "";
                    SendData = null;

                    SendFileName = SendFileName.Replace("\\", "/");
                    while (SendFileName.IndexOf("/") > -1)
                    {
                        filePath += SendFileName.Substring(0, SendFileName.IndexOf("/") + 1);
                        SendFileName = SendFileName.Substring(SendFileName.IndexOf("/") + 1);
                    }
                    byte[] fileNameByte = Encoding.ASCII.GetBytes(SendFileName);
                    byte[] fileData = File.ReadAllBytes(filePath + SendFileName);
                    SendData = new byte[4 + fileNameByte.Length + fileData.Length];
                    byte[] fileNameLen = BitConverter.GetBytes(fileNameByte.Length);

                    fileNameLen.CopyTo(SendData, 0);
                    fileNameByte.CopyTo(SendData, 4);
                    fileData.CopyTo(SendData, 4 + fileNameByte.Length);

                    try
                    {
                        int index = 0;
                        int SendBytes = 0;
                        while (index < SendData.Length)
                        {
                            if ((index + 8192) < SendData.Length)
                            {
                                SST_Client.Send(SendData, index, 8192, SocketFlags.None);
                                SendBytes = SST_Client.SendBufferSize;
                            }
                            else
                            {
                                SST_Client.Send(SendData, index, SendData.Length - index, SocketFlags.None);
                                SendBytes = SST_Client.SendBufferSize;
                            }
                            index += SendBytes;
                        }

                        // Release the socket.
                        SST_Client.Shutdown(SocketShutdown.Receive);
                        SST_Client.Disconnect(false);

                        //DissConnectingOperation();
                    }
                    catch (Exception)
                    {
                    }
                }
            }
            catch (Exception )
            {
            }
            GetScheduleInfo.ScheduleID = ScheduleId;
            GetScheduleInfo.fillup();

            try
            {
                String SST_NAT_IP;//SST_ORGIP,
                decimal ScheduleID, SSTID, SST_AgentPort;

                //JobScheduleDetails_CDMS GetScheduleInfo = new JobScheduleDetails_CDMS();


                ScheduleID = GetScheduleInfo.ScheduleID;
                SSTID = GetScheduleInfo.SSTID;
                SST_NAT_IP = GetScheduleInfo.SST_NAT_IP;
                SST_AgentPort = GetScheduleInfo.SST_AgentPort;

                IPAddress[] IP = Dns.GetHostAddresses(SST_NAT_IP);
                IPEndPoint ipEnd = new IPEndPoint(IP[0], Convert.ToInt32(SST_AgentPort));
                Socket SST_Client1 = new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp);

                SST_Client1.Connect(ipEnd);
                if (SST_Client1.Connected)
                {

                    string filePath = "";
                    SendData = null;

                    SendFileName = SendFileName.Replace("\\", "/");
                    while (SendFileName.IndexOf("/") > -1)
                    {
                        filePath += SendFileName.Substring(0, SendFileName.IndexOf("/") + 1);
                        SendFileName = SendFileName.Substring(SendFileName.IndexOf("/") + 1);
                    }
                    byte[] fileNameByte = Encoding.ASCII.GetBytes(SendFileName);
                    byte[] fileData = File.ReadAllBytes(filePath + SendFileName);
                    SendData = new byte[4 + fileNameByte.Length + fileData.Length];
                    byte[] fileNameLen = BitConverter.GetBytes(fileNameByte.Length);

                    fileNameLen.CopyTo(SendData, 0);
                    fileNameByte.CopyTo(SendData, 4);
                    fileData.CopyTo(SendData, 4 + fileNameByte.Length);

                    try
                    {
                        int index = 0;
                        int SendBytes = 0;
                        while (index < SendData.Length)
                        {
                            if ((index + 8192) < SendData.Length)
                            {
                                SST_Client1.Send(SendData, index, 8192, SocketFlags.None);
                                SendBytes = SST_Client1.SendBufferSize;
                            }
                            else
                            {
                                SST_Client1.Send(SendData, index, SendData.Length - index, SocketFlags.None);
                                SendBytes = SST_Client1.SendBufferSize;
                            }
                            index += SendBytes;
                        }

                        // Release the socket.
                        SST_Client1.Shutdown(SocketShutdown.Receive);
                        SST_Client1.Disconnect(false);

                        JobActDet.ActivityDetailID = GetScheduleInfo.ActivityDetailID;
                        JobActDet.ScheduleID = GetScheduleInfo.ScheduleID;
                        JobActDet.ScheduleDatetime = GetScheduleInfo.ScheduleDatetime;
                        JobActDet.SSTActivityListID = GetScheduleInfo.SSTActivityListID;
                        JobActDet.ActivityListDetailID = GetScheduleInfo.ActivityListDetailID;
                        JobActDet.SSTID = GetScheduleInfo.SSTID;
                        JobActDet.ActualStartDateTime = DateTime.Now;
                        JobActDet.JobResponseID = 4;//InProcess
                        System.Web.HttpContext.Current.Session["JobResponse"] = "InProcess";
                        JobActDet.JobResponseDateTime = DateTime.Now;
                        JobActDet.RetryCount = 0;
                        JobActDet.Remarks = "";
                        JobActDet.CreatedBy = GetScheduleInfo.CreatedBy;
                        JobActDet.CreatedDatetime = GetScheduleInfo.CreatedDatetime;
                        JobActDet.ModifiedBy = GetScheduleInfo.ModifiedBy;
                        JobActDet.ModifiedDatetime = DateTime.Now;

                        ActionSP.sp_Insert_JobActionDetails_CDMS(JobActDet.ActivityDetailID, JobActDet.ScheduleID, JobActDet.ActualStartDateTime, JobActDet.SSTActivityListID,
                                                                    JobActDet.ActivityListDetailID, JobActDet.SSTID, JobActDet.ActualStartDateTime,
                                                                    JobActDet.JobResponseID, JobActDet.JobResponseDateTime, JobActDet.RetryCount, JobActDet.Remarks,
                                                                    JobActDet.CreatedBy, JobActDet.CreatedDatetime, JobActDet.ModifiedBy, JobActDet.ModifiedDatetime);
                        ActionSP = null;



                        //DissConnectingOperation();
                    }
                    catch (Exception )
                    {
                        JobActDet.ActivityDetailID = GetScheduleInfo.ActivityDetailID;
                        JobActDet.ScheduleID = GetScheduleInfo.ScheduleID;
                        JobActDet.ScheduleDatetime = GetScheduleInfo.ScheduleDatetime;
                        JobActDet.SSTActivityListID = GetScheduleInfo.SSTActivityListID;
                        JobActDet.ActivityListDetailID = GetScheduleInfo.ActivityListDetailID;
                        JobActDet.SSTID = GetScheduleInfo.SSTID;
                        JobActDet.ActualStartDateTime = DateTime.Now;
                        JobActDet.JobResponseID = 3;//Not Coonected
                        System.Web.HttpContext.Current.Session["JobResponse"] = "Not Coonected";
                      
                        JobActDet.JobResponseDateTime = DateTime.Now;
                        JobActDet.RetryCount = 0;
                        JobActDet.Remarks = "";
                        JobActDet.CreatedBy = GetScheduleInfo.CreatedBy;
                        JobActDet.CreatedDatetime = GetScheduleInfo.CreatedDatetime;
                        JobActDet.ModifiedBy = GetScheduleInfo.ModifiedBy;
                        JobActDet.ModifiedDatetime = DateTime.Now;

                        ActionSP.sp_Insert_JobActionDetails_CDMS(JobActDet.ActivityDetailID, JobActDet.ScheduleID, JobActDet.ActualStartDateTime, JobActDet.SSTActivityListID,
                                                                    JobActDet.ActivityListDetailID, JobActDet.SSTID, JobActDet.ActualStartDateTime,
                                                                    JobActDet.JobResponseID, JobActDet.JobResponseDateTime, JobActDet.RetryCount, JobActDet.Remarks,
                                                                    JobActDet.CreatedBy, JobActDet.CreatedDatetime, JobActDet.ModifiedBy, JobActDet.ModifiedDatetime);
                        ActionSP = null;
                    }
                }
            }

            catch (Exception )
            {
                JobActDet.ActivityDetailID = GetScheduleInfo.ActivityDetailID;
                JobActDet.ScheduleID = GetScheduleInfo.ScheduleID;
                JobActDet.ScheduleDatetime = GetScheduleInfo.ScheduleDatetime;
                JobActDet.SSTActivityListID = GetScheduleInfo.SSTActivityListID;
                JobActDet.ActivityListDetailID = GetScheduleInfo.ActivityListDetailID;
                JobActDet.SSTID = GetScheduleInfo.SSTID;
                JobActDet.ActualStartDateTime = DateTime.Now;
                JobActDet.JobResponseID = 3;//Not Coonected
                System.Web.HttpContext.Current.Session["JobResponse"] = "Not Coonected";
                     
                JobActDet.JobResponseDateTime = DateTime.Now;
                JobActDet.RetryCount = 2;
                JobActDet.Remarks = "Not Connected";
                JobActDet.CreatedBy = GetScheduleInfo.CreatedBy;
                JobActDet.CreatedDatetime = GetScheduleInfo.CreatedDatetime;
                JobActDet.ModifiedBy = GetScheduleInfo.ModifiedBy;
                JobActDet.ModifiedDatetime = DateTime.Now;

                ActionSP.sp_Insert_JobActionDetails_CDMS(JobActDet.ActivityDetailID, JobActDet.ScheduleID, JobActDet.ActualStartDateTime, JobActDet.SSTActivityListID,
                                                            JobActDet.ActivityListDetailID, JobActDet.SSTID, JobActDet.ActualStartDateTime,
                                                            JobActDet.JobResponseID, JobActDet.JobResponseDateTime, JobActDet.RetryCount, JobActDet.Remarks,
                                                            JobActDet.CreatedBy, JobActDet.CreatedDatetime, JobActDet.ModifiedBy, JobActDet.ModifiedDatetime);
                ActionSP = null;
            }
        }

        public void MultiSSTPullOperation(decimal ScheduleId)
        {
            Socket SST_Client = (Socket)obj_ConnStatus[0];
            JobScheduleDetails_CDMS GetScheduleInfo = new JobScheduleDetails_CDMS();
            try
            {
                TestPullRequestXML(ScheduleId);

                GetScheduleInfo.ScheduleID = ScheduleId;
                GetScheduleInfo.GetMultiScheduleInfo();
                String LoopFile = SendFileName;
                foreach (DataRow row in GetScheduleInfo.dtScheduleInfo.Rows)
                {
                    SendFileName = "";
                    SendFileName = LoopFile;
                    {
                        try
                        {
                            String SST_NAT_IP;//SST_ORGIP,
                            decimal SSTID, SST_AgentPort;

                            JobActDet.ScheduleID = ScheduleId;
                            SSTID = decimal.Parse(row["SSTID"].ToString());
                            SST_NAT_IP = Convert.ToString(row["SST_NAT_IP"]);
                            SST_AgentPort = decimal.Parse(row["SST_AgentPort"].ToString());

                            IPAddress[] IP = Dns.GetHostAddresses(SST_NAT_IP);
                            IPEndPoint ipEnd = new IPEndPoint(IP[0], Convert.ToInt32(SST_AgentPort));
                            Socket SST_Client1 = new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp);

                            SST_Client1.Connect(ipEnd);
                            if (SST_Client1.Connected)
                            {
                                string filePath = "";
                                SendData = null;

                                SendFileName = SendFileName.Replace("\\", "/");
                                while (SendFileName.IndexOf("/") > -1)
                                {
                                    filePath += SendFileName.Substring(0, SendFileName.IndexOf("/") + 1);
                                    SendFileName = SendFileName.Substring(SendFileName.IndexOf("/") + 1);
                                }
                                byte[] fileNameByte = Encoding.ASCII.GetBytes(SendFileName);
                                byte[] fileData = File.ReadAllBytes(filePath + SendFileName);
                                SendData = new byte[4 + fileNameByte.Length + fileData.Length];
                                byte[] fileNameLen = BitConverter.GetBytes(fileNameByte.Length);

                                fileNameLen.CopyTo(SendData, 0);
                                fileNameByte.CopyTo(SendData, 4);
                                fileData.CopyTo(SendData, 4 + fileNameByte.Length);

                                try
                                {
                                    int index = 0;
                                    int SendBytes = 0;
                                    while (index < SendData.Length)
                                    {
                                        if ((index + 8192) < SendData.Length)
                                        {
                                            SST_Client1.Send(SendData, index, 8192, SocketFlags.None);
                                            SendBytes = SST_Client1.SendBufferSize;
                                        }
                                        else
                                        {
                                            SST_Client1.Send(SendData, index, SendData.Length - index, SocketFlags.None);
                                            SendBytes = SST_Client1.SendBufferSize;
                                        }
                                        index += SendBytes;
                                    }
                                }
                                catch (Exception )
                                {
                                }
                                // Release the socket.
                                SST_Client1.Shutdown(SocketShutdown.Receive);
                                SST_Client1.Disconnect(false);

                                JobActDet.ActivityDetailID = decimal.Parse(row["ActivityDetailID"].ToString());
                                JobActDet.ScheduleID = ScheduleId;
                                JobActDet.ScheduleDatetime = DateTime.Parse(row["ScheduleDatetime"].ToString());
                                JobActDet.SSTActivityListID = decimal.Parse(row["SSTActivityListID"].ToString());
                                JobActDet.ActivityListDetailID = decimal.Parse(row["ActivityListDetailID"].ToString());
                                JobActDet.SSTID = decimal.Parse(row["SSTID"].ToString());
                                JobActDet.ActualStartDateTime = DateTime.Now;
                                JobActDet.JobResponseID = 4;  //Pending
                                System.Web.HttpContext.Current.Session["JobResponse"] = "Pending";
                     
                                JobActDet.JobResponseDateTime = DateTime.Now;
                                JobActDet.RetryCount = 0;
                                JobActDet.Remarks = "";
                                JobActDet.CreatedBy = decimal.Parse(row["CreatedBy"].ToString());
                                JobActDet.CreatedDatetime = DateTime.Parse(row["CreatedDatetime"].ToString());
                                JobActDet.ModifiedBy = decimal.Parse(row["ModifiedBy"].ToString());
                                JobActDet.ModifiedDatetime = DateTime.Now;

                                ActionSP.sp_Insert_JobActionDetails_CDMS(JobActDet.ActivityDetailID, JobActDet.ScheduleID, JobActDet.ActualStartDateTime, JobActDet.SSTActivityListID,
                                                                            JobActDet.ActivityListDetailID, JobActDet.SSTID, JobActDet.ActualStartDateTime,
                                                                            JobActDet.JobResponseID, JobActDet.JobResponseDateTime, JobActDet.RetryCount, JobActDet.Remarks,
                                                                            JobActDet.CreatedBy, JobActDet.CreatedDatetime, JobActDet.ModifiedBy, JobActDet.ModifiedDatetime);
                                //ActionSP = null;
                                //DissConnectingOperation();
                            }
                        }
                        catch (Exception )
                        {

                            JobActDet.ActivityDetailID = decimal.Parse(row["ActivityDetailID"].ToString());
                            JobActDet.ScheduleID = ScheduleId;
                            JobActDet.ScheduleDatetime = DateTime.Parse(row["ScheduleDatetime"].ToString());
                            JobActDet.SSTActivityListID = decimal.Parse(row["SSTActivityListID"].ToString());
                            JobActDet.ActivityListDetailID = decimal.Parse(row["ActivityListDetailID"].ToString());
                            JobActDet.SSTID = decimal.Parse(row["SSTID"].ToString());
                            JobActDet.ActualStartDateTime = DateTime.Now;
                            JobActDet.JobResponseID = 2;//Failed
                            System.Web.HttpContext.Current.Session["JobResponse"] = "Failed";
        
                            JobActDet.JobResponseDateTime = DateTime.Now;
                            JobActDet.RetryCount = 0;
                            JobActDet.Remarks = "Not Connected";
                            JobActDet.CreatedBy = decimal.Parse(row["CreatedBy"].ToString());
                            JobActDet.CreatedDatetime = DateTime.Parse(row["CreatedDatetime"].ToString());
                            JobActDet.ModifiedBy = decimal.Parse(row["ModifiedBy"].ToString());
                            JobActDet.ModifiedDatetime = DateTime.Now;

                            ActionSP.sp_Insert_JobActionDetails_CDMS(JobActDet.ActivityDetailID, JobActDet.ScheduleID, JobActDet.ActualStartDateTime, JobActDet.SSTActivityListID,
                                                                        JobActDet.ActivityListDetailID, JobActDet.SSTID, JobActDet.ActualStartDateTime,
                                                                        JobActDet.JobResponseID, JobActDet.JobResponseDateTime, JobActDet.RetryCount, JobActDet.Remarks,
                                                                        JobActDet.CreatedBy, JobActDet.CreatedDatetime, JobActDet.ModifiedBy, JobActDet.ModifiedDatetime);
                            //ActionSP = null;

                        }
                    }
                }
            }
            catch (Exception )
            {
            }
        }

        public void TestPullRequestXML(decimal ScheduleId)
        {
            try
            {
                String Activity, JobType, SST_ORGIP, SST_NAT_IP, FilePath, FileName;
                decimal ScheduleID, SSTID, SST_AgentPort;

                clsPullRequest GetReuestInfo = new clsPullRequest();
                GetReuestInfo.ScheduleID = ScheduleId;
                GetReuestInfo.fillup();

                ScheduleID = GetReuestInfo.ScheduleID;
                Activity = GetReuestInfo.Activity;
                JobType = GetReuestInfo.JobType;
                SSTID = GetReuestInfo.SSTID;
                SST_ORGIP = GetReuestInfo.SST_ORGIP;
                SST_NAT_IP = GetReuestInfo.SST_NAT_IP;
                SST_AgentPort = GetReuestInfo.SST_AgentPort;

                FilePath = GetReuestInfo.JobTypeAgentPath;
                FileName = GetReuestInfo.EDCFileName_OnSST;

                String XMLFilePath = ConfigurationSettings.AppSettings["XMLPullPath"];

                if (!Directory.Exists(ConfigurationSettings.AppSettings["XMLPullPath"]))
                {
                    Directory.CreateDirectory(ConfigurationSettings.AppSettings["XMLPullPath"]);
                }

                SendFileName = XMLFilePath + @"\Pull" + "_" + ScheduleID + "_" + DateTime.Now.ToString("ddMMyyyy") + ".xml";

                XmlDocument xmldoc = new XmlDocument();

                XmlTextWriter writer = new XmlTextWriter(SendFileName, System.Text.Encoding.UTF8);
                writer.WriteStartDocument(true);
                writer.Formatting = Formatting.Indented;
                writer.Indentation = 3;
                writer.WriteStartElement("CDMS");
                switch (JobType)
                {
                    case "EJ":
                    case "Tracer":
                    case "ICON":
                        {
                            createPullNode(Activity, JobType, FileName, FilePath, ".*", "*", DateTime.Now.ToString("ddMMyyyy") + "-" + DateTime.Now.ToString("ddMMyyyy"), "", writer);
                            break;
                        }
                    case "AUDIO":
                    case "MOVIE":
                        {
                            createPullNode(Activity, JobType, FileName, FilePath, ".*", "*", DateTime.Now.ToString("ddMMyyyy") + "-" + DateTime.Now.ToString("ddMMyyyy"), "", writer);
                            break;
                        }
                }
                writer.WriteEndElement();
                writer.WriteEndDocument();
                writer.Close();

                using (ZipFile zip = new ZipFile())
                {
                    if (!Directory.Exists(ConfigurationSettings.AppSettings["XMLPullTempSavePath"]))
                    {
                        Directory.CreateDirectory(ConfigurationSettings.AppSettings["XMLPullTempSavePath"]);
                    }
                    //to call the below method
                    EmptyFolder(new DirectoryInfo(ConfigurationSettings.AppSettings["XMLPullTempSavePath"]));

                    //string[] file = Directory.GetFiles(SendFileName);
                    string name = Path.GetFileName(SendFileName);
                    string dest = Path.Combine(ConfigurationSettings.AppSettings["XMLPullTempSavePath"], name);

                    // To copy a file to another location and // overwrite the destination file if it already exists.
                    System.IO.File.Copy(SendFileName, dest, true);

                    string ZipFileName = ConfigurationSettings.AppSettings["XMLPullTempSavePath"];
                    ZipFileName = ZipFileName.Replace("\\", "/");
                    while (ZipFileName.IndexOf("/") > 0)
                    {
                        //filePath += ZipFileName.Substring(0, ZipFileName.IndexOf("/") + 1);
                        ZipFileName = ZipFileName.Substring(ZipFileName.IndexOf("/") + 1);
                    }

                    Passwd pwd = new Passwd();
                    zip.Password = pwd.GetPassword(ZipFileName + "_" + ScheduleID + "_" + DateTime.Now.ToString("ddMMyyyy"));
                    // add this map file into the "images" directory in the zip archive
                    zip.AddDirectory(ConfigurationSettings.AppSettings["XMLPullTempSavePath"]);
                    // add the report into a different directory in the archive
                    zip.Save(ConfigurationSettings.AppSettings["XMLPullTempSavePath"] + "_" + ScheduleID + "_" + DateTime.Now.ToString("ddMMyyyy") + ".zip");
                }
                SendFileName = ConfigurationSettings.AppSettings["XMLPullTempSavePath"] + "_" + ScheduleID + "_" + DateTime.Now.ToString("ddMMyyyy") + ".zip";
            }
            catch (Exception )
            {

            }

        }

        public void createPullNode(string OperationType, string ContentType, string FileName, string FilePath, string Extention, string WildCard, string Date_ddMMyyyy, string Reserve, XmlTextWriter writer)
        {
            writer.WriteStartElement(OperationType);

            writer.WriteStartElement("ContentType");
            writer.WriteString(ContentType);
            writer.WriteEndElement();
            writer.WriteStartElement("FileName");
            writer.WriteString(FileName);
            writer.WriteEndElement();
            writer.WriteStartElement("FilePath");
            writer.WriteString(FilePath);
            writer.WriteEndElement();
            writer.WriteStartElement("Extention");
            writer.WriteString(Extention);
            writer.WriteEndElement();
            writer.WriteStartElement("WildCard");
            writer.WriteString(WildCard);
            writer.WriteEndElement();
            writer.WriteStartElement("Date");
            writer.WriteString(Date_ddMMyyyy);
            writer.WriteEndElement();
            writer.WriteStartElement("Responses");
            writer.WriteEndElement();
            writer.WriteStartElement("Reserve");
            writer.WriteString(Reserve);
            writer.WriteEndElement();

            writer.WriteEndElement();
        }

        #endregion

        //#region TestPush

        public void MultiSSTPushOperation(decimal ScheduleId, string SourceDirectoryPath)
        {
            Socket SST_Client = (Socket)obj_ConnStatus[0];
            JobScheduleDetails_CDMS GetScheduleInfo = new JobScheduleDetails_CDMS();
            try
            {
                TestPushRequestXML(ScheduleId, SourceDirectoryPath);
                GetScheduleInfo.ScheduleID = ScheduleId;
                GetScheduleInfo.GetMultiScheduleInfo();
                String LoopFile = SendFileName;
                foreach (DataRow row in GetScheduleInfo.dtScheduleInfo.Rows)
                {
                    SendFileName = "";
                    SendFileName = LoopFile;
                    {
                        try
                        {
                            String SST_NAT_IP;
                            decimal SSTID, SST_AgentPort;
                            JobActDet.ScheduleID = ScheduleId;
                            SSTID = decimal.Parse(row["SSTID"].ToString());
                            SST_NAT_IP = Convert.ToString(row["SST_NAT_IP"]);
                            SST_AgentPort = decimal.Parse(row["SST_AgentPort"].ToString());
                            IPAddress[] IP = Dns.GetHostAddresses(SST_NAT_IP);
                            IPEndPoint ipEnd = new IPEndPoint(IP[0], Convert.ToInt32(SST_AgentPort));
                            Socket SST_Client1 = new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp);
                            SST_Client1.Connect(ipEnd);
                            if (SST_Client1.Connected)
                            {
                                string filePath = string.Empty;
                                SendData = null;
                                SendFileName = SendFileName.Replace("\\", "/");
                                while (SendFileName.IndexOf("/") > -1)
                                {
                                    filePath += SendFileName.Substring(0, SendFileName.IndexOf("/") + 1);
                                    SendFileName = SendFileName.Substring(SendFileName.IndexOf("/") + 1);
                                }
                                byte[] fileNameByte = Encoding.ASCII.GetBytes(SendFileName);
                                byte[] fileData = File.ReadAllBytes(filePath + SendFileName);
                                SendData = new byte[4 + fileNameByte.Length + fileData.Length];
                                byte[] fileNameLen = BitConverter.GetBytes(fileNameByte.Length);
                                fileNameLen.CopyTo(SendData, 0);
                                fileNameByte.CopyTo(SendData, 4);
                                fileData.CopyTo(SendData, 4 + fileNameByte.Length);
                                try
                                {
                                    int index = 0;
                                    int SendBytes = 0;
                                    while (index < SendData.Length)
                                    {
                                        if ((index + 8192) < SendData.Length)
                                        {
                                            SST_Client1.Send(SendData, index, 8192, SocketFlags.None);
                                            SendBytes = SST_Client1.SendBufferSize;
                                        }
                                        else
                                        {
                                            SST_Client1.Send(SendData, index, SendData.Length - index, SocketFlags.None);
                                            SendBytes = SST_Client1.SendBufferSize;
                                        }
                                        index += SendBytes;
                                    }
                                }
                                catch (Exception)
                                {
                                }
                                SST_Client1.Shutdown(SocketShutdown.Receive);
                                SST_Client1.Disconnect(false);
                                JobActDet.ActivityDetailID = decimal.Parse(row["ActivityDetailID"].ToString());
                                JobActDet.ScheduleID = ScheduleId;
                                JobActDet.ScheduleDatetime = DateTime.Parse(row["ScheduleDatetime"].ToString());
                                JobActDet.SSTActivityListID = decimal.Parse(row["SSTActivityListID"].ToString());
                                JobActDet.ActivityListDetailID = decimal.Parse(row["ActivityListDetailID"].ToString());
                                JobActDet.SSTID = decimal.Parse(row["SSTID"].ToString());
                                JobActDet.ActualStartDateTime = DateTime.Now;
                                JobActDet.JobResponseID = 4;//pending
                                System.Web.HttpContext.Current.Session["JobResponse"] = "Pending";
                                JobActDet.JobResponseDateTime = DateTime.Now;
                                JobActDet.RetryCount = 0;
                                JobActDet.Remarks = string.Empty;
                                JobActDet.CreatedBy = decimal.Parse(row["CreatedBy"].ToString());
                                JobActDet.CreatedDatetime = DateTime.Parse(row["CreatedDatetime"].ToString());
                                JobActDet.ModifiedBy = decimal.Parse(row["ModifiedBy"].ToString());
                                JobActDet.ModifiedDatetime = DateTime.Now;
                                ActionSP.sp_Insert_JobActionDetails_CDMS(JobActDet.ActivityDetailID, JobActDet.ScheduleID, JobActDet.ActualStartDateTime, JobActDet.SSTActivityListID,
                                                                            JobActDet.ActivityListDetailID, JobActDet.SSTID, JobActDet.ActualStartDateTime,
                                                                            JobActDet.JobResponseID, JobActDet.JobResponseDateTime, JobActDet.RetryCount, JobActDet.Remarks,
                                                                            JobActDet.CreatedBy, JobActDet.CreatedDatetime, JobActDet.ModifiedBy, JobActDet.ModifiedDatetime);
                            }
                        }
                        catch (Exception)
                        {

                            JobActDet.ActivityDetailID = decimal.Parse(row["ActivityDetailID"].ToString());
                            JobActDet.ScheduleID = ScheduleId;
                            JobActDet.ScheduleDatetime = DateTime.Parse(row["ScheduleDatetime"].ToString());
                            JobActDet.SSTActivityListID = decimal.Parse(row["SSTActivityListID"].ToString());
                            JobActDet.ActivityListDetailID = decimal.Parse(row["ActivityListDetailID"].ToString());
                            JobActDet.SSTID = decimal.Parse(row["SSTID"].ToString());
                            JobActDet.ActualStartDateTime = DateTime.Now;
                            JobActDet.JobResponseID = 2;//Failed
                            System.Web.HttpContext.Current.Session["JobResponse"] = "Failed";
                            
                            JobActDet.JobResponseDateTime = DateTime.Now;
                            JobActDet.RetryCount = 0;
                            JobActDet.Remarks = "Not Connected";
                            JobActDet.CreatedBy = decimal.Parse(row["CreatedBy"].ToString());
                            JobActDet.CreatedDatetime = DateTime.Parse(row["CreatedDatetime"].ToString());
                            JobActDet.ModifiedBy = decimal.Parse(row["ModifiedBy"].ToString());
                            JobActDet.ModifiedDatetime = DateTime.Now;

                            ActionSP.sp_Insert_JobActionDetails_CDMS(JobActDet.ActivityDetailID, JobActDet.ScheduleID, JobActDet.ActualStartDateTime, JobActDet.SSTActivityListID,
                                                                        JobActDet.ActivityListDetailID, JobActDet.SSTID, JobActDet.ActualStartDateTime,
                                                                        JobActDet.JobResponseID, JobActDet.JobResponseDateTime, JobActDet.RetryCount, JobActDet.Remarks,
                                                                        JobActDet.CreatedBy, JobActDet.CreatedDatetime, JobActDet.ModifiedBy, JobActDet.ModifiedDatetime);
                            ActionSP = null;

                        }
                    }
                }
            }
            catch (Exception)
            {
            }
        }
        private void TestPushRequestXML(decimal ScheduleId, String SourceDirectoryPath)
        {
            try
            {
                String Activity, JobType, SST_ORGIP, SST_NAT_IP, FilePath, FileName;
                decimal ScheduleID, SSTID, SST_AgentPort;

                clsPullRequest GetReuestInfo = new clsPullRequest();
                GetReuestInfo.ScheduleID = ScheduleId;
                GetReuestInfo.fillup();

                ScheduleID = GetReuestInfo.ScheduleID;
                Activity = GetReuestInfo.Activity;
                JobType = GetReuestInfo.JobType;
                SSTID = GetReuestInfo.SSTID;
                SST_ORGIP = GetReuestInfo.SST_ORGIP;
                SST_NAT_IP = GetReuestInfo.SST_NAT_IP;
                SST_AgentPort = GetReuestInfo.SST_AgentPort;

                FilePath = GetReuestInfo.JobTypeAgentPath;
                FileName = GetReuestInfo.EDCFileName_OnSST;

                String XMLFilePath = ConfigurationSettings.AppSettings["XMLPushPath"];

                if (!Directory.Exists(ConfigurationSettings.AppSettings["XMLPushPath"]))
                {
                    Directory.CreateDirectory(ConfigurationSettings.AppSettings["XMLPushPath"]);
                }

                SendFileName = XMLFilePath + @"\Push" + "_" + ScheduleID + "_" + DateTime.Now.ToString("ddMMyyyy") + ".xml";

                XmlDocument xmldoc = new XmlDocument();

                XmlTextWriter writer = new XmlTextWriter(SendFileName, System.Text.Encoding.UTF8);
                writer.WriteStartDocument(true);
                writer.Formatting = Formatting.Indented;
                writer.Indentation = 3;
                writer.WriteStartElement("RMMS");
                switch (JobType)
                {
                    case "ICON":
                        {
                            createPushNode(Activity, JobType, FileName, FilePath, writer);
                            break;
                        }
                }
                writer.WriteEndElement();
                writer.WriteEndDocument();
                writer.Close();

                using (ZipFile zip = new ZipFile())
                {
                    if (!Directory.Exists(ConfigurationSettings.AppSettings["XMLPushTempSavePath"]))
                        Directory.CreateDirectory(ConfigurationSettings.AppSettings["XMLPushTempSavePath"]);

                    EmptyFolder(new DirectoryInfo(ConfigurationSettings.AppSettings["XMLPushTempSavePath"]));

                    string name = Path.GetFileName(SendFileName);
                    string dest = Path.Combine(ConfigurationSettings.AppSettings["XMLPushTempSavePath"], name);

                    File.Copy(SendFileName, dest, true);

                    string ZipFileName = ConfigurationSettings.AppSettings["XMLPushTempSavePath"];
                    ZipFileName = ZipFileName.Replace("\\", "/");

                    while (ZipFileName.IndexOf("/") > 0)
                        ZipFileName = ZipFileName.Substring(ZipFileName.IndexOf("/") + 1);

                    Passwd pwd = new Passwd();
                    zip.Password = pwd.GetPassword(ZipFileName + "_" + ScheduleID + "_" + DateTime.Now.ToString("ddMMyyyy"));

                    zip.AddDirectory(ConfigurationSettings.AppSettings["XMLPushTempSavePath"]);
                    zip.AddDirectory(SourceDirectoryPath);

                    zip.Save(ConfigurationSettings.AppSettings["XMLPushTempSavePath"] + "_" + ScheduleID + "_" + DateTime.Now.ToString("ddMMyyyy") + ".zip");
                }
                SendFileName = ConfigurationSettings.AppSettings["XMLPushTempSavePath"] + "_" + ScheduleID + "_" + DateTime.Now.ToString("ddMMyyyy") + ".zip";
            }
            catch (Exception)
            { }
        }
        private void createPushNode(string OperationType, string ContentType, string FileName, string FilePath, XmlTextWriter writer)
        {
            writer.WriteStartElement(OperationType);
            writer.WriteStartElement("ContentType");
            writer.WriteString(ContentType);
            writer.WriteEndElement();
            writer.WriteStartElement("FileName");
            writer.WriteString("");
            writer.WriteEndElement();
            writer.WriteStartElement("FilePathPerm");
            writer.WriteString(FilePath);
            writer.WriteEndElement();
            writer.WriteStartElement("FilePathTemp");
            writer.WriteString("");
            writer.WriteEndElement();
            writer.WriteStartElement("ReplaceExisting");
            writer.WriteString("");
            writer.WriteEndElement();
            writer.WriteStartElement("RenameExisting");
            writer.WriteString("");
            writer.WriteEndElement();
            writer.WriteStartElement("RemoveCurrent");
            writer.WriteString("");
            writer.WriteEndElement();
            writer.WriteStartElement("RemoveCurrentAfter");
            writer.WriteString("");
            writer.WriteEndElement();
            writer.WriteStartElement("BackupExisting");
            writer.WriteString("");
            writer.WriteEndElement();
            writer.WriteStartElement("Responses");
            writer.WriteEndElement();
            writer.WriteStartElement("Reserve");
            writer.WriteString("");
            writer.WriteEndElement();

            writer.WriteEndElement();
        }
        private void EmptyFolder(DirectoryInfo directoryInfo)
        {
            foreach (FileInfo file in directoryInfo.GetFiles())
            {
                string name = file.Directory + "\\" + file.Name;
                File.Delete(name);
            }
            foreach (DirectoryInfo subfolder in directoryInfo.GetDirectories())
            {
                EmptyFolder(subfolder);
            }
        }

        //#endregion

        #region TestRestart

            public void MultiSSTRestartOperation(decimal ScheduleId)
            {
                Socket SST_Client = (Socket)obj_ConnStatus[0];
                JobScheduleDetails_CDMS GetScheduleInfo = new JobScheduleDetails_CDMS();
                try
                {
                    TestRestartRequestXML(ScheduleId);

                    GetScheduleInfo.ScheduleID = ScheduleId;
                    GetScheduleInfo.GetMultiScheduleInfo();
                    String LoopFile = SendFileName;
                    foreach (DataRow row in GetScheduleInfo.dtScheduleInfo.Rows)
                    {
                        SendFileName = "";
                        SendFileName = LoopFile;
                        {
                            try
                            {
                                String SST_NAT_IP;//SST_ORGIP,
                                decimal SSTID, SST_AgentPort;

                                JobActDet.ScheduleID = ScheduleId;
                                SSTID = decimal.Parse(row["SSTID"].ToString());
                                SST_NAT_IP = Convert.ToString(row["SST_NAT_IP"]);
                                SST_AgentPort = decimal.Parse(row["SST_AgentPort"].ToString());

                                IPAddress[] IP = Dns.GetHostAddresses(SST_NAT_IP);
                                IPEndPoint ipEnd = new IPEndPoint(IP[0], Convert.ToInt32(SST_AgentPort));
                                Socket SST_Client1 = new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp);
                                 IAsyncResult result = SST_Client1.BeginConnect(SST_NAT_IP, Convert.ToInt16(SST_AgentPort), null, null);
                                bool success = result.AsyncWaitHandle.WaitOne(1000, true);
                                if (success)
                                {

                                    // SST_Client1.Connect(ipEnd);
                                    if (SST_Client1.Connected)
                                    {
                                        string filePath = "";
                                        SendData = null;

                                        SendFileName = SendFileName.Replace("\\", "/");
                                        while (SendFileName.IndexOf("/") > -1)
                                        {
                                            filePath += SendFileName.Substring(0, SendFileName.IndexOf("/") + 1);
                                            SendFileName = SendFileName.Substring(SendFileName.IndexOf("/") + 1);
                                        }
                                        byte[] fileNameByte = Encoding.ASCII.GetBytes(SendFileName);
                                        byte[] fileData = File.ReadAllBytes(filePath + SendFileName);
                                        SendData = new byte[4 + fileNameByte.Length + fileData.Length];
                                        byte[] fileNameLen = BitConverter.GetBytes(fileNameByte.Length);

                                        fileNameLen.CopyTo(SendData, 0);
                                        fileNameByte.CopyTo(SendData, 4);
                                        fileData.CopyTo(SendData, 4 + fileNameByte.Length);

                                        try
                                        {
                                            int index = 0;
                                            int SendBytes = 0;
                                            while (index < SendData.Length)
                                            {
                                                if ((index + 8192) < SendData.Length)
                                                {
                                                    SST_Client1.Send(SendData, index, 8192, SocketFlags.None);
                                                    SendBytes = SST_Client1.SendBufferSize;
                                                }
                                                else
                                                {
                                                    SST_Client1.Send(SendData, index, SendData.Length - index, SocketFlags.None);
                                                    SendBytes = SST_Client1.SendBufferSize;
                                                }
                                                index += SendBytes;
                                            }
                                        }
                                        catch (Exception)
                                        {
                                        }
                                        // Release the socket.
                                        SST_Client1.Shutdown(SocketShutdown.Receive);
                                        SST_Client1.Dispose();

                                        JobActDet.ActivityDetailID = decimal.Parse(row["ActivityDetailID"].ToString());
                                        JobActDet.ScheduleID = ScheduleId;
                                        JobActDet.ScheduleDatetime = DateTime.Parse(row["ScheduleDatetime"].ToString());
                                        JobActDet.SSTActivityListID = decimal.Parse(row["SSTActivityListID"].ToString());
                                        JobActDet.ActivityListDetailID = decimal.Parse(row["ActivityListDetailID"].ToString());
                                        JobActDet.SSTID = decimal.Parse(row["SSTID"].ToString());
                                        JobActDet.ActualStartDateTime = DateTime.Now;
                                        JobActDet.JobResponseID = 4;  //Pending
                                        System.Web.HttpContext.Current.Session["JobResponse"] = "Pending";

                                        JobActDet.JobResponseDateTime = DateTime.Now;
                                        JobActDet.RetryCount = 0;
                                        JobActDet.Remarks = "";
                                        JobActDet.CreatedBy = decimal.Parse(row["CreatedBy"].ToString());
                                        JobActDet.CreatedDatetime = DateTime.Parse(row["CreatedDatetime"].ToString());
                                        JobActDet.ModifiedBy = decimal.Parse(row["ModifiedBy"].ToString());
                                        JobActDet.ModifiedDatetime = DateTime.Now;

                                        ActionSP.sp_Insert_JobActionDetails_CDMS(JobActDet.ActivityDetailID, JobActDet.ScheduleID, JobActDet.ActualStartDateTime, JobActDet.SSTActivityListID,
                                                                                    JobActDet.ActivityListDetailID, JobActDet.SSTID, JobActDet.ActualStartDateTime,
                                                                                    JobActDet.JobResponseID, JobActDet.JobResponseDateTime, JobActDet.RetryCount, JobActDet.Remarks,
                                                                                    JobActDet.CreatedBy, JobActDet.CreatedDatetime, JobActDet.ModifiedBy, JobActDet.ModifiedDatetime);
                                        //ActionSP = null;
                                        //DissConnectingOperation();
                                    }
                                    else
                                    {
                                        JobActDet.ActivityDetailID = decimal.Parse(row["ActivityDetailID"].ToString());
                                        JobActDet.ScheduleID = ScheduleId;
                                        JobActDet.ScheduleDatetime = DateTime.Parse(row["ScheduleDatetime"].ToString());
                                        JobActDet.SSTActivityListID = decimal.Parse(row["SSTActivityListID"].ToString());
                                        JobActDet.ActivityListDetailID = decimal.Parse(row["ActivityListDetailID"].ToString());
                                        JobActDet.SSTID = decimal.Parse(row["SSTID"].ToString());
                                        JobActDet.ActualStartDateTime = DateTime.Now;
                                        JobActDet.JobResponseID = 4;  //Pending
                                        System.Web.HttpContext.Current.Session["JobResponse"] = "Pending";

                                        JobActDet.JobResponseDateTime = DateTime.Now;
                                        JobActDet.RetryCount = 0;
                                        JobActDet.Remarks = "";
                                        JobActDet.CreatedBy = decimal.Parse(row["CreatedBy"].ToString());
                                        JobActDet.CreatedDatetime = DateTime.Parse(row["CreatedDatetime"].ToString());
                                        JobActDet.ModifiedBy = decimal.Parse(row["ModifiedBy"].ToString());
                                        JobActDet.ModifiedDatetime = DateTime.Now;

                                        ActionSP.sp_Insert_JobActionDetails_CDMS(JobActDet.ActivityDetailID, JobActDet.ScheduleID, JobActDet.ActualStartDateTime, JobActDet.SSTActivityListID,
                                                                                    JobActDet.ActivityListDetailID, JobActDet.SSTID, JobActDet.ActualStartDateTime,
                                                                                    JobActDet.JobResponseID, JobActDet.JobResponseDateTime, JobActDet.RetryCount, JobActDet.Remarks,
                                                                                    JobActDet.CreatedBy, JobActDet.CreatedDatetime, JobActDet.ModifiedBy, JobActDet.ModifiedDatetime);
                                        //ActionSP = null;
                                    }
                                }
                                else 
                                {
                                    JobActDet.ActivityDetailID = decimal.Parse(row["ActivityDetailID"].ToString());
                                    JobActDet.ScheduleID = ScheduleId;
                                    JobActDet.ScheduleDatetime = DateTime.Parse(row["ScheduleDatetime"].ToString());
                                    JobActDet.SSTActivityListID = decimal.Parse(row["SSTActivityListID"].ToString());
                                    JobActDet.ActivityListDetailID = decimal.Parse(row["ActivityListDetailID"].ToString());
                                    JobActDet.SSTID = decimal.Parse(row["SSTID"].ToString());
                                    JobActDet.ActualStartDateTime = DateTime.Now;
                                    JobActDet.JobResponseID = 2;  //Pending
                                    System.Web.HttpContext.Current.Session["JobResponse"] = "Pending";

                                    JobActDet.JobResponseDateTime = DateTime.Now;
                                    JobActDet.RetryCount = 0;
                                    JobActDet.Remarks = "Not Connected";
                                    JobActDet.CreatedBy = decimal.Parse(row["CreatedBy"].ToString());
                                    JobActDet.CreatedDatetime = DateTime.Parse(row["CreatedDatetime"].ToString());
                                    JobActDet.ModifiedBy = decimal.Parse(row["ModifiedBy"].ToString());
                                    JobActDet.ModifiedDatetime = DateTime.Now;

                                    ActionSP.sp_Insert_JobActionDetails_CDMS(JobActDet.ActivityDetailID, JobActDet.ScheduleID, JobActDet.ActualStartDateTime, JobActDet.SSTActivityListID,
                                                                                JobActDet.ActivityListDetailID, JobActDet.SSTID, JobActDet.ActualStartDateTime,
                                                                                JobActDet.JobResponseID, JobActDet.JobResponseDateTime, JobActDet.RetryCount, JobActDet.Remarks,
                                                                                JobActDet.CreatedBy, JobActDet.CreatedDatetime, JobActDet.ModifiedBy, JobActDet.ModifiedDatetime);
                                    //ActionSP = null;
                                }
                            }
                            catch (Exception )
                            {

                                JobActDet.ActivityDetailID = decimal.Parse(row["ActivityDetailID"].ToString());
                                JobActDet.ScheduleID = ScheduleId;
                                JobActDet.ScheduleDatetime = DateTime.Parse(row["ScheduleDatetime"].ToString());
                                JobActDet.SSTActivityListID = decimal.Parse(row["SSTActivityListID"].ToString());
                                JobActDet.ActivityListDetailID = decimal.Parse(row["ActivityListDetailID"].ToString());
                                JobActDet.SSTID = decimal.Parse(row["SSTID"].ToString());
                                JobActDet.ActualStartDateTime = DateTime.Now;
                                JobActDet.JobResponseID = 2;//Failed
                                System.Web.HttpContext.Current.Session["JobResponse"] = "Failed";
                            
                                JobActDet.JobResponseDateTime = DateTime.Now;
                                JobActDet.RetryCount = 0;
                                JobActDet.Remarks = "Not Connected";
                                JobActDet.CreatedBy = decimal.Parse(row["CreatedBy"].ToString());
                                JobActDet.CreatedDatetime = DateTime.Parse(row["CreatedDatetime"].ToString());
                                JobActDet.ModifiedBy = decimal.Parse(row["ModifiedBy"].ToString());
                                JobActDet.ModifiedDatetime = DateTime.Now;

                                ActionSP.sp_Insert_JobActionDetails_CDMS(JobActDet.ActivityDetailID, JobActDet.ScheduleID, JobActDet.ActualStartDateTime, JobActDet.SSTActivityListID,
                                                                            JobActDet.ActivityListDetailID, JobActDet.SSTID, JobActDet.ActualStartDateTime,
                                                                            JobActDet.JobResponseID, JobActDet.JobResponseDateTime, JobActDet.RetryCount, JobActDet.Remarks,
                                                                            JobActDet.CreatedBy, JobActDet.CreatedDatetime, JobActDet.ModifiedBy, JobActDet.ModifiedDatetime);
                                //ActionSP = null;

                            }
                        }
                    }
                }
                catch (Exception )
                {
                }
            }

            public void TestRestartRequestXML(decimal ScheduleId)
            {
                try
                {
                    String Activity,  SST_ORGIP, SST_NAT_IP,  CommandName;
                    decimal ScheduleID, SSTID, SST_AgentPort;

                    clsPullRequest GetReuestInfo = new clsPullRequest();
                    GetReuestInfo.ScheduleID = ScheduleId;
                    GetReuestInfo.fillup();

                    ScheduleID = GetReuestInfo.ScheduleID;
                    Activity = GetReuestInfo.Activity;
                    CommandName = GetReuestInfo.JobType;
                    SSTID = GetReuestInfo.SSTID;
                    SST_ORGIP = GetReuestInfo.SST_ORGIP;
                    SST_NAT_IP = GetReuestInfo.SST_NAT_IP;
                    SST_AgentPort = GetReuestInfo.SST_AgentPort;

                    String XMLFilePath = ConfigurationSettings.AppSettings["XMLPullPath"];

                    if (!Directory.Exists(ConfigurationSettings.AppSettings["XMLPullPath"]))
                    {
                        Directory.CreateDirectory(ConfigurationSettings.AppSettings["XMLPullPath"]);
                    }

                    SendFileName = XMLFilePath + @"\Command" + "_" + ScheduleID + "_" + DateTime.Now.ToString("ddMMyyyy") + ".xml";

                    XmlDocument xmldoc = new XmlDocument();

                    XmlTextWriter writer = new XmlTextWriter(SendFileName, System.Text.Encoding.UTF8);
                    writer.WriteStartDocument(true);
                    writer.Formatting = Formatting.Indented;
                    writer.Indentation = 3;
                    writer.WriteStartElement("RMMS");
                
                    createRestartNode(Activity,  CommandName, DateTime.Now.ToString("ddMMyyyy") + "-" + DateTime.Now.ToString("ddMMyyyy"), "", writer);

                    //createPullNode(string OperationType, string ContentType, string CommandName, string Date_ddMMyyyy, string Reserve, XmlTextWriter writer)
                    //switch (JobType)
                    //{
                    //    case "EJ":
                    //    case "Tracer":
                    //    case "ICON":
                    //        {
                    //            createPullNode(Activity, JobType, FileName, FilePath, ".*", "*", DateTime.Now.ToString("ddMMyyyy") + "-" + DateTime.Now.ToString("ddMMyyyy"), "", writer);
                    //            break;
                    //        }
                    //    case "AUDIO":
                    //    case "MOVIE":
                    //        {
                    //            createPullNode(Activity, JobType, FileName, FilePath, ".*", "*", DateTime.Now.ToString("ddMMyyyy") + "-" + DateTime.Now.ToString("ddMMyyyy"), "", writer);
                    //            break;
                    //        }
                    //}
                    writer.WriteEndElement();
                    writer.WriteEndDocument();
                    writer.Close();

                    using (ZipFile zip = new ZipFile())
                    {
                        if (!Directory.Exists(ConfigurationSettings.AppSettings["XMLPullTempSavePath"]))
                        {
                            Directory.CreateDirectory(ConfigurationSettings.AppSettings["XMLPullTempSavePath"]);
                        }
                        //to call the below method
                        EmptyFolder(new DirectoryInfo(ConfigurationSettings.AppSettings["XMLPullTempSavePath"]));

                        //string[] file = Directory.GetFiles(SendFileName);
                        string name = Path.GetFileName(SendFileName);
                        string dest = Path.Combine(ConfigurationSettings.AppSettings["XMLPullTempSavePath"], name);

                        // To copy a file to another location and // overwrite the destination file if it already exists.
                        System.IO.File.Copy(SendFileName, dest, true);

                        string ZipFileName = ConfigurationSettings.AppSettings["XMLPullTempSavePath"];
                        ZipFileName = ZipFileName.Replace("\\", "/");
                        while (ZipFileName.IndexOf("/") > 0)
                        {
                            //filePath += ZipFileName.Substring(0, ZipFileName.IndexOf("/") + 1);
                            ZipFileName = ZipFileName.Substring(ZipFileName.IndexOf("/") + 1);
                        }

                        Passwd pwd = new Passwd();
                        zip.Password = pwd.GetPassword(ZipFileName + "_" + ScheduleID + "_" + DateTime.Now.ToString("ddMMyyyy"));
                        // add this map file into the "images" directory in the zip archive
                        zip.AddDirectory(ConfigurationSettings.AppSettings["XMLPullTempSavePath"]);
                        // add the report into a different directory in the archive
                        zip.Save(ConfigurationSettings.AppSettings["XMLPullTempSavePath"] + "_" + ScheduleID + "_" + DateTime.Now.ToString("ddMMyyyy") + ".zip");
                    }
                    SendFileName = ConfigurationSettings.AppSettings["XMLPullTempSavePath"] + "_" + ScheduleID + "_" + DateTime.Now.ToString("ddMMyyyy") + ".zip";
                }
                catch (Exception )
                {

                }

            }

            public void createRestartNode(string OperationType, string CommandName, string Date_ddMMyyyy, string Reserve, XmlTextWriter writer)
            {
                writer.WriteStartElement(OperationType);

                writer.WriteStartElement("ContentType");
                writer.WriteString(CommandName);
                writer.WriteEndElement();
                writer.WriteStartElement("Date");
                writer.WriteString(Date_ddMMyyyy);
                writer.WriteEndElement();
                writer.WriteStartElement("Responses");
                writer.WriteEndElement();
                writer.WriteStartElement("Reserve");
                writer.WriteString(Reserve);
                writer.WriteEndElement();

                writer.WriteEndElement();
            }

        #endregion

            #region Restrat Any Service On SST
            private void RequestXmlForRestartAPPNService(decimal ScheduleId, string SourceDirectoryPath, string SourceFileName, string WildCard)
            {
                try
                {
                    String Activity, SST_ORGIP, SST_NAT_IP, CommandName;
                    decimal ScheduleID, SSTID, SST_AgentPort;

                    clsPullRequest GetReuestInfo = new clsPullRequest();
                    GetReuestInfo.ScheduleID = ScheduleId;
                    GetReuestInfo.fillup();

                    ScheduleID = GetReuestInfo.ScheduleID;
                    Activity = GetReuestInfo.Activity;
                    CommandName = GetReuestInfo.JobType;
                    SSTID = GetReuestInfo.SSTID;
                    SST_ORGIP = GetReuestInfo.SST_ORGIP;
                    SST_NAT_IP = GetReuestInfo.SST_NAT_IP;
                    SST_AgentPort = GetReuestInfo.SST_AgentPort;

                    String XMLFilePath = ConfigurationSettings.AppSettings["XMLPullPath"];

                    if (!Directory.Exists(ConfigurationSettings.AppSettings["XMLPullPath"]))
                        Directory.CreateDirectory(ConfigurationSettings.AppSettings["XMLPullPath"]);

                    SendFileName = XMLFilePath + @"\RestartAPPNService" + "_" + ScheduleID + "_" + DateTime.Now.ToString("ddMMyyyy") + ".xml";

                    XmlDocument xmldoc = new XmlDocument();

                    XmlTextWriter writer = new XmlTextWriter(SendFileName, System.Text.Encoding.UTF8);
                    writer.WriteStartDocument(true);
                    writer.Formatting = Formatting.Indented;
                    writer.Indentation = 3;
                    writer.WriteStartElement("RMMS");

                    //  createRestartServiceNode(Activity, CommandName, DateTime.Now.ToString("ddMMyyyy")+ "-" + DateTime.Now.ToString("ddMMyyyy"), "", writer);
                    createPullFileNode(Activity, CommandName, SourceDirectoryPath, SourceFileName, WildCard, DateTime.Now.ToString("ddMMyyyy") + "-" + DateTime.Now.ToString("ddMMyyyy"), "", writer);

                    writer.WriteEndElement();
                    writer.WriteEndDocument();
                    writer.Close();

                    using (ZipFile zip = new ZipFile())
                    {
                        if (!Directory.Exists(ConfigurationSettings.AppSettings["XMLPullTempSavePath"]))
                        {
                            Directory.CreateDirectory(ConfigurationSettings.AppSettings["XMLPullTempSavePath"]);
                        }
                        //to call the below method
                        EmptyFolder(new DirectoryInfo(ConfigurationSettings.AppSettings["XMLPullTempSavePath"]));

                        //string[] file = Directory.GetFiles(SendFileName);
                        string name = Path.GetFileName(SendFileName);
                        string dest = Path.Combine(ConfigurationSettings.AppSettings["XMLPullTempSavePath"], name);

                        // To copy a file to another location and // overwrite the destination file if it already exists.
                        System.IO.File.Copy(SendFileName, dest, true);

                        string ZipFileName = ConfigurationSettings.AppSettings["XMLPullTempSavePath"];
                        ZipFileName = ZipFileName.Replace("\\", "/");
                        while (ZipFileName.IndexOf("/") > 0)
                        {
                            //filePath += ZipFileName.Substring(0, ZipFileName.IndexOf("/") + 1);
                            ZipFileName = ZipFileName.Substring(ZipFileName.IndexOf("/") + 1);
                        }

                        Passwd pwd = new Passwd();
                        zip.Password = pwd.GetPassword(ZipFileName + "_" + ScheduleID + "_" + DateTime.Now.ToString("ddMMyyyy"));
                        // add this map file into the "images" directory in the zip archive
                        zip.AddDirectory(ConfigurationSettings.AppSettings["XMLPullTempSavePath"]);
                        // add the report into a different directory in the archive
                        zip.Save(ConfigurationSettings.AppSettings["XMLPullTempSavePath"] + "_" + ScheduleID + "_" + DateTime.Now.ToString("ddMMyyyy") + ".zip");
                    }
                    SendFileName = ConfigurationSettings.AppSettings["XMLPullTempSavePath"] + "_" + ScheduleID + "_" + DateTime.Now.ToString("ddMMyyyy") + ".zip";
                }
                catch (Exception)
                {

                }

            }
      
            private void RequestXmlForRestartService(decimal ScheduleId)
            {
                try
                {
                    String Activity, SST_ORGIP, SST_NAT_IP, CommandName;
                    decimal ScheduleID, SSTID, SST_AgentPort;

                    clsPullRequest GetReuestInfo = new clsPullRequest();
                    GetReuestInfo.ScheduleID = ScheduleId;
                    GetReuestInfo.fillup();

                    ScheduleID = GetReuestInfo.ScheduleID;
                    Activity = GetReuestInfo.Activity;
                    CommandName = GetReuestInfo.JobType;
                    SSTID = GetReuestInfo.SSTID;
                    SST_ORGIP = GetReuestInfo.SST_ORGIP;
                    SST_NAT_IP = GetReuestInfo.SST_NAT_IP;
                    SST_AgentPort = GetReuestInfo.SST_AgentPort;

                    String XMLFilePath = ConfigurationSettings.AppSettings["XMLPullPath"];

                    if (!Directory.Exists(ConfigurationSettings.AppSettings["XMLPullPath"]))
                        Directory.CreateDirectory(ConfigurationSettings.AppSettings["XMLPullPath"]);

                    SendFileName = XMLFilePath + @"\RestartService" + "_" + ScheduleID + "_" + DateTime.Now.ToString("ddMMyyyy") + ".xml";

                    XmlDocument xmldoc = new XmlDocument();

                    XmlTextWriter writer = new XmlTextWriter(SendFileName, System.Text.Encoding.UTF8);
                    writer.WriteStartDocument(true);
                    writer.Formatting = Formatting.Indented;
                    writer.Indentation = 3;
                    writer.WriteStartElement("RMMS");

                    createRestartServiceNode(Activity, CommandName, DateTime.Now.ToString("ddMMyyyy") + "-" + DateTime.Now.ToString("ddMMyyyy"), "", writer);

                    writer.WriteEndElement();
                    writer.WriteEndDocument();
                    writer.Close();

                    using (ZipFile zip = new ZipFile())
                    {
                        if (!Directory.Exists(ConfigurationSettings.AppSettings["XMLPullTempSavePath"]))
                        {
                            Directory.CreateDirectory(ConfigurationSettings.AppSettings["XMLPullTempSavePath"]);
                        }
                        //to call the below method
                        EmptyFolder(new DirectoryInfo(ConfigurationSettings.AppSettings["XMLPullTempSavePath"]));

                        //string[] file = Directory.GetFiles(SendFileName);
                        string name = Path.GetFileName(SendFileName);
                        string dest = Path.Combine(ConfigurationSettings.AppSettings["XMLPullTempSavePath"], name);

                        // To copy a file to another location and // overwrite the destination file if it already exists.
                        System.IO.File.Copy(SendFileName, dest, true);

                        string ZipFileName = ConfigurationSettings.AppSettings["XMLPullTempSavePath"];
                        ZipFileName = ZipFileName.Replace("\\", "/");
                        while (ZipFileName.IndexOf("/") > 0)
                        {
                            //filePath += ZipFileName.Substring(0, ZipFileName.IndexOf("/") + 1);
                            ZipFileName = ZipFileName.Substring(ZipFileName.IndexOf("/") + 1);
                        }

                        Passwd pwd = new Passwd();
                        zip.Password = pwd.GetPassword(ZipFileName + "_" + ScheduleID + "_" + DateTime.Now.ToString("ddMMyyyy"));
                        // add this map file into the "images" directory in the zip archive
                        zip.AddDirectory(ConfigurationSettings.AppSettings["XMLPullTempSavePath"]);
                        // add the report into a different directory in the archive
                        zip.Save(ConfigurationSettings.AppSettings["XMLPullTempSavePath"] + "_" + ScheduleID + "_" + DateTime.Now.ToString("ddMMyyyy") + ".zip");
                    }
                    SendFileName = ConfigurationSettings.AppSettings["XMLPullTempSavePath"] + "_" + ScheduleID + "_" + DateTime.Now.ToString("ddMMyyyy") + ".zip";
                }
                catch (Exception )
                {

                }

            }

            private void createRestartServiceNode(string OperationType, string CommandName, string Date_ddMMyyyy, string Reserve, XmlTextWriter writer)
            {
                writer.WriteStartElement(OperationType);

                writer.WriteStartElement("ContentType");
                writer.WriteString(CommandName);
                writer.WriteEndElement();
                writer.WriteStartElement("Name");
                writer.WriteString(ConfigurationSettings.AppSettings["RESTRAT_SERVICE_NAME"]);
                writer.WriteEndElement();
                writer.WriteStartElement("Date");
                writer.WriteString(Date_ddMMyyyy);
                writer.WriteEndElement();
                writer.WriteStartElement("Responses");
                writer.WriteEndElement();
                writer.WriteStartElement("Reserve");
                writer.WriteString(Reserve);
                writer.WriteEndElement();

                writer.WriteEndElement();
            }

            public void RestartService(decimal ScheduleId)
            {
                Socket SST_Client = (Socket)obj_ConnStatus[0];
                JobScheduleDetails_CDMS GetScheduleInfo = new JobScheduleDetails_CDMS();
                try
                {
                    RequestXmlForRestartService(ScheduleId);

                    GetScheduleInfo.ScheduleID = ScheduleId;
                    GetScheduleInfo.GetMultiScheduleInfo();
                    String LoopFile = SendFileName;
                    foreach (DataRow row in GetScheduleInfo.dtScheduleInfo.Rows)
                    {
                        SendFileName = "";
                        SendFileName = LoopFile;
                        {
                            try
                            {
                                String SST_NAT_IP;//SST_ORGIP,
                                decimal SSTID, SST_AgentPort;

                                JobActDet.ScheduleID = ScheduleId;
                                SSTID = decimal.Parse(row["SSTID"].ToString());
                                SST_NAT_IP = Convert.ToString(row["SST_NAT_IP"]);
                                SST_AgentPort = decimal.Parse(row["SST_AgentPort"].ToString());

                                IPAddress[] IP = Dns.GetHostAddresses(SST_NAT_IP);
                                IPEndPoint ipEnd = new IPEndPoint(IP[0], Convert.ToInt32(SST_AgentPort));
                                Socket SST_Client1 = new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp);

                                IAsyncResult result = SST_Client1.BeginConnect(SST_NAT_IP, Convert.ToInt16(SST_AgentPort), null, null);
                                bool success = result.AsyncWaitHandle.WaitOne(1000, true);


                                if (success)
                                {
                                    // SST_Client1.Connect(ipEnd);
                                    if (SST_Client1.Connected)
                                    {
                                        string filePath = "";
                                        SendData = null;

                                        SendFileName = SendFileName.Replace("\\", "/");
                                        while (SendFileName.IndexOf("/") > -1)
                                        {
                                            filePath += SendFileName.Substring(0, SendFileName.IndexOf("/") + 1);
                                            SendFileName = SendFileName.Substring(SendFileName.IndexOf("/") + 1);
                                        }
                                        byte[] fileNameByte = Encoding.ASCII.GetBytes(SendFileName);
                                        byte[] fileData = File.ReadAllBytes(filePath + SendFileName);
                                        SendData = new byte[4 + fileNameByte.Length + fileData.Length];
                                        byte[] fileNameLen = BitConverter.GetBytes(fileNameByte.Length);

                                        fileNameLen.CopyTo(SendData, 0);
                                        fileNameByte.CopyTo(SendData, 4);
                                        fileData.CopyTo(SendData, 4 + fileNameByte.Length);

                                        try
                                        {
                                            int index = 0;
                                            int SendBytes = 0;
                                            while (index < SendData.Length)
                                            {
                                                if ((index + 8192) < SendData.Length)
                                                {
                                                    SST_Client1.Send(SendData, index, 8192, SocketFlags.None);
                                                    SendBytes = SST_Client1.SendBufferSize;
                                                }
                                                else
                                                {
                                                    SST_Client1.Send(SendData, index, SendData.Length - index, SocketFlags.None);
                                                    SendBytes = SST_Client1.SendBufferSize;
                                                }
                                                index += SendBytes;
                                            }
                                        }
                                        catch (Exception)
                                        {
                                        }
                                        // Release the socket.
                                        SST_Client1.Shutdown(SocketShutdown.Receive);
                                        SST_Client1.Disconnect(false);

                                        JobActDet.ActivityDetailID = decimal.Parse(row["ActivityDetailID"].ToString());
                                        JobActDet.ScheduleID = ScheduleId;
                                        JobActDet.ScheduleDatetime = DateTime.Parse(row["ScheduleDatetime"].ToString());
                                        JobActDet.SSTActivityListID = decimal.Parse(row["SSTActivityListID"].ToString());
                                        JobActDet.ActivityListDetailID = decimal.Parse(row["ActivityListDetailID"].ToString());
                                        JobActDet.SSTID = decimal.Parse(row["SSTID"].ToString());
                                        JobActDet.ActualStartDateTime = DateTime.Now;
                                        JobActDet.JobResponseID = 4;  //Pending
                                        System.Web.HttpContext.Current.Session["JobResponse"] = "Pending";

                                        JobActDet.JobResponseDateTime = DateTime.Now;
                                        JobActDet.RetryCount = 0;
                                        JobActDet.Remarks = "";
                                        JobActDet.CreatedBy = decimal.Parse(row["CreatedBy"].ToString());
                                        JobActDet.CreatedDatetime = DateTime.Parse(row["CreatedDatetime"].ToString());
                                        JobActDet.ModifiedBy = decimal.Parse(row["ModifiedBy"].ToString());
                                        JobActDet.ModifiedDatetime = DateTime.Now;

                                        ActionSP.sp_Insert_JobActionDetails_CDMS(JobActDet.ActivityDetailID, JobActDet.ScheduleID, JobActDet.ActualStartDateTime, JobActDet.SSTActivityListID,
                                                                                    JobActDet.ActivityListDetailID, JobActDet.SSTID, JobActDet.ActualStartDateTime,
                                                                                    JobActDet.JobResponseID, JobActDet.JobResponseDateTime, JobActDet.RetryCount, JobActDet.Remarks,
                                                                                    JobActDet.CreatedBy, JobActDet.CreatedDatetime, JobActDet.ModifiedBy, JobActDet.ModifiedDatetime);
                                        //ActionSP = null;
                                        //DissConnectingOperation();
                                    }
                                    else
                                    {
                                        JobActDet.ActivityDetailID = decimal.Parse(row["ActivityDetailID"].ToString());
                                        JobActDet.ScheduleID = ScheduleId;
                                        JobActDet.ScheduleDatetime = DateTime.Parse(row["ScheduleDatetime"].ToString());
                                        JobActDet.SSTActivityListID = decimal.Parse(row["SSTActivityListID"].ToString());
                                        JobActDet.ActivityListDetailID = decimal.Parse(row["ActivityListDetailID"].ToString());
                                        JobActDet.SSTID = decimal.Parse(row["SSTID"].ToString());
                                        JobActDet.ActualStartDateTime = DateTime.Now;
                                        JobActDet.JobResponseID = 2;  //Pending
                                        System.Web.HttpContext.Current.Session["JobResponse"] = "Pending";

                                        JobActDet.JobResponseDateTime = DateTime.Now;
                                        JobActDet.RetryCount = 0;
                                        JobActDet.Remarks = "Not Connected";
                                        JobActDet.CreatedBy = decimal.Parse(row["CreatedBy"].ToString());
                                        JobActDet.CreatedDatetime = DateTime.Parse(row["CreatedDatetime"].ToString());
                                        JobActDet.ModifiedBy = decimal.Parse(row["ModifiedBy"].ToString());
                                        JobActDet.ModifiedDatetime = DateTime.Now;

                                        ActionSP.sp_Insert_JobActionDetails_CDMS(JobActDet.ActivityDetailID, JobActDet.ScheduleID, JobActDet.ActualStartDateTime, JobActDet.SSTActivityListID,
                                                                                    JobActDet.ActivityListDetailID, JobActDet.SSTID, JobActDet.ActualStartDateTime,
                                                                                    JobActDet.JobResponseID, JobActDet.JobResponseDateTime, JobActDet.RetryCount, JobActDet.Remarks,
                                                                                    JobActDet.CreatedBy, JobActDet.CreatedDatetime, JobActDet.ModifiedBy, JobActDet.ModifiedDatetime);
                                        //ActionSP = null;
                                    }
                                }
                                else
                                {
                                    JobActDet.ActivityDetailID = decimal.Parse(row["ActivityDetailID"].ToString());
                                    JobActDet.ScheduleID = ScheduleId;
                                    JobActDet.ScheduleDatetime = DateTime.Parse(row["ScheduleDatetime"].ToString());
                                    JobActDet.SSTActivityListID = decimal.Parse(row["SSTActivityListID"].ToString());
                                    JobActDet.ActivityListDetailID = decimal.Parse(row["ActivityListDetailID"].ToString());
                                    JobActDet.SSTID = decimal.Parse(row["SSTID"].ToString());
                                    JobActDet.ActualStartDateTime = DateTime.Now;
                                    JobActDet.JobResponseID = 2;  //Pending
                                    System.Web.HttpContext.Current.Session["JobResponse"] = "Pending";

                                    JobActDet.JobResponseDateTime = DateTime.Now;
                                    JobActDet.RetryCount = 0;
                                    JobActDet.Remarks = "Not Connected";
                                    JobActDet.CreatedBy = decimal.Parse(row["CreatedBy"].ToString());
                                    JobActDet.CreatedDatetime = DateTime.Parse(row["CreatedDatetime"].ToString());
                                    JobActDet.ModifiedBy = decimal.Parse(row["ModifiedBy"].ToString());
                                    JobActDet.ModifiedDatetime = DateTime.Now;

                                    ActionSP.sp_Insert_JobActionDetails_CDMS(JobActDet.ActivityDetailID, JobActDet.ScheduleID, JobActDet.ActualStartDateTime, JobActDet.SSTActivityListID,
                                                                                JobActDet.ActivityListDetailID, JobActDet.SSTID, JobActDet.ActualStartDateTime,
                                                                                JobActDet.JobResponseID, JobActDet.JobResponseDateTime, JobActDet.RetryCount, JobActDet.Remarks,
                                                                                JobActDet.CreatedBy, JobActDet.CreatedDatetime, JobActDet.ModifiedBy, JobActDet.ModifiedDatetime);
                                    //ActionSP = null;
                                }
                            }
                            catch (Exception )
                            {

                                JobActDet.ActivityDetailID = decimal.Parse(row["ActivityDetailID"].ToString());
                                JobActDet.ScheduleID = ScheduleId;
                                JobActDet.ScheduleDatetime = DateTime.Parse(row["ScheduleDatetime"].ToString());
                                JobActDet.SSTActivityListID = decimal.Parse(row["SSTActivityListID"].ToString());
                                JobActDet.ActivityListDetailID = decimal.Parse(row["ActivityListDetailID"].ToString());
                                JobActDet.SSTID = decimal.Parse(row["SSTID"].ToString());
                                JobActDet.ActualStartDateTime = DateTime.Now;
                                JobActDet.JobResponseID = 2;//Failed
                               System.Web.HttpContext.Current.Session["JobResponse"] = "Failed";
                                
                                JobActDet.JobResponseDateTime = DateTime.Now;
                                JobActDet.RetryCount = 0;
                                JobActDet.Remarks = "Not Connected";
                                JobActDet.CreatedBy = decimal.Parse(row["CreatedBy"].ToString());
                                JobActDet.CreatedDatetime = DateTime.Parse(row["CreatedDatetime"].ToString());
                                JobActDet.ModifiedBy = decimal.Parse(row["ModifiedBy"].ToString());
                                JobActDet.ModifiedDatetime = DateTime.Now;

                                ActionSP.sp_Insert_JobActionDetails_CDMS(JobActDet.ActivityDetailID, JobActDet.ScheduleID, JobActDet.ActualStartDateTime, JobActDet.SSTActivityListID,
                                                                            JobActDet.ActivityListDetailID, JobActDet.SSTID, JobActDet.ActualStartDateTime,
                                                                            JobActDet.JobResponseID, JobActDet.JobResponseDateTime, JobActDet.RetryCount, JobActDet.Remarks,
                                                                            JobActDet.CreatedBy, JobActDet.CreatedDatetime, JobActDet.ModifiedBy, JobActDet.ModifiedDatetime);
                                //ActionSP = null;

                            }
                        }
                    }
                }
                catch (Exception )
                {
                }
            }
            public void MultiSSTRestartOperationAppn(decimal ScheduleId, string SourceDirectoryPath, string FileName, string WildCard)
            {
                Socket SST_Client = (Socket)obj_ConnStatus[0];
                JobScheduleDetails_CDMS GetScheduleInfo = new JobScheduleDetails_CDMS();
                try
                {
                    RequestXmlForRestartAPPNService(ScheduleId, SourceDirectoryPath, FileName, WildCard);

                    GetScheduleInfo.ScheduleID = ScheduleId;
                    GetScheduleInfo.GetMultiScheduleInfo();
                    String LoopFile = SendFileName;
                    foreach (DataRow row in GetScheduleInfo.dtScheduleInfo.Rows)
                    {
                        SendFileName = "";
                        SendFileName = LoopFile;
                        {
                            try
                            {
                                String SST_NAT_IP;//SST_ORGIP,
                                decimal SSTID, SST_AgentPort;

                                JobActDet.ScheduleID = ScheduleId;
                                SSTID = decimal.Parse(row["SSTID"].ToString());
                                SST_NAT_IP = Convert.ToString(row["SST_NAT_IP"]);
                                SST_AgentPort = decimal.Parse(row["SST_AgentPort"].ToString());

                                IPAddress[] IP = Dns.GetHostAddresses(SST_NAT_IP);
                                IPEndPoint ipEnd = new IPEndPoint(IP[0], Convert.ToInt32(SST_AgentPort));
                                Socket SST_Client1 = new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp);
                                IAsyncResult result = SST_Client1.BeginConnect(SST_NAT_IP, Convert.ToInt16(SST_AgentPort), null, null);
                                bool success = result.AsyncWaitHandle.WaitOne(1000, true);
                                if (success)
                                {

                                    // SST_Client1.Connect(ipEnd);
                                    if (SST_Client1.Connected)
                                    {
                                        string filePath = "";
                                        SendData = null;

                                        SendFileName = SendFileName.Replace("\\", "/");
                                        while (SendFileName.IndexOf("/") > -1)
                                        {
                                            filePath += SendFileName.Substring(0, SendFileName.IndexOf("/") + 1);
                                            SendFileName = SendFileName.Substring(SendFileName.IndexOf("/") + 1);
                                        }
                                        byte[] fileNameByte = Encoding.ASCII.GetBytes(SendFileName);
                                        byte[] fileData = File.ReadAllBytes(filePath + SendFileName);
                                        SendData = new byte[4 + fileNameByte.Length + fileData.Length];
                                        byte[] fileNameLen = BitConverter.GetBytes(fileNameByte.Length);

                                        fileNameLen.CopyTo(SendData, 0);
                                        fileNameByte.CopyTo(SendData, 4);
                                        fileData.CopyTo(SendData, 4 + fileNameByte.Length);

                                        try
                                        {
                                            int index = 0;
                                            int SendBytes = 0;
                                            while (index < SendData.Length)
                                            {
                                                if ((index + 8192) < SendData.Length)
                                                {
                                                    SST_Client1.Send(SendData, index, 8192, SocketFlags.None);
                                                    SendBytes = SST_Client1.SendBufferSize;
                                                }
                                                else
                                                {
                                                    SST_Client1.Send(SendData, index, SendData.Length - index, SocketFlags.None);
                                                    SendBytes = SST_Client1.SendBufferSize;
                                                }
                                                index += SendBytes;
                                            }
                                        }
                                        catch (Exception)
                                        {
                                        }
                                        // Release the socket.
                                        SST_Client1.Shutdown(SocketShutdown.Receive);
                                        SST_Client1.Disconnect(false);

                                        JobActDet.ActivityDetailID = decimal.Parse(row["ActivityDetailID"].ToString());
                                        JobActDet.ScheduleID = ScheduleId;
                                        JobActDet.ScheduleDatetime = DateTime.Parse(row["ScheduleDatetime"].ToString());
                                        JobActDet.SSTActivityListID = decimal.Parse(row["SSTActivityListID"].ToString());
                                        JobActDet.ActivityListDetailID = decimal.Parse(row["ActivityListDetailID"].ToString());
                                        JobActDet.SSTID = decimal.Parse(row["SSTID"].ToString());
                                        JobActDet.ActualStartDateTime = DateTime.Now;

                                        JobActDet.JobResponseID =4;  //Success
                                        System.Web.HttpContext.Current.Session["JobResponse"] = "Pending";

                                        JobActDet.JobResponseDateTime = DateTime.Now;
                                        JobActDet.RetryCount = 0;
                                        JobActDet.Remarks = "";
                                        JobActDet.CreatedBy = decimal.Parse(row["CreatedBy"].ToString());
                                        JobActDet.CreatedDatetime = DateTime.Parse(row["CreatedDatetime"].ToString());
                                        JobActDet.ModifiedBy = decimal.Parse(row["ModifiedBy"].ToString());
                                        JobActDet.ModifiedDatetime = DateTime.Now;

                                        ActionSP.sp_Insert_JobActionDetails_CDMS(JobActDet.ActivityDetailID, JobActDet.ScheduleID, JobActDet.ActualStartDateTime, JobActDet.SSTActivityListID,
                                                                                    JobActDet.ActivityListDetailID, JobActDet.SSTID, JobActDet.ActualStartDateTime,
                                                                                    JobActDet.JobResponseID, JobActDet.JobResponseDateTime, JobActDet.RetryCount, JobActDet.Remarks,
                                                                                    JobActDet.CreatedBy, JobActDet.CreatedDatetime, JobActDet.ModifiedBy, JobActDet.ModifiedDatetime);
                                        //ActionSP = null;
                                        //DissConnectingOperation();
                                    }
                                    else
                                    {
                                        JobActDet.ActivityDetailID = decimal.Parse(row["ActivityDetailID"].ToString());
                                        JobActDet.ScheduleID = ScheduleId;
                                        JobActDet.ScheduleDatetime = DateTime.Parse(row["ScheduleDatetime"].ToString());
                                        JobActDet.SSTActivityListID = decimal.Parse(row["SSTActivityListID"].ToString());
                                        JobActDet.ActivityListDetailID = decimal.Parse(row["ActivityListDetailID"].ToString());
                                        JobActDet.SSTID = decimal.Parse(row["SSTID"].ToString());
                                        JobActDet.ActualStartDateTime = DateTime.Now;

                                       // JobActDet.JobResponseID = 2;

                                        System.Web.HttpContext.Current.Session["JobResponse"] = "Pending";

                                        JobActDet.JobResponseDateTime = DateTime.Now;
                                        JobActDet.RetryCount = 0;
                                        JobActDet.Remarks = "Not Connected";
                                        JobActDet.CreatedBy = decimal.Parse(row["CreatedBy"].ToString());
                                        JobActDet.CreatedDatetime = DateTime.Parse(row["CreatedDatetime"].ToString());
                                        JobActDet.ModifiedBy = decimal.Parse(row["ModifiedBy"].ToString());
                                        JobActDet.ModifiedDatetime = DateTime.Now;

                                        ActionSP.sp_Insert_JobActionDetails_CDMS(JobActDet.ActivityDetailID, JobActDet.ScheduleID, JobActDet.ActualStartDateTime, JobActDet.SSTActivityListID,
                                                                                    JobActDet.ActivityListDetailID, JobActDet.SSTID, JobActDet.ActualStartDateTime,
                                                                                    JobActDet.JobResponseID, JobActDet.JobResponseDateTime, JobActDet.RetryCount, JobActDet.Remarks,
                                                                                    JobActDet.CreatedBy, JobActDet.CreatedDatetime, JobActDet.ModifiedBy, JobActDet.ModifiedDatetime);
                                        //ActionSP = null;
                                    }
                                }
                                else
                                {
                                    JobActDet.ActivityDetailID = decimal.Parse(row["ActivityDetailID"].ToString());
                                    JobActDet.ScheduleID = ScheduleId;
                                    JobActDet.ScheduleDatetime = DateTime.Parse(row["ScheduleDatetime"].ToString());
                                    JobActDet.SSTActivityListID = decimal.Parse(row["SSTActivityListID"].ToString());
                                    JobActDet.ActivityListDetailID = decimal.Parse(row["ActivityListDetailID"].ToString());
                                    JobActDet.SSTID = decimal.Parse(row["SSTID"].ToString());
                                    JobActDet.ActualStartDateTime = DateTime.Now;

                                    JobActDet.JobResponseID = 2;

                                    System.Web.HttpContext.Current.Session["JobResponse"] = "Pending";

                                    JobActDet.JobResponseDateTime = DateTime.Now;
                                    JobActDet.RetryCount = 0;
                                    JobActDet.Remarks = "Not Connected";
                                    JobActDet.CreatedBy = decimal.Parse(row["CreatedBy"].ToString());
                                    JobActDet.CreatedDatetime = DateTime.Parse(row["CreatedDatetime"].ToString());
                                    JobActDet.ModifiedBy = decimal.Parse(row["ModifiedBy"].ToString());
                                    JobActDet.ModifiedDatetime = DateTime.Now;

                                    ActionSP.sp_Insert_JobActionDetails_CDMS(JobActDet.ActivityDetailID, JobActDet.ScheduleID, JobActDet.ActualStartDateTime, JobActDet.SSTActivityListID,
                                                                                JobActDet.ActivityListDetailID, JobActDet.SSTID, JobActDet.ActualStartDateTime,
                                                                                JobActDet.JobResponseID, JobActDet.JobResponseDateTime, JobActDet.RetryCount, JobActDet.Remarks,
                                                                                JobActDet.CreatedBy, JobActDet.CreatedDatetime, JobActDet.ModifiedBy, JobActDet.ModifiedDatetime);
                                     
                                }
                            }
                            catch (Exception)
                            {

                                JobActDet.ActivityDetailID = decimal.Parse(row["ActivityDetailID"].ToString());
                                JobActDet.ScheduleID = ScheduleId;
                                JobActDet.ScheduleDatetime = DateTime.Parse(row["ScheduleDatetime"].ToString());
                                JobActDet.SSTActivityListID = decimal.Parse(row["SSTActivityListID"].ToString());
                                JobActDet.ActivityListDetailID = decimal.Parse(row["ActivityListDetailID"].ToString());
                                JobActDet.SSTID = decimal.Parse(row["SSTID"].ToString());
                                JobActDet.ActualStartDateTime = DateTime.Now;
                                JobActDet.JobResponseID = 2;//Failed
                                System.Web.HttpContext.Current.Session["JobResponse"] = "Failed";

                                JobActDet.JobResponseDateTime = DateTime.Now;
                                JobActDet.RetryCount = 0;
                                JobActDet.Remarks = "Not Connected";
                                JobActDet.CreatedBy = decimal.Parse(row["CreatedBy"].ToString());
                                JobActDet.CreatedDatetime = DateTime.Parse(row["CreatedDatetime"].ToString());
                                JobActDet.ModifiedBy = decimal.Parse(row["ModifiedBy"].ToString());
                                JobActDet.ModifiedDatetime = DateTime.Now;

                                ActionSP.sp_Insert_JobActionDetails_CDMS(JobActDet.ActivityDetailID, JobActDet.ScheduleID, JobActDet.ActualStartDateTime, JobActDet.SSTActivityListID,
                                                                            JobActDet.ActivityListDetailID, JobActDet.SSTID, JobActDet.ActualStartDateTime,
                                                                            JobActDet.JobResponseID, JobActDet.JobResponseDateTime, JobActDet.RetryCount, JobActDet.Remarks,
                                                                            JobActDet.CreatedBy, JobActDet.CreatedDatetime, JobActDet.ModifiedBy, JobActDet.ModifiedDatetime);
                                //ActionSP = null;

                            }
                        }
                    }
                }
                catch (Exception)
                {
                }
            }
    
            #endregion

            #region Get System Performance Of SST

            public void GetSystemPerformanceOfSST(decimal ScheduleId)
            {
                Socket SST_Client = (Socket)obj_ConnStatus[0];
                JobScheduleDetails_CDMS GetScheduleInfo = new JobScheduleDetails_CDMS();
                try
                {
                    RequestXMLForSystemPerformance(ScheduleId);

                    GetScheduleInfo.ScheduleID = ScheduleId;
                    GetScheduleInfo.GetMultiScheduleInfo();
                    String LoopFile = SendFileName;
                    foreach (DataRow row in GetScheduleInfo.dtScheduleInfo.Rows)
                    {
                        SendFileName = "";
                        SendFileName = LoopFile;
                        {
                            try
                            {
                                String SST_NAT_IP;//SST_ORGIP,
                                decimal SSTID, SST_AgentPort;

                                JobActDet.ScheduleID = ScheduleId;
                                SSTID = decimal.Parse(row["SSTID"].ToString());
                                SST_NAT_IP = Convert.ToString(row["SST_NAT_IP"]);
                                SST_AgentPort = decimal.Parse(row["SST_AgentPort"].ToString());

                                IPAddress[] IP = Dns.GetHostAddresses(SST_NAT_IP);
                                IPEndPoint ipEnd = new IPEndPoint(IP[0], Convert.ToInt32(SST_AgentPort));
                                Socket SST_Client1 = new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp);

                                SST_Client1.Connect(ipEnd);
                                if (SST_Client1.Connected)
                                {
                                    string filePath = "";
                                    SendData = null;

                                    SendFileName = SendFileName.Replace("\\", "/");
                                    while (SendFileName.IndexOf("/") > -1)
                                    {
                                        filePath += SendFileName.Substring(0, SendFileName.IndexOf("/") + 1);
                                        SendFileName = SendFileName.Substring(SendFileName.IndexOf("/") + 1);
                                    }
                                    byte[] fileNameByte = Encoding.ASCII.GetBytes(SendFileName);
                                    byte[] fileData = File.ReadAllBytes(filePath + SendFileName);
                                    SendData = new byte[4 + fileNameByte.Length + fileData.Length];
                                    byte[] fileNameLen = BitConverter.GetBytes(fileNameByte.Length);

                                    fileNameLen.CopyTo(SendData, 0);
                                    fileNameByte.CopyTo(SendData, 4);
                                    fileData.CopyTo(SendData, 4 + fileNameByte.Length);

                                    try
                                    {
                                        int index = 0;
                                        int SendBytes = 0;
                                        while (index < SendData.Length)
                                        {
                                            if ((index + 8192) < SendData.Length)
                                            {
                                                SST_Client1.Send(SendData, index, 8192, SocketFlags.None);
                                                SendBytes = SST_Client1.SendBufferSize;
                                            }
                                            else
                                            {
                                                SST_Client1.Send(SendData, index, SendData.Length - index, SocketFlags.None);
                                                SendBytes = SST_Client1.SendBufferSize;
                                            }
                                            index += SendBytes;
                                        }
                                    }
                                    catch (Exception )
                                    {
                                    }
                                    // Release the socket.
                                    SST_Client1.Shutdown(SocketShutdown.Receive);
                                    SST_Client1.Disconnect(false);

                                    JobActDet.ActivityDetailID = decimal.Parse(row["ActivityDetailID"].ToString());
                                    JobActDet.ScheduleID = ScheduleId;
                                    JobActDet.ScheduleDatetime = DateTime.Parse(row["ScheduleDatetime"].ToString());
                                    JobActDet.SSTActivityListID = decimal.Parse(row["SSTActivityListID"].ToString());
                                    JobActDet.ActivityListDetailID = decimal.Parse(row["ActivityListDetailID"].ToString());
                                    JobActDet.SSTID = decimal.Parse(row["SSTID"].ToString());
                                    JobActDet.ActualStartDateTime = DateTime.Now;
                                    JobActDet.JobResponseID = 4;  //Pending
                                    System.Web.HttpContext.Current.Session["JobResponse"] = "Pending";
                            

                                    JobActDet.JobResponseDateTime = DateTime.Now;
                                    JobActDet.RetryCount = 0;
                                    JobActDet.Remarks = "";
                                    JobActDet.CreatedBy = decimal.Parse(row["CreatedBy"].ToString());
                                    JobActDet.CreatedDatetime = DateTime.Parse(row["CreatedDatetime"].ToString());
                                    JobActDet.ModifiedBy = decimal.Parse(row["ModifiedBy"].ToString());
                                    JobActDet.ModifiedDatetime = DateTime.Now;

                                    ActionSP.sp_Insert_JobActionDetails_CDMS(JobActDet.ActivityDetailID, JobActDet.ScheduleID, JobActDet.ActualStartDateTime, JobActDet.SSTActivityListID,
                                                                                JobActDet.ActivityListDetailID, JobActDet.SSTID, JobActDet.ActualStartDateTime,
                                                                                JobActDet.JobResponseID, JobActDet.JobResponseDateTime, JobActDet.RetryCount, JobActDet.Remarks,
                                                                                JobActDet.CreatedBy, JobActDet.CreatedDatetime, JobActDet.ModifiedBy, JobActDet.ModifiedDatetime);
                                    //ActionSP = null;
                                    //DissConnectingOperation();
                                }
                            }
                            catch (Exception )
                            {

                                JobActDet.ActivityDetailID = decimal.Parse(row["ActivityDetailID"].ToString());
                                JobActDet.ScheduleID = ScheduleId;
                                JobActDet.ScheduleDatetime = DateTime.Parse(row["ScheduleDatetime"].ToString());
                                JobActDet.SSTActivityListID = decimal.Parse(row["SSTActivityListID"].ToString());
                                JobActDet.ActivityListDetailID = decimal.Parse(row["ActivityListDetailID"].ToString());
                                JobActDet.SSTID = decimal.Parse(row["SSTID"].ToString());
                                JobActDet.ActualStartDateTime = DateTime.Now;
                                JobActDet.JobResponseID = 2;//Failed
                                System.Web.HttpContext.Current.Session["JobResponse"] = "Failed";
                            
                                JobActDet.JobResponseDateTime = DateTime.Now;
                                JobActDet.RetryCount = 0;
                                JobActDet.Remarks = "Not Connected";
                                JobActDet.CreatedBy = decimal.Parse(row["CreatedBy"].ToString());
                                JobActDet.CreatedDatetime = DateTime.Parse(row["CreatedDatetime"].ToString());
                                JobActDet.ModifiedBy = decimal.Parse(row["ModifiedBy"].ToString());
                                JobActDet.ModifiedDatetime = DateTime.Now;

                                ActionSP.sp_Insert_JobActionDetails_CDMS(JobActDet.ActivityDetailID, JobActDet.ScheduleID, JobActDet.ActualStartDateTime, JobActDet.SSTActivityListID,
                                                                            JobActDet.ActivityListDetailID, JobActDet.SSTID, JobActDet.ActualStartDateTime,
                                                                            JobActDet.JobResponseID, JobActDet.JobResponseDateTime, JobActDet.RetryCount, JobActDet.Remarks,
                                                                            JobActDet.CreatedBy, JobActDet.CreatedDatetime, JobActDet.ModifiedBy, JobActDet.ModifiedDatetime);
                                //ActionSP = null;

                            }
                        }
                    }
                }
                catch (Exception )
                {
                }
            }

            private void RequestXMLForSystemPerformance(decimal ScheduleId)
            {
                try
                {
                    String Activity, SST_ORGIP, SST_NAT_IP, CommandName;
                    decimal ScheduleID, SSTID, SST_AgentPort;

                    clsPullRequest GetReuestInfo = new clsPullRequest();
                    GetReuestInfo.ScheduleID = ScheduleId;
                    GetReuestInfo.fillup();

                    ScheduleID = GetReuestInfo.ScheduleID;
                    Activity = GetReuestInfo.Activity;
                    CommandName = GetReuestInfo.JobType;
                    SSTID = GetReuestInfo.SSTID;
                    SST_ORGIP = GetReuestInfo.SST_ORGIP;
                    SST_NAT_IP = GetReuestInfo.SST_NAT_IP;
                    SST_AgentPort = GetReuestInfo.SST_AgentPort;

                    String XMLFilePath = ConfigurationSettings.AppSettings["XMLPullPath"];

                    if (!Directory.Exists(ConfigurationSettings.AppSettings["XMLPullPath"]))
                    {
                        Directory.CreateDirectory(ConfigurationSettings.AppSettings["XMLPullPath"]);
                    }

                    SendFileName = XMLFilePath + @"\Command" + "_" + ScheduleID + "_" + DateTime.Now.ToString("ddMMyyyy") + ".xml";

                    XmlDocument xmldoc = new XmlDocument();

                    XmlTextWriter writer = new XmlTextWriter(SendFileName, System.Text.Encoding.UTF8);
                    writer.WriteStartDocument(true);
                    writer.Formatting = Formatting.Indented;
                    writer.Indentation = 3;
                    writer.WriteStartElement("RMMS");

                    CreateSystemPerformanceNode(Activity, CommandName, DateTime.Now.ToString("ddMMyyyy") + "-" + DateTime.Now.ToString("ddMMyyyy"), "", writer);

                    //createPullNode(string OperationType, string ContentType, string CommandName, string Date_ddMMyyyy, string Reserve, XmlTextWriter writer)
                    //switch (JobType)
                    //{
                    //    case "EJ":
                    //    case "Tracer":
                    //    case "ICON":
                    //        {
                    //            createPullNode(Activity, JobType, FileName, FilePath, ".*", "*", DateTime.Now.ToString("ddMMyyyy") + "-" + DateTime.Now.ToString("ddMMyyyy"), "", writer);
                    //            break;
                    //        }
                    //    case "AUDIO":
                    //    case "MOVIE":
                    //        {
                    //            createPullNode(Activity, JobType, FileName, FilePath, ".*", "*", DateTime.Now.ToString("ddMMyyyy") + "-" + DateTime.Now.ToString("ddMMyyyy"), "", writer);
                    //            break;
                    //        }
                    //}
                    writer.WriteEndElement();
                    writer.WriteEndDocument();
                    writer.Close();

                    using (ZipFile zip = new ZipFile())
                    {
                        if (!Directory.Exists(ConfigurationSettings.AppSettings["XMLPullTempSavePath"]))
                        {
                            Directory.CreateDirectory(ConfigurationSettings.AppSettings["XMLPullTempSavePath"]);
                        }
                        //to call the below method
                        EmptyFolder(new DirectoryInfo(ConfigurationSettings.AppSettings["XMLPullTempSavePath"]));

                        //string[] file = Directory.GetFiles(SendFileName);
                        string name = Path.GetFileName(SendFileName);
                        string dest = Path.Combine(ConfigurationSettings.AppSettings["XMLPullTempSavePath"], name);

                        // To copy a file to another location and // overwrite the destination file if it already exists.
                        System.IO.File.Copy(SendFileName, dest, true);

                        string ZipFileName = ConfigurationSettings.AppSettings["XMLPullTempSavePath"];
                        ZipFileName = ZipFileName.Replace("\\", "/");
                        while (ZipFileName.IndexOf("/") > 0)
                        {
                            //filePath += ZipFileName.Substring(0, ZipFileName.IndexOf("/") + 1);
                            ZipFileName = ZipFileName.Substring(ZipFileName.IndexOf("/") + 1);
                        }

                        Passwd pwd = new Passwd();
                        zip.Password = pwd.GetPassword(ZipFileName + "_" + ScheduleID + "_" + DateTime.Now.ToString("ddMMyyyy"));
                        // add this map file into the "images" directory in the zip archive
                        zip.AddDirectory(ConfigurationSettings.AppSettings["XMLPullTempSavePath"]);
                        // add the report into a different directory in the archive
                        zip.Save(ConfigurationSettings.AppSettings["XMLPullTempSavePath"] + "_" + ScheduleID + "_" + DateTime.Now.ToString("ddMMyyyy") + ".zip");
                    }
                    SendFileName = ConfigurationSettings.AppSettings["XMLPullTempSavePath"] + "_" + ScheduleID + "_" + DateTime.Now.ToString("ddMMyyyy") + ".zip";
                }
                catch (Exception )
                {

                }

            }

            public void CreateSystemPerformanceNode(string OperationType, string CommandName, string Date_ddMMyyyy, string Reserve, XmlTextWriter writer)
            {
                writer.WriteStartElement(OperationType);

                writer.WriteStartElement("ContentType");
                writer.WriteString(CommandName);
                writer.WriteEndElement();
                writer.WriteStartElement("Date");
                writer.WriteString(Date_ddMMyyyy);
                writer.WriteEndElement();
                writer.WriteStartElement("Responses");
                writer.WriteEndElement();
                writer.WriteStartElement("Reserve");
                writer.WriteString(Reserve);
                writer.WriteEndElement();

                writer.WriteEndElement();
            }


            #endregion

            #region Pull File From SST


            public void GetPullFileFromSST(decimal ScheduleId, string SourceDirectoryPath, string SourceFileName, string WildCard)
            {
                Socket SST_Client = (Socket)obj_ConnStatus[0];
                JobScheduleDetails_CDMS GetScheduleInfo = new JobScheduleDetails_CDMS();
                try
                {
                    PullFileRequestXML(ScheduleId, SourceDirectoryPath, SourceFileName, WildCard);
                    GetScheduleInfo.ScheduleID = ScheduleId;
                    GetScheduleInfo.GetMultiScheduleInfo();
                    String LoopFile = SendFileName;
                    foreach (DataRow row in GetScheduleInfo.dtScheduleInfo.Rows)
                    {
                        SendFileName = "";
                        SendFileName = LoopFile;
                        {
                            try
                            {
                                String SST_NAT_IP;//SST_ORGIP,
                                decimal SSTID, SST_AgentPort;

                                JobActDet.ScheduleID = ScheduleId;
                                SSTID = decimal.Parse(row["SSTID"].ToString());
                                SST_NAT_IP = Convert.ToString(row["SST_NAT_IP"]);
                                SST_AgentPort = decimal.Parse(row["SST_AgentPort"].ToString());

                                IPAddress[] IP = Dns.GetHostAddresses(SST_NAT_IP);
                                IPEndPoint ipEnd = new IPEndPoint(IP[0], Convert.ToInt32(SST_AgentPort));

                             
                                Socket SST_Client1 = new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp);
                               
                               // Socket SST_Client1 = new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp);
                                IAsyncResult result = SST_Client1.BeginConnect(SST_NAT_IP, Convert.ToInt16(SST_AgentPort), null, null);
                                bool success = result.AsyncWaitHandle.WaitOne(1000, true);
                                if (success)
                                {
                                    // SST_Client1.Connect(ipEnd);
                                    if (SST_Client1.Connected)
                                    {
                                        string filePath = "";
                                        SendData = null;

                                        SendFileName = SendFileName.Replace("\\", "/");
                                        while (SendFileName.IndexOf("/") > -1)
                                        {
                                            filePath += SendFileName.Substring(0, SendFileName.IndexOf("/") + 1);
                                            SendFileName = SendFileName.Substring(SendFileName.IndexOf("/") + 1);
                                        }
                                        byte[] fileNameByte = Encoding.ASCII.GetBytes(SendFileName);
                                        byte[] fileData = File.ReadAllBytes(filePath + SendFileName);
                                        SendData = new byte[4 + fileNameByte.Length + fileData.Length];
                                        byte[] fileNameLen = BitConverter.GetBytes(fileNameByte.Length);

                                        fileNameLen.CopyTo(SendData, 0);
                                        fileNameByte.CopyTo(SendData, 4);
                                        fileData.CopyTo(SendData, 4 + fileNameByte.Length);

                                        try
                                        {
                                            int index = 0;
                                            int SendBytes = 0;
                                            while (index < SendData.Length)
                                            {
                                                if ((index + 8192) < SendData.Length)
                                                {
                                                    SST_Client1.Send(SendData, index, 8192, SocketFlags.None);
                                                    SendBytes = SST_Client1.SendBufferSize;
                                                }
                                                else
                                                {
                                                    SST_Client1.Send(SendData, index, SendData.Length - index, SocketFlags.None);
                                                    SendBytes = SST_Client1.SendBufferSize;
                                                }
                                                index += SendBytes;
                                            }
                                        }
                                        catch (Exception)
                                        {
                                        }
                                        // Release the socket.
                                        SST_Client1.Shutdown(SocketShutdown.Receive);
                                        SST_Client1.Dispose();

                                        JobActDet.ActivityDetailID = decimal.Parse(row["ActivityDetailID"].ToString());
                                        JobActDet.ScheduleID = ScheduleId;
                                        JobActDet.ScheduleDatetime = DateTime.Parse(row["ScheduleDatetime"].ToString());
                                        JobActDet.SSTActivityListID = decimal.Parse(row["SSTActivityListID"].ToString());
                                        JobActDet.ActivityListDetailID = decimal.Parse(row["ActivityListDetailID"].ToString());
                                        JobActDet.SSTID = decimal.Parse(row["SSTID"].ToString());
                                        JobActDet.ActualStartDateTime = DateTime.Now;
                                        JobActDet.JobResponseID = 4;  //Pending
                                        System.Web.HttpContext.Current.Session["JobResponse"] = "Pending";
                                        JobActDet.JobResponseDateTime = DateTime.Now;
                                        JobActDet.RetryCount = 0;
                                        JobActDet.Remarks = "";
                                        JobActDet.CreatedBy = decimal.Parse(row["CreatedBy"].ToString());
                                        JobActDet.CreatedDatetime = DateTime.Parse(row["CreatedDatetime"].ToString());
                                        JobActDet.ModifiedBy = decimal.Parse(row["ModifiedBy"].ToString());
                                        JobActDet.ModifiedDatetime = DateTime.Now;

                                        ActionSP.sp_Insert_JobActionDetails_CDMS(JobActDet.ActivityDetailID, JobActDet.ScheduleID, JobActDet.ActualStartDateTime, JobActDet.SSTActivityListID,
                                                                                    JobActDet.ActivityListDetailID, JobActDet.SSTID, JobActDet.ActualStartDateTime,
                                                                                    JobActDet.JobResponseID, JobActDet.JobResponseDateTime, JobActDet.RetryCount, JobActDet.Remarks,
                                                                                    JobActDet.CreatedBy, JobActDet.CreatedDatetime, JobActDet.ModifiedBy, JobActDet.ModifiedDatetime);
                                        //ActionSP = null;
                                        //DissConnectingOperation();
                                    }
                                    else
                                    {
                                        JobActDet.JobResponseID = 2;  //Pending
                                        System.Web.HttpContext.Current.Session["JobResponse"] = "Failed";

                                        JobActDet.ActivityDetailID = decimal.Parse(row["ActivityDetailID"].ToString());
                                        JobActDet.ScheduleID = ScheduleId;
                                        JobActDet.ScheduleDatetime = DateTime.Parse(row["ScheduleDatetime"].ToString());
                                        JobActDet.SSTActivityListID = decimal.Parse(row["SSTActivityListID"].ToString());
                                        JobActDet.ActivityListDetailID = decimal.Parse(row["ActivityListDetailID"].ToString());
                                        JobActDet.SSTID = decimal.Parse(row["SSTID"].ToString());
                                        JobActDet.ActualStartDateTime = DateTime.Now;
                                        //JobActDet.JobResponseID = 4;  //Pending
                                        System.Web.HttpContext.Current.Session["JobResponse"] = "Pending";
                                        JobActDet.JobResponseDateTime = DateTime.Now;
                                        JobActDet.RetryCount = 0;
                                        JobActDet.Remarks = "Not Connected";
                                        JobActDet.CreatedBy = decimal.Parse(row["CreatedBy"].ToString());
                                        JobActDet.CreatedDatetime = DateTime.Parse(row["CreatedDatetime"].ToString());
                                        JobActDet.ModifiedBy = decimal.Parse(row["ModifiedBy"].ToString());
                                        JobActDet.ModifiedDatetime = DateTime.Now;

                                        ActionSP.sp_Insert_JobActionDetails_CDMS(JobActDet.ActivityDetailID, JobActDet.ScheduleID, JobActDet.ActualStartDateTime, JobActDet.SSTActivityListID,
                                                                                    JobActDet.ActivityListDetailID, JobActDet.SSTID, JobActDet.ActualStartDateTime,
                                                                                    JobActDet.JobResponseID, JobActDet.JobResponseDateTime, JobActDet.RetryCount, JobActDet.Remarks,
                                                                                    JobActDet.CreatedBy, JobActDet.CreatedDatetime, JobActDet.ModifiedBy, JobActDet.ModifiedDatetime);
                                    
                                    }
                                }
                                else
                                {
                                    JobActDet.JobResponseID = 2;  //Pending
                                    System.Web.HttpContext.Current.Session["JobResponse"] = "Failed";

                                    JobActDet.ActivityDetailID = decimal.Parse(row["ActivityDetailID"].ToString());
                                    JobActDet.ScheduleID = ScheduleId;
                                    JobActDet.ScheduleDatetime = DateTime.Parse(row["ScheduleDatetime"].ToString());
                                    JobActDet.SSTActivityListID = decimal.Parse(row["SSTActivityListID"].ToString());
                                    JobActDet.ActivityListDetailID = decimal.Parse(row["ActivityListDetailID"].ToString());
                                    JobActDet.SSTID = decimal.Parse(row["SSTID"].ToString());
                                    JobActDet.ActualStartDateTime = DateTime.Now;
                                    //JobActDet.JobResponseID = 4;  //Pending
                                    System.Web.HttpContext.Current.Session["JobResponse"] = "Pending";
                                    JobActDet.JobResponseDateTime = DateTime.Now;
                                    JobActDet.RetryCount = 0;
                                    JobActDet.Remarks = "Not Connected";
                                    JobActDet.CreatedBy = decimal.Parse(row["CreatedBy"].ToString());
                                    JobActDet.CreatedDatetime = DateTime.Parse(row["CreatedDatetime"].ToString());
                                    JobActDet.ModifiedBy = decimal.Parse(row["ModifiedBy"].ToString());
                                    JobActDet.ModifiedDatetime = DateTime.Now;

                                    ActionSP.sp_Insert_JobActionDetails_CDMS(JobActDet.ActivityDetailID, JobActDet.ScheduleID, JobActDet.ActualStartDateTime, JobActDet.SSTActivityListID,
                                                                                JobActDet.ActivityListDetailID, JobActDet.SSTID, JobActDet.ActualStartDateTime,
                                                                                JobActDet.JobResponseID, JobActDet.JobResponseDateTime, JobActDet.RetryCount, JobActDet.Remarks,
                                                                                JobActDet.CreatedBy, JobActDet.CreatedDatetime, JobActDet.ModifiedBy, JobActDet.ModifiedDatetime);
                                    
                                }

                            }
                            catch (Exception )
                            {

                                JobActDet.ActivityDetailID = decimal.Parse(row["ActivityDetailID"].ToString());
                                JobActDet.ScheduleID = ScheduleId;
                                JobActDet.ScheduleDatetime = DateTime.Parse(row["ScheduleDatetime"].ToString());
                                JobActDet.SSTActivityListID = decimal.Parse(row["SSTActivityListID"].ToString());
                                JobActDet.ActivityListDetailID = decimal.Parse(row["ActivityListDetailID"].ToString());
                                JobActDet.SSTID = decimal.Parse(row["SSTID"].ToString());
                                JobActDet.ActualStartDateTime = DateTime.Now;
                                JobActDet.JobResponseID = 2;//Failed
                                System.Web.HttpContext.Current.Session["JobResponse"] = "Failed";
                                
                                JobActDet.JobResponseDateTime = DateTime.Now;
                                JobActDet.RetryCount = 0;
                                JobActDet.Remarks = "Not Connected";
                                JobActDet.CreatedBy = decimal.Parse(row["CreatedBy"].ToString());
                                JobActDet.CreatedDatetime = DateTime.Parse(row["CreatedDatetime"].ToString());
                                JobActDet.ModifiedBy = decimal.Parse(row["ModifiedBy"].ToString());
                                JobActDet.ModifiedDatetime = DateTime.Now;

                                ActionSP.sp_Insert_JobActionDetails_CDMS(JobActDet.ActivityDetailID, JobActDet.ScheduleID, JobActDet.ActualStartDateTime, JobActDet.SSTActivityListID,
                                                                            JobActDet.ActivityListDetailID, JobActDet.SSTID, JobActDet.ActualStartDateTime,
                                                                            JobActDet.JobResponseID, JobActDet.JobResponseDateTime, JobActDet.RetryCount, JobActDet.Remarks,
                                                                            JobActDet.CreatedBy, JobActDet.CreatedDatetime, JobActDet.ModifiedBy, JobActDet.ModifiedDatetime);
                                //ActionSP = null;

                            }
                        }
                    }
                }
                catch (Exception )
                {
                }
            }


            public void GetScreenShotFromSST(decimal ScheduleId, string SourceDirectoryPath, string SourceFileName, string WildCard)
            {
                Socket SST_Client = (Socket)obj_ConnStatus[0];
                JobScheduleDetails_CDMS GetScheduleInfo = new JobScheduleDetails_CDMS();
                try
                {
                    ScreenShotFileRequestXML(ScheduleId, SourceDirectoryPath, SourceFileName, WildCard);
                    GetScheduleInfo.ScheduleID = ScheduleId;
                    GetScheduleInfo.GetMultiScheduleInfo();
                    String LoopFile = SendFileName;
                    foreach (DataRow row in GetScheduleInfo.dtScheduleInfo.Rows)
                    {
                        SendFileName = "";
                        SendFileName = LoopFile;
                        {
                            try
                            {
                                String SST_NAT_IP;//SST_ORGIP,
                                decimal SSTID, SST_AgentPort;

                                JobActDet.ScheduleID = ScheduleId;
                                SSTID = decimal.Parse(row["SSTID"].ToString());
                                SST_NAT_IP = Convert.ToString(row["SST_NAT_IP"]);
                                SST_AgentPort = decimal.Parse(row["SST_AgentPort"].ToString());

                                IPAddress[] IP = Dns.GetHostAddresses(SST_NAT_IP);
                                IPEndPoint ipEnd = new IPEndPoint(IP[0], Convert.ToInt32(SST_AgentPort));
                                Socket SST_Client1 = new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp);
                                 IAsyncResult result = SST_Client1.BeginConnect(SST_NAT_IP,Convert.ToInt16(SST_AgentPort), null, null);
                                 bool success = result.AsyncWaitHandle.WaitOne(1000, true);
                               
                              //  SST_Client1.Blocking = false;
                                 if (success)
                                 {
                                     // SST_Client1.Connect(ipEnd);
                                     if (SST_Client1.Connected)
                                     {
                                         string filePath = "";
                                         SendData = null;

                                         SendFileName = SendFileName.Replace("\\", "/");
                                         while (SendFileName.IndexOf("/") > -1)
                                         {
                                             filePath += SendFileName.Substring(0, SendFileName.IndexOf("/") + 1);
                                             SendFileName = SendFileName.Substring(SendFileName.IndexOf("/") + 1);
                                         }
                                         byte[] fileNameByte = Encoding.ASCII.GetBytes(SendFileName);
                                         byte[] fileData = File.ReadAllBytes(filePath + SendFileName);
                                         SendData = new byte[4 + fileNameByte.Length + fileData.Length];
                                         byte[] fileNameLen = BitConverter.GetBytes(fileNameByte.Length);

                                         fileNameLen.CopyTo(SendData, 0);
                                         fileNameByte.CopyTo(SendData, 4);
                                         fileData.CopyTo(SendData, 4 + fileNameByte.Length);

                                         try
                                         {
                                             int index = 0;
                                             int SendBytes = 0;
                                             while (index < SendData.Length)
                                             {
                                                 if ((index + 8192) < SendData.Length)
                                                 {
                                                     SST_Client1.Send(SendData, index, 8192, SocketFlags.None);
                                                     SendBytes = SST_Client1.SendBufferSize;
                                                 }
                                                 else
                                                 {
                                                     SST_Client1.Send(SendData, index, SendData.Length - index, SocketFlags.None);
                                                     SendBytes = SST_Client1.SendBufferSize;
                                                 }
                                                 index += SendBytes;
                                             }
                                         }
                                         catch (Exception)
                                         {
                                         }
                                         SST_Client1.Shutdown(SocketShutdown.Receive);
                                         SST_Client1.Dispose();
                                         JobActDet.JobResponseID = 4;  //Pending
                                         System.Web.HttpContext.Current.Session["JobResponse"] = "Pending";
                                     }
                                     else
                                     {
                                         JobActDet.JobResponseID = 2;  //Pending
                                         System.Web.HttpContext.Current.Session["JobResponse"] = "Failed";
                                     }
                                 }

                                 else
                                 {
                                     JobActDet.JobResponseID = 2;  //Pending
                                     System.Web.HttpContext.Current.Session["JobResponse"] = "Failed";
                                 }
                                    // Release the socket.
                                   
                                    
                                    JobActDet.ActivityDetailID = decimal.Parse(row["ActivityDetailID"].ToString());
                                    JobActDet.ScheduleID = ScheduleId;
                                    JobActDet.ScheduleDatetime = DateTime.Parse(row["ScheduleDatetime"].ToString());
                                    JobActDet.SSTActivityListID = decimal.Parse(row["SSTActivityListID"].ToString());
                                    JobActDet.ActivityListDetailID = decimal.Parse(row["ActivityListDetailID"].ToString());
                                    JobActDet.SSTID = decimal.Parse(row["SSTID"].ToString());
                                    JobActDet.ActualStartDateTime = DateTime.Now;
                                    
                                    JobActDet.JobResponseDateTime = DateTime.Now;
                                    JobActDet.RetryCount = 0;
                                    JobActDet.Remarks = "Not Connected";
                                    JobActDet.CreatedBy = decimal.Parse(row["CreatedBy"].ToString());
                                    JobActDet.CreatedDatetime = DateTime.Parse(row["CreatedDatetime"].ToString());
                                    JobActDet.ModifiedBy = decimal.Parse(row["ModifiedBy"].ToString());
                                    JobActDet.ModifiedDatetime = DateTime.Now;

                                    ActionSP.sp_Insert_JobActionDetails_CDMS(JobActDet.ActivityDetailID, JobActDet.ScheduleID, JobActDet.ActualStartDateTime, JobActDet.SSTActivityListID,
                                                                                JobActDet.ActivityListDetailID, JobActDet.SSTID, JobActDet.ActualStartDateTime,
                                                                                JobActDet.JobResponseID, JobActDet.JobResponseDateTime, JobActDet.RetryCount, JobActDet.Remarks,
                                                                                JobActDet.CreatedBy, JobActDet.CreatedDatetime, JobActDet.ModifiedBy, JobActDet.ModifiedDatetime);
                                    //ActionSP = null;
                                    //DissConnectingOperation();
                                
                            }
                            catch (Exception)
                            {

                                JobActDet.ActivityDetailID = decimal.Parse(row["ActivityDetailID"].ToString());
                                JobActDet.ScheduleID = ScheduleId;
                                JobActDet.ScheduleDatetime = DateTime.Parse(row["ScheduleDatetime"].ToString());
                                JobActDet.SSTActivityListID = decimal.Parse(row["SSTActivityListID"].ToString());
                                JobActDet.ActivityListDetailID = decimal.Parse(row["ActivityListDetailID"].ToString());
                                JobActDet.SSTID = decimal.Parse(row["SSTID"].ToString());
                                JobActDet.ActualStartDateTime = DateTime.Now;
                                JobActDet.JobResponseID = 2;//Failed
                                System.Web.HttpContext.Current.Session["JobResponse"] = "Failed";

                                JobActDet.JobResponseDateTime = DateTime.Now;
                                JobActDet.RetryCount = 0;
                                JobActDet.Remarks = "Not Connected";
                                JobActDet.CreatedBy = decimal.Parse(row["CreatedBy"].ToString());
                                JobActDet.CreatedDatetime = DateTime.Parse(row["CreatedDatetime"].ToString());
                                JobActDet.ModifiedBy = decimal.Parse(row["ModifiedBy"].ToString());
                                JobActDet.ModifiedDatetime = DateTime.Now;

                                ActionSP.sp_Insert_JobActionDetails_CDMS(JobActDet.ActivityDetailID, JobActDet.ScheduleID, JobActDet.ActualStartDateTime, JobActDet.SSTActivityListID,
                                                                            JobActDet.ActivityListDetailID, JobActDet.SSTID, JobActDet.ActualStartDateTime,
                                                                            JobActDet.JobResponseID, JobActDet.JobResponseDateTime, JobActDet.RetryCount, JobActDet.Remarks,
                                                                            JobActDet.CreatedBy, JobActDet.CreatedDatetime, JobActDet.ModifiedBy, JobActDet.ModifiedDatetime);
                                //ActionSP = null;

                            }
                        }
                    }
                }
                catch (Exception)
                {
                }
            }


            public void  PullFileRequestXML(decimal ScheduleId, string SourceDirectoryPath, string SourceFileName, string WildCard)
            {
                try
                {
                    String Activity, SST_ORGIP, SST_NAT_IP, CommandName;
                    decimal ScheduleID, SSTID, SST_AgentPort;

                    clsPullRequest GetReuestInfo = new clsPullRequest();
                    GetReuestInfo.ScheduleID = ScheduleId;
                    GetReuestInfo.fillup();

                    ScheduleID = GetReuestInfo.ScheduleID;
                    Activity = GetReuestInfo.Activity;
                    CommandName = GetReuestInfo.JobType;
                    SSTID = GetReuestInfo.SSTID;
                    SST_ORGIP = GetReuestInfo.SST_ORGIP;
                    SST_NAT_IP = GetReuestInfo.SST_NAT_IP;
                    SST_AgentPort = GetReuestInfo.SST_AgentPort;

                    String XMLFilePath = ConfigurationSettings.AppSettings["XMLPullPath"];

                    if (!Directory.Exists(ConfigurationSettings.AppSettings["XMLPullPath"]))
                    {
                        Directory.CreateDirectory(ConfigurationSettings.AppSettings["XMLPullPath"]);
                    }

                    SendFileName = XMLFilePath + @"\Pull" + "_" + ScheduleID + "_" + DateTime.Now.ToString("ddMMyyyy") + ".xml";

                    XmlDocument xmldoc = new XmlDocument();

                    XmlTextWriter writer = new XmlTextWriter(SendFileName, System.Text.Encoding.UTF8);
                    writer.WriteStartDocument(true);
                    writer.Formatting = Formatting.Indented;
                    writer.Indentation = 3;
                    writer.WriteStartElement("RMMS");

                    createPullFileNode(Activity, CommandName, SourceDirectoryPath,  SourceFileName,  WildCard, DateTime.Now.ToString("ddMMyyyy") + "-" + DateTime.Now.ToString("ddMMyyyy"), "", writer);

                    writer.WriteEndElement();
                    writer.WriteEndDocument();
                    writer.Close();

                    using (ZipFile zip = new ZipFile())
                    {
                        if (!Directory.Exists(ConfigurationSettings.AppSettings["XMLPullTempSavePath"]))
                            Directory.CreateDirectory(ConfigurationSettings.AppSettings["XMLPullTempSavePath"]);
                        EmptyFolder(new DirectoryInfo(ConfigurationSettings.AppSettings["XMLPullTempSavePath"]));
                        string name = Path.GetFileName(SendFileName);
                        string dest = Path.Combine(ConfigurationSettings.AppSettings["XMLPullTempSavePath"], name);
                        System.IO.File.Copy(SendFileName, dest, true);

                        string ZipFileName = ConfigurationSettings.AppSettings["XMLPullTempSavePath"];
                        ZipFileName = ZipFileName.Replace("\\", "/");
                        while (ZipFileName.IndexOf("/") > 0)
                            ZipFileName = ZipFileName.Substring(ZipFileName.IndexOf("/") + 1);

                        Passwd pwd = new Passwd();
                        zip.Password = pwd.GetPassword(ZipFileName + "_" + ScheduleID + "_" + DateTime.Now.ToString("ddMMyyyy"));
                        zip.AddDirectory(ConfigurationSettings.AppSettings["XMLPullTempSavePath"]);
                        zip.Save(ConfigurationSettings.AppSettings["XMLPullTempSavePath"] + "_" + ScheduleID + "_" + DateTime.Now.ToString("ddMMyyyy") + ".zip");
                    }
                    SendFileName = ConfigurationSettings.AppSettings["XMLPullTempSavePath"] + "_" + ScheduleID + "_" + DateTime.Now.ToString("ddMMyyyy") + ".zip";
                }
                catch (Exception )
                { }
            }

            public void ScreenShotFileRequestXML(decimal ScheduleId, string SourceDirectoryPath, string SourceFileName, string WildCard)
            {
                try
                {
                    String Activity, SST_ORGIP, SST_NAT_IP, CommandName;
                    decimal ScheduleID, SSTID, SST_AgentPort;

                    clsPullRequest GetReuestInfo = new clsPullRequest();
                    GetReuestInfo.ScheduleID = ScheduleId;
                    GetReuestInfo.fillup();

                    ScheduleID = GetReuestInfo.ScheduleID;
                    Activity = GetReuestInfo.Activity;
                    CommandName = GetReuestInfo.JobType;
                    SSTID = GetReuestInfo.SSTID;
                    SST_ORGIP = GetReuestInfo.SST_ORGIP;
                    SST_NAT_IP = GetReuestInfo.SST_NAT_IP;
                    SST_AgentPort = GetReuestInfo.SST_AgentPort;

                    String XMLFilePath = System.Configuration.ConfigurationManager.AppSettings["XMLPullPath"];

                    if (!Directory.Exists(System.Configuration.ConfigurationManager.AppSettings["XMLPullPath"]))
                    {
                        Directory.CreateDirectory(System.Configuration.ConfigurationManager.AppSettings["XMLPullPath"]);
                    }

                    SendFileName = XMLFilePath + @"\ScreenShot" + "_" + ScheduleID + "_" + DateTime.Now.ToString("ddMMyyyy") + ".xml";

                    XmlDocument xmldoc = new XmlDocument();

                    XmlTextWriter writer = new XmlTextWriter(SendFileName, System.Text.Encoding.UTF8);
                    writer.WriteStartDocument(true);
                    writer.Formatting = Formatting.Indented;
                    writer.Indentation = 3;
                    writer.WriteStartElement("RMMS");

                    createPullFileNode(Activity, CommandName, SourceDirectoryPath, SourceFileName, WildCard, DateTime.Now.ToString("ddMMyyyy") + "-" + DateTime.Now.ToString("ddMMyyyy"), "", writer);

                    writer.WriteEndElement();
                    writer.WriteEndDocument();
                    writer.Close();

                    using (ZipFile zip = new ZipFile())
                    {
                        if (!Directory.Exists(System.Configuration.ConfigurationManager.AppSettings["XMLPullTempSavePath"]))
                            Directory.CreateDirectory(System.Configuration.ConfigurationManager.AppSettings["XMLPullTempSavePath"]);
                        EmptyFolder(new DirectoryInfo(System.Configuration.ConfigurationManager.AppSettings["XMLPullTempSavePath"]));
                        string name = Path.GetFileName(SendFileName);
                        string dest = Path.Combine(System.Configuration.ConfigurationManager.AppSettings["XMLPullTempSavePath"], name);
                        System.IO.File.Copy(SendFileName, dest, true);

                        string ZipFileName = System.Configuration.ConfigurationManager.AppSettings["XMLPullTempSavePath"];
                        ZipFileName = ZipFileName.Replace("\\", "/");
                        while (ZipFileName.IndexOf("/") > 0)
                            ZipFileName = ZipFileName.Substring(ZipFileName.IndexOf("/") + 1);

                        Passwd pwd = new Passwd();
                        zip.Password = pwd.GetPassword(ZipFileName + "_" + ScheduleID + "_" + DateTime.Now.ToString("ddMMyyyy"));
                        zip.AddDirectory(System.Configuration.ConfigurationManager.AppSettings["XMLPullTempSavePath"]);

                        zip.Save(System.Configuration.ConfigurationManager.AppSettings["XMLPullTempSavePath"] + "_" + ScheduleID + "_" + DateTime.Now.ToString("ddMMyyyy") + ".zip");
                    }
                    SendFileName = System.Configuration.ConfigurationManager.AppSettings["XMLPullTempSavePath"] + "_" + ScheduleID + "_" + DateTime.Now.ToString("ddMMyyyy") + ".zip";
                }
                catch (Exception)
                { }
            }

            public void createPullFileNode(string OperationType, string ContentType, string SourceDirectoryPath, string  SourceFileName, string WildCard, string Date_ddMMyyyy, string Reserve, XmlTextWriter writer)
            {
                writer.WriteStartElement(OperationType);
                writer.WriteStartElement("ContentType");
                writer.WriteString(ContentType);
                writer.WriteEndElement();
                writer.WriteStartElement("SourceDirectory");
                writer.WriteString(SourceDirectoryPath);
                writer.WriteEndElement();
                writer.WriteStartElement("SourceFile");
                writer.WriteString(SourceFileName);
                writer.WriteEndElement();
                writer.WriteStartElement("SourceWildCard");
                writer.WriteString(WildCard);
                writer.WriteEndElement();
                writer.WriteStartElement("Date");
                writer.WriteString(Date_ddMMyyyy);
                writer.WriteEndElement();
                writer.WriteStartElement("Responses");
                writer.WriteEndElement();
                writer.WriteStartElement("Reserve");
                writer.WriteString(Reserve);
                writer.WriteEndElement();
                writer.WriteEndElement();
            }

            #endregion

            #region Run Self Diagonstic on SST

            public void RunDiagnosticOnSST(decimal ScheduleId)
            {
                Socket SST_Client = (Socket)obj_ConnStatus[0];
                JobScheduleDetails_CDMS GetScheduleInfo = new JobScheduleDetails_CDMS();
                try
                {
                    RequestXMLForSelfDiagnostic(ScheduleId);

                    GetScheduleInfo.ScheduleID = ScheduleId;
                    GetScheduleInfo.GetMultiScheduleInfo();
                    String LoopFile = SendFileName;
                    foreach (DataRow row in GetScheduleInfo.dtScheduleInfo.Rows)
                    {
                        SendFileName = "";
                        SendFileName = LoopFile;
                        {
                            try
                            {
                                String SST_NAT_IP;//SST_ORGIP,
                                decimal SSTID, SST_AgentPort;

                                JobActDet.ScheduleID = ScheduleId;
                                SSTID = decimal.Parse(row["SSTID"].ToString());
                                SST_NAT_IP = Convert.ToString(row["SST_NAT_IP"]);
                                SST_AgentPort = decimal.Parse(row["SST_AgentPort"].ToString());

                                IPAddress[] IP = Dns.GetHostAddresses(SST_NAT_IP);
                                IPEndPoint ipEnd = new IPEndPoint(IP[0], Convert.ToInt32(SST_AgentPort));
                                Socket SST_Client1 = new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp);

                                SST_Client1.Connect(ipEnd);
                                if (SST_Client1.Connected)
                                {
                                    string filePath = "";
                                    SendData = null;

                                    SendFileName = SendFileName.Replace("\\", "/");
                                    while (SendFileName.IndexOf("/") > -1)
                                    {
                                        filePath += SendFileName.Substring(0, SendFileName.IndexOf("/") + 1);
                                        SendFileName = SendFileName.Substring(SendFileName.IndexOf("/") + 1);
                                    }
                                    byte[] fileNameByte = Encoding.ASCII.GetBytes(SendFileName);
                                    byte[] fileData = File.ReadAllBytes(filePath + SendFileName);
                                    SendData = new byte[4 + fileNameByte.Length + fileData.Length];
                                    byte[] fileNameLen = BitConverter.GetBytes(fileNameByte.Length);

                                    fileNameLen.CopyTo(SendData, 0);
                                    fileNameByte.CopyTo(SendData, 4);
                                    fileData.CopyTo(SendData, 4 + fileNameByte.Length);

                                    try
                                    {
                                        int index = 0;
                                        int SendBytes = 0;
                                        while (index < SendData.Length)
                                        {
                                            if ((index + 8192) < SendData.Length)
                                            {
                                                SST_Client1.Send(SendData, index, 8192, SocketFlags.None);
                                                SendBytes = SST_Client1.SendBufferSize;
                                            }
                                            else
                                            {
                                                SST_Client1.Send(SendData, index, SendData.Length - index, SocketFlags.None);
                                                SendBytes = SST_Client1.SendBufferSize;
                                            }
                                            index += SendBytes;
                                        }
                                    }
                                    catch (Exception )
                                    {
                                    }
                                    SST_Client1.Shutdown(SocketShutdown.Receive);
                                    SST_Client1.Disconnect(false);

                                    JobActDet.ActivityDetailID = decimal.Parse(row["ActivityDetailID"].ToString());
                                    JobActDet.ScheduleID = ScheduleId;
                                    JobActDet.ScheduleDatetime = DateTime.Parse(row["ScheduleDatetime"].ToString());
                                    JobActDet.SSTActivityListID = decimal.Parse(row["SSTActivityListID"].ToString());
                                    JobActDet.ActivityListDetailID = decimal.Parse(row["ActivityListDetailID"].ToString());
                                    JobActDet.SSTID = decimal.Parse(row["SSTID"].ToString());
                                    JobActDet.ActualStartDateTime = DateTime.Now;

                                  
                                    JobActDet.JobResponseID = 4;  //Pending
                                    System.Web.HttpContext.Current.Session["JobResponse"] = "Pending";
                                 
                                    
                                    JobActDet.JobResponseDateTime = DateTime.Now;
                                    JobActDet.RetryCount = 0;
                                    JobActDet.Remarks = "";
                                    JobActDet.CreatedBy = decimal.Parse(row["CreatedBy"].ToString());
                                    JobActDet.CreatedDatetime = DateTime.Parse(row["CreatedDatetime"].ToString());
                                    JobActDet.ModifiedBy = decimal.Parse(row["ModifiedBy"].ToString());
                                    JobActDet.ModifiedDatetime = DateTime.Now;

                                    ActionSP.sp_Insert_JobActionDetails_CDMS(JobActDet.ActivityDetailID, JobActDet.ScheduleID, JobActDet.ActualStartDateTime, JobActDet.SSTActivityListID,
                                                                                JobActDet.ActivityListDetailID, JobActDet.SSTID, JobActDet.ActualStartDateTime,
                                                                                JobActDet.JobResponseID, JobActDet.JobResponseDateTime, JobActDet.RetryCount, JobActDet.Remarks,
                                                                                JobActDet.CreatedBy, JobActDet.CreatedDatetime, JobActDet.ModifiedBy, JobActDet.ModifiedDatetime);
                                }


                            }
                            catch (Exception )
                            {

                                JobActDet.ActivityDetailID = decimal.Parse(row["ActivityDetailID"].ToString());
                                JobActDet.ScheduleID = ScheduleId;
                                JobActDet.ScheduleDatetime = DateTime.Parse(row["ScheduleDatetime"].ToString());
                                JobActDet.SSTActivityListID = decimal.Parse(row["SSTActivityListID"].ToString());
                                JobActDet.ActivityListDetailID = decimal.Parse(row["ActivityListDetailID"].ToString());
                                JobActDet.SSTID = decimal.Parse(row["SSTID"].ToString());
                                JobActDet.ActualStartDateTime = DateTime.Now;
                                JobActDet.JobResponseID = 2;//Failed
                                System.Web.HttpContext.Current.Session["JobResponse"] = "Failed";
                                JobActDet.JobResponseDateTime = DateTime.Now;
                                JobActDet.RetryCount = 0;
                                JobActDet.Remarks = "Not Connected";
                                JobActDet.CreatedBy = decimal.Parse(row["CreatedBy"].ToString());
                                JobActDet.CreatedDatetime = DateTime.Parse(row["CreatedDatetime"].ToString());
                                JobActDet.ModifiedBy = decimal.Parse(row["ModifiedBy"].ToString());
                                JobActDet.ModifiedDatetime = DateTime.Now;

                                ActionSP.sp_Insert_JobActionDetails_CDMS(JobActDet.ActivityDetailID, JobActDet.ScheduleID, JobActDet.ActualStartDateTime, JobActDet.SSTActivityListID,
                                                                            JobActDet.ActivityListDetailID, JobActDet.SSTID, JobActDet.ActualStartDateTime,
                                                                            JobActDet.JobResponseID, JobActDet.JobResponseDateTime, JobActDet.RetryCount, JobActDet.Remarks,
                                                                            JobActDet.CreatedBy, JobActDet.CreatedDatetime, JobActDet.ModifiedBy, JobActDet.ModifiedDatetime);

                            }
                        }
                    }
                }
                catch (Exception )
                {
                }
            }

            private void RequestXMLForSelfDiagnostic(decimal ScheduleId)
            {
                try
                {
                    String Activity, SST_ORGIP, SST_NAT_IP, CommandName;
                    decimal ScheduleID, SSTID, SST_AgentPort;

                    clsPullRequest GetReuestInfo = new clsPullRequest();
                    GetReuestInfo.ScheduleID = ScheduleId;
                    GetReuestInfo.fillup();

                    ScheduleID = GetReuestInfo.ScheduleID;
                    Activity = GetReuestInfo.Activity;
                    CommandName = GetReuestInfo.JobType;
                    SSTID = GetReuestInfo.SSTID;
                    SST_ORGIP = GetReuestInfo.SST_ORGIP;
                    SST_NAT_IP = GetReuestInfo.SST_NAT_IP;
                    SST_AgentPort = GetReuestInfo.SST_AgentPort;

                    String XMLFilePath = ConfigurationSettings.AppSettings["XMLPullPath"];

                    if (!Directory.Exists(ConfigurationSettings.AppSettings["XMLPullPath"]))
                        Directory.CreateDirectory(ConfigurationSettings.AppSettings["XMLPullPath"]);

                    SendFileName = XMLFilePath + @"\Command" + "_" + ScheduleID + "_" + DateTime.Now.ToString("ddMMyyyy") + ".xml";

                    XmlDocument xmldoc = new XmlDocument();

                    XmlTextWriter writer = new XmlTextWriter(SendFileName, System.Text.Encoding.UTF8);
                    writer.WriteStartDocument(true);
                    writer.Formatting = Formatting.Indented;
                    writer.Indentation = 3;
                    writer.WriteStartElement("RMMS");

                    CreateSelfDiagnosticNode(Activity, CommandName, DateTime.Now.ToString("ddMMyyyy") + "-" + DateTime.Now.ToString("ddMMyyyy"), "", writer);

                   
                    writer.WriteEndElement();
                    writer.WriteEndDocument();
                    writer.Close();

                    using (ZipFile zip = new ZipFile())
                    {
                        if (!Directory.Exists(ConfigurationSettings.AppSettings["XMLPullTempSavePath"]))
                            Directory.CreateDirectory(ConfigurationSettings.AppSettings["XMLPullTempSavePath"]);
                        EmptyFolder(new DirectoryInfo(ConfigurationSettings.AppSettings["XMLPullTempSavePath"]));

                        string name = Path.GetFileName(SendFileName);
                        string dest = Path.Combine(ConfigurationSettings.AppSettings["XMLPullTempSavePath"], name);

                        System.IO.File.Copy(SendFileName, dest, true);

                        string ZipFileName = ConfigurationSettings.AppSettings["XMLPullTempSavePath"];
                        ZipFileName = ZipFileName.Replace("\\", "/");
                        while (ZipFileName.IndexOf("/") > 0)
                            ZipFileName = ZipFileName.Substring(ZipFileName.IndexOf("/") + 1);

                        Passwd pwd = new Passwd();
                        zip.Password = pwd.GetPassword(ZipFileName + "_" + ScheduleID + "_" + DateTime.Now.ToString("ddMMyyyy"));
                        zip.AddDirectory(ConfigurationSettings.AppSettings["XMLPullTempSavePath"]);
                        zip.Save(ConfigurationSettings.AppSettings["XMLPullTempSavePath"] + "_" + ScheduleID + "_" + DateTime.Now.ToString("ddMMyyyy") + ".zip");
                    }
                    SendFileName = ConfigurationSettings.AppSettings["XMLPullTempSavePath"] + "_" + ScheduleID + "_" + DateTime.Now.ToString("ddMMyyyy") + ".zip";
                }
                catch (Exception )
                {}
            }

            private void CreateSelfDiagnosticNode(string OperationType, string CommandName, string Date_ddMMyyyy, string Reserve, XmlTextWriter writer)
            {
                writer.WriteStartElement(OperationType);

                writer.WriteStartElement("ContentType");
                writer.WriteString(CommandName);
                writer.WriteEndElement();
                writer.WriteStartElement("EXEPATH");
                writer.WriteString(ConfigurationSettings.AppSettings["RUN_SELF_DIAHONSTIC_PATH"]);
                writer.WriteEndElement();
                writer.WriteStartElement("Date");
                writer.WriteString(Date_ddMMyyyy);
                writer.WriteEndElement();
                writer.WriteStartElement("Responses");
                writer.WriteEndElement();
                writer.WriteStartElement("Reserve");
                writer.WriteString(Reserve);
                writer.WriteEndElement();

                writer.WriteEndElement();
            }


            #endregion
        
        #region TestDissConnection

        public void DissConnectingOperation(string SST_IP, int SST_Port)
        {
            Socket SST_Client = (Socket)obj_ConnStatus[0];
            try
            {
                if (SST_Client.Connected)
                {
                    SST_Client.Shutdown(SocketShutdown.Receive);
                    SST_Client.Disconnect(false);
                    StreamWriter log;
                    String dateStr = DateTime.Now.ToString("yyyy-MM-dd");   //DateTime.Now.ToString("yyyy-MM-dd HH_mm_ss");
                    if (!File.Exists(@"D:\SSTLog_" + dateStr + ".txt"))
                        log = new StreamWriter(@"D:\SSTLog_" + dateStr + ".txt");
                    else
                        log = File.AppendText(@"D:\SSTLog_" + dateStr + ".txt");
                    log.WriteLine(DateTime.Now.ToString("yyyy-MM-dd HH_mm") + ": IP : " + SST_IP + ":" + SST_Port + "  Disconnected..");
                    log.Close();
                }
                else
                {
                    StreamWriter log;
                    String dateStr = DateTime.Now.ToString("yyyy-MM-dd");   //DateTime.Now.ToString("yyyy-MM-dd HH_mm_ss");
                    if (!File.Exists(@"D:\SSTLog_" + dateStr + ".txt"))
                        log = new StreamWriter(@"D:\SSTLog_" + dateStr + ".txt");
                    else
                        log = File.AppendText(@"D:\SSTLog_" + dateStr + ".txt");
                    log.WriteLine(DateTime.Now.ToString("yyyy-MM-dd HH_mm") + ": IP : " + SST_IP + ":" + SST_Port + "  already Dissconnected..");
                    log.Close();
                }

            }
            catch (Exception ex)
            {
                StreamWriter log;
                String dateStr = DateTime.Now.ToString("yyyy-MM-dd");
                if (!File.Exists(@"D:\SSTLog_" + dateStr + ".txt"))
                    log = new StreamWriter(@"D:\SSTLog_" + dateStr + ".txt");
                else
                    log = File.AppendText(@"D:\SSTLog_" + dateStr + ".txt");
                log.WriteLine(DateTime.Now.ToString("yyyy-MM-dd HH_mm") + ": IP : " + SST_IP + ":" + SST_Port + "  Client not connected.." + ex.Message);
                log.Close();
            }
        }

        #endregion


        public bool IsZipFileExistOnServer(string ZipFilePath)
        {
            if (File.Exists(ZipFilePath))
                return true;
            else
                return false;
        }

    }

    [DataContract]
    public class DirectoryStructure
    {

        public static void CreateDirectory(string SrcPath)
        {
            try
            {
                string directory = Path.GetDirectoryName(SrcPath);
                CreateDirectory(new DirectoryInfo(directory));
            }
            catch (Exception)
            { }
        }


        private static void CreateDirectory(DirectoryInfo directory)
        {
            try
            {
                if (!directory.Parent.Exists)
                    CreateDirectory(directory.Parent);
                directory.Create();
            }
            catch (Exception)
            { }
        }

    }
     
    public class CDMSScheduler
    {

        public void SchedulerMain()
        {
            // TODO: Add code here to start your service.
          
            JobScheduleMaster_CDMS GetScheduleDate = new JobScheduleMaster_CDMS();
            foreach (DataRow dr in GetScheduleDate.dtScheduleDateInfo.Rows)
            {

                DateTime dtDob = (DateTime)dr["dob"];
                DateTime now = DateTime.Now;
                string dow = now.DayOfWeek.ToString().ToLower();
                if (dow == "monday")
                {
                    DateTime daybefore = now.AddDays(-1);
                    if ((GetScheduleDate.ScheduleDatetime.Day == daybefore.Day) && (GetScheduleDate.ScheduleDatetime.Month == daybefore.Month))
                    {
                      //  sendmail(dr);
                    }
                    if ((GetScheduleDate.ScheduleDatetime.Day == now.Day) && (GetScheduleDate.ScheduleDatetime.Month == now.Month))
                    {
                       // sendmail(dr);
                    }
                }
                else
                {
                    if ((dtDob.Day == now.Day) && (dtDob.Month == now.Month))
                    {
                        //sendmail(dr);
                    }
                }
            }
        }

        //public bool sendmail(DataRow dr1)
        //{
        //    String mailtxt = "";
        //    MailMessage mm = new MailMessage();
        //    mm.BodyFormat = MailFormat.Html;
        //    mm.To = dr1["emp_email"].ToString();
        //    mm.From = "abc@abc.com";
        //    mm.Subject = "Happy Scheduler";
        //    mailtxt = "<font face='verdana' color='#FF9900'><b>" + "Hi " + dr1["emp_name"].ToString() + "," + "</b></font><br><br>";
        //    mailtxt = mailtxt + "<font face='verdana' color='#FF0000'><b>" + "Wishing you a very HAPPY BIRTHDAY........and many more." + "</b></font><br><br>";
        //    mailtxt = mailtxt + "<font face='verdana' color='#008080'><b>" + "May today be filled with sunshine and smile, laughter and love." + "</b></font><br><br>";
        //    mailtxt = mailtxt + "<font face='verdana' color='#0000FF'><b>Cheers!" + "<br><br>";
        //    mm.Body = mailtxt;
        //    SmtpMail.SmtpServer = "localhost";
        //    SmtpMail.Send(mm);
        //    return (true);
        //}
    }


}

