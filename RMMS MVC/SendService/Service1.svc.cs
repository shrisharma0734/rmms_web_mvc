﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;
using System.IO;
using System.Threading;
using System.Threading.Tasks;
//using SendService;

namespace RMMS_MVC.SendService
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the class name "Service1" in code, svc and config file together.
     [ServiceBehavior(ConcurrencyMode = ConcurrencyMode.Multiple)]
    public class Service1 : IService1
    {
        CDMSServer obj_SendOperation = new CDMSServer();

        public void TestConnectSSTs(decimal ScheduleId)
        {
            obj_SendOperation.TestConnectingOperation(ScheduleId);
            // Task.Factory.StartNew(() => obj_SendOperation.TestConnectingOperation(), TaskCreationOptions.LongRunning );
        }
        public void PullFile(decimal ScheduleId)
        {

            obj_SendOperation.MultiSSTPullOperation(ScheduleId);
            //obj_SendOperation.TestPullOperation(ScheduleId);
            //obj_SendOperation.TestPullRequestXML(ScheduleId);
            //Task.Factory.StartNew(() => obj_SendOperation.TestPullRequestXML(ScheduleId), TaskCreationOptions.LongRunning);
            ////ThreadPool.QueueUserWorkItem(new WaitCallback(used => obj_SendOperation.ConnectingOperation(SST_IP, SST_Port)));
        }


        public void RestartCommand(decimal ScheduleId)
        {
            obj_SendOperation.MultiSSTRestartOperation(ScheduleId);
        }
        public void RestartAppn(decimal ScheduleId, string SourceDirectoryPath, string FileName, string WildCard)
        {
            obj_SendOperation.MultiSSTRestartOperationAppn(ScheduleId, SourceDirectoryPath, FileName, WildCard);
        }


        public void GetScreenShotFromSST(decimal ScheduleId, string SourceDirectoryPath, string FileName, string WildCard)
        {
            obj_SendOperation.GetScreenShotFromSST(ScheduleId, SourceDirectoryPath, FileName, WildCard);
        }

        public void RestartService(decimal ScheduleId)
        {
            obj_SendOperation.RestartService(ScheduleId);
        }

        public void GetSystemPerformanceOfSST(decimal ScheduleId)
        {
            obj_SendOperation.GetSystemPerformanceOfSST(ScheduleId);
        }

        public void GetPullFileFromSST(decimal ScheduleId, string SourceDirectoryPath, string FileName, string WildCard)
        {
            obj_SendOperation.GetPullFileFromSST(ScheduleId, SourceDirectoryPath, FileName, WildCard);
        }





        public void PushFileOnSST(decimal ScheduleId, string SourceDirectoryPath, string SourceDirOnAgent)
        {
            ClsDetails.writeStringTotransactionLog("Inside PushFileOnSST");
            PushFile ObjpushFile = new PushFile();
            ObjpushFile.MultiSSTPushOperation(ScheduleId, SourceDirectoryPath, SourceDirOnAgent);
            ClsDetails.writeStringTotransactionLog("Completed MultiSSTPushOperation in PushFileonSST");
            ObjpushFile = null;
        }

        public void CreatePushRequestXml(string SourceDir, string SourceDirOnAgent)
        {
            PushFile ObjpushFile = new PushFile();
            ObjpushFile.CreatePushRequestXml(SourceDir, SourceDirOnAgent);
            ObjpushFile = null;
        }
        public void SelfDiagnostic(decimal ScheduleId)
        {
            obj_SendOperation.RunDiagnosticOnSST(ScheduleId);
        }

        public bool IsZipFileExistOnServer(string ZipFilePath)
        {
            return obj_SendOperation.IsZipFileExistOnServer(ZipFilePath);
        }


    }





 }
