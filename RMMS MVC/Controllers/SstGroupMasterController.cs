﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using RMMS_MVC.Models;
using RMMS_MVC.DAL.Sql;
using RMMS_MVC.DAL;

namespace RMMS_MVC.Controllers
{
    public class SstGroupMasterController : Controller
    {
        SstGroupMasterDAO SstDB = new SstGroupMasterDAO();
        // GET: /SstGroupMaster/

        public ActionResult Index()
        {
            return View();
        }

        //
        // GET: /SstGroupMaster/Details/5
        public JsonResult List()
        {
            return Json(SstDB.ListAll(), JsonRequestBehavior.AllowGet);
        }
        public JsonResult Add(SstGroupMaster Sst)
        {
            return Json(SstDB.Add(Sst), JsonRequestBehavior.AllowGet);
        }




         public JsonResult GetbyID(int ID)
        {
            var Sst = SstDB.ListAll().Find(x => x.SSTGroupID.Equals(ID));
            return Json(Sst, JsonRequestBehavior.AllowGet);
        }

          public JsonResult Update(SstGroupMaster Sst)
        {
            return Json(SstDB.Update(Sst), JsonRequestBehavior.AllowGet);
        }
        public JsonResult Delete(int ID)
        {
            return Json(SstDB.Delete(ID), JsonRequestBehavior.AllowGet);
        }  


    
        
    }
}
