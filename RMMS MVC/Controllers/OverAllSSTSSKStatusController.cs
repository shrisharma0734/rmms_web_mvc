﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;
using RMMS_MVC.Models;
using System.Web;
using System.Web.Mvc;
using RMMS_MVC.DAL.Sql;
using System.Web.UI.WebControls;
using RMMS_MVC;
using RMMS_MVC.SendService;
using RMMS_MVC.MVCActionStoreProcedure;
using System.Web.UI;
using System.Collections;
using System.IO;
using Ionic.Zip;
using RMMS_MVC.BLL;


namespace RMMS_MVC.Controllers
{
    public class OverAllSSTSSKStatusController : Controller
    {
        BLogic ObjDB = new BLogic();
        OverAllSSTSSKDAO OverAllSSTSSKStatusDB = new OverAllSSTSSKDAO();
        OpenTroubleTicketDAO OpenTroubleTicketDB = new OpenTroubleTicketDAO();
        private static DateTime datetime;
        public string str;

        public ActionResult Index()
        {
            return View();
        }
        public ActionResult OverAllSSTSSKStatusGrid()
        {
            List<OverAllSSTSSK> lmd = new List<OverAllSSTSSK>();

            DataSet ds = new DataSet();

            SqlConnection con = new SqlConnection();

            ds = OverAllSSTSSKStatusDB.OverAllMAchineDetails();

            foreach (DataRow dr in ds.Tables[0].Rows)
            {

                lmd.Add(new OverAllSSTSSK
                {
                    SST_Name = dr["SST_Name"].ToString(),
                    SSTID = dr["SSTID"].ToString(),
                    SST_TerminalID = dr["SST_TerminalID"].ToString(),
                    BranchName = dr["BranchName"].ToString(),
                    BranchCode = dr["BranchCode"].ToString(),
                    RegionName = dr["RegionName"].ToString(),
                    RegionID = dr["RegionID"].ToString(),
                    SST_ServiceArea = dr["SST_ServiceArea"].ToString(),
                    SST_TypeID = dr["SST_TypeID"].ToString(),
                    SST_TYPE = dr["SST_TYPE"].ToString(),
                    FirstFeed_Time = dr["FirstFeed_Time"].ToString(),
                    LastFeed_Time = dr["LastFeed_Time"].ToString(),
                    ProblemID = dr["ProblemID"].ToString(),
                    Time_Elapsed_in_Minute = dr["Time_Elapsed_in_Minute"].ToString(),
                    TroubleTicketID = dr["TroubleTicketID"].ToString(),
                    IconText = dr["IconText"].ToString(),
                    Color = dr["Color"].ToString(),
                    Severity = dr["Severity"].ToString(),
                    ProblemName = dr["ProblemName"].ToString(),
                    MachineStatus = dr["MachineStatus"].ToString(),
                    BankShotName = dr["BankShotName"].ToString(),
                    BankID = dr["BankID"].ToString(),
                    CityName = dr["CityName"].ToString(),

                });

            }

            return View(lmd);
        }
        [HttpGet]
        public ActionResult SSTID(string SSTID)
        {
            string SourceDirectoryPath = string.Empty;
            string SourceFilePath = string.Empty;
            string SourceWildCardPath = string.Empty;
            string StrScheduleIDList = string.Empty;
            string ScheduleIDList = string.Empty;


            string GroupName = "ScreenShot" + SSTID + DateTime.Now.ToString("_ddMMyyyy_hhmmss");
            //  string SSTID = dr["SSTID"].ToString();
            int UserID = 1;
            DataSet ds = new DataSet();
            ds = OverAllSSTSSKStatusDB.DoScreenShot(SSTID, GroupName, UserID);
            decimal ScheduleID = Convert.ToInt32(ds.Tables[0].Rows[0]["ScheduleID"]);
            if (ScheduleID > 0)
            {
                Service1 ObjService = new Service1();
                ObjService.GetScreenShotFromSST(ScheduleID, SourceDirectoryPath, SourceFilePath, SourceWildCardPath);
                StrScheduleIDList += ScheduleID.ToString() + ",";
            }
            if (StrScheduleIDList.Contains(","))
                ScheduleIDList = StrScheduleIDList.Substring(0, StrScheduleIDList.Length - 1);

            return Json(new { scheduleIdList = ScheduleIDList },JsonRequestBehavior.AllowGet);
            List<ScreenShotResult> lmd1 = GetScreenShotDetails(ScheduleIDList);
            if (lmd1.Count > 0)
                return PartialView(lmd1[0]);
            return PartialView();
        }
        [HttpGet]
        public ActionResult RefreshScreenShot(string ScheduleIDList)
        {
            List<ScreenShotResult> lmd1 = GetScreenShotDetails(ScheduleIDList);
            if (lmd1.Count > 0)
                return PartialView("SSTID", lmd1[0]);
            return PartialView("SSTID");
        }

        private List<ScreenShotResult> GetScreenShotDetails(string ScheduleIDList)
        {
            List<ScreenShotResult> lmd1 = new List<ScreenShotResult>();

            DataSet ds1 = new DataSet();

            SqlConnection con = new SqlConnection();

            ds1 = OverAllSSTSSKStatusDB.ScreenShotResult(ScheduleIDList);

            foreach (DataRow dr1 in ds1.Tables[0].Rows)
            {

                lmd1.Add(new ScreenShotResult
                {

                    SSTID = dr1["SSTID"].ToString(),
                    SST_ServiceArea = dr1["SST_ServiceArea"].ToString(),
                    TerminalID = dr1["TerminalID"].ToString(),
                    IP = dr1["IP"].ToString(),
                    BankName = dr1["BankName"].ToString(),
                    RegionName = dr1["RegionName"].ToString(),
                    BranchName = dr1["BranchName"].ToString(),
                    OperationStatus = dr1["OperationStatus"].ToString(),
                    Status = dr1["Status"].ToString(),
                    SST_TYPE = dr1["SST_TYPE"].ToString(),
                });
            }
            return lmd1;
        }
        public ActionResult Restart(string SSTID)
        {
            string StrScheduleIDList1 = string.Empty;
            string ScheduleIDList1 = string.Empty;
            string RestartGroupName = "RESTART_WIN_SER_" + SSTID + DateTime.Now.ToString("_ddMMyyyy_hhmmss");
            int UserIDN = 1;
            DataSet ds = new DataSet();
            ds = OverAllSSTSSKStatusDB.DoRestart(SSTID, RestartGroupName, UserIDN);
            decimal ScheduleID = Convert.ToInt32(ds.Tables[0].Rows[0]["ScheduleID"]);
            if (ScheduleID > 0)
            {
                Service1 ObjService = new Service1();
                ObjService.RestartCommand(ScheduleID);//Restrt
                //  string str = System.Web.HttpContext.Current.Session["JobResponse"].ToString();

                StrScheduleIDList1 += ScheduleID.ToString() + ',';
            }
            if (StrScheduleIDList1.Contains(","))
                ScheduleIDList1 = StrScheduleIDList1.Substring(0, StrScheduleIDList1.Length - 1);

            List<RestartResult> lmd2 = new List<RestartResult>();

            DataSet ds2 = new DataSet();

            SqlConnection con = new SqlConnection();

            ds2 = OverAllSSTSSKStatusDB.RestartResult(ScheduleIDList1);

            foreach (DataRow dr2 in ds2.Tables[0].Rows)
            {

                lmd2.Add(new RestartResult
                {

                    SSTID = dr2["SSTID"].ToString(),
                    SST_ServiceArea = dr2["SST_ServiceArea"].ToString(),
                    TerminalID = dr2["TerminalID"].ToString(),
                    IP = dr2["IP"].ToString(),
                    BankName = dr2["BankName"].ToString(),
                    RegionName = dr2["RegionName"].ToString(),
                    BranchName = dr2["BranchName"].ToString(),
                    Status = dr2["Status"].ToString(),
                    SST_TYPE = dr2["SST_TYPE"].ToString(),
                    Remarks = dr2["Remarks"].ToString(),
                });
            }

            return View(lmd2);
        }


        public ActionResult TerminalID(string TerminalID)
        {
            List<OpenTroubleTicketTerminal> lmd = new List<OpenTroubleTicketTerminal>();

            //creating list of model.  

            DataSet ds = new DataSet();

            SqlConnection con = new SqlConnection();

            //The connection namespace as we mentioned earlier .  
            //namespace Gridsample.Connection.  

            // connection to getdata.  

            ds = OpenTroubleTicketDB.myterminaldata(TerminalID);

            // fill dataset  


            foreach (DataRow dr in ds.Tables[0].Rows)

            // loop for adding add from dataset to list<modeldata>  
            {

                lmd.Add(new OpenTroubleTicketTerminal
                {

                    TerminlaID = dr["TerminlaID"].ToString(),
                    Bank = dr["Bank"].ToString(),
                    Region = dr["Region"].ToString(),
                    Branch = dr["Branch"].ToString(),
                    SSTType = dr["SSTType"].ToString(),
                    LinkType = dr["LinkType"].ToString(),
                    IP = dr["IP"].ToString(),
                    Address = dr["Address"].ToString(),
                    SST_ServiceArea = dr["SST_ServiceArea"].ToString(),
                    Sunday_FromTime = dr["Sunday_FromTime"].ToString(),
                    Sunday_ToTime = dr["Sunday_ToTime"].ToString(),
                    Monday_FromTime = dr["Monday_FromTime"].ToString(),
                    Monday_ToTime = dr["Monday_ToTime"].ToString(),
                    Tuesday_FromTime = dr["Tuesday_FromTime"].ToString(),
                    Tuesday_ToTime = dr["Tuesday_ToTime"].ToString(),
                    Wednesday_FromTime = dr["Wednesday_FromTime"].ToString(),
                    Wednesday_ToTime = dr["Wednesday_ToTime"].ToString(),
                    Thursday_FromTime = dr["Thursday_FromTime"].ToString(),
                    Thursday_ToTime = dr["Thursday_ToTime"].ToString(),
                    Friday_FromTime = dr["Friday_FromTime"].ToString(),
                    Friday_ToTime = dr["Friday_ToTime"].ToString(),
                    Saturday_FromTime = dr["Saturday_FromTime"].ToString(),
                    Saturday_ToTime = dr["Saturday_ToTime"].ToString(),
                    FLM = dr["FLM"].ToString(),
                    FLM_ContactPerson = dr["FLM_ContactPerson"].ToString(),
                    FLM_ContactNumber = dr["FLM_ContactNumber"].ToString(),
                    FLM_EmailID = dr["FLM_EmailID"].ToString(),
                    SLM = dr["SLM"].ToString(),
                    SLM_ContactPerson = dr["SLM_ContactPerson"].ToString(),
                    SLM_ContactNumber = dr["SLM_ContactNumber"].ToString(),
                    SLM_EmailID = dr["SLM_EmailID"].ToString(),
                    Cash = dr["Cash"].ToString(),
                    Cash_ContactPerson = dr["Cash_ContactPerson"].ToString(),
                    Cash_ContactNumber = dr["Cash_ContactNumber"].ToString(),
                    Cash_EmailID = dr["Cash_EmailID"].ToString(),
                    Monitoring = dr["Monitoring"].ToString(),
                    Monitoring_ContactPerson = dr["Monitoring_ContactPerson"].ToString(),
                    Monitoring_ContactNumber = dr["Monitoring_ContactNumber"].ToString(),
                    Monitoring_EmailID = dr["Monitoring_EmailID"].ToString(),
                    Network = dr["Network"].ToString(),
                    Network_ContactPerson = dr["Network_ContactPerson"].ToString(),
                    Network_ContactNumber = dr["Network_ContactNumber"].ToString(),
                    Network_EmailID = dr["Network_EmailID"].ToString(),
                    Other = dr["Other"].ToString(),
                    Other_ContactPerson = dr["Other_ContactPerson"].ToString(),
                    Other_ContactNumber = dr["Other_ContactNumber"].ToString(),
                    Other_EmailID = dr["Other_EmailID"].ToString(),
                    HouseKeeping = dr["HouseKeeping"].ToString(),
                    HouseKeeping_ContactPerson = dr["HouseKeeping_ContactPerson"].ToString(),
                    HouseKeeping_ContactNumber = dr["HouseKeeping_ContactNumber"].ToString(),
                    HouseKeeping_EmailID = dr["HouseKeeping_EmailID"].ToString(),
                    Consumable = dr["Consumable"].ToString(),
                    Consumable_ContactPerson = dr["Consumable_ContactPerson"].ToString(),
                    Consumable_ContactNumber = dr["Consumable_ContactNumber"].ToString(),
                    Consumable_EmailID = dr["Consumable_EmailID"].ToString(),
                    UPS = dr["UPS"].ToString(),
                    UPS_ContactPerson = dr["UPS_ContactPerson"].ToString(),
                    UPS_ContactNumber = dr["UPS_ContactNumber"].ToString(),
                    UPS_EmailID = dr["UPS_EmailID"].ToString(),
                    AC = dr["AC"].ToString(),
                    AC_ContactPerson = dr["AC_ContactPerson"].ToString(),
                    AC_ContactNumber = dr["AC_ContactNumber"].ToString(),
                    AC_EmailID = dr["AC_EmailID"].ToString(),
                    HelpDesk = dr["HelpDesk"].ToString(),
                    HelpDesk_ContactPerson = dr["HelpDesk_ContactPerson"].ToString(),
                    HelpDesk_ContactNumber = dr["HelpDesk_ContactNumber"].ToString(),
                    HelpDesk_EmailID = dr["HelpDesk_EmailID"].ToString(),
                    TIS = dr["TIS"].ToString(),
                    TIS_ContactPerson = dr["TIS_ContactPerson"].ToString(),
                    TIS_ContactNumber = dr["TIS_ContactNumber"].ToString(),
                    TIS_EmailID = dr["TIS_EmailID"].ToString(),

                });
            }
            return View(lmd);

        }
        public ActionResult Problem(string TerminalID)
        {
            List<OpenTroubleTicketProblem> lmd = new List<OpenTroubleTicketProblem>();
            DataSet ds = new DataSet();
            SqlConnection con = new SqlConnection();
            // ViewBag.cafID = Problem;
            // ViewBag.personID = TerminalID;
            ds = OpenTroubleTicketDB.myproblemdata(TerminalID);
            foreach (DataRow dr in ds.Tables[0].Rows)
            {
                lmd.Add(new OpenTroubleTicketProblem
                {
                    Color = dr["Color"].ToString(),
                    IconText = dr["IconText"].ToString(),
                    TicketID = dr["TicketID"].ToString(),
                    Time = dr["Time"].ToString(),
                    Device = dr["Device"].ToString(),
                    Problem = dr["Problem"].ToString(),
                    Serverity = dr["Serverity"].ToString(),
                    TimeElapsed = dr["TimeElapsed"].ToString(),
                    ProblemStatus = dr["ProblemStatus"].ToString(),
                    Comment = dr["Comment"].ToString(),
                    AssignedVendor = dr["AssignedVendor"].ToString(),
                    CloseStatus = dr["CloseStatus"].ToString(),
                    LastUpdate_DateTime = dr["LastUpdate_DateTime"].ToString(),
                    Resolution = dr["Resolution"].ToString(),


                });
            }
            return View(lmd);

        }
        public ActionResult Screenshot()
        {
            return PartialView("TerminalID");

        }

    }
}
