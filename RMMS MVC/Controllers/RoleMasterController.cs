﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using RMMS_MVC.Models;
using RMMS_MVC.DAL.Sql;

namespace RMMS_MVC.Controllers
{
    public class RoleMasterController : Controller
    {
        RoleMasterDAO RoleDB = new RoleMasterDAO();
        //
        // GET: /RoleMaster/

        public ActionResult Index()
        {
            return View();
        }
        public JsonResult List()
        {
            return Json(RoleDB.ListAll(), JsonRequestBehavior.AllowGet);
        }
        public JsonResult Add(RoleMaster Role)
        {
            return Json(RoleDB.Add(Role), JsonRequestBehavior.AllowGet);
        }
        public JsonResult GetbyID(int ID)
        {
            var Role = RoleDB.ListAll().Find(x => x.RoleID.Equals(ID));
            return Json(Role, JsonRequestBehavior.AllowGet);
        }
        public JsonResult Update(RoleMaster Role)
        {
            return Json(RoleDB.Update(Role), JsonRequestBehavior.AllowGet);
        }
        public JsonResult Delete(int ID)
        {
            return Json(RoleDB.Delete(ID), JsonRequestBehavior.AllowGet);
        }  


    }
}
