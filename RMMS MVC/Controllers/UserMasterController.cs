﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using RMMS_MVC.DAL.Sql;
using RMMS_MVC.Models;
using System.Data.Objects;
using System.Data.SqlClient;
using System.Data;
using System.Net.Mail;

namespace RMMS_MVC.Controllers
{
    
    public class UserMasterController : Controller
    {
        UserMasterDAO UserDB = new UserMasterDAO();
        CityMasterDAO City = new CityMasterDAO();
        RegionMasterDAO Region = new RegionMasterDAO();
        RoleMasterDAO Role = new RoleMasterDAO();
        BankMasterDAO Bank = new BankMasterDAO();

        public ActionResult Index()
        {
            return View();
        }


        public ActionResult Create()
        {
            //List<RegionMaster> Regions = Region.ListAll();
            List<RoleMaster> Roles = Role.ListAll();
            List<BankMaster> Banks = Bank.ListAll();
            ViewBag.Banks = Banks;
            //ViewBag.Regions = Regions;
            ViewBag.Roles = Roles;
            return PartialView("_Create");
        }

        public ActionResult GetLoginName(string EmailID)
        {
            //string LoginName = (user.UserEmailID).Split('@')[0]; // you get here username.

            MailAddress addr = new MailAddress(EmailID);
            string username = addr.User;
            return this.Json(username, JsonRequestBehavior.AllowGet);
        }

        
        public JsonResult GetBranch(string Region_id)
        {
            var branch = UserDB.ListBranch(Region_id).ToList();

            return this.Json(new SelectList(branch.ToArray(), "BranchID", "BranchName"), JsonRequestBehavior.AllowGet);
        }

        public JsonResult GetRegion(string Bank_id)
        {
            var region = UserDB.ListRegion(Bank_id).ToList();

            return this.Json(new SelectList(region.ToArray(), "RegionID", "RegionName"), JsonRequestBehavior.AllowGet);
        }

        public JsonResult List()
        {
            return Json(UserDB.ListAll(), JsonRequestBehavior.AllowGet);
        }
        public JsonResult Add(UserMaster User)
        {
            return Json(UserDB.Add(User), JsonRequestBehavior.AllowGet);
        }

        public JsonResult GetbyID(int ID)
        {
            var User = UserDB.ListAll().Find(x => x.UserID.Equals(ID));
            return Json(User, JsonRequestBehavior.AllowGet);
        }

        public JsonResult Update(UserMaster User)
        {
            return Json(UserDB.Update(User), JsonRequestBehavior.AllowGet);
        }
        public JsonResult Delete(int ID)
        {
            return Json(UserDB.Delete(ID), JsonRequestBehavior.AllowGet);
        }

        

    }
}
