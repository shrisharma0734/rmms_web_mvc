﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;
using RMMS_MVC.Models;
using System.Web;
using System.Web.Mvc;
using RMMS_MVC.DAL.Sql;
using System.Web.UI.WebControls;

namespace RMMS_MVC.Controllers
{
    public class ScreenShotController : Controller
    {
        ScreenShotDAO ScreenShotDB = new ScreenShotDAO();
        //
        // GET: /ScreenShot/

        public ActionResult Index()
        {
            return View();
        }
        public ActionResult ScreenShot()
        {
            List<ScreenShot> lmd = new List<ScreenShot>(); 

            DataSet ds = new DataSet();

            SqlConnection con = new SqlConnection();

            ds = ScreenShotDB.ScreenShot();

            foreach (DataRow dr in ds.Tables[0].Rows)

            {

                lmd.Add(new ScreenShot
                {
                    TerminalID = dr["TerminalID"].ToString(),
                    SST_ServiceArea = dr["SST_ServiceArea"].ToString(),
                    BankName = dr["BankName"].ToString(),
                    RegionName = dr["RegionName"].ToString(),
                    BranchName = dr["BranchName"].ToString(),
                    SST_TYPE = dr["SST_TYPE"].ToString(),
                    SSTID = dr["SSTID"].ToString(),
                    IP = dr["IP"].ToString(),
                    Status = dr["Status"].ToString(),
                    Remarks = dr["Remarks"].ToString(),
                    SST_TypeID = dr["SST_TypeID"].ToString(),
                    SSTTypeID = dr["SSTTypeID"].ToString(),
                });
            }
            return View(lmd);
 
        }

    }
}
