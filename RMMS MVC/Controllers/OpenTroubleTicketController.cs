﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;
using RMMS_MVC.Models;
using System.Web;
using System.Web.Mvc;
using RMMS_MVC.DAL.Sql;
using System.Web.UI.WebControls;


namespace RMMS_MVC.Controllers
{
    public class OpenTroubleTicketController : Controller
    {
        OpenTroubleTicketDAO OpenTroubleTicketDB = new OpenTroubleTicketDAO();

        //
        // GET: /OpenTroubleTicket/

        public ActionResult Index()
        {
            return View();
        }
        public ActionResult grid()
        {
            List<OpenTroubleTicket> lmd = new List<OpenTroubleTicket>();

            //creating list of model.  

            DataSet ds = new DataSet();

            SqlConnection con = new SqlConnection();

            //The connection namespace as we mentioned earlier .  
            //namespace Gridsample.Connection.  

            // connection to getdata.  

            ds = OpenTroubleTicketDB.mydata();

            // fill dataset  


            foreach (DataRow dr in ds.Tables[0].Rows)

            // loop for adding add from dataset to list<modeldata>  
            {

                lmd.Add(new OpenTroubleTicket
                {
                    // adding data from dataset row in to list<modeldata>  

                    ProblemColorCode = dr["Color"].ToString(),
                    ProblemIcon = dr["IconText"].ToString(),
                    TroubleTicketID = Convert.ToInt32(dr["TicketID"]),
                    SST_TerminalID = dr["TerminalID"].ToString(),
                    RegionName = dr["Region"].ToString(),
                    BranchName = dr["Branch"].ToString(),
                    BranchCode = dr["BranchCode"].ToString(),
                    SST_TYPE = dr["SST_TYPE"].ToString(),
                    Problem = dr["Problem"].ToString(),//(include both Device Type & Problem)
                    Repeatcount = dr["Repeatcount"].ToString(),
                    ProblemSeverity = dr["Severity"].ToString(),
                    ProblemStatus = dr["ProblemStatus"].ToString(),
                    ProblemDateTime = dr["TimeElapsed"].ToString(),
                    FollowUp = dr["FollowUp"].ToString(),
                    Comment = dr["Comment"].ToString(),
                    SSTID = Convert.ToInt32(dr["SSTID"]),
                    problm = dr["problm"].ToString(),
                    Device = dr["Device"].ToString(),

                    //DeviceType = dr["Problem"].ToString(),
                    //SST_ServiceArea = dr["SST_ServiceArea"].ToString(),
                    //SST_Name = dr["Name"].ToString(),
                    //DeviceName = dr["DeviceName"].ToString(),
                    //DeviceStatus = dr["DeviceStatus"].ToString()

                    //public string ProblemResolution { get; set; }

                    //public int RegionID { get; set; }

                    //public int ProblemID { get; set; }

                    //public int BranchID { get; set; }

                    //public int SSTID { get; set; }

                    //public int ProblemCategoryId { get; set; }

                    //public string ProblemCategory { get; set; }

                    //public int SSTTypeID { get; set; }

                    //public int SSTBranchID { get; set; }

                    //public int SSTBankID { get; set; }

                    //public string BankShotName { get; set; }

                });
            }
            return View(lmd);

        }

        public ActionResult TerminalID(string TerminalID)
        {
            List<OpenTroubleTicketTerminal> lmd = new List<OpenTroubleTicketTerminal>();

            //creating list of model.  

            DataSet ds = new DataSet();

            SqlConnection con = new SqlConnection();

            //The connection namespace as we mentioned earlier .  
            //namespace Gridsample.Connection.  

            // connection to getdata.  

            ds = OpenTroubleTicketDB.myterminaldata(TerminalID);

            // fill dataset  


            foreach (DataRow dr in ds.Tables[0].Rows)

            // loop for adding add from dataset to list<modeldata>  
            {

                lmd.Add(new OpenTroubleTicketTerminal
                {

                    TerminlaID = dr["TerminlaID"].ToString(),
                    Bank = dr["Bank"].ToString(),
                    Region = dr["Region"].ToString(),
                    Branch = dr["Branch"].ToString(),
                    SSTType = dr["SSTType"].ToString(),
                    LinkType = dr["LinkType"].ToString(),
                    IP = dr["IP"].ToString(),
                    Address = dr["Address"].ToString(),
                    SST_ServiceArea = dr["SST_ServiceArea"].ToString(),
                    Sunday_FromTime = dr["Sunday_FromTime"].ToString(),
                    Sunday_ToTime = dr["Sunday_ToTime"].ToString(),
                    Monday_FromTime = dr["Monday_FromTime"].ToString(),
                    Monday_ToTime = dr["Monday_ToTime"].ToString(),
                    Tuesday_FromTime = dr["Tuesday_FromTime"].ToString(),
                    Tuesday_ToTime = dr["Tuesday_ToTime"].ToString(),
                    Wednesday_FromTime = dr["Wednesday_FromTime"].ToString(),
                    Wednesday_ToTime = dr["Wednesday_ToTime"].ToString(),
                    Thursday_FromTime = dr["Thursday_FromTime"].ToString(),
                    Thursday_ToTime = dr["Thursday_ToTime"].ToString(),
                    Friday_FromTime = dr["Friday_FromTime"].ToString(),
                    Friday_ToTime = dr["Friday_ToTime"].ToString(),
                    Saturday_FromTime = dr["Saturday_FromTime"].ToString(),
                    Saturday_ToTime = dr["Saturday_ToTime"].ToString(),
                    FLM = dr["FLM"].ToString(),
                    FLM_ContactPerson = dr["FLM_ContactPerson"].ToString(),
                    FLM_ContactNumber = dr["FLM_ContactNumber"].ToString(),
                    FLM_EmailID = dr["FLM_EmailID"].ToString(),
                    SLM = dr["SLM"].ToString(),
                    SLM_ContactPerson = dr["SLM_ContactPerson"].ToString(),
                    SLM_ContactNumber = dr["SLM_ContactNumber"].ToString(),
                    SLM_EmailID = dr["SLM_EmailID"].ToString(),
                    Cash = dr["Cash"].ToString(),
                    Cash_ContactPerson = dr["Cash_ContactPerson"].ToString(),
                    Cash_ContactNumber = dr["Cash_ContactNumber"].ToString(),
                    Cash_EmailID = dr["Cash_EmailID"].ToString(),
                    Monitoring = dr["Monitoring"].ToString(),
                    Monitoring_ContactPerson = dr["Monitoring_ContactPerson"].ToString(),
                    Monitoring_ContactNumber = dr["Monitoring_ContactNumber"].ToString(),
                    Monitoring_EmailID = dr["Monitoring_EmailID"].ToString(),
                    Network = dr["Network"].ToString(),
                    Network_ContactPerson = dr["Network_ContactPerson"].ToString(),
                    Network_ContactNumber = dr["Network_ContactNumber"].ToString(),
                    Network_EmailID = dr["Network_EmailID"].ToString(),
                    Other = dr["Other"].ToString(),
                    Other_ContactPerson = dr["Other_ContactPerson"].ToString(),
                    Other_ContactNumber = dr["Other_ContactNumber"].ToString(),
                    Other_EmailID = dr["Other_EmailID"].ToString(),
                    HouseKeeping = dr["HouseKeeping"].ToString(),
                    HouseKeeping_ContactPerson = dr["HouseKeeping_ContactPerson"].ToString(),
                    HouseKeeping_ContactNumber = dr["HouseKeeping_ContactNumber"].ToString(),
                    HouseKeeping_EmailID = dr["HouseKeeping_EmailID"].ToString(),
                    Consumable = dr["Consumable"].ToString(),
                    Consumable_ContactPerson = dr["Consumable_ContactPerson"].ToString(),
                    Consumable_ContactNumber = dr["Consumable_ContactNumber"].ToString(),
                    Consumable_EmailID = dr["Consumable_EmailID"].ToString(),
                    UPS = dr["UPS"].ToString(),
                    UPS_ContactPerson = dr["UPS_ContactPerson"].ToString(),
                    UPS_ContactNumber = dr["UPS_ContactNumber"].ToString(),
                    UPS_EmailID = dr["UPS_EmailID"].ToString(),
                    AC = dr["AC"].ToString(),
                    AC_ContactPerson = dr["AC_ContactPerson"].ToString(),
                    AC_ContactNumber = dr["AC_ContactNumber"].ToString(),
                    AC_EmailID = dr["AC_EmailID"].ToString(),
                    HelpDesk = dr["HelpDesk"].ToString(),
                    HelpDesk_ContactPerson = dr["HelpDesk_ContactPerson"].ToString(),
                    HelpDesk_ContactNumber = dr["HelpDesk_ContactNumber"].ToString(),
                    HelpDesk_EmailID = dr["HelpDesk_EmailID"].ToString(),
                    TIS = dr["TIS"].ToString(),
                    TIS_ContactPerson = dr["TIS_ContactPerson"].ToString(),
                    TIS_ContactNumber = dr["TIS_ContactNumber"].ToString(),
                    TIS_EmailID = dr["TIS_EmailID"].ToString(),

                });
            }
            return View(lmd);

        }

        public ActionResult Problem(string TerminalID)
        {
            List<OpenTroubleTicketProblem> lmd = new List<OpenTroubleTicketProblem>();
            DataSet ds = new DataSet();
            SqlConnection con = new SqlConnection();
            // ViewBag.cafID = Problem;
            // ViewBag.personID = TerminalID;
            ds = OpenTroubleTicketDB.myproblemdata(TerminalID);
            foreach (DataRow dr in ds.Tables[0].Rows)
            {
                lmd.Add(new OpenTroubleTicketProblem
                {
                    Color = dr["Color"].ToString(),
                    IconText = dr["IconText"].ToString(),
                    TicketID = dr["TicketID"].ToString(),
                    Time = dr["Time"].ToString(),
                    Device = dr["Device"].ToString(),
                    Problem = dr["Problem"].ToString(),
                    Serverity = dr["Serverity"].ToString(),
                    TimeElapsed = dr["TimeElapsed"].ToString(),
                    ProblemStatus = dr["ProblemStatus"].ToString(),
                    Comment = dr["Comment"].ToString(),
                    AssignedVendor = dr["AssignedVendor"].ToString(),
                    CloseStatus = dr["CloseStatus"].ToString(),
                    LastUpdate_DateTime = dr["LastUpdate_DateTime"].ToString(),
                    Resolution = dr["Resolution"].ToString(),


                });
            }
            return View(lmd);

        }
        public ActionResult RepeatCount(string TerminalID, string problm, string Device)
        {
            List<OpenTroubleTicketRepeatCount> lmd = new List<OpenTroubleTicketRepeatCount>();
            DataSet ds = new DataSet();
            SqlConnection con = new SqlConnection();
            // ViewBag.cafID = Problem;
            // ViewBag.personID = TerminalID;
            ds = OpenTroubleTicketDB.repeatcountdata(TerminalID,problm,Device);
            foreach (DataRow dr in ds.Tables[0].Rows)
            {
                lmd.Add(new OpenTroubleTicketRepeatCount
                {
                    Color = dr["Color"].ToString(),
                    IconText = dr["IconText"].ToString(),
                    Icon = dr["Icon"].ToString(),
                    TicketID = dr["TicketID"].ToString(),
                    Time = dr["Time"].ToString(),
                    Device = dr["Device"].ToString(),
                    Problem = dr["Problem"].ToString(),
                    Serverity = dr["Serverity"].ToString(),
                    TimeElapsed = dr["TimeElapsed"].ToString(),
                    ProblemStatus = dr["ProblemStatus"].ToString(),
                    Comment = dr["Comment"].ToString(),
                    AssignedVendor = dr["AssignedVendor"].ToString(),
                    CloseStatus = dr["CloseStatus"].ToString(),
                    LastUpdate_DateTime = dr["LastUpdate_DateTime"].ToString(),
                    Resolution = dr["Resolution"].ToString(),
                    SST_TerminalID = dr["SST_TerminalID"].ToString(),

                });
            }
            return View(lmd);
        }
    } 
    
}
