﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using RMMS_MVC.Models;
using RMMS_MVC.DAL.Sql;

namespace RMMS_MVC.Controllers
{
    public class BankMasterController: Controller
    {
        BankMasterDAO BankDB = new BankMasterDAO();

        CityMasterDAO CityDB = new CityMasterDAO();
        //
        // GET: /CityMaster/

        public ActionResult Index()
        {
            return View();
        }
        public ActionResult Create()
        {
            List<CityMaster> Cities = CityDB.ListAll();
            ViewBag.Cities = Cities;
            return PartialView("_Create");
        }
        public JsonResult List()
        {
            return Json(BankDB.ListAll(), JsonRequestBehavior.AllowGet);
        }
        public JsonResult Add(BankMaster Bank)
        {
            return Json(BankDB.Add(Bank), JsonRequestBehavior.AllowGet);
        }
        public JsonResult GetbyID(int ID)
        {
            var Bank = BankDB.ListAll().Find(x => x.BankID.Equals(ID));
            return Json(Bank, JsonRequestBehavior.AllowGet);
        }
        public JsonResult Update(BankMaster Bank)
        {
            return Json(BankDB.Update(Bank), JsonRequestBehavior.AllowGet);
        }
        public JsonResult Delete(int ID)
        {
            return Json(BankDB.Delete(ID), JsonRequestBehavior.AllowGet);
        }  
    }
}