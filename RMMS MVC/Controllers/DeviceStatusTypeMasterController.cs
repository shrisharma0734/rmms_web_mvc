﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using RMMS_MVC.DAL.Sql;
using RMMS_MVC.Models;

namespace RMMS_MVC.Controllers
{
    public class DeviceStatusTypeMasterController : Controller
    {            
        //
        // GET: /DeviceStatusTypeMaster/
        DeviceStatusTypeDAO dstDB = new DeviceStatusTypeDAO();
        public ActionResult Index()
        {
            return View();
        }
        public JsonResult List()
        {
            return Json(dstDB.ListAll(), JsonRequestBehavior.AllowGet);
        }
        public JsonResult Add(DeviceStatusTypeProfile Dst)
        {
            return Json(dstDB.Add(Dst), JsonRequestBehavior.AllowGet);
        }




        public JsonResult GetbyID(int ID)
        {
            var Dst = dstDB.ListAll().Find(x => x.StatusTypeID.Equals(ID));
            return Json(Dst, JsonRequestBehavior.AllowGet);
        }

        public JsonResult Update(DeviceStatusTypeProfile Sst)
        {
            return Json(dstDB.Update(Sst), JsonRequestBehavior.AllowGet);
        }
        public JsonResult Delete(int ID)
        {
            return Json(dstDB.Delete(ID), JsonRequestBehavior.AllowGet);
        }  


    }
}
