﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using RMMS_MVC.DAL.Sql;
using RMMS_MVC.Models;
using System.Data.Objects;

namespace RMMS_MVC.Controllers
{
    public class ProblemCategoryMasterController : Controller
    {

        ProblemCategoryMasterDAO ProblemDB = new ProblemCategoryMasterDAO();
        //
        // GET: /ProblemCategoryProfile/

        public ActionResult Index()
        {
            return View();
        }
        public JsonResult List()
        {
            return Json(ProblemDB.ListAll(), JsonRequestBehavior.AllowGet);
        }
        public JsonResult Add(ProblemCategoryMaster Problem)
        {
            return Json(ProblemDB.Add(Problem), JsonRequestBehavior.AllowGet);
        }



        public JsonResult GetbyID(int ID)
        {
            var Problem = ProblemDB.ListAll().Find(x => x.ProblemCategoryId.Equals(ID));
            return Json(Problem, JsonRequestBehavior.AllowGet);
        }

        public JsonResult update(ProblemCategoryMaster Problem)
        {
            return Json(ProblemDB.Update(Problem), JsonRequestBehavior.AllowGet);
        }
        public JsonResult delete(int id)
        {
            return Json(ProblemDB.Delete(id), JsonRequestBehavior.AllowGet);
        }  
    }
}
