﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using RMMS_MVC.Models;
using RMMS_MVC.DAL.Sql;
using RMMS_MVC.DAL;

namespace RMMS_MVC.Controllers
{
    public class CityMasterController : Controller
    {
        CityMasterDAO cityDB = new CityMasterDAO();
        //
        // GET: /CityMaster/
        SstGroupMasterDAO SstGroupMasterDB = new SstGroupMasterDAO();

        public ActionResult Create()
        {
            List<SstGroupMaster> SstGroupMaster = SstGroupMasterDB.ListAll();
            ViewBag.SstGroupMaster = SstGroupMaster;
            return PartialView("_Create");
           
        }
        public ActionResult Index()
        {
            return View();
        }
        public JsonResult List()
        {
            return Json(cityDB.ListAll(), JsonRequestBehavior.AllowGet);
        }
        [HttpPost]
        public JsonResult Add(CityMaster city)
        {
            return Json(cityDB.Add(city));
        }
        public JsonResult GetbyID(int ID)
        {
            var Employee = cityDB.ListAll().Find(x => x.CityID.Equals(ID));
            return Json(Employee, JsonRequestBehavior.AllowGet);
        }
        public JsonResult Update(CityMaster city)
        {
            return Json(cityDB.Update(city), JsonRequestBehavior.AllowGet);
        }
        public JsonResult Delete(int ID)
        {
            return Json(cityDB.Delete(ID), JsonRequestBehavior.AllowGet);
        }  


    }
}
