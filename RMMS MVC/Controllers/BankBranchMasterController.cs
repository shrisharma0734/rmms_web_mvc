﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using RMMS_MVC.Models;
using RMMS_MVC.DAL.Sql;

namespace RMMS_MVC.Controllers
{
    public class BankBranchMasterController : Controller
    {

        BankBranchMasterDAO BankDB = new BankBranchMasterDAO();
        BankMasterDAO Bank = new BankMasterDAO();
        RegionMasterDAO Region = new RegionMasterDAO();
        CityMasterDAO CityDB = new CityMasterDAO();
          public ActionResult Index()
        {
            return View();
        }
      
        public ActionResult CreateRegion()
        {
            List<CityMaster> Cities = CityDB.ListAll();
            List<RegionMaster> Regions = Region.ListAll();
            List<BankMaster> Banks = Bank.ListAll();
            ViewBag.Cities = Cities;
            ViewBag.Regions = Regions;
            ViewBag.Banks = Banks;

            return PartialView("_Create");
           
        }
        public JsonResult List()
        {
            return Json(BankDB.ListAll(), JsonRequestBehavior.AllowGet);
        }
        public JsonResult Add(BankBranchMaster BankBranch)
        {
            return Json(BankDB.Add(BankBranch), JsonRequestBehavior.AllowGet);
        }
        public JsonResult GetbyID(int ID)
        {
            var Bank = BankDB.ListAll().Find(x => x.BranchID.Equals(ID));
            return Json(Bank, JsonRequestBehavior.AllowGet);
        }
        public JsonResult Update(BankBranchMaster BankBranch)
        {
            return Json(BankDB.Update(BankBranch), JsonRequestBehavior.AllowGet);
        }
        public JsonResult Delete(int ID)
        {
            return Json(BankDB.Delete(ID), JsonRequestBehavior.AllowGet);
        }  
    }
}
