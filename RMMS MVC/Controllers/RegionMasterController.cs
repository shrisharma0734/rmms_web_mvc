﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using RMMS_MVC.DAL.Sql;
using RMMS_MVC.Models;

namespace RMMS_MVC.Controllers
{
    public class RegionMasterController : Controller
    {

        BankMasterDAO BankDB = new BankMasterDAO();

        RegionMasterDAO RegionDB = new RegionMasterDAO();
        //
        // GET: /Region/

        public ActionResult Index()
        {
            return View();
        }
        public ActionResult Create()
        {
            List<BankMaster> Banks = BankDB.ListAll();
            ViewBag.Banks = Banks;
            return PartialView("_Create");
        }
        public JsonResult List()
        {
            return Json(RegionDB.ListAll(), JsonRequestBehavior.AllowGet);
        }
        public JsonResult Add(RegionMaster Region)
        {
            return Json(RegionDB.Add(Region), JsonRequestBehavior.AllowGet);
        }
        public JsonResult GetbyID(int ID)
        {
            var Region = RegionDB.ListAll().Find(x => x.RegionID.Equals(ID));
            return Json(Region, JsonRequestBehavior.AllowGet);
        }
        public JsonResult Update(RegionMaster Region)
        {
            return Json(RegionDB.Update(Region), JsonRequestBehavior.AllowGet);
        }
        public JsonResult Delete(int ID)
        {
            return Json(RegionDB.Delete(ID), JsonRequestBehavior.AllowGet);
        }  

    }
}
