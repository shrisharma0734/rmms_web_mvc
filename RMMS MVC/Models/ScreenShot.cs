﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Globalization;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Security;

namespace RMMS_MVC.Models
{
    public class ScreenShot
    {
        public bool Selected { get; set; }
        public string TerminalID { get; set; }
        public string SST_ServiceArea { get; set; }
        public string BankName { get; set; }
        public string RegionName { get; set; }
        public string BranchName { get; set; }
        public string SST_TYPE { get; set; }

        public string SSTID { get; set; }
        public string IP { get; set; } 
        public string Status { get; set; }
        public string Remarks { get; set; }
        public string SST_TypeID { get; set; }
        public string SSTTypeID { get; set; }
        
        
    }
}