﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace RMMS_MVC.Models
{
    public class RestartResult
    {
        public string SSTID { get; set; }
        public string SST_ServiceArea { get; set; }
        public string TerminalID {get;set;}
        public string IP {get;set;}
        public string BankName {get;set;}
        public string RegionName {get;set;}
        public string BranchName {get;set;}
        public string Status {get;set;}
        public string SST_TYPE {get;set;}
        public string Remarks { get; set; }
    }
}