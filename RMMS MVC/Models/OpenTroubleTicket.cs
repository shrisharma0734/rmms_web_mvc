﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Globalization;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Security;

namespace RMMS_MVC.Models
{
    public class OpenTroubleTicket
    {
        //new changes by deepika
        [Display(Name = "Color")]
        public string ProblemColorCode { get; set; }

        [Display(Name = "Icon")]
        public string ProblemIcon { get; set; }

        [Display(Name = "TicketID")]
        public int TroubleTicketID { get; set; }

        [Display(Name = "Terminal ID")]
        public string SST_TerminalID { get; set; }

        [Display(Name = "Circle/Region")]
        public string RegionName { get; set; }

        [Display(Name = "Branch")]
        public string BranchName { get; set; }

        [Display(Name = "Branch Code")]
        public string BranchCode { get; set; }

        [Display(Name = "SST Type")]
        public string SST_TYPE { get; set; }

        public string DeviceType { get; set; }

        public string Problem { get; set; }

        [Display(Name = "Repeated Count")]
        public string Repeatcount { get; set; }

        [Display(Name = "Severity")]
        public string ProblemSeverity { get; set; }

        [Display(Name = "Status")]
        public string ProblemStatus { get; set; }

        [Display(Name = "Time Elapsed")]
        public string ProblemDateTime { get; set; }

        [Display(Name = "Follow Up")]
        public string FollowUp { get; set; }

        [Display(Name = "Comment")]
        public string Comment { get; set; }
        //public string Comment { get { return Comment.Substring(0, 10); } }

        public string SST_ServiceArea { get; set; }

        public string SST_Name { get; set; }

        public string DeviceName { get; set; }

        public string DeviceStatus { get; set; }

        public string ProblemResolution { get; set; }

        public int RegionID { get; set; }

        public int ProblemID { get; set; }

        public int BranchID { get; set; }

        public int SSTID { get; set; }

        public int ProblemCategoryId { get; set; }

        public string ProblemCategory { get; set; }

        public int SSTTypeID { get; set; }

        public int SSTBranchID { get; set; }

        public int SSTBankID { get; set; }

        public string BankShotName { get; set; }

       public string problm { get; set; }

       public string Device { get; set; }

        public string Bank { get; set; }
    }
}