﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Globalization;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Security;

namespace RMMS_MVC.Models
{
    public class OpenTroubleTicketTerminal
    {
        
        [Display(Name = "Terminal ID")]
        public string TerminlaID { get; set; }
        public string Bank { get; set; }
        public string Region { get; set; }
        public string Branch { get; set; }
        public string SSTType { get; set; }
        public string LinkType { get; set; }
        public string IP { get; set; }
        public string Address { get; set; }
        public string SST_ServiceArea { get; set; }
        public string Sunday_FromTime { get; set; }
        public string Sunday_ToTime { get; set; }
        public string Monday_FromTime { get; set; }
        public string Monday_ToTime { get; set; }
        public string Tuesday_FromTime { get; set; }
        public string Tuesday_ToTime { get; set; }
        public string Wednesday_FromTime { get; set; }
        public string Wednesday_ToTime { get; set; }
        public string Thursday_FromTime { get; set; }
        public string Thursday_ToTime { get; set; }
        public string Friday_FromTime { get; set; }
        public string Friday_ToTime { get; set; }
        public string Saturday_FromTime { get; set; }
        public string Saturday_ToTime { get; set; }
        public string FLM { get; set; }
        public string FLM_ContactPerson { get; set; }
        public string FLM_ContactNumber { get; set; }
        public string FLM_EmailID { get; set; }
        public string SLM { get; set; }
        public string SLM_ContactPerson { get; set; }
        public string SLM_ContactNumber { get; set; }
        public string SLM_EmailID { get; set; }
        public string Cash { get; set; }
        public string Cash_ContactPerson { get; set; }
        public string Cash_ContactNumber { get; set; }
        public string Cash_EmailID { get; set; }
        public string Monitoring { get; set; }
        public string Monitoring_ContactPerson { get; set; }
        public string Monitoring_ContactNumber { get; set; }
        public string Monitoring_EmailID { get; set; }
        public string Network { get; set; }
        public string Network_ContactPerson { get; set; }
        public string Network_ContactNumber { get; set; }
        public string Network_EmailID { get; set; }
        public string Other { get; set; }
        public string Other_ContactPerson { get; set; }
        public string Other_ContactNumber { get; set; }
        public string Other_EmailID { get; set; }
        public string HouseKeeping { get; set; }
        public string HouseKeeping_ContactPerson { get; set; }
        public string HouseKeeping_ContactNumber { get; set; }
        public string HouseKeeping_EmailID { get; set; }
        public string Consumable { get; set; }
        public string Consumable_ContactPerson { get; set; }
        public string Consumable_ContactNumber { get; set; }
        public string Consumable_EmailID { get; set; }
        public string UPS { get; set; }
        public string UPS_ContactPerson { get; set; }
        public string UPS_ContactNumber { get; set; }
        public string UPS_EmailID { get; set; }
        public string AC { get; set; }
        public string AC_ContactPerson { get; set; }
        public string AC_ContactNumber { get; set; }
        public string AC_EmailID { get; set; }
        public string HelpDesk { get; set; }
        public string HelpDesk_ContactPerson { get; set; }
        public string HelpDesk_ContactNumber { get; set; }
        public string HelpDesk_EmailID { get; set; }
        public string TIS { get; set; }
        public string TIS_ContactPerson { get; set; }
        public string TIS_ContactNumber { get; set; }
        public string TIS_EmailID { get; set; }
    }
}