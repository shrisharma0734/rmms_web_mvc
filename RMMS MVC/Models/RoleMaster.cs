﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Globalization;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Security;

namespace RMMS_MVC.Models
{

    #region Models


    public class RoleMaster
    {
        
        public int RoleID { get; set; }

        [Required]
        [RegularExpression(@"^.{1,}$", ErrorMessage = "Minimum 1 characters required")]
         //[RegularExpression("^[a-zA-Z0-9]{50}$")]

        //[StringLength(50, ErrorMessage = "Maximum {2} characters exceeded")]
        [StringLength(50, ErrorMessage = "Maximum 50 Character allowed")]
        public string RoleName { get; set; }

        [Display(Name = "Role Description")]
        [StringLength(300, ErrorMessage = "Maximum 300 Character allowed")]
   
        public string RoleDescription { get; set; }
        public string Action { get; set; }
        public int CreatedBy { get; set; }

        public DateTime CreatedDatetime { get; set; }

        public int ModifiedBy { get; set; }

        public DateTime ModifiedDatetime { get; set; }

    }


    #endregion
}