﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Globalization;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Security;

namespace RMMS_MVC.Models
{
    public class RegionMaster
    {
        [Display(Name = "Bank Name")]
        public int BankID { get; set; }

        public int RegionID { get; set; }

        //public string RegionName { get; set; }

        [Required]
        [Display(Name = "Bank Name")]
        public string BankName { get; set; }

        [Required]
        [RegularExpression(@"^.{1,}$", ErrorMessage = "Minimum 1 characters required")]
     
        [StringLength(50, ErrorMessage = "Maximum 50 Character allowed")]
        [Display (Name = "Circle/Region Name")]
        public string RegionName { get; set; }

      
        [Display(Name = "Description")]
        [StringLength(300, ErrorMessage = "Maximum 300 Character allowed")]
        [DataType(DataType.MultilineText)]
        public string RegionDescription { get; set; }

        public int CreatedBy { get; set; }

        public DateTime CreatedDatetime { get; set; }

        public int ModifiedBy { get; set; }

        public DateTime ModifiedDatetime { get; set; }

    }
    
}