﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;

namespace RMMS_MVC.Models
{
    public class DeviceStatusTypeProfile
    {

        public int StatusTypeID { get; set; }


        [Required]
        [Display(Name = "Staus Type:")]
        [RegularExpression(@"^.{1,}$", ErrorMessage = "Minimum 1 characters required")]
        [StringLength(50, ErrorMessage = "Maximum 50 Character allowed")]
        public string StatusType { get; set; }

        [Display(Name = "	Description :")]
        [StringLength(300, ErrorMessage = "Maximum 300 Character allowed")]
        public string StatusTypeDescription { get; set; }


        public int CreatedBy { get; set; }

        public DateTime CreatedDatetime { get; set; }

        public int ModifiedBy { get; set; }

        public DateTime ModifiedDatetime { get; set; }

    }
}