﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Globalization;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Security;

namespace RMMS_MVC.Models
{

    #region Models


    public class MenuModel
    {
        public string Title { get; set; }

        public string Url { get; set; }

        public string MenuCode { get; set; }

        public int MenuId { get; set; }

        public int ParentMenuId { get; set; }

        public bool IsActive { get; set; }

        public List<MenuModel> ChildMenus { get; set; }

        public static List<MenuModel> GetMenu(int userId = 0)
        {
            //List<MenuModel> menus = new List<MenuModel>();
            //menus.Add(new MenuModel { Title = "Home", Url = "#", IsActive=true });
            //menus.Add(new MenuModel { Title = "User Management", Url = "javascript:void(0);" });
            //menus[1].ChildMenus = new List<MenuModel>();
            //menus[1].ChildMenus.Add(new MenuModel { Title = "Users"
            //    , Url = "javascript:void(0);"
            ////    , Url = MenuModel.GetBaseUrl() + "Home/Index" 
            //});
            //menus[1].ChildMenus[0].ChildMenus = new List<MenuModel>();
            //menus[1].ChildMenus[0].ChildMenus.Add(new MenuModel { Title = "Child Users", Url = MenuModel.GetBaseUrl() + "Home/Index" });
            //menus[1].ChildMenus.Add(new MenuModel { Title = "Roles", Url = "#/Roles" });
            //menus[1].ChildMenus.Add(new MenuModel { Title = "Kake", Url = MenuModel.GetBaseUrl() + "Home/Kake" });

            //menus.Add(new MenuModel { Title = "Master Info", Url = "javascript:void(0);" });
            //menus[2].ChildMenus = new List<MenuModel>();
            //menus[2].ChildMenus.Add(new MenuModel { Title = "Bank", Url = "#/Bank" });
            //menus[2].ChildMenus.Add(new MenuModel { Title = "City", Url = "#/City" });
            
            //menus.Add(new MenuModel { Title = "FaceBook", Url = "http://www.facebook.com" });
            //return menus;

            List<MenuModel> menus = new List<MenuModel>();
            menus.Add(new MenuModel { Title = "Home", Url = "#", IsActive = true });
            menus.Add(new MenuModel { Title = "User Management", Url = "javascript:void(0);" });
            menus[1].ChildMenus = new List<MenuModel>();
            menus[1].ChildMenus.Add(new MenuModel { Title = "Roles", Url = MenuModel.GetBaseUrl() + "rolemaster/index" });
            menus[1].ChildMenus.Add(new MenuModel { Title = "User", Url = MenuModel.GetBaseUrl() + "UserMaster/index" });
            menus[1].ChildMenus.Add(new MenuModel { Title = "Access Permission", Url = "#/Access Permission" });
            menus[1].ChildMenus.Add(new MenuModel { Title = "Change Password", Url = "#/Change Password" });

            menus.Add(new MenuModel { Title = "Master Info", Url = "javascript:void(0);" });
            menus[2].ChildMenus = new List<MenuModel>();
            menus[2].ChildMenus.Add(new MenuModel { Title = "City", Url = MenuModel.GetBaseUrl() + "citymaster/index" });
            menus[2].ChildMenus.Add(new MenuModel { Title = "Bank", Url = MenuModel.GetBaseUrl() + "bankmaster/index" });
            menus[2].ChildMenus.Add(new MenuModel { Title = "Circle/Region", Url = MenuModel.GetBaseUrl() + "regionmaster/index" });
            menus[2].ChildMenus.Add(new MenuModel { Title = "Bank Branch", Url = MenuModel.GetBaseUrl() + "BankBranchMaster/index" });

            menus[2].ChildMenus.Add(new MenuModel { Title = "SST/SSk Link Type", Url = "#/SST/SSk Link Type" });
            menus[2].ChildMenus.Add(new MenuModel { Title = "SST/SSk Type", Url = "#/SST/SSk Type" });
            menus[2].ChildMenus.Add(new MenuModel { Title = "SST/SSk Group", Url = MenuModel.GetBaseUrl() + "SstGroupMaster/index" });
            menus[2].ChildMenus.Add(new MenuModel { Title = "Vendor", Url = "#/Vendor" });
            menus[2].ChildMenus.Add(new MenuModel { Title = "SST/SSK Profile", Url = "#/SST/SSK Profile" });
            menus[2].ChildMenus.Add(new MenuModel { Title = "Device Type", Url = "#/Device Type" });
            menus[2].ChildMenus.Add(new MenuModel { Title = "Device Status Type", Url = MenuModel.GetBaseUrl() + "DeviceStatusTypeMaster/index" });
            menus[2].ChildMenus.Add(new MenuModel { Title = "Problem Category", Url = MenuModel.GetBaseUrl() + "ProblemCategoryMaster/index" });

            menus.Add(new MenuModel { Title = "Configuration Info", Url = "javascript:void(0);" });
            menus[3].ChildMenus = new List<MenuModel>();
            menus[3].ChildMenus.Add(new MenuModel { Title = "Device", Url = "#/Device" });
            menus[3].ChildMenus.Add(new MenuModel { Title = "Device Status", Url = "#/Device Status" });
            menus[3].ChildMenus.Add(new MenuModel { Title = "Problem", Url = "#/Problem" });
            menus[3].ChildMenus.Add(new MenuModel { Title = "Escalation", Url = "#/Escalation" });
            menus[3].ChildMenus.Add(new MenuModel { Title = "Region wise Email", Url = "#/Region Wise Email" });

            menus.Add(new MenuModel { Title = "Operations", Url = "javascript:void(0);" });
            menus[4].ChildMenus = new List<MenuModel>();
            menus[4].ChildMenus.Add(new MenuModel { Title = "All Tickets", Url = "#/All Tickets" });
            menus[4].ChildMenus.Add(new MenuModel { Title = "Open Tickets", Url = MenuModel.GetBaseUrl() + "OpenTroubleTicket/grid" });
            menus[4].ChildMenus.Add(new MenuModel { Title = "Follow Up", Url = "#/Follow Up" });
            menus[4].ChildMenus.Add(new MenuModel { Title = "Closed Tickets", Url = "#/Closed Tickets" });
            menus[4].ChildMenus.Add(new MenuModel { Title = "Branch Wise Ticket", Url = "#/Branch Wise Ticket" });
            menus[4].ChildMenus.Add(new MenuModel { Title = "Overall SST/SSK Status", Url = MenuModel.GetBaseUrl() + "OverAllSSTSSKStatus/OverAllSSTSSKStatusGrid" });

            menus.Add(new MenuModel { Title = "Activity", Url = "javascript:void(0);" });
            menus[5].ChildMenus = new List<MenuModel>();
            menus[5].ChildMenus.Add(new MenuModel { Title = "Pull", Url = "#/Pull" });
            menus[5].ChildMenus.Add(new MenuModel { Title = "Push", Url = "#/Push" });
            menus[5].ChildMenus.Add(new MenuModel { Title = "Manual Close Ticket", Url = "#/Manual Close Ticket" });
            menus[5].ChildMenus.Add(new MenuModel { Title = "Restart SST/SSK", Url = "#/Restart SST/SSK" });
            menus[5].ChildMenus.Add(new MenuModel { Title = "Screen Shot", Url = MenuModel.GetBaseUrl() + "ScreenShot/ScreenShot" });

            menus.Add(new MenuModel { Title = "Dashboard", Url = "javascript:void(0);" });
            menus[6].ChildMenus = new List<MenuModel>();
            menus[6].ChildMenus.Add(new MenuModel { Title = "Circle/Region", Url = "#/Circle/Region" });
            menus[6].ChildMenus.Add(new MenuModel { Title = "Branch", Url = "#/Branch" });
            menus[6].ChildMenus.Add(new MenuModel { Title = "Status SST/SSK Type", Url = "#/Status SST/SSK Type" });

            menus.Add(new MenuModel { Title = "Reports", Url = "javascript:void(0);" });
            menus[7].ChildMenus = new List<MenuModel>();
            menus[7].ChildMenus.Add(new MenuModel { Title = "Trouble Ticket Reports", Url = "#/Trouble Ticket Reports" });
            menus[7].ChildMenus.Add(new MenuModel { Title = "Frequent Issue Reports", Url = "#/Frequent Issue Reports" });
            menus[7].ChildMenus.Add(new MenuModel { Title = "MIS Data Reports", Url = "#/MIS Data Reports" });
            menus[7].ChildMenus.Add(new MenuModel { Title = "Terminal Availability Reports", Url = "#/Terminal Availability Reports" });

            return menus;
        }

        public static string GetBaseUrl() 
        {
            HttpRequest Request = HttpContext.Current.Request;
            string baseUrl = Request.Url.Scheme + "://" + Request.Url.Authority + Request.ApplicationPath.TrimEnd('/') + "/";
            return baseUrl;
        }
    }


    #endregion
}