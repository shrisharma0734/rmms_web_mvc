﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;

namespace RMMS_MVC.Models
{
    public class BankBranchMaster
    {
        public int BranchID { get; set; }

        [Required]
        [Display(Name = "Institution Name")]

        public int BankID { get; set; }

        public String State { get; set; }

        public String BankName { get; set; }


        [Required]
        [Display(Name = "Office Name")]
        [RegularExpression(@"^.{1,}$", ErrorMessage = "Minimum 1 characters required")]
       
        [StringLength(50, ErrorMessage = "Maximum 50 Character allowed")]
        public String BranchName { get; set; }

        [Required]
        [Display(Name = "Office Code")]
        [RegularExpression(@"^.{1,}$", ErrorMessage = "Minimum 1 characters required")]

        [StringLength(6, ErrorMessage = "Maximum 6 Character allowed")]
        public String BranchCode { get; set; }

        [Required]
        [Display(Name = "Locality")]
        [RegularExpression(@"^.{1,}$", ErrorMessage = "Minimum 1 characters required")]

        [StringLength(50, ErrorMessage = "Maximum 50 Character allowed")]
        public String Locality { get; set; }

        [Required]
        [DataType(DataType.MultilineText)]
        [Display(Name = "Office Address1")]
        [StringLength(300, ErrorMessage = "Maximum 300 Character allowed")]
        public String BranchAddress1 { get; set; }

        [Display(Name = "Office Address2")]
        [StringLength(300, ErrorMessage = "Maximum 300 Character allowed")]
        [DataType(DataType.MultilineText)]
        public String BranchAddress2 { get; set; }

        [Required]
        [Display(Name = "City ")]
        public int CityID { get; set; }


        public string CityName { get; set; }

        [Required]
        [Display(Name = "Circle/Region Name")]
        public int RegionID { get; set; }

        public String RegionName { get; set; }

        public int CreatedBy { get; set; }

        public DateTime CreatedDatetime { get; set; }

        public int ModifiedBy { get; set; }

        public DateTime ModifiedDatetime { get; set; }


    }
}