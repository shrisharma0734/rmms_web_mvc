﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;

namespace RMMS_MVC.Models
{
    public class CityMaster
    {
        public int CityID { get; set; }

        //[Required]
        //[Display(Name = "Zone Name")]
        //public string ZoneName { get; set; }

        [Required]
        [Display(Name = "City Name")]
        public string CityName { get; set; }

        [Required]
        [Display(Name = "State Name")]
        public string State { get; set; }

        [Required]
        [Display(Name = "Pincode")]
        [RegularExpression("^[0-9]{6}$",ErrorMessage="Enter valid Pincode")]
        [StringLength(6,ErrorMessage="Maximum 6 digits allowed")]
        public string PinCode
        {
            get;
            set;
        }
        public string CityDisplayName
        {
            get { return CityName + "-" + State + "-" + SSTGroupName; }
        }
        //[StringLength(6, ErrorMessage = "PinCode cannot be longer than 6 characters.")]
        /*public int Pincode 
        { 
            get
            {
                return Convert.ToInt32(PinCode);
            }
            
            set
            {
                PinCode = value.ToString();
            }
        }*/
        [Required]
        [Display(Name = "Zone Name")]
        public int SSTGroupID { get; set; }
        public string SSTGroupName { get; set; }
    }
}