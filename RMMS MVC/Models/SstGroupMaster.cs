﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Globalization;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Security;

namespace RMMS_MVC.Models
{

 

    public class SstGroupMaster
    {
        public Decimal SSTGroupID { get; set; }


        [Required]
        [Display(Name = "Group Name")]
        [RegularExpression(@"^.{1,}$", ErrorMessage = "Minimum 1 characters required")]
        [StringLength(50, ErrorMessage = "Maximum 50 Character allowed")]
        public string SSTGroupName { get; set; }

        [Display(Name = "Group Description")]
        [StringLength(300, ErrorMessage = "Maximum 300 Character allowed")]
        public string  SSTGroupDescription { get; set; }


        public int CreatedBy { get; set; }

        public DateTime CreatedDatetime { get; set; }

        public int ModifiedBy { get; set; }

        public DateTime ModifiedDatetime { get; set; }

    }
}  