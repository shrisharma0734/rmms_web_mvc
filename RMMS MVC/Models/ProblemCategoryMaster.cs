﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;

namespace RMMS_MVC.Models
{
    public class ProblemCategoryMaster
    {
        public int ProblemCategoryId { get; set; }

        [Required]
        [Display(Name = "Category")]
        [RegularExpression(@"^.{1,}$", ErrorMessage = "Minimum 1 characters required")]
      
        [StringLength(50, ErrorMessage = "Maximum 50 Character allowed")]
        public String ProblemCategory { get; set; }

        [DataType(DataType.MultilineText)]
        [StringLength(300, ErrorMessage = "Maximum 300 Character allowed")]
        [Display(Name = "Description")]
        public String ProblemCategoryDescription { get; set; }

        [Required]
        [Display(Name = "Colorcode")]
        public String ProblemColorCode { get; set; }


        public int CreatedBy { get; set; }

        public DateTime CreatedDatetime { get; set; }

        public int ModifiedBy { get; set; }

        public DateTime ModifiedDatetime { get; set; }
    }
}