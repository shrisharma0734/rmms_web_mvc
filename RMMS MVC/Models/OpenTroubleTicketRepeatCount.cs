﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Globalization;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Security;

namespace RMMS_MVC.Models
{
    public class OpenTroubleTicketRepeatCount
    {
        public string Color { get; set; }
        public string IconText { get; set; }
        public string Icon { get; set; }
        public string TicketID { get; set; }
        public string Time { get; set; }
        public string Device { get; set; }
        public string Problem { get; set; }
        public string Serverity { get; set; }
        public string TimeElapsed { get; set; }
        public string ProblemStatus { get; set; }
        public string Comment { get; set; }
        public string AssignedVendor { get; set; }
        public string CloseStatus { get; set; }
        public string LastUpdate_DateTime { get; set; }
        public string Resolution { get; set; }
        public string SST_TerminalID { get; set; }

    }
}