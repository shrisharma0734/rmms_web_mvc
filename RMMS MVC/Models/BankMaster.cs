﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Globalization;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Security;

namespace RMMS_MVC.Models
{
    public class BankMaster
    {
        public int BankID { get; set; }

        [Required]
        [Display(Name = "Bank Name")]
        [RegularExpression(@"^.{1,}$", ErrorMessage = "Minimum 1 characters required")]
      
        [StringLength(50, ErrorMessage = "Maximum 50 Character allowed")]
        public string BankName { get; set; }

        [Required]
        [Display (Name = "Bank Short Name")]
        [RegularExpression(@"^.{1,}$", ErrorMessage = "Minimum 1 characters required")]

        [StringLength(6, ErrorMessage = "Maximum 6 Character allowed")]
        public string BankShotName { get; set; }

        [Required]
        [Display(Name = "Address1")]
        [StringLength(300, ErrorMessage = "Maximum 300 Character allowed")]
        [DataType(DataType.MultilineText)] 
        public string Bank_HO_Address1 { get; set; }

        [Display(Name = "Address2")]
        [StringLength(300, ErrorMessage = "Maximum 300 Character allowed")]
        [DataType(DataType.MultilineText)]
        public string Bank_HO_Address2 { get; set; }

        [Required]
        [Display(Name = "City")]
        public int CityID { get; set; }

        public string CityName { get; set; }

        [Required]
        [Display(Name = "Contact Person")]
        [RegularExpression(@"^.{1,}$", ErrorMessage = "Minimum 1 characters required")]

        [StringLength(50, ErrorMessage = "Maximum 50 Character allowed")]
        public string Bank_ContactName { get; set; }

        [Required]
        [Display(Name = "ContactNo.1")]
        [RegularExpression(@"^.{10,}$", ErrorMessage = "Minimum 10 characters required")]

        [StringLength(15, ErrorMessage = "Maximum 15 Character allowed")]
        public string Bank_ContactNo1 { get; set; }

        [Display(Name = "ContactNo.2")]
        [RegularExpression(@"^.{10,}$", ErrorMessage = "Minimum 10 characters required")]

        [StringLength(15, ErrorMessage = "Maximum 15 Character allowed")]
        public string Bank_ContactNo2 { get; set; }

        [Required]
        [RegularExpression(@"^([a-zA-Z0-9_\-\.]+)@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([a-zA-Z0-9\-]+\.)+))([a-zA-Z]{2,4}|[0-9]           {1,3})(\]?)$", ErrorMessage = "Please enter a valid e-mail adress")]
        [Display(Name = "EmailID 1")]
        public string Bank_EmailID1 { get; set; }

        [Display(Name = "EmailID 2")]
        [RegularExpression(@"^([a-zA-Z0-9_\-\.]+)@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([a-zA-Z0-9\-]+\.)+))([a-zA-Z]{2,4}|[0-9]           {1,3})(\]?)$", ErrorMessage = "Please enter a valid e-mail adress")]
        public string Bank_EmailID2 { get; set; }

        [Required]
        [Display(Name = "FTPAddress 1")]
        //[Range(0, 65535, ErrorMessage = "Enter number between 0 to 65535")]
        public string Bank_FTPAddress1 { get; set; }

        [Display(Name = "FTPAddress 2")]
        //[Range(0, 65535, ErrorMessage = "Enter number between 0 to 65535")]
        public string Bank_FTPAddress2 { get; set; }
        [Required]
        [Display(Name = "FTPPort 1")]
        [Range(0, 65535, ErrorMessage = "Port must be between 0 and 65535.")] 
        public int Bank_FTPPort1 { get; set; }

        [Display(Name = "FTPPort 2")]
        [Range(0, 65535, ErrorMessage = "Port must be between 0 and 65535.")] 
        public int ? Bank_FTPPort2 { get; set; }

        [Required]
        [Display(Name = "FTP UserID 1")]
        public string Bank_FTP1UserID { get; set; }

        [Display(Name = "FTP UserID 2")]
        public string Bank_FTP2UserID { get; set; }

        [Required]
        [DataType(DataType.Password)]
        [Display(Name = "FTP UserID1 Password")]
        public string Bank_FTP1Password { get; set; }

        [Display(Name = "FTP UserID2 Password")]
        [DataType(DataType.Password)]
        public string Bank_FTP2Password { get; set; }

        [Required]
        [Display(Name = "Extracted File Location")]
        public string Bank_ExtrctdFileLoc { get; set; }

        public int CreatedBy { get; set; }

        public DateTime CreatedDatetime { get; set; }

        public int ModifiedBy { get; set; }

        public DateTime ModifiedDatetime { get; set; }

    }
}