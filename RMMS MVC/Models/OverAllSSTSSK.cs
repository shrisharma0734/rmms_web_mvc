﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Globalization;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Security;

namespace RMMS_MVC.Models
{
    public class OverAllSSTSSK
    {
        public string Status { get; set; }
        [Display(Name = "Download")]
        public string Remarks { get; set; }
        [Display(Name = "Icon")]
        public string RestartStatus {get;set;}

        public string IconText { get; set; }
         [Display(Name = "TicketID")]
        public string TroubleTicketID { get; set; }
         [Display(Name = "SST TerminalID")]
        public string SST_TerminalID { get; set; }
         [Display(Name = "Circle/Region")]
        public string RegionName { get; set; }
         [Display(Name = "Office")]
        public string BranchName { get; set; }
         [Display(Name = "Office Code")]
        public string BranchCode { get; set; }
         [Display(Name = "SST Type")]
        public string SST_TYPE { get; set; }
         [Display(Name = "Location Name")]
        public string CityName { get; set; }
         [Display(Name = "Color")]
        public string Color { get; set; }
         [Display(Name = "Problem")]
        public string ProblemName { get; set; }
         [Display(Name = "Severity")]
        public string Severity { get; set; }
         [Display(Name = "Status")]
        public string MachineStatus { get; set; }
         [Display(Name = "FirstFeed Time")]
        public string FirstFeed_Time { get; set; }
         [Display(Name = "LastFeed Time")]
        public string LastFeed_Time { get; set; }
         [Display(Name = "Time Elapsed")]
        public string Time_Elapsed_in_Minute { get; set; }

        public string SST_Name { get; set; }
        public string SSTID { get; set; }
        public string RegionID { get; set; }
        public string SST_ServiceArea { get; set; }
        public string SST_TypeID { get; set; }
        public string ProblemID { get; set; }
        public string BankShotName { get; set; }
        public string BankID { get; set; }
       
       
    }

}