﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using System.Web.WebPages.Html;
using System.Web.Mvc;

namespace RMMS_MVC.Models
{
    public class UserMaster
    {

        public int UserID { get; set; }

        [Required]
        [Display(Name = "Role")]
        public int RoleID { get; set; }

        public string RoleName { get; set; }

        [Required]
        [Display(Name = "Login Name")]
        public string UserLoginName { get; set; }

        [Required]
        [Display(Name = "Full Name")]
        public string UserFullName { get; set; }

        [Required(ErrorMessage = "Password is required.")]
        [Display(Name = "Login Password")]
        [DataType(DataType.Password)]
        [StringLength(255, ErrorMessage = "Must be between 5 and 255 characters", MinimumLength = 5)]
        public string UserLoginPassword { get; set; }

       [Required(ErrorMessage = "Confirmation Password is required.")]
        [Display(Name = "Confirm Password")]
        [DataType(DataType.Password)]
        [CompareAttribute("UserLoginPassword", ErrorMessage = "Password and Confirmation Password must match.")]    
        public string UserConfirmPassword { get; set; }

        [Required]
        [Display(Name = "Default Page")]
        public string DefaultPage { get; set; }

        [Required]
        [RegularExpression(@"^([a-zA-Z0-9_\-\.]+)@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([a-zA-Z0-9\-]+\.)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\]?)$", ErrorMessage = "Please enter a valid e-mail adress")]
        [Display(Name = "EmailID")]
        public string UserEmailID { get; set; }

        [Required]
        [Display(Name = "Contact No")]
        public string UserContactNo { get; set; }

        [Required]
        [Display(Name = "Status ")]
        public string UserStatus { get; set; }

        public int CreatedBy { get; set; }

        public DateTime CreatedDatetime { get; set; }

        public int ModifiedBy { get; set; }

        public DateTime ModifiedDatetime { get; set; }

        [Required]
        [Display(Name = "Bank")]
        public int BankID { get; set; }
        public string BankName { get; set; }

        [Required]
        [Display(Name = "Region")]
        public int RegionID { get; set; }
        public string RegionName { get; set; }

        [Display(Name = "Branch")]
        public int BranchID { get; set; }
        public string BranchName { get; set; }

        
    }
}