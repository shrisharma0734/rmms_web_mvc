﻿/// <reference path="jquery-1.9.1.intellisense.js" />
//Load Data in Table when documents is ready
$(document).ready(function () {
debugger;
    loadData();
});

//Load Data function
function loadData() {
debugger;
    $.ajax({

        url: "/RoleMaster/List",
        type: "GET",
        contentType: "application/json;charset=utf-8",
        dataType: "json",
        success: function (result) {
        debugger;
            /// <reference path="../Views/CityMaster/Index.cshtml" />
            var html = '';
            var index=1;
            $.each(result, function (key, item) {
                html += '<tr>';
                html += '<td>' + index + '</td>';
                html += '<td>' + item.RoleName + '</td>';/// <reference path="../images/edit.png" />
                html += '<td>' + item.RoleDescription + '</td>';
                html += '<td><a href="#" onclick="return getbyID(' + item.RoleID + ')"> <img alt="Edit" src="../images/edit.png" height="30px" width="30px"> </a> | <a href="#" onclick="return delele(' + item.RoleID + ')"><img alt="Edit" src="../images/delete.jpg" height="30px" width="30px"> </a></td>';
                html += '</tr>';
                index++;
            });
            
            $('#tblRoles').DataTable().destroy();
            $("#tblRoles").find('.tbody').html(html);
            $('#tblRoles').DataTable({"lengthMenu": [ [10, 20, 30, -1], [10, 20, 30, "All"] ]});
        },
        error: function (errormessage) {
            alert(errormessage.responseText);
        }
    });
}

////Add Data Function 
function Add() {
debugger;
    var res = validate();
    if (res == false) {
        return false;
    }
    var empObj = {
        RoleID: $('#RoleID').val(),
        RoleName: $('#RoleName').val(),
        RoleDescription: $('#RoleDescription').val()
    };
    $.ajax({
        url: "/RoleMaster/Add",
        data: JSON.stringify(empObj),
        type: "POST",
        contentType: "application/json;charset=utf-8",
        dataType: "json",
        success: function (result) {
            $('#myModal').modal('hide');
                loadData();   
                 alert("Save Successfully");     
           },
               
        error: function (errormessage) {
            alert(errormessage.responseText);
        }
    });
}

////Function for getting the Data Based upon Employee ID
function getbyID(RoleID) {
debugger;
   /* $('#RoleName').css('border-color', 'lightgrey');
    $('#CityName').css('border-color', 'lightgrey');
    $('#RoleDescription').css('border-color', 'lightgrey');
    $('#Pincode').css('border-color', 'lightgrey');*/
    $.ajax({
        url: "/RoleMaster/getbyID/" + RoleID,
        typr: "GET",
        contentType: "application/json;charset=UTF-8",
        dataType: "json",
        success: function (result) {
            $('#RoleID').val(result.RoleID);
            $('#RoleName').val(result.RoleName);
            $('#RoleDescription').val(result.RoleDescription);

            $('#myModal').modal('show');
            $('#btnUpdate').show();
            $('#btnAdd').hide();
        },
        error: function (errormessage) {
            alert(errormessage.responseText);
        }
    });
    return false;
}

//function for updating employee's record
function Update() {
debugger;
    var res = validate();
    if (res == false) {
        return false;
    }
    var empObj = {
        RoleID: $('#RoleID').val(),
        RoleName: $('#RoleName').val(),
        RoleDescription: $('#RoleDescription').val(),
    };
    $.ajax({
        url: "/RoleMaster/Update",
        data: JSON.stringify(empObj),
        type: "POST",
        contentType: "application/json;charset=utf-8",
        dataType: "json",
        success: function (result) {
            loadData();
            $('#myModal').modal('hide');
            $('#RoleID').val("");
            $('#RoleName').val("");
            $('#RoleDescription').val("");
          alert("Update Successfully");
        },
        error: function (errormessage) {
            alert("Error "+errormessage.responseText);
        }
    });
}

////function for deleting employee's record
function delele(id) {
debugger;
    var ans = confirm("are you sure you want to delete this record?");
    if (ans) {
        $.ajax({
            url: "/RoleMaster/Delete/" + id,
            type: "post",
            contenttype: "application/json;charset=utf-8",
            datatype: "json",
            success: function (result) {
                loadData();
            },
            error: function (errormessage) {
                alert(errormessage.responsetext);
            }
        });
    }
}

////Function for clearing the textboxes
function clearTextBox() {
    $('#RoleID').val("");
    $('#RoleName').val("");
    $('#RoleDescription').val("");
    $('#btnUpdate').hide();
    $('#btnAdd').show();
   /* $('#RoleName').css('border-color', 'lightgrey');
    $('#RoleDescription').css('border-color', 'lightgrey');*/
}
////Valdidation using jquery
function validate() {
    return $('#frmRoleMaster').valid();
}


