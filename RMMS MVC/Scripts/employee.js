﻿/// <reference path="jquery-1.9.1.intellisense.js" />
//Load Data in Table when documents is ready
$(document).ready(function () {
    loadData();
});

//Load Data function
function loadData() {
debugger;
    $.ajax({
        url: "/CityMaster/List",
        type: "GET",
        contentType: "application/json;charset=utf-8",
        dataType: "json",
        success: function (result) {
            /// <reference path="../Views/CityMaster/Index.cshtml" />
            var html = '';
            var index=1;
            $.each(result, function (key, item) {
                html += '<tr>';
                html += '<td>' + index + '</td>';
                html += '<td>' + item.CityName + '</td>';/// <reference path="../images/edit.png" />
                html += '<td>' + item.SSTGroupName + '</td>';
                html += '<td>' + item.State + '</td>';
                html += '<td>' + item.PinCode + '</td>';
                html += '<td><a href="#" onclick="return getbyID(' + item.CityID + ')"> <img alt="Edit" src="../images/edit.png" height="30px" width="30px"> </a> | <a href="#" onclick="return delele(' + item.CityID + ')"><img alt="Edit" src="../images/delete.jpg" height="30px" width="30px"> </a></td>';
                html += '</tr>';
                index++;
            });
            
            $('#tblCities').DataTable().destroy();
            $("#tblCities").find('.tbody').html(html);
            $('#tblCities').DataTable({"lengthMenu": [ [2, 5, 15, -1], [2, 5, 15, "All"] ]});
        },
        error: function (errormessage) {
            alert(errormessage.responseText);
        }
    });
}

////Add Data Function 
function Add() {
debugger;
    var res = validate();
    if (res == false) {
        return false;
    }
    var empObj = {
     SSTGroupID: $('#SSTGroupID').val(),
        CityName: $('#CityName').val(),
        State: $('#State').val(),
        PinCode: $('#PinCode').val()
    };
    $.ajax({
        url: "/CityMaster/Add",
        data: JSON.stringify(empObj),
        type: "POST",
        contentType: "application/json;charset=utf-8",
        dataType: "json",
        success: function (result) {
            $('#myModal').modal('hide');
                loadData();        
           },
        error: function (errormessage) {
            alert(errormessage.responseText);
        }
    });
}

////Function for getting the Data Based upon Employee ID
function getbyID(CityID) {
debugger;
  /*  $('#ZoneName').css('border-color', 'lightgrey');
    $('#CityName').css('border-color', 'lightgrey');
    $('#State').css('border-color', 'lightgrey');
    $('#PinCode').css('border-color', 'lightgrey');*/
    $.ajax({
        url: "/CityMaster/getbyID/" + CityID,
        typr: "GET",
        contentType: "application/json;charset=UTF-8",
        dataType: "json",
        success: function (result) {
            $('#CityID').val(result.CityID);
            $('#SSTGroupID').val(result.SSTGroupID);
            $('#CityName').val(result.CityName);
            $('#State').val(result.State);
            $('#PinCode').val(result.PinCode);

            $('#myModal').modal('show');
            $('#btnUpdate').show();
            $('#btnAdd').hide();
        },
        error: function (errormessage) {
            alert(errormessage.responseText);
        }
    });
    return false;
}

//function for updating employee's record
function Update() {
debugger;
    var res = validate();
    if (res == false) {
        return false;
    }
    var empObj = {
        CityID: $('#CityID').val(),
        SSTGroupID: $('#SSTGroupID').val(),
        CityName: $('#CityName').val(),
        State: $('#State').val(),
        PinCode: $('#PinCode').val(),
    };
    $.ajax({
        url: "/CityMaster/Update",
        data: JSON.stringify(empObj),
        type: "POST",
        contentType: "application/json;charset=utf-8",
        dataType: "json",
        success: function (result) {
            loadData();
            $('#myModal').modal('hide');
            $('#CityID').val("");
             $('#SSTGroupID').val("");
            $('#CityName').val("");
            $('#State').val("");
            $('#PinCode').val("");
          
        },
        error: function (errormessage) {
            alert(errormessage.responseText);
        }
    });
}

////function for deleting employee's record
function delele(id) {
    var ans = confirm("are you sure you want to delete this record?");
    if (ans) {
        $.ajax({
            url: "/CityMaster/Delete/" + id,
            type: "post",
            contenttype: "application/json;charset=utf-8",
            datatype: "json",
            success: function (result) {
                loadData();
            },
            error: function (errormessage) {
                alert(errormessage.responsetext);
            }
        });
    }
}

////Function for clearing the textboxes
function clearTextBox() {
    $('#CityID').val("");
    $('#SSTGroupID').val("");
    $('#CityName').val("");
    $('#State').val("");
    $('#PinCode').val("");
    $('#btnUpdate').hide();
    $('#btnAdd').show();
    /*
    $('#ZoneName').css('border-color', 'lightgrey');
    $('#CityName').css('border-color', 'lightgrey');
    $('#State').css('border-color', 'lightgrey');
    $('#PinCode').css('border-color', 'lightgrey');*/
}
////Valdidation using jquery
function validate() {
    return $('#frmCityMaster').valid();
}


