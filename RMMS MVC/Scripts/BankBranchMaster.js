﻿//Load Data in Table when documents is ready  
$(document).ready(function () {  
debugger;
    loadData();  

    
});  
function ValidateEmailID() {
    debugger;    
    var isValid=true;
    $('#errMessage').html('');
    $("#tblBankBranch").find("tr[data-group]").each(function(i,e){
        var element='input';
        if($(e).data('target'))
            element = $(e).data('target');
        var string1 = $(e).find(element+":eq(0)").val();
        var string2 = $(e).find(element+":eq(1)").val();
            
        if(string1!=""&&string1==string2)
        {
            $('#errMessage').append("<li>Enter another "+ $(e).data("group") + "</li>");
            isValid=false;
        }
    });
    return isValid;
       
}
  
//Load Data function  
function loadData() {  
debugger;
    $.ajax({  
        url: "/BankBranchMaster/List",  
        type: "GET",  
        contentType: "application/json;charset=utf-8",  
        dataType: "json",  
        success: function (result) {     
            var html = '';  
            var index=1;
            $.each(result, function (key, item) {  
                html += '<tr>';  
                html += '<td>' + index + '</td>';
                html += '<td>' + item.BankName+ '</td>';  
                html += '<td>' + item.BranchName+ '</td>';
                html += '<td>' + item.BranchCode + '</td>';
                html += '<td>' + item.Locality + '</td>';     
                html += '<td>' + item.RegionName+ '</td>';   
                html += '<td>' + item.CityName+ '</td>';    
                html += '<td>' + item.BranchAddress1 + '</td>';       
                html += '<td><a href="#" onclick="return getbyID(' + item.BranchID + ')"> <img alt="Edit" src="../images/edit.png" height="30px" width="30px"> </a> | <a href="#" onclick="Delete(' + item.BranchID + ')"><img alt="Edit" src="../images/delete.jpg" height="30px" width="30px"> </a></td>';
                html += '</tr>'; 
                index++;

            });  
            $('#tblBankBranch').DataTable().destroy();
            $("#tblBankBranch").find('.tbody').html(html);
            $('#tblBankBranch').DataTable({ "lengthMenu": [[10, 20, 30, -1], [10, 20, 30, "All"]] });
        },  
        error: function (errormessage) {  
            alert(errormessage.responseText);  
        }  
    });  
}  
  
//Add Data Function   
function Add() {  
debugger;
    var res = validate();  
    if (res == false) {  
        return false;  
    }  
    var empObj = {  
        BranchID          :$('#BranchID').val(),
        BankID            :$('#BankID').val(),
        BranchName        :$('#BranchName').val(),  
        BranchCode        :$('#BranchCode').val(),
        Locality          :$('#Locality').val(),
        BranchAddress1    :$('#BranchAddress1').val(),
        BranchAddress2    :$('#BranchAddress2').val(),
        CityID            :$('#CityID').val(),
        RegionID          :$('#RegionID').val() 

    };  
    $.ajax({  
        url: "/BankBranchMaster/Add",  
        data: JSON.stringify(empObj),  
        type: "POST",  
        contentType: "application/json;charset=utf-8",  
        dataType: "json",  
        success: function (result) {  
     
            loadData();  
            clearTextBox();
            $('#myModal').modal('hide');  
            clearTextBox();
        },  
        error: function (errormessage) {  
            alert(errormessage.responseText);  
        }  
    });  
}  
  
//Function for getting the Data Based upon Employee ID  
function getbyID(BranchID) {
debugger;
//    $('#GroupName').css('border-color', 'lightgrey');
//    $('#GroupDescription').css('border-color', 'lightgrey');
    $.ajax({
        url: "/BankBranchMaster/getbyID/" + BranchID,
        typr: "GET",
        contentType: "application/json;charset=UTF-8",
        dataType: "json",
        success: function (result) {
            $('#BranchID').val(result.BranchID); 
            $('#BankID').val(result.BankID);
            $('#BranchName').val(result.BranchName); 
            $('#BranchCode').val(result.BranchCode);
            $('#Locality').val(result.Locality);
            $('#BranchAddress1').val(result.BranchAddress1);
            $('#BranchAddress2').val(result.BranchAddress2);
            $('#CityID').val(result.CityID); 
            $('#CityhName').val(result.CityName); 
            $('#RegionID').val(result.RegionID);
            $('#RegionName').val(result.RegionName); 
            $('#myModal').modal('show');
            $('#btnUpdate').show();
            $('#btnAdd').hide();
          
        },
        error: function (errormessage) {
            alert(errormessage.responseText);
        }
    });
    return false;
}
  
//function for updating employee's record  
function Update() {
debugger;
    var res = validate();
    if (res == false) {
        return false;
    }
    var empObj = {
        BranchID: $('#BranchID').val(),                         
        BankID: $('#BankID').val(),
        BranchName: $('#BranchName').val(),
        BranchCode: $('#BranchCode').val(),
        Locality: $('#Locality').val(),
        BranchAddress1: $('#BranchAddress1').val(),
        BranchAddress2: $('#BranchAddress2').val(),
        CityID: $('#CityID').val(),
        RegionID: $('#RegionID').val()
    };
    $.ajax({
        url: "/BankBranchMaster/Update",
        data: JSON.stringify(empObj),
        type: "POST",
        contentType: "application/json;charset=utf-8",
        dataType: "json",
        success: function (result) {
            loadData();
            $('#myModal').modal('hide');
            $('#BranchID').val("");
            $('#BankID').val("");
            $('#BranchName').val("");
            $('#BranchCode').val("");
            $('#Locality').val("");
            $('#BranchAddress1').val("");
            $('#BranchAddress2').val("");
            $('#CityID').val("");
            $('#RegionID').val("");
            alert("Update Successfully");
          
        },
        error: function (errormessage) {
            alert(errormessage.responseText);
        }
    });
}
  
//function for deleting employee's record  
function Delete(ID) {  
debugger;
    var ans = confirm("Are you sure you want to delete this Record?");  
    if (ans) {  
        $.ajax({  
            url: "/BankBranchMaster/Delete/" + ID,  
            type: "POST",  
            contentType: "application/json;charset=UTF-8",  
            dataType: "json",  
            success: function (result) {  
                loadData();  
            },  
            error: function (errormessage) {  
                alert(errormessage.responseText);  
            }  
        });  
    }  
}  




  
//Function for clearing the textboxes  
function clearTextBox() {  

$('#BranchID').val(""),
$('#BankID ').val(""),
$('#BranchName').val(""),
$('#BranchCode').val(""),
$('#Locality').val(""),
$('#BranchAddress1').val(""),
$('#BranchAddress2').val(""),
$('#CityID').val(""),
$('#RegionID').val(""), 
 $('#btnUpdate').hide();  
 $('#btnAdd').show();  
  
}  


//function validate() {
//    return $('#frmBankBranchMaster').valid();
//}

function validate() {
    if (!$('#frmBankBranchMaster').valid()) {
        ValidateEmailID();
        return false;
    }
    return ValidateEmailID();
}