﻿//Load Data in Table when documents is ready  
$(document).ready(function () {  
    loadData();  
    
});  
  
//Load Data function  
function loadData() {  
debugger;
    $.ajax({  
        url: "/SstGroupMaster/List",  
        type: "GET",  
        contentType: "application/json;charset=utf-8",  
        dataType: "json",  
        success: function (result) {     
            var html = '';  
             var index=1;
            $.each(result, function (key, item) {  
                html += '<tr>';  
                html += '<td>' + index + '</td>';
                html += '<td>' + item.SSTGroupName + '</td>';  
                html += '<td>' + item.SSTGroupDescription + '</td>';                
                html += '<td><a href="#" onclick="return getbyID(' + item.SSTGroupID + ')"> <img alt="Edit" src="../images/edit.png" height="30px" width="30px"> </a> | <a href="#" onclick="Delete(' + item.SSTGroupID + ')"><img alt="Edit" src="../images/delete.jpg" height="30px" width="30px"> </a></td>';
                html += '</tr>';  
                index++;
             
            });  
             $('#tblSstGroup').DataTable().destroy();
            $("#tblSstGroup").find('.tbody').html(html);
            $('#tblSstGroup').DataTable({"lengthMenu": [ [10, 20 , 30, -1], [10, 20 , 30, "All"] ]});
        },  
        error: function (errormessage) {  
            alert(errormessage.responseText);  
        }  
    });  
}  
  
//Add Data Function   
function Add() {  
debugger;
    var res = validate();  
    if (res == false) {  
        return false;  
    }  
    var empObj = {  
        SSTGroupID: $('#SSTGroupID').val(),
        SSTGroupName: $('#SSTGroupName').val(),  
        SSTGroupDescription: $('#SSTGroupDescription').val()
    };  
    $.ajax({  
        url: "/SstGroupMaster/Add",  
        data: JSON.stringify(empObj),  
        type: "POST",  
        contentType: "application/json;charset=utf-8",  
        dataType: "json",  
        success: function (result) {  
     
            loadData();  
            $('#myModal').modal('hide');  
          loadData();   
                 alert("Save Successfully");  
        },  
        error: function (errormessage) {  
            alert(errormessage.responseText);  
        }  
    });  
}  
  
//Function for getting the Data Based upon Employee ID  
function getbyID(SSTGroupID) {
debugger;
//    $('#GroupName').css('border-color', 'lightgrey');
//    $('#GroupDescription').css('border-color', 'lightgrey');
    $.ajax({
        url: "/SstGroupMaster/getbyID/" + SSTGroupID,
        typr: "GET",
        contentType: "application/json;charset=UTF-8",
        dataType: "json",
        success: function (result) {
            $('#SSTGroupID').val(result.SSTGroupID);
            $('#SSTGroupName').val(result.SSTGroupName);
            $('#SSTGroupDescription').val(result.SSTGroupDescription);

            $('#myModal').modal('show');
            $('#btnUpdate').show();
            $('#btnAdd').hide();
        },
        error: function (errormessage) {
            alert(errormessage.responseText);
        }
    });
    return false;
}
  
//function for updating employee's record  
function Update() {  
debugger;
    var res = validate();  
    if (res == false) {  
        return false;  
    }  
    var empObj = {  
        SSTGroupID : $('#SSTGroupID').val(),
        SSTGroupName: $('#SSTGroupName').val(),  
        SSTGroupDescription: $('#SSTGroupDescription').val(),  
    };  
    $.ajax({  
        url: "/SstGroupMaster/Update",  
        data: JSON.stringify(empObj),  
        type: "POST",  
        contentType: "application/json;charset=utf-8",  
        dataType: "json",  
        success: function (result) {  
            loadData();  
            $('#myModal').modal('hide');  
            $('#SSTGroupID').val(),
            $('#SSTGroupName').val();  
            $('#SSTGroupDescription').val(); 
             alert("Update Successfully"); 
        },  
        error: function (errormessage) {  
            alert(errormessage.responseText);  
        }  
    });  
}  
  
//function for deleting employee's record  
function Delete(ID) {  
debugger;
    var ans = confirm("Are you sure you want to delete this Record?");  
    if (ans) {  
        $.ajax({  
            url: "/SstGroupMaster/Delete/" + ID,  
            type: "POST",  
            contentType: "application/json;charset=UTF-8",  
            dataType: "json",  
            success: function (result) {  
                loadData();  
            },  
            error: function (errormessage) {  
                alert(errormessage.responseText);  
            }  
        });  
    }  
}  







  
//Function for clearing the textboxes  
function clearTextBox() {  

    $('#SSTGroupName').val("");  
    $('#SSTGroupDescription').val(""); 
    $('#btnUpdate').hide();  
    $('#btnAdd').show();  
//    $('#GroupName').css('border-color', 'lightgrey');  
//    $('#GroupDescription').css('border-color', 'lightgrey');     
}  


function validate() {
    return $('#frmSstGroupMaster').valid();
}
