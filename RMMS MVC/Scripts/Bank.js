﻿/// <reference path="jquery-1.9.1.intellisense.js" />
//Load Data in Table when documents is ready
$(document).ready(function () {
debugger;
    loadData();
});

//Load Data function
function loadData() {
debugger;
    $.ajax({

        url: "/BankMaster/List",
        type: "GET",
        contentType: "application/json;charset=utf-8",
        dataType: "json",
        success: function (result) {
        debugger;
            /// <reference path="../Views/CityMaster/Index.cshtml" />
            var html = '';
            var index=1;
            $.each(result, function (key, item) {
                html += '<tr>';
                html += '<td>' + index + '</td>';
                html += '<td>' + item.BankName + '</td>';/// <reference path="../images/edit.png" />
                // html += '<td>' + item.BankShotName + '</td>';
                //html += '<td>' + item.Bank_HO_Address1 + '</td>';
                //html += '<td>' + item.Bank_HO_Address2 + '</td>';
                html += '<td>' + item.CityName + '</td>';
                html += '<td>' + item.Bank_ContactName + '</td>';
                html += '<td>' + item.Bank_ContactNo1 + '</td>';
                //html += '<td>' + item.Bank_ContactNo2 + '</td>';
                html += '<td>' + item.Bank_EmailID1 + '</td>';
                //html += '<td>' + item.Bank_EmailID2+ '</td>';
                //html += '<td>' + item.Bank_FTPAddress1 + '</td>';
                //html += '<td>' + item.Bank_FTPAddress2 + '</td>';
                //html += '<td>' + item.Bank_FTPPort1 + '</td>';
                //html += '<td>' + item.Bank_FTPPort2 + '</td>';
                //html += '<td>' + item.Bank_FTP1UserID + '</td>';
                //html += '<td>' + item.Bank_FTP2UserID + '</td>';
                //html += '<td>' + item.Bank_FTP1Password + '</td>';
                //html += '<td>' + item.Bank_FTP2Password + '</td>';
                //html += '<td>' + item.Bank_ExtrctdFileLoc + '</td>';
                html += '<td><a href="#" onclick="return getbyID(' + item.BankID + ')"> <img alt="Edit" src="../images/edit.png" height="30px" width="30px"> </a> | <a href="#" onclick="return delele(' + item.BankID + ')"><img alt="Edit" src="../images/delete.jpg" height="30px" width="30px"> </a></td>';
                html += '</tr>';
                index++;
            });
            
            $('#tblBanks').DataTable().destroy();
            $("#tblBanks").find('.tbody').html(html);
            $('#tblBanks').DataTable({"lengthMenu": [ [2, 5, 15, -1], [2, 5, 15, "All"] ]});
        },
        error: function (errormessage) {
            alert(errormessage.responseText);
        }
    });
}

function ValidateEmailID() {
   debugger;     
    var isValid=true;
    $('#errMessage').html('');
    $("#tblBank").find("tr[data-group]").each(function(i,e){
        var element='input';
        if($(e).data('target'))
            element = $(e).data('target');
        var string1 = $(e).find(element+":eq(0)").val();
        var string2 = $(e).find(element+":eq(1)").val();
            
        if(string1!=""&&string1==string2)
        {
            $('#errMessage').append("<li>Enter another "+ $(e).data("group") + "</li>");
            isValid=false;
        }
    });
    return isValid;
       
}

////Add Data Function 
function Add() {
debugger;
    var res = validate();
    if (res == false) {
        return false;
    }
    var empObj = {
        BankID: $('#BankID').val(),
        BankName: $('#BankName').val(),
        BankShotName: $('#BankShotName').val(),
        Bank_HO_Address1: $('#Bank_HO_Address1').val(),
        Bank_HO_Address2: $('#Bank_HO_Address2').val(),
        CityID: $('#CityID').val(),
        Bank_ContactName: $('#Bank_ContactName').val(),
        Bank_ContactNo1: $('#Bank_ContactNo1').val(),
        Bank_ContactNo2: $('#Bank_ContactNo2').val(),
        Bank_EmailID1: $('#Bank_EmailID1').val(),
        Bank_EmailID2: $('#Bank_EmailID2').val(),
        Bank_FTPAddress1 : $('#Bank_FTPAddress1').val(),
        Bank_FTPAddress2: $('#Bank_FTPAddress2').val(),
        Bank_FTPPort1: $('#Bank_FTPPort1').val(),
        Bank_FTPPort2: $('#Bank_FTPPort2').val(),
        Bank_FTP1UserID: $('#Bank_FTP1UserID').val(),
        Bank_FTP2UserID: $('#Bank_FTP2UserID').val(),
        Bank_FTP1Password : $('#Bank_FTP1Password').val(),
        Bank_FTP2Password: $('#Bank_FTP2Password').val(),
        Bank_ExtrctdFileLoc: $('#Bank_ExtrctdFileLoc').val()

    };
    $.ajax({
        url: "/BankMaster/Add",
        data: JSON.stringify(empObj),
        type: "POST",
        contentType: "application/json;charset=utf-8",
        dataType: "json",
        success: function (result) {
            $('#myModal').modal('hide');
                loadData();        
           },
        error: function (errormessage) {
            alert(errormessage.responseText);
        }
    });
}

////Function for getting the Data Based upon Employee ID
function getbyID(BankID) {
/*
debugger;
    $('#BankName').css('border-color', 'lightgrey');
    $('#BankShotName').css('border-color', 'lightgrey');
    $('#Bank_HO_Address1').css('border-color', 'lightgrey');
    $('#Bank_HO_Address2').css('border-color', 'lightgrey');
    $('#CityID').css('border-color', 'lightgrey');
    $('#Bank_ContactName').css('border-color', 'lightgrey');
    $('#Bank_ContactNo1').css('border-color', 'lightgrey');
    $('#Bank_ContactNo2').css('border-color', 'lightgrey');
    $('#Bank_EmailID1').css('border-color', 'lightgrey');
    $('#Bank_EmailID2').css('border-color', 'lightgrey');
    $('#Bank_FTPAddress1').css('border-color', 'lightgrey');
    $('#Bank_FTPAddress2').css('border-color', 'lightgrey');
    $('#Bank_FTPPort1').css('border-color', 'lightgrey');
    $('#Bank_FTPPort2').css('border-color', 'lightgrey');
    $('#Bank_FTP1UserID').css('border-color', 'lightgrey');
    $('#Bank_FTP2UserID').css('border-color', 'lightgrey');
    $('#Bank_FTP1Password').css('border-color', 'lightgrey');
    $('#Bank_FTP2Password').css('border-color', 'lightgrey');
    $('#Bank_ExtrctdFileLoc').css('border-color', 'lightgrey');
    */
    $.ajax({
        url: "/BankMaster/getbyID/" + BankID,
        typr: "GET",
        contentType: "application/json;charset=UTF-8",
        dataType: "json",
        success: function (result) {
            $('#BankID').val(result.BankID);
            $('#BankName').val(result.BankName);
            $('#BankShotName').val(result.BankShotName);
            $('#Bank_HO_Address1').val(result.Bank_HO_Address1);
            $('#Bank_HO_Address2').val(result.Bank_HO_Address2);
            $('#CityID').val(result.CityID);
            $('#Bank_ContactName').val(result.Bank_ContactName);
            $('#Bank_ContactNo1').val(result.Bank_ContactNo1);
            $('#Bank_ContactNo2').val(result.Bank_ContactNo2);
            $('#Bank_ContactNo2').val(result.Bank_ContactNo2);
            $('#Bank_EmailID1').val(result.Bank_EmailID1);
            $('#Bank_EmailID2').val(result.Bank_EmailID2);
            $('#Bank_FTPAddress1').val(result.Bank_FTPAddress1);
            $('#Bank_FTPAddress2').val(result.Bank_FTPAddress2);
            $('#Bank_FTPPort1').val(result.Bank_FTPPort1);
            $('#Bank_FTPPort2').val(result.Bank_FTPPort2);
            $('#Bank_FTP1UserID').val(result.Bank_FTP1UserID);
            $('#Bank_FTP2UserID').val(result.Bank_FTP2UserID);
            $('#Bank_FTP1Password').val(result.Bank_FTP1Password);
            $('#Bank_FTP2Password').val(result.Bank_FTP2Password);
            $('#Bank_ExtrctdFileLoc').val(result.Bank_ExtrctdFileLoc);
            $('#myModal').modal('show');
            $('#btnUpdate').show();
            $('#btnAdd').hide();
        },
        error: function (errormessage) {
            alert(errormessage.responseText);
        }
    });
    return false;
}

//function for updating employee's record
function Update() {
debugger;
    var res = validate();
    if (res == false) {
        return false;
    }
    var empObj = {
        BankID: $('#BankID').val(),
        BankName: $('#BankName').val(),
        BankShotName: $('#BankShotName').val(),
        Bank_HO_Address1: $('#Bank_HO_Address1').val(),
        Bank_HO_Address2: $('#Bank_HO_Address2').val(),
        CityID: $('#CityID').val(),
        Bank_ContactName: $('#Bank_ContactName').val(),
        Bank_ContactNo1: $('#Bank_ContactNo1').val(),
        Bank_ContactNo2: $('#Bank_ContactNo2').val(),
        Bank_EmailID1: $('#Bank_EmailID1').val(),
        Bank_EmailID2: $('#Bank_EmailID2').val(),
        Bank_FTPAddress1: $('#Bank_FTPAddress1').val(),
        Bank_FTPAddress2: $('#Bank_FTPAddress2').val(),
        Bank_FTPPort1: $('#Bank_FTPPort1').val(),
        Bank_FTPPort2: $('#Bank_FTPPort2').val(),
        Bank_FTP1UserID: $('#Bank_FTP1UserID').val(),
        Bank_FTP2UserID: $('#Bank_FTP2UserID').val(),
        Bank_FTP1Password: $('#Bank_FTP1Password').val(),
        Bank_FTP2Password: $('#Bank_FTP2Password').val(),
        Bank_ExtrctdFileLoc: $('#Bank_ExtrctdFileLoc').val(),
    };
    $.ajax({
        url: "/BankMaster/Update",
        data: JSON.stringify(empObj),
        type: "POST",
        contentType: "application/json;charset=utf-8",
        dataType: "json",
        success: function (result) {
            loadData();
            $('#myModal').modal('hide');
            $('#BankID').val("");
            $('#BankName').val("");
            $('#BankShotName').val("");
            $('#Bank_HO_Address1').val("");
            $('#Bank_HO_Address2').val("");
            $('#CityID').val("");
            $('#Bank_ContactName').val("");
            $('#Bank_ContactNo1').val("");
            $('#Bank_ContactNo2').val("");
            $('#Bank_EmailID1').val("");
            $('#Bank_EmailID2').val("");
            $('#Bank_FTPAddress1').val("");
            $('#Bank_FTPAddress2').val("");
            $('#Bank_FTPPort1').val("");
            $('#Bank_FTPPort2').val("");
            $('#Bank_FTP1UserID').val("");
            $('#Bank_FTP2UserID').val("");
            $('#Bank_FTP1Password').val("");
            $('#Bank_FTP2Password').val("");
            $('#Bank_ExtrctdFileLoc').val("");

        },
        error: function (errormessage) {
            alert(errormessage.responseText);
        }
    });
}

////function for deleting employee's record
function delele(id) {
debugger;
    var ans = confirm("are you sure you want to delete this record?");
    if (ans) {
        $.ajax({
            url: "/BankMaster/Delete/" + id,
            type: "post",
            contenttype: "application/json;charset=utf-8",
            datatype: "json",
            success: function (result) {
                loadData();
            },
            error: function (errormessage) {
                alert(errormessage.responsetext);
            }
        });
    }
}

////Function for clearing the textboxes
function clearTextBox() {
    $('input,select,textarea',$('#frmBankMaster')).val("").removeClass('input-validation-error');
    //$('#frmBankMaster').valid();
    //$('#frmBankMaster').validate().resetForm();
    $('#frmBankMaster .field-validation-error>span').hide();
    
    /*
            $('#BankName').val("");
            $('#BankShotName').val("");
            $('#Bank_HO_Address1').val("");
            $('#Bank_HO_Address2').val("");
            $('#CityID').val("");
            $('#Bank_ContactName').val("");
            $('#Bank_ContactNo1').val("");
            $('#Bank_ContactNo2').val("");
            $('#Bank_EmailID1').val("");
            $('#Bank_EmailID2').val("");
            $('#Bank_FTPAddress1').val("");
            $('#Bank_FTPAddress2').val("");
            $('#Bank_FTPPort1').val("");
            $('#Bank_FTPPort2').val("");
            $('#Bank_FTP1UserID').val("");
            $('#Bank_FTP2UserID').val("");
            $('#Bank_FTP1Password').val("");
            $('#Bank_FTP2Password').val("");
            $('#Bank_ExtrctdFileLoc').val("");
            $('#btnUpdate').hide();
            $('#btnAdd').show();
            
            /*
            $('#BankName').css('border-color', 'lightgrey');
            $('#BankShotName').css('border-color', 'lightgrey');
            $('#Bank_HO_Address1').css('border-color', 'lightgrey');
            $('#Bank_HO_Address2').css('border-color', 'lightgrey');
            $('#CityID').css('border-color', 'lightgrey');
            $('#Bank_ContactName').css('border-color', 'lightgrey');
            $('#Bank_ContactNo1').css('border-color', 'lightgrey');
            $('#Bank_ContactNo2').css('border-color', 'lightgrey');
            $('#Bank_EmailID1').css('border-color', 'lightgrey');
            $('#Bank_EmailID2').css('border-color', 'lightgrey');
            $('#Bank_FTPAddress1').css('border-color', 'lightgrey');
            $('#Bank_FTPAddress2').css('border-color', 'lightgrey');
            $('#Bank_FTPPort1').css('border-color', 'lightgrey');
            $('#Bank_FTPPort2').css('border-color', 'lightgrey');
            $('#Bank_FTP1Password').css('border-color', 'lightgrey');
            $('#Bank_FTP2Password').css('border-color', 'lightgrey');
            $('#Bank_ExtrctdFileLoc').css('border-color', 'lightgrey');
            */
            }
////Valdidation using jquery
function validate() {
    if(!$('#frmBankMaster').valid())
    {
        ValidateEmailID();
        return false;
    }
    return ValidateEmailID();
}


