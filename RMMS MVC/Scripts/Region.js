﻿/// <reference path="jquery-1.9.1.intellisense.js" />
//Load Data in Table when documents is ready
$(document).ready(function () {

    loadData();
});

//Load Data function
function loadData() {

    $.ajax({

        url: "/RegionMaster/List",
        type: "GET",
        contentType: "application/json;charset=utf-8",
        dataType: "json",
        success: function (result) {
        debugger;
            /// <reference path="../Views/CityMaster/Index.cshtml" />
            var html = '';
            var index=1;
            $.each(result, function (key, item) {
                html += '<tr>';
                html += '<td>' + index + '</td>';
                html += '<td>' + item.BankName + '</td>';
                html += '<td>' + item.RegionName + '</td>';
                html += '<td>' + item.RegionDescription + '</td>';
                html += '<td><a href="#" onclick="return getbyID(' + item.RegionID + ')"> <img alt="Edit" src="../images/edit.png" height="30px" width="30px"> </a> | <a href="#" onclick="return delele(' + item.RegionID + ')"><img alt="Edit" src="../images/delete.jpg" height="30px" width="30px"> </a></td>';
                html += '</tr>';
                index++;
            });
            
            $('#tblRegion').DataTable().destroy();
            $("#tblRegion").find('.tbody').html(html);
            $('#tblRegion').DataTable({ "lengthMenu": [[10, 20, 30, -1], [10, 20, 30, "All"]] });
        },
        error: function (errormessage) {
            alert(errormessage.responseText);
        }
    });
}


////Add Data Function 
function Add() {
debugger;
    var res = validate();
    if (res == false) {
        return false;
    }
    var empObj = {
        RegionID: $('#RegionID').val(),
        RegionName: $('#RegionName').val(),
        BankID: $('#BankID').val(),
        RegionDescription: $('#RegionDescription').val()
    };

    $.ajax({
        url: "/RegionMaster/Add",
        data: JSON.stringify(empObj),
        type: "POST",
        contentType: "application/json;charset=utf-8",
        dataType: "json",
        success: function (result) {
            $('#myModal').modal('hide');
            loadData();
            alert("Save Successfully");           
           },
        error: function (errormessage) {
            alert(errormessage.responseText);
        }
    });
}

////Function for getting the Data Based upon Employee ID
function getbyID(RegionID) {
/*
debugger;
    $('#BankName').css('border-color', 'lightgrey');
    $('#BankShotName').css('border-color', 'lightgrey');
    $('#Bank_HO_Address1').css('border-color', 'lightgrey');
    $('#Bank_HO_Address2').css('border-color', 'lightgrey');
    $('#CityID').css('border-color', 'lightgrey');
    $('#Bank_ContactName').css('border-color', 'lightgrey');
    $('#Bank_ContactNo1').css('border-color', 'lightgrey');
    $('#Bank_ContactNo2').css('border-color', 'lightgrey');
    $('#Bank_EmailID1').css('border-color', 'lightgrey');
    $('#Bank_EmailID2').css('border-color', 'lightgrey');
    $('#Bank_FTPAddress1').css('border-color', 'lightgrey');
    $('#Bank_FTPAddress2').css('border-color', 'lightgrey');
    $('#Bank_FTPPort1').css('border-color', 'lightgrey');
    $('#Bank_FTPPort2').css('border-color', 'lightgrey');
    $('#Bank_FTP1UserID').css('border-color', 'lightgrey');
    $('#Bank_FTP2UserID').css('border-color', 'lightgrey');
    $('#Bank_FTP1Password').css('border-color', 'lightgrey');
    $('#Bank_FTP2Password').css('border-color', 'lightgrey');
    $('#Bank_ExtrctdFileLoc').css('border-color', 'lightgrey');
    */
    $.ajax({
        url: "/RegionMaster/getbyID/" + RegionID,
        typr: "GET",
        contentType: "application/json;charset=UTF-8",
        dataType: "json",
        success: function (result) {
            $('#RegionID').val(result.RegionID);
            $('#BankID').val(result.BankID);
            $('#RegionName').val(result.RegionName);
            $('#RegionDescription').val(result.RegionDescription);
            $('#myModal').modal('show');
            $('#btnUpdate').show();
            $('#btnAdd').hide();
        },
        error: function (errormessage) {
            alert(errormessage.responseText);
        }
    });
    return false;
}

//function for updating employee's record
function Update() {
debugger;
    var res = validate();
    if (res == false) {
        return false;
    }
    var empObj = {
        RegionID: $('#RegionID').val(),
        RegionName: $('#RegionName').val(),
        BankID: $('#BankID').val(),
        RegionDescription: $('#RegionDescription').val()
    };
    $.ajax({
        url: "/RegionMaster/Update",
        data: JSON.stringify(empObj),
        type: "POST",
        contentType: "application/json;charset=utf-8",
        dataType: "json",
        success: function (result) {
            loadData();
            $('#myModal').modal('hide');
            $('#RegionID').val("");
            $('#BankID').val("");
            //$('#BankName').val("");
            $('#RegionName').val("");
            $('#RegionDescription').val("");
            alert("Update Successfully");

        },
        error: function (errormessage) {
            alert(errormessage.responseText);
        }
    });
}

////function for deleting employee's record
function delele(id) {
debugger;
    var ans = confirm("are you sure you want to delete this record?");
    if (ans) {
        $.ajax({
            url: "/RegionMaster/Delete/" + id,
            type: "post",
            contenttype: "application/json;charset=utf-8",
            datatype: "json",
            success: function (result) {
                loadData();
            },
            error: function (errormessage) {
                alert(errormessage.responsetext);
            }
        });
    }
}

////Function for clearing the textboxes
function clearTextBox() {
    $('input,select,textarea',$('#frmRegionMaster')).val("").removeClass('input-validation-error');
    //$('#frmBankMaster').valid();
    //$('#frmBankMaster').validate().resetForm();
    $('#frmRegionMaster .field-validation-error>span').hide();
    
    /*
            $('#BankName').val("");
            $('#BankShotName').val("");
            $('#Bank_HO_Address1').val("");
            $('#Bank_HO_Address2').val("");
            $('#CityID').val("");
            $('#Bank_ContactName').val("");
            $('#Bank_ContactNo1').val("");
            $('#Bank_ContactNo2').val("");
            $('#Bank_EmailID1').val("");
            $('#Bank_EmailID2').val("");
            $('#Bank_FTPAddress1').val("");
            $('#Bank_FTPAddress2').val("");
            $('#Bank_FTPPort1').val("");
            $('#Bank_FTPPort2').val("");
            $('#Bank_FTP1UserID').val("");
            $('#Bank_FTP2UserID').val("");
            $('#Bank_FTP1Password').val("");
            $('#Bank_FTP2Password').val("");
            $('#Bank_ExtrctdFileLoc').val("");
            $('#btnUpdate').hide();
            $('#btnAdd').show();
            
            /*
            $('#BankName').css('border-color', 'lightgrey');
            $('#BankShotName').css('border-color', 'lightgrey');
            $('#Bank_HO_Address1').css('border-color', 'lightgrey');
            $('#Bank_HO_Address2').css('border-color', 'lightgrey');
            $('#CityID').css('border-color', 'lightgrey');
            $('#Bank_ContactName').css('border-color', 'lightgrey');
            $('#Bank_ContactNo1').css('border-color', 'lightgrey');
            $('#Bank_ContactNo2').css('border-color', 'lightgrey');
            $('#Bank_EmailID1').css('border-color', 'lightgrey');
            $('#Bank_EmailID2').css('border-color', 'lightgrey');
            $('#Bank_FTPAddress1').css('border-color', 'lightgrey');
            $('#Bank_FTPAddress2').css('border-color', 'lightgrey');
            $('#Bank_FTPPort1').css('border-color', 'lightgrey');
            $('#Bank_FTPPort2').css('border-color', 'lightgrey');
            $('#Bank_FTP1Password').css('border-color', 'lightgrey');
            $('#Bank_FTP2Password').css('border-color', 'lightgrey');
            $('#Bank_ExtrctdFileLoc').css('border-color', 'lightgrey');
            */
}
////Valdidation using jquery
function validate() {
 return $('#frmRegionMaster').valid();
}


