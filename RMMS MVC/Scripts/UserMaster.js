﻿//Load Data in Table when documents is ready  
$(document).ready(function () {  
    loadData();  
    
});  
  
//Load Data function  
function loadData() {  
debugger;
    $.ajax({  
        url: "/UserMaster/List",  
        type: "GET",  
        contentType: "application/json;charset=utf-8",  
        dataType: "json",  
        success: function (result) {     
            var html = '';
            $.each(result, function (key, item) {  
            
                html += '<tr>';  
               
                html += '<td>' + item.UserFullName + '</td>';  
                html += '<td>' + item.UserLoginName+ '</td>';
                html += '<td>' + item.UserEmailID + '</td>';
                html += '<td>' + item.UserContactNo + '</td>';     
                html += '<td>' + item.UserStatus + '</td>';             
                html += '<td><a href="#" onclick="return getbyID(' + item.UserID + ')"> <img alt="Edit" src="../images/edit.png" height="30px" width="30px"> </a> | <a href="#" onclick="Delete(' + item.UserID + ')"><img alt="Edit" src="../images/delete.jpg" height="30px" width="30px"> </a></td>';
                html += '</tr>'; 

            });  
            $('#tblUserMaster').DataTable().destroy();
            $("#tblUserMaster").find('.tbody').html(html);
            $('#tblSstUserMaster').DataTable({"lengthMenu": [ [2, 4 , 5, 15, -1], [2, 4 , 5, 15, "All"] ]});
        },  
        error: function (errormessage) {  
            alert(errormessage.responseText);  
        }  
    });  
}  
  
//Add Data Function   
function Add() {  
debugger;
    var res = validate();  
    if (res == false) {  
        return false;  
    }  
    var empObj = {  
//        UserID            :$('#UserID').val(),
        RoleID            :$('#RoleID').val(),
        RegionId          :$('#Region').val(), 
        BankID            :$('#Bank').val(), 
        BranchID          :$('#Branch').val(), 
        UserFullName      :$('#UserFullName').val(),  
        UserLoginName     :$('#UserLoginName').val(),
        UserLoginPassword :$('#UserLoginPassword').val(),
        UserEmailID       :$('#UserEmailID').val(),
        UserContactNo     :$('#UserContactNo').val(),
        UserStatus        :$("#Statusdropdown option:selected").text(),
        DefaultPage       :$('#Defaultdropdown option:selected').text()
    };  
    $.ajax({  
        url: "/UserMaster/Add",  
        data: JSON.stringify(empObj),  
        type: "POST",  
        contentType: "application/json;charset=utf-8",  
        dataType: "json",  
        success: function (result) {  
     
            loadData();  
            clearTextBox();
            $('#myModal').modal('hide');  
            clearTextBox();
        },  
        error: function (errormessage) {  
            alert(errormessage.responseText);  
        }  
    });  
}  
  
//Function for getting the Data Based upon Employee ID  
function getbyID(UserID) {
debugger;
//    $('#GroupName').css('border-color', 'lightgrey');
//    $('#GroupDescription').css('border-color', 'lightgrey');
$.ajax({
        url: "/UserMaster/getbyID/" + UserID,
        type: "GET",
        contentType: "application/json;charset=UTF-8",
        dataType: "json",
        success: function (result) {
            debugger;
            $('#UserID').val(result.UserID);
            $('#UserFullName').val(result.UserFullName);
            $('#UserLoginName').val(result.UserLoginName);
            $('#UserEmailID').val(result.UserEmailID);
            $('#UserLoginPassword').val(result.UserLoginPassword);
            $('#UserConfirmPassword').val(result.UserLoginPassword);
            $('#UserContactNo').val(result.UserContactNo);
            $('#RoleID').val(result.RoleID);
            $('#Defaultdropdown').val(result.DefaultPage);
            $('#Statusdropdown').val(result.UserStatus);
            $('#Bank').val(result.BankID);
            $('#Region').val(result.RegionId);
            $('#Branch').val(result.BranchID);
            $('#myModal').modal('show');
            $('#btnUpdate').show();
            $('#btnAdd').hide();
          
        },
        error: function (errormessage) {
            alert(errormessage.responseText);
        }
    });
    return false;
}
  
//function for updating employee's record  
function Update() {  
debugger;
    var res = validate();  
    if (res == false) {  
        return false;  
    }
    var empObj = {
        UserID            :$('#UserID').val(),
        RoleID            :$('#RoleID').val(),
        RegionId          :$('#Region').val(),
        BankID            :$('#Bank').val(),
        BranchID          :$('#Branch').val(), 
        UserFullName      :$('#UserFullName').val(),  
        UserLoginName     :$('#UserLoginName').val(),
        UserLoginPassword :$('#UserLoginPassword').val(),
        UserEmailID       :$('#UserEmailID').val(),
        UserContactNo     :$('#UserContactNo').val(),
        UserStatus        :$("#Statusdropdown option:selected").text(),
        DefaultPage       :$('#Defaultdropdown option:selected').text()
    };
    $.ajax({
        url: "/UserMaster/Update",
        data: JSON.stringify(empObj),
        type: "POST",
        contentType: "application/json;charset=utf-8",
        dataType: "json",
        success: function (result) {
            loadData();
            $('#myModal').modal('hide');
            $('#UserID').val("");
            $('#RoleID').val("");
            $('#Region').val("");
            $('#Bank').val("");
            $('#Branch').val("");
            $('#UserFullName').val("");
            $('#UserLoginName').val("");
            $('#UserEmailID').val("");
            $('#UserContactNo').val("");
            $('#UserLoginPassword').val("");
            $('#Defaultdropdown').val("");
            $('#Statusdropdown').val("")

        },
        error: function (errormessage) {
            alert(errormessage.responseText);
        }
    });  
}  
  
//function for deleting employee's record  
function Delete(ID) {  
debugger;
    var ans = confirm("Are you sure you want to delete this Record?");  
    if (ans) {  
        $.ajax({  
            url: "/UserMaster/Delete/" + ID,  
            type: "POST",  
            contentType: "application/json;charset=UTF-8",  
            dataType: "json",  
            success: function (result) {  
                loadData();  
            },  
            error: function (errormessage) {  
                alert(errormessage.responseText);  
            }  
        });  
    }  
}  


function Savedmsg() {  
debugger;
if($('#GroupName').val() >= 1){

    var ans = confirm("Data Saved Successfully");  
    if (ans) {  
        $.ajax({  
            url: "/UserMaster/Savedmsg/",  
            type: "POST",  
            contentType: "application/json;charset=UTF-8",  
            dataType: "json",  
            success: function (result) {  
                loadData();  
               
                location.reload();
            },  
            error: function (errormessage) {  
                alert(errormessage.responseText);  
            }  
        });  
    }  
 }
}  

function Updatedmsg() {  
debugger;
    var ans = confirm("Data Updated Successfully");  
    if (ans) {  
        $.ajax({  
            url: "/UserMaster/Savedmsg/",  
            type: "POST",  
            contentType: "application/json;charset=UTF-8",  
            dataType: "json",  
            success: function (result) {  
                loadData();  
                location.reload();
            },  
            error: function (errormessage) {  
                alert(errormessage.responseText);  
            }  
        });  
    }  
}





//Function for clearing the textboxes  

function clearTextBox() {
    debugger;
    $('#UserID').val(""),
    $('#RoleID').val("");
    $('#UserLoginName').val(""),
    $('#UserFullName').val(""),
    $('#UserLoginPassword').val(""),
     $('#UserConfirmPassword').val(""),
     $('#DefaultPage').val(""),
     $('#UserEmailID').val(""),
        $('#UserContactNo').val(""),
    $('#UserStatus').val("");
    $('#BankID').val(""),
   
    $('#RegionID').val(""),
     $('#RegionName').val(""),
     $('#BranchID').val(""),
     $('#BranchName').val(""),
    $('#btnUpdate').hide();
    $('#btnAdd').show();

  
}  



function validate() {
    return $('#frmUserMaster').valid();
}
