﻿//Load Data in Table when documents is ready  
$(document).ready(function () {  
    loadData();  
    
});  
  
//Load Data function  
function loadData() {  
debugger;
    $.ajax({  
        url: "/ProblemCategoryMaster/List",  
        type: "GET",  
        contentType: "application/json;charset=utf-8",  
        dataType: "json",  
        success: function (result) { 
        debugger;    
            var html = '';  
             var index=1;
            $.each(result, function (key, item) {  
                html += '<tr>';  
                html += '<td>' + index + '</td>';
                html += '<td bgcolor='+ item.ProblemColorCode + '>' + item.ProblemCategory + '</td>';   
//                html += '<td>' + item.ProblemColorCode + '</td>';  
                html += '<td>' + item.ProblemCategoryDescription + '</td>';                
                html += '<td><a href="#" onclick="return getbyID(' + item.ProblemCategoryId + ')"> <img alt="Edit" src="../images/edit.png" height="30px" width="30px"> </a> | <a href="#" onclick="Delete(' + item.ProblemCategoryId + ')"><img alt="Edit" src="../images/delete.jpg" height="30px" width="30px"> </a></td>';
                html += '</tr>';  
                index++;
             
            });  
             $('#tblProblemDetails').DataTable().destroy();
            $("#tblProblemDetails").find('.tbody').html(html);
            $('#tblProblemDetails').DataTable({ "lengthMenu": [[10, 20, 30, -1], [10, 20, 30, "All"]] });
        },  
        error: function (errormessage) {  
            alert(errormessage.responseText);  
        }  
    });  
}  
  
//Add Data Function   
function Add() {  
debugger;
    var res = validate();  
    if (res == false) {  
        return false;  
    }  
    var empObj = {  
        ProblemCategoryId           :$('#ProblemCategoryId').val(),
        ProblemCategory             :$('#ProblemCategory ').val(),
        ProblemColorCode            :$('#ProblemColorCode').val(),  
        ProblemCategoryDescription  :$('#ProblemCategoryDescription').val()
    };  
    $.ajax({  
        url: "/ProblemCategoryMaster/Add",  
        data: JSON.stringify(empObj),  
        type: "POST",  
        contentType: "application/json;charset=utf-8",  
        dataType: "json",  
        success: function (result) {  
     
            loadData();  
            $('#myModal').modal('hide');
            loadData();  
            alert("Save Successfully");   
        },  
        error: function (errormessage) {  
            alert(errormessage.responseText);  
        }  
    });  
}  
  
//Function for getting the Data Based upon Employee ID  
function getbyID(ProblemCategoryId) {
debugger;
//    $('#GroupName').css('border-color', 'lightgrey');
//    $('#GroupDescription').css('border-color', 'lightgrey');
    $.ajax({
        url: "/ProblemCategoryMaster/getbyID/" + ProblemCategoryId,
        typr: "GET",
        contentType: "application/json;charset=UTF-8",
        dataType: "json",
        success: function (result) {
            $('#ProblemCategoryId').val(result.ProblemCategoryId);
            $('#ProblemCategory ').val(result.ProblemCategory);
            $('#ProblemColorCode').val(result.ProblemColorCode);
            $('#ProblemCategoryDescription').val(result.ProblemCategoryDescription);

            $('#myModal').modal('show');
            $('#btnUpdate').show();
            $('#btnAdd').hide();
        },
        error: function (errormessage) {
            alert(errormessage.responseText);
        }
    });
    return false;
}
  
//function for updating employee's record  
function Update() {  
debugger;
    var res = validate();  
    if (res == false) {  
        return false;  
    }
    var empObj = {
        ProblemCategoryId: $('#ProblemCategoryId').val(),
        ProblemCategory: $('#ProblemCategory ').val(),
        ProblemColorCode: $('#ProblemColorCode').val(),
        ProblemCategoryDescription: $('#ProblemCategoryDescription').val()
    };  
    $.ajax({  
        url: "/ProblemCategoryMaster/Update",  
        data: JSON.stringify(empObj),  
        type: "POST",  
        contentType: "application/json;charset=utf-8",  
        dataType: "json",  
        success: function (result) {  
            loadData();  
            $('#myModal').modal('hide');  
            $('#ProblemCategoryId').val(),
            $('#ProblemColorCode').val();
            $('#ProblemCategory ').val();
            $('#ProblemCategoryDescription').val();
            alert("Update Successfully");
        },  
        error: function (errormessage) {  
            alert(errormessage.responseText);  
        }  
    });  
}  
  
//function for deleting employee's record  
function Delete(ID) {  
debugger;
    var ans = confirm("Are you sure you want to delete this Record?");  
    if (ans) {  
        $.ajax({  
            url: "/ProblemCategoryMaster/Delete/" + ID,  
            type: "POST",  
            contentType: "application/json;charset=UTF-8",  
            dataType: "json",  
            success: function (result) {  
                loadData();  
            },  
            error: function (errormessage) {  
                alert(errormessage.responseText);  
            }  
        });  
    }  
}  




  
//Function for clearing the textboxes  
function clearTextBox() {  

     $('#ProblemCategoryId').val(""),
     $('#ProblemColorCode').val("");
     $('#ProblemCategory').val("");
     $('#ProblemCategoryDescription').val("");
     $('#cp2').val("");
     $('#btnAdd').show();
     $('#btnUpdate').hide();

//    $('#GroupName').css('border-color', 'lightgrey');  
//    $('#GroupDescription').css('border-color', 'lightgrey');     
}  


function validate() {
    return $('#frmProblemDetails').valid();
}
